﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class SI_GES : MonoBehaviour {
    int id_usuario = 8;//esto hay que conectarlo con el menu inicial
    public static float Felicidad;
    public static float Temor;
    public static float Disgusto;
    public static float Tristeza;
    public static float Enojo;
    public static float Sorpresa;
    public static float Desprecio;
    public string imagen;
    public string promgestos;
    public float[] valor = new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    string Fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    public string escena;
    public string Usuario;
    public string nombreUsuario;
    float max;
    void Start()
    {
        Usuario = PlayerPrefs.GetString("Usuario");
        nombreUsuario = PlayerPrefs.GetString("nombreUsuario");
        id_usuario = PlayerPrefs.GetInt("idUsuario");
        escena = PlayerPrefs.GetString("Juego");
        max = 0;
    }
    public void DBWrite()
    {
        valor = GameObject.Find("Expresiones").GetComponent<ListenerExpressions>().Valores;
        max = valor.Max();
        Felicidad = valor[0] / max;
        Temor = valor[1] / max;
        Disgusto = valor[2] / max;
        Tristeza = valor[3] / max;
        Enojo = valor[4] / max;
        Sorpresa = valor[5] / max;
        Desprecio = valor[6] / max;
        switch (escena)
            {
            case "ASE":
                {
                    imagen = ActASE.ImgAct;
                    break;
                }
            case "NB":
                {
                    imagen = ActNB.ImgAnt;
                    break;
                }
            case "GNG":
                {
                    imagen = ActGNG.ImgAnt;
                    break;
                }
            case "SS":
                {
                    imagen = ActSS.ImgAct;
                    break;
                }
            case "CA":
                {
                    imagen = ActCa.ImgAct;
                    break;
                }
            case "C":
                {
                    imagen = Actualizar.imagenes;
                    break;
                }
        }
        
        promgestos = Felicidad.ToString() + "|" + Temor.ToString() + "|" + Disgusto.ToString() + "|" + Tristeza.ToString() + "|" + Enojo.ToString() + "|" + Sorpresa.ToString() + "|" + Desprecio.ToString();
        for (int i = 0; i < 7; i++)
            valor[i] = 0;//se reinician los valores de los contadores de gestos
        GameObject.Find("Expresiones").GetComponent<ListenerExpressions>().Valores=valor;
        StartCoroutine(GESWrite());
    }

    public void DBRead()
    {

    }
    IEnumerator GESWrite()
    {
        //WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/GESwrite.php?idu=" + id_usuario + "&img=" + imagen + "&prg=" + promgestos + "&esc=" + escena + "&fec=" + Fecha  );
        //WWW conexion = new WWW("http://localhost/CTA/GESwrite.php?idu=" + id_usuario + "&img=" + imagen + "&prg=" + promgestos + "&esc=" + escena + "&fec=" + Fecha);
        WWW conexion = new WWW("http://localhost/CTA/GESwriteE.php?idu=" + id_usuario + "&img=" + imagen + "&prg=" + promgestos + "&fel=" + Felicidad + "&tem=" + Temor + "&dis=" + Disgusto + "&tri=" + Tristeza + "&eno=" + Enojo + "&sor=" + Sorpresa + "&des=" + Desprecio + "&esc=" + escena + "&fec=" + Fecha);


        Debug.Log(conexion.text);
        yield return (conexion);

        if (conexion.text == "402")
        {
            Debug.LogError("El usuario ya existe");
        }
        else if (conexion.text == "203")
        {
            Debug.LogError("Registro realizado");
        }
        else
        {
            Debug.LogError(conexion.text);
           Debug.LogError("Error en la conexión con la base de datos");
        }
    }
}
