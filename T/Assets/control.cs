﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class control : MonoBehaviour {
    public Text TiempoText;
    public float tiempo = 0.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        tiempo -= Time.deltaTime;
        TiempoText.text = "" + tiempo.ToString("f0");
	}
}
