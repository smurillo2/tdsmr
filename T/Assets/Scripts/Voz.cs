﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SpeechLib;
using System.Xml;
using System.IO;
using UnityEngine.SceneManagement;

public class Voz : MonoBehaviour {
    private SpVoice voice;
    public Text Tiempo;
    public float time = 7;
    // Use this for initialization
    void Start () {
        TextandVoice();
    }
    void TextandVoice()
    {
        // Cambiar las instruciones para el usuario según la categoría seleccionada
        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;  //   Rate (no xml)     
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-ES'>"
                                            + "A continuación, tiene un minuto para decir todos los nombres de animales que recuerde"
                                            /*+ Instrucciones.text*/
                                            + "</speak>",
                                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
    }
    void Update()
    {
        time -= Time.deltaTime;
        Tiempo.text = ("Tiempo: " + time.ToString("0"));
        int tiempo = (int)time;
        if (tiempo == 0)
        {
        SceneManager.LoadScene("Prueba");
        }
    }
}
