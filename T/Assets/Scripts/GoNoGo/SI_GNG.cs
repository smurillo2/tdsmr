﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class SI_GNG : MonoBehaviour {
    public int id_usuario = 0;//esto hay que conectarlo con el menu inicial
    public int aciertos = 0;
    public int errores = 0;
    public string categoria = "";
    public int NumEstimulos = 0;
    public float TiempoGeneral = 0f;
    public float PromTiempoRes = 0f;
    public float[] Tiempoporimagen;
    public string TiempoXImg = "";
    public string Imagenes = "";
    public float TBlancos;
    public float PBlanco;
    string Fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

    public string Usuario;
    public string Categoria;
    public string nombreUsuario;
    public string escena;

    public string aciertosyerrores;
    public float[] GE_tiempo;
    public string GE_tiempos;
    public string palabraserror;
    /*
     400 - No establece conexion
     401 - No encuentra datos
     402 - El usuario ya existe
     200 - Datos encontrados
     201 - Usuario registrado
     202 - Puntaje actualizado
     203 - Palabras y comentarios guardados
     */
    // Use this for initialization
    void Start () {
        Usuario = PlayerPrefs.GetString("Usuario");
        nombreUsuario = PlayerPrefs.GetString("nombreUsuario");
        id_usuario = PlayerPrefs.GetInt("idUsuario");
        escena = "GNG";
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SI()
    {

    }

    public void DBWrite()
    {
        float aux=0f;
        float aux1 = 0f;
        Tiempoporimagen = ActGNG.G_tiempo;
        for (int i =1; i < ActGNG.Nblanco + 1;i++)
        {
            aux = aux + Tiempoporimagen[i];
            TiempoXImg += Tiempoporimagen[i].ToString() + "|";
        }
        aux1 = ActGNG.Nblanco;
        PromTiempoRes = aux / aux1;
        aciertos = PerGNG.acierto;
        errores = PerGNG.error;
        TBlancos = ActGNG.Dtiempo;
        PBlanco=ActGNG.PBlanco;
        NumEstimulos = ActGNG.Nblanco;
        TiempoGeneral = ActGNG.tiempoG;
        Imagenes = ActGNG.NamesFotos;
        Categoria = ActGNG.Categoria;
        aciertosyerrores = PerGNG.aciertosyerrores;
        palabraserror = PerGNG.palabraserror;
        GE_tiempo = ActGNG.GE_tiempo;
        for (int i = 0; i < ActGNG.e; i++)
        {
            GE_tiempos = GE_tiempos + GE_tiempo[i].ToString() + "|";
        }

        StartCoroutine(GNGWrite());
    }

    public void DBRead()
    {

    }
    IEnumerator GNGWrite()
    {
        //WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/ASEwrite.php?idu=" + id_usuario + "&aci=" + aciertos + "&err=" + errores + "&cat=" + categoria + "&nue=" + NumEstimulos + "&tig=" + TiempoGeneral + "&ptr=" + PromTiempoRes + "&txi=" + TiempoXImg + "&fec=" + Fecha + "&img=" + Imagenes);
        WWW conexion = new WWW("http://localhost/CTA/GNGwrite.php?idu=" + id_usuario + "&aci=" + aciertos + "&err=" + errores + "&nue=" + NumEstimulos + "&tig=" + TiempoGeneral + "&tbl=" + TBlancos + "&pbl=" + PBlanco + "&ptr=" + PromTiempoRes + "&txi=" + TiempoXImg + "&img=" + Imagenes + "&fec=" + Fecha+ "&cat=" + Categoria + "&aye=" + aciertosyerrores + "&pae=" + palabraserror + "&ter=" + GE_tiempos);
        Debug.Log(conexion.text);
        yield return (conexion);
       
        if (conexion.text == "402")
        {         
            Debug.LogError("El usuario ya existe");
        }
        else if (conexion.text == "203")
        {
            Debug.LogError("Registro realizado");
        }
        else
        {
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
}


/*
     IEnumerator Datos()
    {
        WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/datos.php?uss=" + txtUsuario.text);
        yield return (conexion);
        //Debug.Log(conexion.text);
        if (conexion.text == "401")
        {
            Tbase.text = "Usuario o contraseña incorrectos";
            print("Usuario o contraseña incorrectos");
        }
        else
        {
            string[] ndatos = conexion.text.Split('|');
            if (ndatos.Length != 5)
            {
                Tbase.text = "Error en el split";
                print("Error en el split");
            }
            else
            {
                Usuario = ndatos[0];
                nombreUsuario = ndatos[1];
                scoreUsuario = int.Parse(ndatos[2]);
                idUsuario = int.Parse(ndatos[3]);
                contrasena = ndatos[4];
                sesioniniciada = true;
                PlayerPrefs.SetString("Usuario", Usuario);
                PlayerPrefs.SetString("nombreUsuario", nombreUsuario);
                PlayerPrefs.SetInt("scoreUsuario", scoreUsuario);
                PlayerPrefs.SetInt("idUsuario", idUsuario);
                PlayerPrefs.SetString("contrasena", contrasena);
                PlayerPrefs.SetString("escena", "GestionDB");
                SceneManager.LoadScene("Lobby");
                //StartCoroutine(Actualizar(0));
            }
        }
    }
    IEnumerator Login()
    {
        WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/login.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text);
        yield return (conexion);
        //Debug.Log(conexion.text);
        if (conexion.text == "200")
        {
            print("El usuario si existe");
            StartCoroutine(Datos());
        }
        else if (conexion.text == "401")
        {
            Tbase.text = "Usuario o contraseña incorrectos";
            print("Usuario o contraseña incorrectos");
        }
        else
        {
            Tbase.text = "Error en la conexión con la base de datos";
            print("Error en la conexión con la base de datos");
        }
    }
    IEnumerator Registro()
    {
        String Hoy = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //string[] words = Hoy.Split('/');
        //Hoy = "";
        // Hoy += words[0] + "-" + words[1] + "-" + words[2];
        //Debug.Log(Hoy);
        //string[] words1 = Hoy.Split(' ');
        //Hoy = "";
        // Hoy += words1[0] + "%20" + words1[1];
        //Hoy += "%2000:00:00";
        //Debug.Log(Hoy);
        //WWW conexion = new WWW("http://localhost/CTA/registro.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text + "&nom=" + txtNombre.text + "&pap=" + txtPApellido.text + "&sap=" + txtSApellido.text + "&ced=" + txtCedula.text + "&eda=" + txtEdad.text + "&lat=" + txtLateralidad + "&ocu=" + txtOcupacion.text + "&esc=" + txtEscolaridad + "&sex=" + txtSexo + "&fec=2019-02-06%2017:42:35" );
        conexion1 = "https://fluidezverbal.autonoma.edu.co/registro.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text + "&nom=" + txtNombre.text + "&pap=" + txtPApellido.text + "&sap=" + txtSApellido.text + "&ced=" + txtCedula.text + "&eda=" + txtEdad.text + "&lat=" + txtLateralidad + "&ocu=" + txtOcupacion.text + "&esc=" + txtEscolaridad + "&sex=" + txtSexo + "&fec=" + Hoy;
        WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/registro.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text + "&nom=" + txtNombre.text + "&pap=" + txtPApellido.text + "&sap=" + txtSApellido.text + "&ced=" + txtCedula.text + "&eda=" + txtEdad.text + "&lat=" + txtLateralidad + "&ocu=" + txtOcupacion.text + "&esc=" + txtEscolaridad + "&sex=" + txtSexo + "&fec=" + Hoy);

        //WWW conexion = new WWW(conexion1);
        yield return (conexion);
        Debug.Log(conexion.text);
        if (conexion.text == "402")
        {
            Tbase.text = "El usuario ya existe";
            Debug.LogError("El usuario ya existe");
        }
        else if (conexion.text == "201")
        {
            nombreUsuario = txtUsuario.text;
            scoreUsuario = 0;
            sesioniniciada = true;
        }
        else
        {
            print(conexion.text);
            Tbase.text = "Error en la conexión con la base de datos";
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
    IEnumerator Actualizar(int nScore)
    {
        Debug.Log("nuevo puntaje" + scoreUsuario.ToString());
        //WWW conexion = new WWW("http://localhost/CTA/update.php?uss=" + txtUsuario.text + "&nScore=" + (nScore+10).ToString());
        WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/update.php?uss=" + txtUsuario.text + "&nScore=" + nScore + 10.ToString());//con esto se corrige el registro de la fecha
        yield return (conexion);
        Debug.Log(conexion.text);
        if (conexion.text == "202")
        {
            Debug.Log("Valor actualizado");
            scoreUsuario = nScore;
        }
        else
        {
            print(conexion.text);
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
    IEnumerator GuardarP()
    {
        String Hoy = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        palabras = comentarios = GameObject.Find("Palabras").GetComponent<UnityEngine.UI.Text>().text;
        conexion1 = "http://localhost/CTA/gpfvsa.php?uss=" + idUsuario + "&pal=" + palabras + "&fec=" + Hoy;
        WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/gpfvsa.php?uss=" + idUsuario + "&pal=" + palabras + "&fec=" + Hoy);
        yield return (conexion);
        if (conexion.text == "203")
        {
            Debug.Log("Palabras y comentarios guardados");
        }
        else
        {
            print(conexion.text);
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
     
     
     */
