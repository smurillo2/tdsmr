﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using SpeechLib;//texto a voz

public class ActGNG : MonoBehaviour {
    public GameObject Boton;
    public GameObject Global;
    GameController Variables;
    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "", "avestruz", "ballena", "burro", "caballo", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "", "aguacate", "banano", "durazno", "fresa", "guayaba", "lulo", "mandarina", "mango", "manzana", "naranja", "papaya", "pera", "piña" };
    List<string> NamesFotosProfesiones = new List<string>() { "", "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "pintor", "policía", "profesor", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    /*
    List<string> NamesFotosAnimales = new List<string>() { "aguila", "avestruz", "ballena", "burro", "caballo", "caiman", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "raton", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    */
    List<string> Todas = new List<string>();
    List<string> TodasM = new List<string>();
    public int[] indexA = new int[200];
    int[] indexF = new int[200];
    int[] indexP = new int[200];
    public string imgpos = null;

    public static bool bandera;
    public static string ImgAct;
    public static string ImgAnt;
    public static List<string> bandera1 = new List<string>();
    public static List<string> bandera2 = new List<string>();

    public Text tiempoText;
    public static int i = 0;
    public static int Pi = 0;
    public int cont = 0;

    public int maxAleatoriedad = 2;//IA esto lo controlara el agente
    public int Aleatoriedad;//IA esto lo controlara el agente
    public static float Dtiempo = 5f;
    public static float PBlanco = 0f;
    public string NBlanco;
    public static int Nblanco;
    public static string Categoria;
    public static float[] G_tiempo;
    public static float[] auxtiempo;
    float tiempo = 8.0f;
    float tiempoI = 8.0f;
    public static float tiempoG = 0f;
    public static string NamesFotos;
    public SI_GES objeto_GES;
    public SI_GNG objeto_GNG;

    public static float[] GE_tiempo;
    public static int e;
    private SpVoice voice;
    string genero;
    string categorias;
    // Use this for initialization
    void Start () {
        tiempoG = 0;
        i = 0;
        Pi = 0;
         
        bandera1.Clear();
        bandera1.Add("Null");
        bandera2.Clear();
        

        NamesFotos = "";
        ImgAnt = "";

        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Texto1.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;
        
        textoBlanco = Texto3.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        //OrdenarSegunNivel();
        //NamesFotosAnimales.Sort();
        TodasM.AddRange(NamesFotosAnimales);
        TodasM.AddRange(NamesFotosFrutas);
        TodasM.AddRange(NamesFotosProfesiones);
        TodasM.Sort();//mejorar las formas de barajada

        NBlanco = PlayerPrefs.GetString("GNGNBlanco");
        Nblanco = int.Parse(NBlanco);
        Dtiempo = Int32.Parse(PlayerPrefs.GetString("GNGTBlanco"));
        PBlanco = float.Parse(PlayerPrefs.GetString("GNGPBlanco"));
        Categoria = PlayerPrefs.GetString("GNGCategoria");


        if (Categoria == "Animales")
        {
            genero = "un";
            categorias = "animal";
        }
        else if (Categoria == "Frutas")
        {
            genero = "una";
            categorias = "fruta";
        }
        else if (Categoria == "Profesiones")
        {
            genero = "una";
            categorias = "profesión";
        }
        textoCategoria = Texto2.GetComponent<Text>();
        textoCategoria.text = "" + Categoria;
        indexA = RandomNums(Nblanco, NamesFotosAnimales.Count);//IA esto lo puede modificar un agente
        indexF = RandomNums(Nblanco, NamesFotosFrutas.Count);//IA esto lo puede modificar un agente
        indexP = RandomNums(Nblanco, NamesFotosProfesiones.Count);//IA esto lo puede modificar un agente

        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Debes decir ¡igual!, si ves " + genero + " "+ categorias + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);



        /*switch (Variables.VarCategoria)
        {
            case "Animales":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
            case "Frutas":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
            case "Profesiones":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
        }*/

        /*for (int j = 0; j < TodasM.Count; j++)
        {
            Todas.Add(TodasM[index[j]]);
        }*/
        G_tiempo = new float[Nblanco + 1];
        auxtiempo = new float[Nblanco + 1];
        auxtiempo[0] = tiempoI;

        e = 0;
        GE_tiempo = new float[Nblanco + 1];
    }
	
	// Update is called once per frame
	public void Update () {
        //index = RandomNums(NamesFotosAnimales.Count,NamesFotosAnimales.Count);
        tiempo -= Time.deltaTime;
        tiempoText.text = "" + tiempo.ToString("f0");
        tiempoG += Time.deltaTime;
        Pi = i;
        if (tiempo < 0)
        {
            if (i > 0)
            {
                GameObject.Find("Botones").GetComponent<PerGNG>().ActPuntaje1("No", 1);
            }
            bandera = true;
            print("Tiempo cumplido" + cont.ToString());
            tiempo = Dtiempo;
            cont++;
            switch (Categoria)           
            {
                case "Animales":                    
                    Aleatoriedad = UnityEngine.Random.Range(0, 100);
                    if (Aleatoriedad <= PBlanco)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < Nblanco)
                        {
                            ImgAct = NamesFotosAnimales[indexA[i]];//cambiar el tipo de indice
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[indexA[i]]);//Cambiear el directorio el Names y el index
                            bandera1.Add("Animales");//
                            bandera2.Add(NamesFotosAnimales[indexA[i]]);//
                            bandera = true;
                            if (i > 1)//
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }//
                            NamesFotos += NamesFotosAnimales[indexA[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    else if (UnityEngine.Random.Range(0, 100) <= 50)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < Nblanco)//cambiar names
                        {
                            ImgAct = NamesFotosProfesiones[indexP[i]];//cambiar index
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[indexP[i]]);//cambiar ruta names e index
                            bandera1.Add("Profesiones");
                            bandera2.Add(NamesFotosProfesiones[indexP[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosProfesiones[indexP[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    else {
                        if (i < Nblanco)
                        {
                            ImgAct = NamesFotosFrutas[indexF[i]];
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[indexF[i]]);
                            bandera1.Add("Frutas");
                            bandera2.Add(NamesFotosFrutas[indexF[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosFrutas[indexF[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    break;                   
                case "Frutas":
                    Aleatoriedad = UnityEngine.Random.Range(0, 100);
                    if (Aleatoriedad <= PBlanco)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < Nblanco)
                        {
                            ImgAct = NamesFotosFrutas[indexF[i]];//cambiar el tipo de indice
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[indexF[i]]);//Cambiear el directorio el Names y el index
                            bandera1.Add("Frutas");
                            bandera2.Add(NamesFotosFrutas[indexF[i]]);//
                            bandera = true;                           
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosFrutas[indexF[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    else if (UnityEngine.Random.Range(0, 100) <= 50)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < Nblanco)//cambiar names
                        {
                            ImgAct = NamesFotosProfesiones[indexP[i]];//cambiar index
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[indexP[i]]);//cambiar ruta names e index
                            bandera1.Add("Profesiones");
                            bandera2.Add(NamesFotosProfesiones[indexP[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosProfesiones[indexP[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    else
                    {
                        if (i < Nblanco)
                        {
                            ImgAct = NamesFotosAnimales[indexA[i]];
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[indexA[i]]);
                            bandera1.Add("Animales");
                            bandera2.Add(NamesFotosAnimales[indexA[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosAnimales[indexA[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    break;
                case "Profesiones":
                    Aleatoriedad = UnityEngine.Random.Range(0, 100);
                    if (Aleatoriedad <= PBlanco)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < Nblanco)
                        {
                            ImgAct = NamesFotosProfesiones[indexP[i]];//cambiar el tipo de indice
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[indexP[i]]);//Cambiear el directorio el Names y el index
                            bandera1.Add("Profesiones");
                            bandera2.Add(NamesFotosProfesiones[indexP[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosProfesiones[indexP[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    else if (UnityEngine.Random.Range(0, 100) <= 50)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < Nblanco)//cambiar names
                        {
                            ImgAct = NamesFotosAnimales[indexA[i]];//cambiar index
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[indexA[i]]);//cambiar ruta names e index
                            bandera1.Add("Animales");
                            bandera2.Add(NamesFotosAnimales[indexA[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosAnimales[indexA[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    else
                    {
                        if (i < Nblanco)
                        {
                            ImgAct = NamesFotosFrutas[indexF[i]];
                            print(ImgAct);
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[indexF[i]]);
                            bandera1.Add("Frutas");
                            bandera2.Add(NamesFotosFrutas[indexF[i]]);//
                            bandera = true;
                            if (i > 1)
                            {
                                ImgAnt = bandera2[(Pi) - 1];
                                objeto_GES.DBWrite();
                            }
                            NamesFotos += NamesFotosFrutas[indexF[i]] + "|";
                            i++;
                        }
                        else if (i >= Nblanco)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                        {
                            ImgAnt = bandera2[(Pi) - 1];//
                            objeto_GES.DBWrite();
                            objeto_GNG.DBWrite();//
                            SceneManager.LoadScene("Lobby");
                        }
                    }
                    break;                    
            }
        }
    }

    int[] RandomNums(int cantidad, int maxval)
    {
        int aux;
        int[] index = new int[cantidad];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = UnityEngine.Random.Range(1, maxval);
        }
        for (int i = 0; i < cantidad - 1; i++)
        {
            if (index[i] == index[i + 1])
            {
                aux = index[i];
                index[i] = index[cantidad - 1];
                index[cantidad - 1] = index[i];
            }
        }
        return index;

    }
    public static void TiempoRespuesta()
    {
        auxtiempo[Pi] = tiempoG;
        G_tiempo[Pi] = tiempoG - auxtiempo[(Pi) - 1];
    }

    public static void TiempoErrores()
    {
        GE_tiempo[e] = tiempoG - auxtiempo[i - 1];
        e++;
    }


    /* int[] RandomNums(int cantidad, int maxval)
     {
         int[] index = new int[maxval];
         for (int i = 0; i < cantidad; i++)
         {
             index[i] = Random.Range(0, maxval);
         }
         return index;
     }*/
}

/*en este código hay que programar que se pongan categorías aleatorias en pantalla, que se diga el nombre de la categoria con sintesis de voz
y finalmente el reconocimiento de voz y los puntajes. Este código si cambia radicalmente en comparación con el del otro entrenamiento
pues la lógica para este entrenamiento es diferente*/

/*tiempo -= Time.deltaTime;
tiempoText.text = "" + tiempo.ToString("f0");
if (tiempo < 1 && bandera)
{
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);                        
i++;
tiempo = 2.0f;
}
if (i == NamesFotosAnimales.Count)
{
bandera = false;
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/fresa");
}*/
