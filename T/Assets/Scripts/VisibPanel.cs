﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisibPanel : MonoBehaviour {
    public GameObject SelecEntre;
    public GameObject CoPaASE;
    public GameObject CoPaNB;
    public GameObject CoPaS;
    public GameObject CoPaCDT;
    public GameObject CoPa;
    public GameObject CoPaC;
    // Use this for initialization
    void Start() {
        //SelecEntre.gameObject.transform.localScale = new Vector3(0, 0, 0);
        //SelecEntre.gameObject.transform.localScale = new Vector3(1, 1, 1);
        SelecEntre.SetActive(true);
        CoPaASE.SetActive(false);
        CoPaNB.SetActive(false);
        CoPaS.SetActive(false);
        CoPaCDT.SetActive(false);
        CoPa.SetActive(false);
        CoPaC.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
