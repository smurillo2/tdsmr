﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;//reconocimiento de voz
using System.Linq;
using System;
using SpeechLib;
using System.Xml;
using System.IO;

public class PerASE : MonoBehaviour
{
    // Verificar métodos de entrada
    public bool TMouse;
    public bool TTactil;
    public bool TVoz;
    // Actualizar puntaje y actualizar color botones
    public Text Puntaje;
    public string imgpos1;
    public List<string> posimg = new List<string>() { };
    int index;
    public int puntos;
    public int puntosN; //variable para contar los desaciertos. No se muestra para evitar producir un efecto negativo en el entrenado.
    public GameObject LeerVars;
    public GameObject otro;
    ActASE Variable;
    public Text FinNivel;
    GameObject Global;
    GameController Variables;
    public Color colorV = new Color(0.1F, 0.5F, 0F, 0.5F);
    public Color colorR = new Color(0.5F, 0.1F, 0F, 0.5F);
    public int Nivel;
    //public Button Buttons;
    public Button Button0;
    //Reconocimiento de voz
    KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public Text PalaDich;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "ratón", "sapo", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "mandarina", "mango", "manzana", "maracuyá", "melón", "naranja", "papaya", "pera", "sandía" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "padre", "sastre", "secretaria", "vendedor", "zapatero" };
    //List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    //palabras con género
    List<string> AnimalesGen = new List<string>() {"águila","avestruz","ballena", "gallina", "hormiga", "lombriz","jirafa", "oveja", "paloma", "tortuga", "vaca" };
    List<string> FrutasGen = new List<string>() { "aguacate", "banano", "durazno", "limón", "lulo", "mango", "melón", "zapote", "maracuyá" };
    string genero;
    public float contIntentos;
    private SpVoice voice;

    //puntuación
    public static int acierto;
    public static int error;

    //comentarios
    public Comentarios mensaje;
    string frase;


    //sonidos
    AudioSource BeepB;
    public AudioClip bien;
    AudioSource BeepM;
    public AudioClip mal;


    public SI_GES objeto_GES;


    public static string aciertosyerrores;
    public static string palabraserror;

    // Use this for initialization
    void Start()
    {
        //Inicializar variables
        acierto = 0;
        error = 0;

        // Leer Variables de control
        LeerVars = GameObject.Find("ActASE");
        Variable = LeerVars.GetComponent<ActASE>();
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        imgpos1 = Variable.imgpos;
        // Verificar métodos de entrada habilitados
        TMouse = Variables.VarTMouse;
        TTactil = Variables.VarTTactil;
        TVoz = Variables.VarTVoz;
        palabraserror = "";
        aciertosyerrores = "";

        if (TMouse)
        {
            print("Está activo el método Mouse");
            Button0.onClick.AddListener(() => ActPuntaje1(Button0.name, 1));
        }
        if (TTactil)
        {
            print("Está activo el método táctil");
            Button0.onClick.AddListener(() => ActPuntaje1(Button0.name, 1));
        }
        if (TVoz)
        {
            print("Esta activo el método voz");
            keywords.Add("go", () =>
            {
                GoCalled();
            });
            keywords.Add("abeja", () =>
            {
                AbejaCalled();
            });
            keywords.Add("águila", () =>
            {
                AguilaCalled();
            });
            keywords.Add("alacrán", () =>
            {
                AlacranCalled();
            });
            keywords.Add("araña", () =>
            {
                AranaCalled();
            });
            keywords.Add("ardilla", () =>
            {
                ArdillaCalled();
            });
            keywords.Add("armadillo", () =>
            {
                ArmadilloCalled();
            });
            keywords.Add("asno", () =>
            {
                AsnoCalled();
            });
            keywords.Add("avestruz", () =>
            {
                AvestruzCalled();
            });
            keywords.Add("ballena", () =>
            {
                BallenaCalled();
            });
            keywords.Add("barranquero", () =>
            {
                BarranqueroCalled();
            });
            keywords.Add("buey", () =>
            {
                BueyCalled();
            });
            keywords.Add("bufalo", () =>
            {
                BufaloCalled();
            });
            keywords.Add("buho", () =>
            {
                BuhoCalled();
            });
            keywords.Add("buitre", () =>
            {
                BuitreCalled();
            });
            keywords.Add("burro", () =>
            {
                BurroCalled();
            });
            keywords.Add("caballo", () =>
            {
                CaballoCalled();
            });
            keywords.Add("caballo de mar", () =>
            {
                CaballodemarCalled();
            });
            keywords.Add("cabra", () =>
            {
                CabraCalled();
            });
            keywords.Add("caimán", () =>
            {
                CaimanCalled();
            });
            keywords.Add("camaleón", () =>
            {
                CamaleonCalled();
            });
            keywords.Add("camello", () =>
            {
                CamelloCalled();
            });
            keywords.Add("canario", () =>
            {
                CanarioCalled();
            });
            keywords.Add("cangrejo", () =>
            {
                CangrejoCalled();
            });
            keywords.Add("chivo", () =>
            {
                ChivoCalled();
            });
            keywords.Add("cienpiés", () =>
            {
                CienpiesCalled();
            });
            keywords.Add("Chimpancé", () =>
            {
                MicoCalled();
            });
            keywords.Add("Simio", () =>
            {
                MicoCalled();
            });
            keywords.Add("cigüeña", () =>
            {
                CiguenaCalled();
            });
            keywords.Add("cocodrilo", () =>
            {
                CocodriloCalled();
            });
            keywords.Add("colibrí", () =>
            {
                ColibriCalled();
            });
            keywords.Add("cóndor", () =>
            {
                CondorCalled();
            });
            keywords.Add("conejo", () =>
            {
                ConejoCalled();
            });
            keywords.Add("cucaracha", () =>
            {
                CucarachaCalled();
            });
            keywords.Add("cucarrón", () =>
            {
                CucarronCalled();
            });
            keywords.Add("culebra", () =>
            {
                CulebraCalled();
            });
            keywords.Add("delfín", () =>
            {
                DelfinCalled();
            });
            keywords.Add("elefante", () =>
            {
                ElefanteCalled();
            });
            keywords.Add("faisán", () =>
            {
                FaisanCalled();
            });
            keywords.Add("foca", () =>
            {
                FocaCalled();
            });
            keywords.Add("gallina", () =>
            {
                GallinaCalled();
            });
            keywords.Add("gallinazo", () =>
            {
                GallinazoCalled();
            });
            keywords.Add("gallo", () =>
            {
                GalloCalled();
            });
            keywords.Add("ganzo", () =>
            {
                GanzoCalled();
            });
            keywords.Add("garrapata", () =>
            {
                GarrapataCalled();
            });
            keywords.Add("garza", () =>
            {
                GarzaCalled();
            });
            keywords.Add("gato", () =>
            {
                GatoCalled();
            });
            keywords.Add("gavilán", () =>
            {
                GavilanCalled();
            });
            keywords.Add("grillo", () =>
            {
                GrilloCalled();
            });
            keywords.Add("guacamaya", () =>
            {
                GuacamayaCalled();
            });
            keywords.Add("guatín", () =>
            {
                GuatinCalled();
            });
            keywords.Add("hipopótamo", () =>
            {
                HipopotamoCalled();
            });
            keywords.Add("hormiga", () =>
            {
                HormigaCalled();
            });
            keywords.Add("iguana", () =>
            {
                IguanaCalled();
            });
            keywords.Add("jirafa", () =>
            {
                JirafaCalled();
            });
            keywords.Add("lagartija", () =>
            {
                LagartijaCalled();
            });
            keywords.Add("lagartijo", () =>
            {
                LagartijaCalled();
            });
            keywords.Add("lechuza", () =>
            {
                LechuzaCalled();
            });
            keywords.Add("león", () =>
            {
                LeonCalled();
            });
            keywords.Add("leopardo", () =>
            {
                LeopardoCalled();
            });
            keywords.Add("puma", () =>
            {
                PumaCalled();
            });
            keywords.Add("lobo", () =>
            {
                LoboCalled();
            });
            keywords.Add("lombriz", () =>
            {
                LombrizCalled();
            });
            keywords.Add("loro", () =>
            {
                LoroCalled();
            });
            keywords.Add("mariposa", () =>
            {
                MariposaCalled();
            });
            keywords.Add("marrano", () =>
            {
                MarranoCalled();
            });
            keywords.Add("cerdo", () =>
            {
                MarranoCalled();
            });
            keywords.Add("chancho", () =>
            {
                MarranoCalled();
            });
            keywords.Add("mico", () =>
            {
                MicoCalled();
            });
            keywords.Add("mirla", () =>
            {
                MirlaCalled();
            });
            keywords.Add("mosca", () =>
            {
                MoscaCalled();
            });
            keywords.Add("mula", () =>
            {
                MulaCalled();
            });
            keywords.Add("murciélago", () =>
            {
                MurcielagoCalled();
            });
            keywords.Add("novillo", () =>
            {
                ToroCalled();
            });
            keywords.Add("oso", () =>
            {
                OsoCalled();
            });
            keywords.Add("oveja", () =>
            {
                OvejaCalled();
            });
            keywords.Add("ovejo", () =>
            {
                OvejaCalled();
            });
            keywords.Add("pájaro", () =>
            {
                PajaroCalled();
            });
            keywords.Add("pájarito", () =>
            {
                PajaroCalled();
            });
            keywords.Add("paloma", () =>
            {
                PalomaCalled();
            });
            keywords.Add("palomo", () =>
            {
                PalomaCalled();
            });
            keywords.Add("pantera", () =>
            {
                PanteraCalled();
            });
            keywords.Add("papagayo", () =>
            {
                PapagayoCalled();
            });
            keywords.Add("pato", () =>
            {
                PatoCalled();
            });
            keywords.Add("pavo", () =>
            {
                PavoCalled();
            });
            keywords.Add("perro", () =>
            {
                PerroCalled();
            });
            keywords.Add("pescado", () =>
            {
                PezCalled();
            });
            keywords.Add("pez", () =>
            {
                PezCalled();
            });
            keywords.Add("piojo", () =>
            {
                PiojoCalled();
            });
            keywords.Add("pisco", () =>
            {
                PizcoCalled();
            });
            keywords.Add("pollo", () =>
            {
                PolloCalled();
            });
            keywords.Add("pollito", () =>
            {
                PollitoCalled();
            });
            keywords.Add("pulga", () =>
            {
                PulgaCalled();
            });
            keywords.Add("pulpo", () =>
            {
                PulpoCalled();
            });
            keywords.Add("rata", () =>
            {
                RataCalled();
            });
            keywords.Add("rana", () =>
            {
                RanaCalled();
            });
            keywords.Add("ratón", () =>
            {
                RatonCalled();
            });
            keywords.Add("rinoceronte", () =>
            {
                RinoceronteCalled();
            });
            keywords.Add("sapo", () =>
            {
                SapoCalled();
            });
            keywords.Add("serpiente", () =>
            {
                SerpienteCalled();
            });
            keywords.Add("ternero", () =>
            {
                TerneroCalled();
            });
            keywords.Add("tiburón", () =>
            {
                TiburonCalled();
            });
            keywords.Add("tigre", () =>
            {
                TigreCalled();
            });
            keywords.Add("toro", () =>
            {
                ToroCalled();
            });
            keywords.Add("tortuga", () =>
            {
                TortugaCalled();
            });
            keywords.Add("trucha", () =>
            {
                TruchaCalled();
            });
            keywords.Add("vaca", () =>
            {
                VacaCalled();
            });
            keywords.Add("venado", () =>
            {
                VenadoCalled();
            });
            keywords.Add("yegua", () =>
            {
                YeguaCalled();
            });
            keywords.Add("zancudo", () =>
            {
                ZancudoCalled();
            });
            keywords.Add("zebra", () =>
            {
                ZebraCalled();
            });
            keywords.Add("zorro", () =>
            {
                ZorroCalled();
            });
            //frutas
            keywords.Add("aguacate", () =>
            {
                AguacateCalled();
            });
            keywords.Add("albaricoque", () =>
            {
                AlbaricoqueCalled();
            });
            keywords.Add("arazá", () =>
            {
                ArazaCalled();
            });
            keywords.Add("auyama", () =>
            {
                AuyamaCalled();
            });
            keywords.Add("badea", () =>
            {
                BadeaCalled();
            });
            keywords.Add("banano", () =>
            {
                BananoCalled();
            });
            keywords.Add("breva", () =>
            {
                BrevaCalled();
            });
            keywords.Add("carambolo", () =>
            {
                CaramboloCalled();
            });
            keywords.Add("cereza", () =>
            {
                CerezaCalled();
            });
            keywords.Add("chirimoya", () =>
            {
                ChirimoyaCalled();
            });
            keywords.Add("chontaduro", () =>
            {
                ChontaduroCalled();
            });
            keywords.Add("ciruela", () =>
            {
                CiruelaCalled();
            });
            keywords.Add("coco", () =>
            {
                CocoCalled();
            });
            keywords.Add("durazno", () =>
            {
                DuraznoCalled();
            });
            keywords.Add("feijoa", () =>
            {
                FeijoaCalled();
            });
            keywords.Add("frambuesa", () =>
            {
                FrambuesaCalled();
            });
            keywords.Add("fresa", () =>
            {
                FresaCalled();
            });
            keywords.Add("granada", () =>
            {
                GranadaCalled();
            });
            keywords.Add("granadilla", () =>
            {
                GranadillaCalled();
            });
            keywords.Add("guaba", () =>
            {
                GuabaCalled();
            });
            keywords.Add("guanabana", () =>
            {
                GuanabanaCalled();
            });
            keywords.Add("guayaba", () =>
            {
                GuayabaCalled();
            });
            keywords.Add("kiwi", () =>
            {
                KiwiCalled();
            });
            keywords.Add("limón", () =>
            {
                LimonCalled();
            });
            keywords.Add("lulo", () =>
            {
                LuloCalled();
            });
            keywords.Add("madroño", () =>
            {
                MadronoCalled();
            });
            keywords.Add("mamoncillo", () =>
            {
                MamoncilloCalled();
            });
            keywords.Add("mandarina", () =>
            {
                MandarinaCalled();
            });
            keywords.Add("mango", () =>
            {
                MangoCalled();
            });
            keywords.Add("manzana", () =>
            {
                ManzanaCalled();
            });
            keywords.Add("maracuyá", () =>
            {
                MaracuyaCalled();
            });
            keywords.Add("melocotón", () =>
            {
                MelocotonCalled();
            });
            keywords.Add("melón", () =>
            {
                MelonCalled();
            });
            keywords.Add("mora", () =>
            {
                MoraCalled();
            });
            keywords.Add("Naranja", () =>
            {
                NaranjaCalled();
            });
            keywords.Add("níspero", () =>
            {
                NisperoCalled();
            });
            keywords.Add("pomarrosa", () =>
            {
                PomarrosaCalled();
            });
            keywords.Add("papaya", () =>
            {
                PapayaCalled();
            });
            keywords.Add("pepino", () =>
            {
                PepinoCalled();
            });
            keywords.Add("pera", () =>
            {
                PeraCalled();
            });
            keywords.Add("piña", () =>
            {
                PiñaCalled();
            });
            keywords.Add("pitaya", () =>
            {
                PitayaCalled();
            });
            keywords.Add("pomelo", () =>
            {
                PomeloCalled();
            });
            keywords.Add("sandia", () =>
            {
                SandiaCalled();
            });
            keywords.Add("patilla", () =>
            {
                SandiaCalled();
            });
            keywords.Add("tomate", () =>
            {
                Tomate();
            });
            keywords.Add("tomate de árbol", () =>
            {
                TomatedearbolCalled();
            });
            keywords.Add("uchuva", () =>
            {
                UchuvaCalled();
            });
            keywords.Add("victoria", () =>
            {
                VictoriaCalled();
            });
            keywords.Add("zapote", () =>
            {
                ZapoteCalled();
            });
            //Profesiones
            keywords.Add("abogado", () =>
            {
                AbogadoCalled();
            });
            keywords.Add("agricultor", () =>
            {
                AgricultorCalled();
            });
            keywords.Add("campesino", () =>
            {
                CampesinoCalled();
            });
            keywords.Add("sembrador", () =>
            {
                SembradorCalled();
            });
            keywords.Add("albañil", () =>
            {
                AlbanilCalled();
            });
            keywords.Add("ama de casa", () =>
            {
                AmadecasaCalled();
            });
            keywords.Add("arquitecto", () =>
            {
                ArquitectoCalled();
            });
            keywords.Add("artesano", () =>
            {
                ArtesanoCalled();
            });
            keywords.Add("aseador", () =>
            {
                AseadorCalled();
            });
            keywords.Add("azafata", () =>
            {
                AzafataCalled();
            });
            keywords.Add("barrendero", () =>
            {
                BarrenderoCalled();
            });
            keywords.Add("bombero", () =>
            {
                BomberoCalled();
            });
            keywords.Add("camionero", () =>
            {
                CamioneroCalled();
            });
            keywords.Add("cantante", () =>
            {
                CantanteCalled();
            });
            keywords.Add("carnicero", () =>
            {
                CarniceroCalled();
            });
            keywords.Add("carpintero", () =>
            {
                CarpinteroCalled();
            });
            keywords.Add("cerrajero", () =>
            {
                CerrajeroCalled();
            });
            keywords.Add("chef", () =>
            {
                ChefCalled();
            });
            keywords.Add("cirujano", () =>
            {
                CirujanoCalled();
            });
            keywords.Add("conductor", () =>
            {
                ConductorCalled();
            });
            keywords.Add("chofer", () =>
            {
                ChoferCalled();
            });
            keywords.Add("taxista", () =>
            {
                TaxistaCalled();
            });
            keywords.Add("constructor", () =>
            {
                ConstructorCalled();
            });
            keywords.Add("contador", () =>
            {
                ContadorCalled();
            });
            keywords.Add("doctor", () =>
            {
                DoctorCalled();
            });
            keywords.Add("doctora", () =>
            {
                DoctoraCalled();
            });
            keywords.Add("electricista", () =>
            {
                ElectricistaCalled();
            });
            keywords.Add("enfermera", () =>
            {
                EnfermeraCalled();
            });
            keywords.Add("escritor", () =>
            {
                EscritorCalled();
            });
            keywords.Add("escultor", () =>
            {
                EscultorCalled();
            });
            keywords.Add("farmaceuta", () =>
            {
                FarmaceutaCalled();
            });
            keywords.Add("fisioterapeuta", () =>
            {
                FisioterapeutaCalled();
            });
            keywords.Add("futbolista", () =>
            {
                FutbolistaCalled();
            });
            keywords.Add("gimnasta", () =>
            {
                GimnastaCalled();
            });
            keywords.Add("ingeniero", () =>
            {
                IngenieroCalled();
            });
            keywords.Add("jardinero", () =>
            {
                JardineroCalled();
            });
            keywords.Add("lavandera", () =>
            {
                LavanderaCalled();
            });
            keywords.Add("locutor", () =>
            {
                LocutorCalled();
            });
            keywords.Add("manicurista", () =>
            {
                ManicuristaCalled();
            });
            keywords.Add("marinero", () =>
            {
                MarineroCalled();
            });
            keywords.Add("masajista", () =>
            {
                MasajistaCalled();
            });
            keywords.Add("mecánico", () =>
            {
                MecanicoCalled();
            });
            keywords.Add("médica", () =>
            {
                MedicaCalled();
            });
            keywords.Add("médico", () =>
            {
                MedicoCalled();
            });
            keywords.Add("mensajero", () =>
            {
                MensajeroCalled();
            });
            keywords.Add("mesero", () =>
            {
                MeseroCalled();
            });
            keywords.Add("niñera", () =>
            {
                NineraCalled();
            });
            keywords.Add("obrero", () =>
            {
                ObreroCalled();
            });
            keywords.Add("odontólogo", () =>
            {
                OdontologoCalled();
            });
            keywords.Add("dentista", () =>
            {
                DentistaCalled();
            });
            keywords.Add("oficial", () =>
            {
                OficialCalled();
            });
            keywords.Add("ayudante", () =>
            {
                AyudanteCalled();
            });
            keywords.Add("maestro", () =>
            {
                MaestroCalled();
            });
            keywords.Add("maestro de obra", () =>
            {
                MaestrodeobraCalled();
            });
            keywords.Add("oftalmólogo", () =>
            {
                OftalmologoCalled();
            });
            keywords.Add("ordenador", () =>
            {
                OrdenadorCalled();
            });
            keywords.Add("panadero", () =>
            {
                PanaderoCalled();
            });
            keywords.Add("peluquero", () =>
            {
                PeluqueroCalled();
            });
            keywords.Add("periodista", () =>
            {
                PeriodistaCalled();
            });
            keywords.Add("piloto", () =>
            {
                PilotoCalled();
            });
            keywords.Add("pintor", () =>
            {
                PintorCalled();
            });
            keywords.Add("policía", () =>
            {
                PoliciaCalled();
            });
            keywords.Add("político", () =>
            {
                PoliticoCalled();
            });
            keywords.Add("profesor", () =>
            {
                ProfesorCalled();
            });
            keywords.Add("docente", () =>
            {
                ProfesorCalled();
            });
            keywords.Add("psicólogo", () =>
            {
                PsicologoCalled();
            });
            keywords.Add("sacerdote", () =>
            {
                SacerdoteCalled();
            });
            keywords.Add("padre", () =>
            {
                SacerdoteCalled();
            });
            keywords.Add("cura", () =>
            {
                SacerdoteCalled();
            });
            keywords.Add("sastre", () =>
            {
                SastreCalled();
            });
            keywords.Add("costurera", () =>
            {
                SastreCalled();
            });
            keywords.Add("modista", () =>
            {
                SastreCalled();
            });
            keywords.Add("secretaria", () =>
            {
                SecretariaCalled();
            });
            keywords.Add("soldado", () =>
            {
                SoldadoCalled();
            });
            keywords.Add("tenista", () =>
            {
                TenistaCalled();
            });
            keywords.Add("vendedor", () =>
            {
                VendedorCalled();
            });
            keywords.Add("tendero", () =>
            {
                VendedorCalled();
            });
            keywords.Add("veterinario", () =>
            {
                VeterinarioCalled();
            });
            keywords.Add("zapatero", () =>
            {
                ZapateroCalled();
            });
            keywords.Add("hola", () =>
            {
                HolaCalled();
            });
            KeywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
            KeywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
            KeywordRecognizer.Start();
        }
        //Sintesis de voz
        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;
        BeepB = GetComponent<AudioSource>();
        BeepM = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /*void ActPuntaje(string boton, List<string> PosCat, List<string> PosDis)
    {
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        //Nivel = int.Parse(Variables.VarBlanco);
        if (int.Parse(Variables.VarBlanco) > int.Parse(Variables.VarDistra))
        {
            Nivel = int.Parse(Variables.VarBlanco);
        }
        else
        {
            Nivel = int.Parse(Variables.VarDistra);
        }
        if (boton == ("Button21"))
        {
            //boton = ("Button" + PosDis[0]);//esto hace que si no se dijo la palabra que era se asocie con un distractor y cuente error
            puntosN++;
            Puntaje.text = "Puntaje: " + puntos.ToString() + ". Revisa nuevamente la palabra que dijiste no esta en pantalla.";
        }
        for (int i = 0; i < Nivel; i++)
        {
            if (boton == ("Button" + PosCat[i]))
            {
                puntos++;
                Puntaje.text = "Puntaje: " + puntos.ToString() + ".";
                Button b = GameObject.Find("Button" + PosCat[i]).GetComponent<Button>();
                ColorBlock cb = b.colors;
                cb.normalColor = Color.green;
                cb.disabledColor = colorV;
                b.colors = cb;
                b.interactable = false;
            }
        }
        for (int i = 0; i < Nivel; i++)
        {
            if (boton == ("Button" + PosDis[i]))
            {
                puntosN++;
                Puntaje.text = "Puntaje: " + puntos.ToString() + ". Revisa nuevamente no escogiste una opción válida.";
                Button b = GameObject.Find("Button" + PosDis[i]).GetComponent<Button>();
                ColorBlock cb = b.colors;
                cb.normalColor = Color.red;
                cb.disabledColor = colorR;
                b.colors = cb;
                b.interactable = false;// con esto el objeto esta en la pantalla pero no se puede interactuar con el
                //b.gameObject.SetActive(false); // Con esto el objeto no se muestra en la pantalla
            }
        }
        if (puntos == int.Parse(Variables.VarBlanco))
        {
            //WaitForSeconds(5); 
            // Esto toca reprogramarlo pero de una forma funcional
            //FinNivel.gameObject.SetActive(true);
            //FinNivel.text = "Felicitaciones Nivel Completado";
            //StartCoroutine(Pere()); Esto no funciona pero es necesario revisarlo
            SceneManager.LoadScene("NuevoNivel");
        }
    }*/
    public void ActPuntaje1(string Nombre, int iscorrect)/*la función más importante de este script debe ser conectarse con la base de datos para almacenar el dato*/
    {
        if (Variables.VarCategoria == "Animales")
        {
            if (AnimalesGen.Contains(ActASE.ImgAct))
                genero = "una ";
            else
                genero = "un ";
        }
        else if (Variables.VarCategoria == "Frutas")
        {
            if (FrutasGen.Contains(ActASE.ImgAct))
                genero = "un ";
            else
                genero = "una ";
        }
        //corrección tildes, para evitar problemas de reconocimiento.
        //animales
        if (Nombre == "aguila")
            Nombre = "águila";
        if (Nombre == "caiman")
            Nombre = "caimán";
        if (Nombre == "leon")
            Nombre = "león";
        if (Nombre == "pajaro")
            Nombre = "pájaro";
        if (Nombre == "guanabana")
            Nombre = "guanábana";
        if (Nombre == "limon")
            Nombre = "limón";
        if (Nombre == "maracuya")
            Nombre = "maracuyá";
        if (Nombre == "melon")
            Nombre = "melón";
        if (Nombre == "sandia")
            Nombre = "sandía";
        if (Nombre == "amadecasa")
            Nombre = "ama de casa";
        if (Nombre == "mecanico")
            Nombre = "mecánico";
        if (Nombre == "medica")
            Nombre = "médica";
        if (Nombre == "medico")
            Nombre = "médico";
        if (Nombre == "odontologo")
            Nombre = "odontólogo";
        if (Nombre == "psicologo")
            Nombre = "psicólogo";
        if (Nombre == "policia")
            Nombre = "policía";
        //frutas
        if (Nombre == "raton")
            Nombre = "ratón";
        print("El animal es:" + ActASE.ImgAct);
        switch (Variables.VarCategoria)
        {
           case "Animales":
                if (Nombre == "Button")
                {
                    /*BeepM.clip = mal;
                    BeepM.Play();*/
                    if (contIntentos <= 3)
                    {
                        ActASE.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + Nombre + "|";
                        ActASE.bandera = false;
                        print("Incorrecto");
                        error++;
                        contIntentos++;
                        if (contIntentos == 1)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Este animal es" + genero + ActASE.ImgAct + ", Dilo ahora y recuérdalo"                               
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 2)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Recuerda, este animal es" + genero + ActASE.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 3)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Este animal es" + genero + ActASE.ImgAct + ", Recuérdalo, no lo olvides "
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                    }

                    else
                    {
                        ActASE.bandera = true;
                        contIntentos = 0;
                        objeto_GES.DBWrite();
                    }
                }
                else if (Nombre == ActASE.ImgAct)
                {
                    BeepB.clip = bien;
                    BeepB.Play();
                    ActASE.bandera = true;
                    aciertosyerrores = aciertosyerrores + "|1";
                    print("Correcto");
                    objeto_GES.DBWrite();
                    mensaje = new Comentarios("p");
                    frase=mensaje.respuesta("p");
                    voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                    + frase
                                                    + "</speak>",
                                                    SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    acierto++;
                    contIntentos = 0;
                }
                else
                {
                    BeepM.clip = mal;
                    BeepM.Play();
                    if (contIntentos <= 3)
                    {
                        ActASE.bandera = false;
                        ActASE.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + Nombre + "|";
                        print("Incorrecto");                        
                        error++;
                        contIntentos++;
                        if (contIntentos == 1)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Este animal es" + genero + ActASE.ImgAct + ", Dilo ahora y recuérdalo"
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 2)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Recuerda, este animal es" + genero + ActASE.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 3)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Este animal es" + genero + ActASE.ImgAct + ", Recuérdalo, no lo olvides "
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                    }
                    else
                    {
                        ActASE.bandera = true;
                        contIntentos = 0;
                        objeto_GES.DBWrite();
                    }
                }
                    break;
            case "Frutas":
                if (Nombre == "Button")
                {
                    /*BeepM.clip = mal;
                    BeepM.Play();*/
                    if (contIntentos <= 3)
                    {
                        ActASE.bandera = false;
                        ActASE.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + Nombre + "|";
                        print("Incorrecto");
                        error++;
                        contIntentos++;
                        if (contIntentos == 1)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta fruta es" + genero + ActASE.ImgAct + ", Dilo ahora y recuérdalo"
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 2)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Recuerda, ésta fruta es" + genero + ActASE.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 3)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta fruta es" + genero + ActASE.ImgAct + ", Recuérdalo, no lo olvides "
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                    }
                    else
                    {
                        ActASE.bandera = true;
                        contIntentos = 0;
                        objeto_GES.DBWrite();
                    }
                }
                else if (Nombre == ActASE.ImgAct)//esto hay que optimizarse de momento se esta haciendo una estupidez la que cuenta es la imagen actual ese tal imgpos no sirve para nada
                {
                    BeepB.clip = bien;
                    BeepB.Play();
                    ActASE.bandera = true;
                    objeto_GES.DBWrite();
                    print("Correcto");
                    aciertosyerrores = aciertosyerrores + "|1";
                    mensaje = new Comentarios("p");
                    frase = mensaje.respuesta("p");
                    voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                    + frase
                                                    + "</speak>",
                                                    SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    acierto++;
                    contIntentos = 0;
                }
                else
                {
                    BeepM.clip = mal;
                    BeepM.Play();
                    if (contIntentos <= 3)
                    {
                        ActASE.bandera = false;
                        ActASE.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + Nombre + "|";
                        print("Incorrecto");
                        error++;
                        contIntentos++;
                        if (contIntentos == 1)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta fruta es" + genero + ActASE.ImgAct + ", Dilo ahora y recuérdalo"
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 2)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Recuerda, ésta fruta es" + genero + ActASE.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 3)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta fruta es" + genero + ActASE.ImgAct + ", Recuérdalo, no lo olvides "
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                    }
                    else
                    {
                        ActASE.bandera = true;
                        contIntentos = 0;
                        objeto_GES.DBWrite();
                    }
                }
                break;
            case "Profesiones":
                if (Nombre == "Button")
                {
                    /*BeepM.clip = mal;
                    BeepM.Play();*/
                    if (contIntentos <= 3)
                    {
                        ActASE.bandera = false;
                        ActASE.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + Nombre + "|";
                        print("Incorrecto");
                        error++;
                        contIntentos++;
                        if (contIntentos == 1)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta profesión es: " + ActASE.ImgAct + ", Dilo ahora y recuérdalo"
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 2)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Recuerda, ésta profesión es: " + ActASE.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 3)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta profesión es: " + ActASE.ImgAct + ", Recuérdalo, no lo olvides "
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                    }
                    else
                    {
                        ActASE.bandera = true;
                        contIntentos = 0;
                        objeto_GES.DBWrite();
                    }
                }
                else if (Nombre == ActASE.ImgAct)//esto hay que optimizarse de momento se esta haciendo una estupidez la que cuenta es la imagen actual ese tal imgpos no sirve para nada
                {
                    BeepB.clip = bien;
                    BeepB.Play();
                    ActASE.bandera = true;
                    print("Correcto");
                    aciertosyerrores = aciertosyerrores + "|1";
                    objeto_GES.DBWrite();
                    mensaje = new Comentarios("p");
                    frase = mensaje.respuesta("p");
                    voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                    + frase
                                                    + "</speak>",
                                                    SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    acierto++;
                    contIntentos = 0;
                }
                else
                {
                    BeepM.clip = mal;
                    BeepM.Play();
                    if (contIntentos <= 3)
                    {
                        ActASE.bandera = false;
                        ActASE.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + Nombre + "|";
                        print("Incorrecto");
                        error++;
                        contIntentos++;
                        if (contIntentos == 1)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta profesión es: " + ActASE.ImgAct + ", Dilo ahora y recuérdalo"
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 2)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Recuerda, ésta profesión es: " + ActASE.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                        if (contIntentos == 3)
                        {
                            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Ésta profesión es: " + ActASE.ImgAct + ", Recuérdalo, no lo olvides "
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                        }
                    }
                    else
                    {
                        ActASE.bandera = true;
                        contIntentos = 0;
                        objeto_GES.DBWrite();
                    }
                }
                break;
        }
        
    }
    /*    IEnumerator Pere()
        {
            Debug.Log(Time.time);
            yield return new WaitForSeconds(10);
            Debug.Log(Time.time);
            Debug.Log("Just waited 10 second");
        }*/
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    //funciones para el conteo de los puntos
    void GoCalled()
    {
        print("Dijiste Go");
        PalaDich.text += ", go";
    }
    void HolaCalled()
    {
        print("Dijiste Hola");
        PalaDich.text += ", Hola";
    }
    void AbejaCalled()
    {
        print("Dijiste abeja");
        PalaDich.text += ", abeja";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "abeja")
        {
            ActPuntaje1("abeja", 1);
        }
        else
        {
            ActPuntaje1("abeja", 0);
        }
    }
    void AguilaCalled()
    {
        print("Dijiste aguila");
        PalaDich.text += ", aguila";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "aguila")
        {
            ActPuntaje1("aguila", 1);
        }
        else
        {
            ActPuntaje1("aguila", 0);
        }
    }
    void AlacranCalled()
    {
        print("Dijiste alacran");
        PalaDich.text += ", alacran";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "alacran")
        {
            ActPuntaje1("alacran", 1);
        }
        else
        {
            ActPuntaje1("alacran", 0);
        }
    }
    void AranaCalled()
    {
        print("Dijiste araña");
        PalaDich.text += ", araña";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "araña")
        {
            ActPuntaje1("araña", 1);
        }
        else
        {
            ActPuntaje1("araña", 0);
        }
    }
    void ArdillaCalled()
    {
        print("Dijiste ardilla");
        PalaDich.text += ", ardilla";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ardilla")
        {
            ActPuntaje1("ardilla", 1);
        }
        else
        {
            ActPuntaje1("ardilla", 0);
        }
    }
    void ArmadilloCalled()
    {
        print("Dijiste armadillo");
        PalaDich.text += ", armadillo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "armadillo")
        {
            ActPuntaje1("armadillo", 1);
        }
        else
        {
            ActPuntaje1("armadillo", 0);
        }
    }
    void AsnoCalled()
    {
        print("Dijiste asno");
        PalaDich.text += ", asno";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "asno")
        {
            ActPuntaje1("asno", 1);
        }
        else
        {
            ActPuntaje1("asno", 0);
        }
    }
    void AvestruzCalled()
    {
        print("Dijiste avestruz");
        PalaDich.text += ", avestruz";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "avestruz")
        {
            ActPuntaje1("avestruz", 1);
        }
        else
        {
            ActPuntaje1("avestruz", 0);
        }
    }
    void BallenaCalled()
    {
        print("Dijiste ballena");
        PalaDich.text += ", ballena";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ballena")
        {
            ActPuntaje1("ballena", 1);
        }
        else
        {
            ActPuntaje1("ballena", 0);
        }
    }
    void BarranqueroCalled()
    {
        print("Dijiste barranquero");
        PalaDich.text += ", barranquero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "barranquero")
        {
            ActPuntaje1("barranquero", 1);
        }
        else
        {
            ActPuntaje1("barranquero", 0);
        }
    }
    void BueyCalled()
    {
        print("Dijiste buey");
        PalaDich.text += ", buey";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "buey")
        {
            ActPuntaje1("buey", 1);
        }
        else
        {
            ActPuntaje1("buey", 0);
        }
    }
    void BufaloCalled()
    {
        print("Dijiste bufalo");
        PalaDich.text += ", bufalo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "bufalo")
        {
            ActPuntaje1("bufalo", 1);
        }
        else
        {
            ActPuntaje1("bufalo", 0);
        }
    }
    void BuhoCalled()
    {
        print("Dijiste buho");
        PalaDich.text += ", buho";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "buho")
        {
            ActPuntaje1("buho", 1);
        }
        else
        {
            ActPuntaje1("buho", 0);
        }
    }
    void BuitreCalled()
    {
        print("Dijiste buitre");
        PalaDich.text += ", buitre";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "buitre")
        {
            ActPuntaje1("buitre", 1);
        }
        else
        {
            ActPuntaje1("buitre", 0);
        }
    }
    void BurroCalled()
    {
        print("Dijiste burro");
        PalaDich.text += ", burro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "burro")
        {
            ActPuntaje1("burro", 1);
        }
        else
        {
            ActPuntaje1("burro", 0);
        }
    }
    void CaballoCalled()
    {
        print("Dijiste caballo");
        PalaDich.text += ", caballo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "caballo")
        {
            ActPuntaje1("caballo", 1);
        }
        else
        {
            ActPuntaje1("caballo", 0);
        }
    }
    void CaballodemarCalled()
    {
        print("Dijiste Caballo de mar");
        PalaDich.text += ", Caballo de mar";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "Caballo de mar")
        {
            ActPuntaje1("Caballo de mar", 1);
        }
        else
        {
            ActPuntaje1("Caballo de mar", 0);
        }
    }
    void CabraCalled()
    {
        print("Dijiste cabra");
        PalaDich.text += ", cabra";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cabra")
        {
            ActPuntaje1("cabra", 1);
        }
        else
        {
            ActPuntaje1("cabra", 0);
        }
    }
    void CaimanCalled()
    {
        print("Dijiste caiman");
        PalaDich.text += ", caiman";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "caiman")
        {
            ActPuntaje1("caiman", 1);
        }
        else
        {
            ActPuntaje1("caiman", 0);
        }
    }
    void CamaleonCalled()
    {
        print("Dijiste camaleon");
        PalaDich.text += ", camaleon";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "camaleon")
        {
            ActPuntaje1("camaleon", 1);
        }
        else
        {
            ActPuntaje1("camaleon", 0);
        }
    }
    void CamelloCalled()
    {
        print("Dijiste camello");
        PalaDich.text += ", camello";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "camello")
        {
            ActPuntaje1("camello", 1);
        }
        else
        {
            ActPuntaje1("camello", 0);
        }
    }
    void CanarioCalled()
    {
        print("Dijiste canario");
        PalaDich.text += ", canario";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "canario")
        {
            ActPuntaje1("canario", 1);
        }
        else
        {
            ActPuntaje1("canario", 0);
        }
    }
    void CangrejoCalled()
    {
        print("Dijiste cangrejo");
        PalaDich.text += ", cangrejo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cangrejo")
        {
            ActPuntaje1("cangrejo", 1);
        }
        else
        {
            ActPuntaje1("cangrejo", 0);
        }
    }
    void ChivoCalled()
    {
        print("Dijiste chivo");
        PalaDich.text += ", chivo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "chivo")
        {
            ActPuntaje1("chivo", 1);
        }
        else
        {
            ActPuntaje1("chivo", 0);
        }
    }
    void CienpiesCalled()
    {
        print("Dijiste cienpies");
        PalaDich.text += ", cienpies";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cienpies")
        {
            ActPuntaje1("cienpies", 1);
        }
        else
        {
            ActPuntaje1("cienpies", 0);
        }
    }
    void CiguenaCalled()
    {
        print("Dijiste ciguena");
        PalaDich.text += ", ciguena";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ciguena")
        {
            ActPuntaje1("ciguena", 1);
        }
        else
        {
            ActPuntaje1("ciguena", 0);
        }
    }
    void CocodriloCalled()
    {
        print("Dijiste cocodrilo");
        PalaDich.text += ", cocodrilo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cocodrilo")
        {
            ActPuntaje1("cocodrilo", 1);
        }
        else
        {
            ActPuntaje1("cocodrilo", 0);
        }
    }
    void ColibriCalled()
    {
        print("Dijiste colibri");
        PalaDich.text += ", colibri";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "colibri")
        {
            ActPuntaje1("colibri", 1);
        }
        else
        {
            ActPuntaje1("colibri", 0);
        }
    }
    void CondorCalled()
    {
        print("Dijiste condor");
        PalaDich.text += ", condor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "condor")
        {
            ActPuntaje1("condor", 1);
        }
        else
        {
            ActPuntaje1("condor", 0);
        }
    }
    void ConejoCalled()
    {
        print("Dijiste conejo");
        PalaDich.text += ", conejo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "conejo")
        {
            ActPuntaje1("conejo", 1);
        }
        else
        {
            ActPuntaje1("conejo", 0);
        }
    }
    void CucarachaCalled()
    {
        print("Dijiste cucaracha");
        PalaDich.text += ", cucaracha";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cucaracha")
        {
            ActPuntaje1("cucaracha", 1);
        }
        else
        {
            ActPuntaje1("cucaracha", 0);
        }
    }
    void CucarronCalled()
    {
        print("Dijiste cucarron");
        PalaDich.text += ", cucarron";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cucarron")
        {
            ActPuntaje1("cucarron", 1);
        }
        else
        {
            ActPuntaje1("cucarron", 0);
        }
    }
    void CulebraCalled()
    {
        print("Dijiste culebra");
        PalaDich.text += ", culebra";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "culebra")
        {
            ActPuntaje1("culebra", 1);
        }
        else
        {
            ActPuntaje1("culebra", 0);
        }
    }
    void DelfinCalled()
    {
        print("Dijiste delfin");
        PalaDich.text += ", delfin";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "delfin")
        {
            ActPuntaje1("delfin", 1);
        }
        else
        {
            ActPuntaje1("delfin", 0);
        }
    }
    void ElefanteCalled()
    {
        print("Dijiste elefante");
        PalaDich.text += ", elefante";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "elefante")
        {
            ActPuntaje1("elefante", 1);
        }
        else
        {
            ActPuntaje1("elefante", 0);
        }
    }
    void FaisanCalled()
    {
        print("Dijiste faisan");
        PalaDich.text += ", faisan";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "faisan")
        {
            ActPuntaje1("faisan", 1);
        }
        else
        {
            ActPuntaje1("faisan", 0);
        }
    }
    void FocaCalled()
    {
        print("Dijiste foca");
        PalaDich.text += ", foca";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "foca")
        {
            ActPuntaje1("foca", 1);
        }
        else
        {
            ActPuntaje1("foca", 0);
        }
    }
    void GallinaCalled()
    {
        print("Dijiste gallina");
        PalaDich.text += ", gallina";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "gallina")
        {
            ActPuntaje1("gallina", 1);
        }
        else
        {
            ActPuntaje1("gallina", 0);
        }
    }
    void GallinazoCalled()
    {
        print("Dijiste gallinazo");
        PalaDich.text += ", gallinazo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "gallinazo")
        {
            ActPuntaje1("gallinazo", 1);
        }
        else
        {
            ActPuntaje1("gallinazo", 0);
        }
    }
    void GalloCalled()
    {
        print("Dijiste gallo");
        PalaDich.text += ", gallo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "gallo")
        {
            ActPuntaje1("gallo", 1);
        }
        else
        {
            ActPuntaje1("gallo", 0);
        }
    }
    void GanzoCalled()
    {
        print("Dijiste ganzo");
        PalaDich.text += ", ganzo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ganzo")
        {
            ActPuntaje1("ganzo", 1);
        }
        else
        {
            ActPuntaje1("ganzo", 0);
        }
    }
    void GarrapataCalled()
    {
        print("Dijiste garrapata");
        PalaDich.text += ", garrapata";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "garrapata")
        {

            ActPuntaje1("garrapata", 1);
        }
        else
        {
            ActPuntaje1("garrapata", 0);
        }
    }
    void GarzaCalled()
    {
        print("Dijiste garza");
        PalaDich.text += ", garza";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "garza")
        {

            ActPuntaje1("garza", 1);
        }
        else
        {
            ActPuntaje1("garza", 0);
        }
    }
    void GatoCalled()
    {
        print("Dijiste gato");
        PalaDich.text += ", gato";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "gato")
        {

            ActPuntaje1("gato", 1);
        }
        else
        {
            ActPuntaje1("gato", 0);
        }
    }
    void GavilanCalled()
    {
        print("Dijiste gavilan");
        PalaDich.text += ", gavilan";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "gavilan")
        {

            ActPuntaje1("gavilan", 1);
        }
        else
        {
            ActPuntaje1("gavilan", 0);
        }
    }
    void GrilloCalled()
    {
        print("Dijiste grillo");
        PalaDich.text += ", grillo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "grillo")
        {

            ActPuntaje1("grillo", 1);
        }
        else
        {
            ActPuntaje1("grillo", 0);
        }
    }
    void GuacamayaCalled()
    {
        print("Dijiste guacamaya");
        PalaDich.text += ", guacamaya";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "guacamaya")
        {

            ActPuntaje1("guacamaya", 1);
        }
        else
        {
            ActPuntaje1("guacamaya", 0);
        }
    }
    void GuatinCalled()
    {
        print("Dijiste guatin");
        PalaDich.text += ", guatin";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "guatin")
        {

            ActPuntaje1("guatin", 1);
        }
        else
        {
            ActPuntaje1("guatin", 0);
        }
    }
    void HipopotamoCalled()
    {
        print("Dijiste hipopotamo");
        PalaDich.text += ", hipopotamo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "hipopotamo")
        {

            ActPuntaje1("hipopotamo", 1);
        }
        else
        {
            ActPuntaje1("hipopotamo", 0);
        }
    }
    void HormigaCalled()
    {
        print("Dijiste hormiga");
        PalaDich.text += ", hormiga";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "hormiga")
        {

            ActPuntaje1("hormiga", 1);
        }
        else
        {
            ActPuntaje1("hormiga", 0);
        }
    }
    void IguanaCalled()
    {
        print("Dijiste iguana");
        PalaDich.text += ", iguana";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "iguana")
        {

            ActPuntaje1("iguana", 1);
        }
        else
        {
            ActPuntaje1("iguana", 0);
        }
    }
    void JirafaCalled()
    {
        print("Dijiste jirafa");
        PalaDich.text += ", jirafa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "jirafa")
        {

            ActPuntaje1("jirafa", 1);
        }
        else
        {
            ActPuntaje1("jirafa", 0);
        }
    }
    void LagartijaCalled()
    {
        print("Dijiste lagartija");
        PalaDich.text += ", lagartija";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "lagartija")
        {

            ActPuntaje1("lagartija", 1);
        }
        else
        {
            ActPuntaje1("lagartija", 0);
        }
    }
    void LechuzaCalled()
    {
        print("Dijiste lechuza");
        PalaDich.text += ", lechuza";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "lechuza")
        {

            ActPuntaje1("lechuza", 1);
        }
        else
        {
            ActPuntaje1("lechuza", 0);
        }
    }
    void LeonCalled()
    {
        print("Dijiste leon");
        PalaDich.text += ", leon";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "leon")
        {

            ActPuntaje1("leon", 1);
        }
        else
        {
            ActPuntaje1("leon", 0);
        }
    }
    void LeopardoCalled()
    {
        print("Dijiste leopardo");
        PalaDich.text += ", leopardo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "leopardo")
        {

            ActPuntaje1("leopardo", 1);
        }
        else
        {
            ActPuntaje1("leopardo", 0);
        }
    }
    void PumaCalled()
    {
        print("Dijiste puma");
        PalaDich.text += ", puma";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "puma")
        {

            ActPuntaje1("puma", 1);
        }
        else
        {
            ActPuntaje1("puma", 0);
        }
    }
    void LoboCalled()
    {
        print("Dijiste lobo");
        PalaDich.text += ", lobo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "lobo")
        {

            ActPuntaje1("lobo", 1);
        }
        else
        {
            ActPuntaje1("lobo", 0);
        }
    }
    void LombrizCalled()
    {
        print("Dijiste lombriz");
        PalaDich.text += ", lombriz";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "lombriz")
        {

            ActPuntaje1("lombriz", 1);
        }
        else
        {
            ActPuntaje1("lombriz", 0);
        }
    }
    void LoroCalled()
    {
        print("Dijiste loro");
        PalaDich.text += ", loro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "loro")
        {

            ActPuntaje1("loro", 1);
        }
        else
        {
            ActPuntaje1("loro", 0);
        }
    }
    void MariposaCalled()
    {
        print("Dijiste mariposa");
        PalaDich.text += ", mariposa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mariposa")
        {

            ActPuntaje1("mariposa", 1);
        }
        else
        {
            ActPuntaje1("mariposa", 0);
        }
    }
    void MarranoCalled()
    {
        print("Dijiste marrano");
        PalaDich.text += ", marrano";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "marrano")
        {

            ActPuntaje1("marrano", 1);
        }
        else
        {
            ActPuntaje1("marrano", 0);
        }
    }
    void MicoCalled()
    {
        print("Dijiste mico");
        PalaDich.text += ", mico";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mico")
        {

            ActPuntaje1("mico", 1);
        }
        else
        {
            ActPuntaje1("mico", 0);
        }
    }

    void MirlaCalled()
    {
        print("Dijiste mirla");
        PalaDich.text += ", mirla";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mirla")
        {

            ActPuntaje1("mirla", 1);
        }
        else
        {
            ActPuntaje1("mirla", 0);
        }
    }
    void MoscaCalled()
    {
        print("Dijiste mosca");
        PalaDich.text += ", mosca";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mosca")
        {

            ActPuntaje1("mosca", 1);
        }
        else
        {
            ActPuntaje1("mosca", 0);
        }
    }
    void MulaCalled()
    {
        print("Dijiste mula");
        PalaDich.text += ", mula";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mula")
        {

            ActPuntaje1("mula", 1);
        }
        else
        {
            ActPuntaje1("mula", 0);
        }
    }
    void MurcielagoCalled()
    {
        print("Dijiste murciélago");
        PalaDich.text += ", murciélago";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "murciélago")
        {

            ActPuntaje1("murciélago", 1);
        }
        else
        {
            ActPuntaje1("murciélago", 0);
        }
    }
    void OsoCalled()
    {
        print("Dijiste oso");
        PalaDich.text += ", oso";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "oso")
        {

            ActPuntaje1("oso", 1);
        }
        else
        {
            ActPuntaje1("oso", 0);
        }
    }
    void OvejaCalled()
    {
        print("Dijiste oveja");
        PalaDich.text += ", oveja";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "oveja")
        {

            ActPuntaje1("oveja", 1);
        }
        else
        {
            ActPuntaje1("oveja", 0);
        }
    }
    void PajaroCalled()
    {
        print("Dijiste pajaro");
        PalaDich.text += ", pajaro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pajaro")
        {

            ActPuntaje1("pajaro", 1);
        }
        else
        {
            ActPuntaje1("pajaro", 0);
        }
    }
    void PalomaCalled()
    {
        print("Dijiste paloma");
        PalaDich.text += ", paloma";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "paloma")
        {

            ActPuntaje1("paloma", 1);
        }
        else
        {
            ActPuntaje1("paloma", 0);
        }
    }
    void PanteraCalled()
    {
        print("Dijiste pantera");
        PalaDich.text += ", pantera";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pantera")
        {

            ActPuntaje1("pantera", 1);
        }
        else
        {
            ActPuntaje1("pantera", 0);
        }
    }
    void PapagayoCalled()
    {
        print("Dijiste papagayo");
        PalaDich.text += ", papagayo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "papagayo")
        {

            ActPuntaje1("papagayo", 0);
        }
        else
        {
            ActPuntaje1("papagayo", 1);
        }
    }
    void PatoCalled()
    {
        print("Dijiste pato");
        PalaDich.text += ", pato";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pato")
        {

            ActPuntaje1("pato", 1);
        }
        else
        {
            ActPuntaje1("pato", 0);
        }
    }
    void PavoCalled()
    {
        print("Dijiste pavo");
        PalaDich.text += ", pavo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pavo")
        {

            ActPuntaje1("pavo", 1);
        }
        else
        {
            ActPuntaje1("pavo", 0);
        }
    }
    void PerroCalled()
    {
        print("Dijiste perro");
        PalaDich.text += ", perro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "perro")
        {

            ActPuntaje1("perro", 1);
        }
        else
        {
            ActPuntaje1("perro", 0);
        }
    }
    void PescadoCalled()
    {
        print("Dijiste pescado");
        PalaDich.text += ", pescado";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pescado")
        {

            ActPuntaje1("pescado", 1);
        }
        else
        {
            ActPuntaje1("pescado", 0);
        }
    }
    void PezCalled()
    {
        print("Dijiste pez");
        PalaDich.text += ", pez";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pez")
        {

            ActPuntaje1("pez", 1);
        }
        else
        {
            ActPuntaje1("pez", 0);
        }
    }
    void PiojoCalled()
    {
        print("Dijiste piojo");
        PalaDich.text += ", piojo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "piojo")
        {

            ActPuntaje1("piojo", 1);
        }
        else
        {
            ActPuntaje1("piojo", 0);
        }
    }
    void PizcoCalled()
    {
        print("Dijiste pizco");
        PalaDich.text += ", pizco";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pizco")
        {

            ActPuntaje1("pizco", 1);
        }
        else
        {
            ActPuntaje1("pizco", 0);
        }
    }
    void PolloCalled()
    {
        print("Dijiste pollo");
        PalaDich.text += ", pollo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pollo")
        {

            ActPuntaje1("pollo", 1);
        }
        else
        {
            ActPuntaje1("pollo", 0);
        }
    }
    void PollitoCalled()
    {
        print("Dijiste pollito");
        PalaDich.text += ", pollo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pollo")
        {

            ActPuntaje1("pollo", 1);
        }
        else
        {
            ActPuntaje1("pollo", 0);
        }
    }
    void PulgaCalled()
    {
        print("Dijiste pulga");
        PalaDich.text += ", pulga";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pulga")
        {

            ActPuntaje1("pulga", 1);
        }
        else
        {
            ActPuntaje1("pulga", 0);
        }
    }
    void PulpoCalled()
    {
        print("Dijiste pulpo");
        PalaDich.text += ", pulpo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pulpo")
        {

            ActPuntaje1("pulpo", 1);
        }
        else
        {
            ActPuntaje1("pulpo", 0);
        }
    }
    void RataCalled()
    {
        print("Dijiste rata");
        PalaDich.text += ", rata";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "rata")
        {

            ActPuntaje1("rata", 1);
        }
        else
        {
            ActPuntaje1("rata", 0);
        }
    }
    void RanaCalled()
    {
        print("Dijiste rana");
        PalaDich.text += ", rana";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "rana")
        {

            ActPuntaje1("rana", 1);
        }
        else
        {
            ActPuntaje1("rana", 0);
        }
    }
    void RatonCalled()
    {
        print("Dijiste raton");
        PalaDich.text += ", raton";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "raton")
        {

            ActPuntaje1("raton", 1);
        }
        else
        {
            ActPuntaje1("raton", 0);
        }
    }
    void RinoceronteCalled()
    {
        print("Dijiste rinoceronte");
        PalaDich.text += ", rinoceronte";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "rinoceronte")
        {

            ActPuntaje1("rinoceronte", 1);
        }
        else
        {
            ActPuntaje1("rinoceronte", 0);
        }
    }
    void SapoCalled()
    {
        print("Dijiste sapo");
        PalaDich.text += ", sapo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "sapo")
        {

            ActPuntaje1("sapo", 1);
        }
        else
        {
            ActPuntaje1("sapo", 0);
        }
    }
    void SerpienteCalled()
    {
        print("Dijiste serpiente");
        PalaDich.text += ", serpiente";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "serpiente")
        {

            ActPuntaje1("serpiente", 1);
        }
        else
        {
            ActPuntaje1("serpiente", 0);
        }
    }
    void TerneroCalled()
    {
        print("Dijiste ternero");
        PalaDich.text += ", ternero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ternero")
        {

            ActPuntaje1("ternero", 1);
        }
        else
        {
            ActPuntaje1("ternero", 0);
        }
    }
    void TiburonCalled()
    {
        print("Dijiste tiburon");
        PalaDich.text += ", tiburon";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "tiburon")
        {

            ActPuntaje1("tiburon", 1);
        }
        else
        {
            ActPuntaje1("tiburon", 0);
        }
    }
    void TigreCalled()
    {
        print("Dijiste tigre");
        PalaDich.text += ", tigre";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "tigre")
        {

            ActPuntaje1("tigre", 1);
        }
        else
        {
            ActPuntaje1("tigre", 0);
        }
    }
    void ToroCalled()
    {
        print("Dijiste toro");
        PalaDich.text += ", toro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "toro")
        {

            ActPuntaje1("toro", 1);
        }
        else
        {
            ActPuntaje1("toro", 0);
        }
    }
    void TortugaCalled()
    {
        print("Dijiste tortuga");
        PalaDich.text += ", tortuga";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "tortuga")
        {

            ActPuntaje1("tortuga", 1);
        }
        else
        {
            ActPuntaje1("tortuga", 0);
        }
    }
    void TruchaCalled()
    {
        print("Dijiste trucha");
        PalaDich.text += ", trucha";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "trucha")
        {

            ActPuntaje1("trucha", 1);
        }
        else
        {
            ActPuntaje1("trucha", 0);
        }
    }
    void VacaCalled()
    {
        print("Dijiste vaca");
        PalaDich.text += ", vaca";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "vaca")
        {

            ActPuntaje1("vaca", 1);
        }
        else
        {
            ActPuntaje1("vaca", 0);
        }
    }
    void VenadoCalled()
    {
        print("Dijiste venado");
        PalaDich.text += ", venado";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "venado")
        {

            ActPuntaje1("venado", 1);
        }
        else
        {
            ActPuntaje1("venado", 0);
        }
    }
    void YeguaCalled()
    {
        print("Dijiste yegua");
        PalaDich.text += ", yegua";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "yegua")
        {

            ActPuntaje1("yegua", 1);
        }
        else
        {
            ActPuntaje1("yegua", 0);
        }
    }
    void ZancudoCalled()
    {
        print("Dijiste zancudo");
        PalaDich.text += ", zancudo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "zancudo")
        {

            ActPuntaje1("zancudo", 1);
        }
        else
        {
            ActPuntaje1("zancudo", 0);
        }
    }
    void ZebraCalled()
    {
        print("Dijiste zebra");
        PalaDich.text += ", zebra";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "zebra")
        {

            ActPuntaje1("zebra", 1);
        }
        else
        {
            ActPuntaje1("zebra", 0);
        }
    }
    void ZorroCalled()
    {
        print("Dijiste zorro");
        PalaDich.text += ", zorro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "zorro")
        {

            ActPuntaje1("zorro", 1);
        }
        else
        {
            ActPuntaje1("zorro", 0);
        }
    }
    //frutas
    private void AguacateCalled()
    {
        print("Dijiste aguacate");
        PalaDich.text += ", aguacate";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "aguacate")
        {

            ActPuntaje1("aguacate", 1);
        }
        else
        {
            ActPuntaje1("aguacate", 0);
        }
    }
    private void AlbaricoqueCalled()
    {
        print("Dijiste albaricoque");
        PalaDich.text += ", albaricoque";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "albaricoque")
        {

            ActPuntaje1("albaricoque", 1);
        }
        else
        {
            ActPuntaje1("albaricoque", 0);
        }
    }
    private void ArazaCalled()
    {
        print("Dijiste araza");
        PalaDich.text += ", araza";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "araza")
        {

            ActPuntaje1("araza", 1);
        }
        else
        {
            ActPuntaje1("araza", 0);
        }
    }
    private void AuyamaCalled()
    {
        print("Dijiste auyama");
        PalaDich.text += ", auyama";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "auyama")
        {

            ActPuntaje1("auyama", 1);
        }
        else
        {
            ActPuntaje1("auyama", 0);
        }
    }
    private void BadeaCalled()
    {
        print("Dijiste badea");
        PalaDich.text += ", badea";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "badea")
        {

            ActPuntaje1("badea", 1);
        }
        else
        {
            ActPuntaje1("badea", 0);
        }
    }
    private void BananoCalled()
    {
        print("Dijiste banano");
        PalaDich.text += ", banano";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "banano")
        {

            ActPuntaje1("banano", 1);
        }
        else
        {
            ActPuntaje1("banano", 0);
        }
    }
    private void BrevaCalled()
    {
        print("Dijiste breva");
        PalaDich.text += ", breva";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "breva")
        {

            ActPuntaje1("breva", 1);
        }
        else
        {
            ActPuntaje1("breva", 0);
        }
    }
    private void CaramboloCalled()
    {
        print("Dijiste carambolo");
        PalaDich.text += ", carambolo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "carambolo")
        {

            ActPuntaje1("carambolo", 1);
        }
        else
        {
            ActPuntaje1("carambolo", 0);
        }
    }
    private void CerezaCalled()
    {
        print("Dijiste cereza");
        PalaDich.text += ", cereza";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cereza")
        {

            ActPuntaje1("cereza", 1);
        }
        else
        {
            ActPuntaje1("cereza", 0);
        }
    }
    private void ChirimoyaCalled()
    {
        print("Dijiste chirimoya");
        PalaDich.text += ", chirimoya";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "chirimoya")
        {

            ActPuntaje1("chirimoya", 1);
        }
        else
        {
            ActPuntaje1("chirimoya", 0);
        }
    }
    private void ChontaduroCalled()
    {
        print("Dijiste chontaduro");
        PalaDich.text += ", chontaduro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "chontaduro")
        {

            ActPuntaje1("chontaduro", 1);
        }
        else
        {
            ActPuntaje1("chontaduro", 0);
        }
    }
    private void CiruelaCalled()
    {
        print("Dijiste ciruela");
        PalaDich.text += ", ciruela";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ciruela")
        {

            ActPuntaje1("ciruela", 1);
        }
        else
        {
            ActPuntaje1("ciruela", 0);
        }
    }
    private void CocoCalled()
    {
        print("Dijiste coco");
        PalaDich.text += ", coco";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "coco")
        {

            ActPuntaje1("coco", 1);
        }
        else
        {
            ActPuntaje1("coco", 0);
        }
    }
    private void DuraznoCalled()
    {
        print("Dijiste durazno");
        PalaDich.text += ", durazno";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "durazno")
        {

            ActPuntaje1("durazno", 1);
        }
        else
        {
            ActPuntaje1("durazno", 0);
        }
    }
    private void FeijoaCalled()
    {
        print("Dijiste feijoa");
        PalaDich.text += ", feijoa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "feijoa")
        {

            ActPuntaje1("feijoa", 1);
        }
        else
        {
            ActPuntaje1("feijoa", 0);
        }
    }
    private void FrambuesaCalled()
    {
        print("Dijiste frambuesa");
        PalaDich.text += ", frambuesa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "frambuesa")
        {

            ActPuntaje1("frambuesa", 1);
        }
        else
        {
            ActPuntaje1("frambuesa", 0);
        }
    }
    private void FresaCalled()
    {
        print("Dijiste fresa");
        PalaDich.text += ", fresa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "fresa")
        {

            ActPuntaje1("fresa", 1);
        }
        else
        {
            ActPuntaje1("fresa", 0);
        }
    }
    private void GranadaCalled()
    {
        print("Dijiste granada");
        PalaDich.text += ", granada";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "granada")
        {

            ActPuntaje1("granada", 1);
        }
        else
        {
            ActPuntaje1("granada", 0);
        }
    }
    private void GranadillaCalled()
    {
        print("Dijiste granadilla");
        PalaDich.text += ", granadilla";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "granadilla")
        {

            ActPuntaje1("granadilla", 1);
        }
        else
        {
            ActPuntaje1("granadilla", 0);
        }
    }
    private void GuabaCalled()
    {
        print("Dijiste guaba");
        PalaDich.text += ", guaba";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "guaba")
        {

            ActPuntaje1("guaba", 1);
        }
        else
        {
            ActPuntaje1("guaba", 0);
        }
    }
    private void GuanabanaCalled()
    {
        print("Dijiste Guanabana");
        PalaDich.text += ", guanabana";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "guanabana")
        {

            ActPuntaje1("guanabana", 1);
        }
        else
        {
            ActPuntaje1("guanabana", 0);
        }
    }

    private void GuayabaCalled()
    {
        print("Dijiste Guayaba");
        PalaDich.text += ", guayaba";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "guayaba")
        {

            ActPuntaje1("guayaba", 1);
        }
        else
        {
            ActPuntaje1("guayaba", 0);
        }
    }
    private void KiwiCalled()
    {
        print("Dijiste kiwi");
        PalaDich.text += ", kiwi";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "kiwi")
        {

            ActPuntaje1("kiwi", 1);
        }
        else
        {
            ActPuntaje1("kiwi", 0);
        }
    }
    private void LimonCalled()
    {
        print("Dijiste limon");
        PalaDich.text += ", limon";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "limon")
        {

            ActPuntaje1("limon", 1);
        }
        else
        {
            ActPuntaje1("limon", 0);
        }
    }
    private void LuloCalled()
    {
        print("Dijiste lulo");
        PalaDich.text += ", lulo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "lulo")
        {

            ActPuntaje1("lulo", 1);
        }
        else
        {
            ActPuntaje1("lulo", 0);
        }
    }
    private void MadronoCalled()
    {
        print("Dijiste madrono");
        PalaDich.text += ", madrono";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "madrono")
        {

            ActPuntaje1("madrono", 1);
        }
        else
        {
            ActPuntaje1("madrono", 0);
        }
    }
    private void MamoncilloCalled()
    {
        print("Dijiste mamoncillo");
        PalaDich.text += ", mamoncillo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mamoncillo")
        {

            ActPuntaje1("mamoncillo", 1);
        }
        else
        {
            ActPuntaje1("mamoncillo", 0);
        }
    }
    private void MandarinaCalled()
    {
        print("Dijiste mandarina");
        PalaDich.text += ", mandarina";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mandarina")
        {

            ActPuntaje1("mandarina", 1);
        }
        else
        {
            ActPuntaje1("mandarina", 0);
        }
    }

    private void MangoCalled()
    {
        print("Dijiste manGo");
        PalaDich.text += ", mango";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mango")
        {

            ActPuntaje1("mango", 1);
        }
        else
        {
            ActPuntaje1("mango", 0);
        }
    }

    private void ManzanaCalled()
    {
        print("Dijiste manzana");
        PalaDich.text += ", manzana";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "manzana")
        {

            ActPuntaje1("manzana", 1);
        }
        else
        {
            ActPuntaje1("manzana", 0);
        }
    }
    private void MaracuyaCalled()
    {
        print("Dijiste maracuya");
        PalaDich.text += ", maracuya";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "maracuya")
        {

            ActPuntaje1("maracuya", 1);
        }
        else
        {
            ActPuntaje1("maracuya", 0);
        }
    }
    private void MelocotonCalled()
    {
        print("Dijiste melocoton");
        PalaDich.text += ", melocoton";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "melocoton")
        {

            ActPuntaje1("melocoton", 1);
        }
        else
        {
            ActPuntaje1("melocoton", 0);
        }
    }
    private void MelonCalled()
    {
        print("Dijiste melon");
        PalaDich.text += ", melon";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "melon")
        {

            ActPuntaje1("melon", 1);
        }
        else
        {
            ActPuntaje1("melon", 0);
        }
    }
    private void MoraCalled()
    {
        print("Dijiste Mora");
        PalaDich.text += ", mora";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mora")
        {

            ActPuntaje1("mora", 1);
        }
        else
        {
            ActPuntaje1("mora", 0);
        }
    }

    private void NaranjaCalled()
    {
        print("Dijiste Naranja");
        PalaDich.text += ", naranja";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "naranja")
        {

            ActPuntaje1("naranja", 1);
        }
        else
        {
            ActPuntaje1("naranja", 0);
        }
    }
    private void NisperoCalled()
    {
        print("Dijiste nispero");
        PalaDich.text += ", nispero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "nispero")
        {

            ActPuntaje1("nispero", 1);
        }
        else
        {
            ActPuntaje1("nispero", 0);
        }
    }
    private void PapayaCalled()
    {
        print("Dijiste papaya");
        PalaDich.text += ", papaya";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "papaya")
        {

            ActPuntaje1("papaya", 1);
        }
        else
        {
            ActPuntaje1("papaya", 0);
        }
    }
    private void PepinoCalled()
    {
        print("Dijiste pepino");
        PalaDich.text += ", pepino";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pepino")
        {

            ActPuntaje1("pepino", 1);
        }
        else
        {
            ActPuntaje1("pepino", 0);
        }
    }
    private void PeraCalled()
    {
        print("Dijiste pera");
        PalaDich.text += ", pera";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pera")
        {

            ActPuntaje1("pera", 1);
        }
        else
        {
            ActPuntaje1("pera", 0);
        }
    }
    private void PiñaCalled()
    {
        print("Dijiste piña");
        PalaDich.text += ", piña";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "piña")
        {

            ActPuntaje1("piña", 1);
        }
        else
        {
            ActPuntaje1("piña", 0);
        }
    }
    private void PitayaCalled()
    {
        print("Dijiste pitaya");
        PalaDich.text += ", pitaya";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pitaya")
        {

            ActPuntaje1("pitaya", 1);
        }
        else
        {
            ActPuntaje1("pitaya", 0);
        }
    }
    private void PomarrosaCalled()
    {
        print("Dijiste pomarrosa");
        PalaDich.text += ", pomarrosa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pomarrosa")
        {

            ActPuntaje1("pomarrosa", 1);
        }
        else
        {
            ActPuntaje1("zorro", 0);
        }
    }
    private void PomeloCalled()
    {
        print("Dijiste pomelo");
        PalaDich.text += ", pomelo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pomelo")
        {

            ActPuntaje1("pomelo", 1);
        }
        else
        {
            ActPuntaje1("pomelo", 0);
        }
    }
    private void SandiaCalled()
    {
        print("Dijiste sandia");
        PalaDich.text += ", sandia";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "sandia")
        {

            ActPuntaje1("sandia", 1);
        }
        else
        {
            ActPuntaje1("sandia", 0);
        }
    }
    private void Tomate()
    {
        print("Dijiste tomate");
        PalaDich.text += ", tomate";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "tomate")
        {

            ActPuntaje1("tomate", 1);
        }
        else
        {
            ActPuntaje1("tomate", 0);
        }
    }
    private void TomatedearbolCalled()
    {
        print("Dijiste tomatedearbol");
        PalaDich.text += ", tomatedearbol";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "tomatedearbol")
        {

            ActPuntaje1("tomatedearbol", 1);
        }
        else
        {
            ActPuntaje1("tomatedearbol", 0);
        }
    }
    private void UchuvaCalled()
    {
        print("Dijiste uchuva");
        PalaDich.text += ", uchuva";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "uchuva")
        {

            ActPuntaje1("uchuva", 1);
        }
        else
        {
            ActPuntaje1("uchuva", 0);
        }
    }
    private void VictoriaCalled()
    {
        print("Dijiste victoria");
        PalaDich.text += ", victoria";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "victoria")
        {

            ActPuntaje1("victoria", 1);
        }
        else
        {
            ActPuntaje1("victoria", 0);
        }
    }
    private void ZapoteCalled()
    {
        print("Dijiste zapote");
        PalaDich.text += ", zapote";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "zapote")
        {

            ActPuntaje1("zapote", 1);
        }
        else
        {
            ActPuntaje1("zapote", 0);
        }
    }
    //profesiones
    private void AbogadoCalled()
    {
        print("Dijiste abogado");
        PalaDich.text += ", abogado";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "abogado")
        {

            ActPuntaje1("abogado", 1);
        }
        else
        {
            ActPuntaje1("abogado", 0);
        }
    }
    private void AgricultorCalled()
    {
        print("Dijiste agricultor");
        PalaDich.text += ", agricultor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "agricultor")
        {

            ActPuntaje1("agricultor", 1);
        }
        else
        {
            ActPuntaje1("agricultor", 0);
        }
    }
    private void CampesinoCalled()
    {
        print("Dijiste campesino");
        PalaDich.text += ", campesino";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "agricultor")
        {

            ActPuntaje1("agricultor", 1);
        }
        else
        {
            ActPuntaje1("agricultor", 0);
        }
    }
    private void SembradorCalled()
    {
        print("Dijiste sembrador");
        PalaDich.text += ", sembrador";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "agricultor")
        {

            ActPuntaje1("agricultor", 1);
        }
        else
        {
            ActPuntaje1("agricultor", 0);
        }
    }
    private void AlbanilCalled()
    {
        print("Dijiste albañil");
        PalaDich.text += ", albañil";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "albañil")
        {

            ActPuntaje1("albañil", 1);
        }
        else
        {
            ActPuntaje1("albañil", 0);
        }
    }
    private void AmadecasaCalled()
    {
        print("Dijiste amadecasa");
        PalaDich.text += ", amadecasa";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "amadecasa")
        {

            ActPuntaje1("amadecasa", 1);
        }
        else
        {
            ActPuntaje1("amadecasa", 0);
        }
    }
    private void ArquitectoCalled()
    {
        print("Dijiste arquitecto");
        PalaDich.text += ", arquitecto";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "arquitecto")
        {

            ActPuntaje1("arquitecto", 1);
        }
        else
        {
            ActPuntaje1("arquitecto", 0);
        }
    }
    private void ArtesanoCalled()
    {
        print("Dijiste artesano");
        PalaDich.text += ", artesano";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "artesano")
        {

            ActPuntaje1("artesano", 1);
        }
        else
        {
            ActPuntaje1("artesano", 0);
        }
    }
    private void AseadorCalled()
    {
        print("Dijiste aseador");
        PalaDich.text += ", aseador";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "aseador")
        {

            ActPuntaje1("aseador", 1);
        }
        else
        {
            ActPuntaje1("aseador", 0);
        }
    }
    private void AzafataCalled()
    {
        print("Dijiste azafata");
        PalaDich.text += ", azafata";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "azafata")
        {

            ActPuntaje1("azafata", 1);
        }
        else
        {
            ActPuntaje1("azafata", 0);
        }
    }
    private void BarrenderoCalled()
    {
        print("Dijiste barrendero");
        PalaDich.text += ", barrendero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "barrendero")
        {

            ActPuntaje1("barrendero", 1);
        }
        else
        {
            ActPuntaje1("barrendero", 0);
        }
    }
    private void BomberoCalled()
    {
        print("Dijiste bombero");
        PalaDich.text += ", bombero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "bombero")
        {

            ActPuntaje1("bombero", 1);
        }
        else
        {
            ActPuntaje1("bombero", 0);
        }
    }
    private void CamioneroCalled()
    {
        print("Dijiste camionero");
        PalaDich.text += ", camionero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "camionero")
        {

            ActPuntaje1("camionero", 1);
        }
        else
        {
            ActPuntaje1("camionero", 0);
        }
    }
    private void CantanteCalled()
    {
        print("Dijiste cantante");
        PalaDich.text += ", cantante";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cantante")
        {

            ActPuntaje1("cantante", 1);
        }
        else
        {
            ActPuntaje1("cantante", 0);
        }
    }
    private void CarniceroCalled()
    {
        print("Dijiste carnicero");
        PalaDich.text += ", carnicero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "carnicero")
        {

            ActPuntaje1("carnicero", 1);
        }
        else
        {
            ActPuntaje1("carnicero", 0);
        }
    }
    private void CarpinteroCalled()
    {
        print("Dijiste carpintero");
        PalaDich.text += ", carpintero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "carpintero")
        {

            ActPuntaje1("carpintero", 1);
        }
        else
        {
            ActPuntaje1("carpintero", 0);
        }
    }
    private void CerrajeroCalled()
    {
        print("Dijiste cerrajero");
        PalaDich.text += ", cerrajero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cerrajero")
        {

            ActPuntaje1("cerrajero", 1);
        }
        else
        {
            ActPuntaje1("cerrajero", 0);
        }
    }
    private void ChefCalled()
    {
        print("Dijiste chef");
        PalaDich.text += ", chef";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "chef")
        {

            ActPuntaje1("chef", 1);
        }
        else
        {
            ActPuntaje1("chef", 0);
        }
    }
    private void CirujanoCalled()
    {
        print("Dijiste cirujano");
        PalaDich.text += ", cirujano";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "cirujano")
        {

            ActPuntaje1("cirujano", 1);
        }
        else
        {
            ActPuntaje1("cirujano", 0);
        }
    }
    private void ConductorCalled()
    {
        print("Dijiste conductor");
        PalaDich.text += ", coductor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "conductor")
        {

            ActPuntaje1("conductor", 1);
        }
        else
        {
            ActPuntaje1("conductor", 0);
        }
    }
    private void ChoferCalled()
    {
        print("Dijiste chofer");
        PalaDich.text += ", chofer";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "conductor")
        {

            ActPuntaje1("conductor", 1);
        }
        else
        {
            ActPuntaje1("conductor", 0);
        }
    }
    private void TaxistaCalled()
    {
        print("Dijiste taxista");
        PalaDich.text += ", taxista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "conductor")
        {

            ActPuntaje1("conductor", 1);
        }
        else
        {
            ActPuntaje1("conductor", 0);
        }
    }
    private void ConstructorCalled()
    {
        print("Dijiste constructor");
        PalaDich.text += ", constructor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "constructor")
        {

            ActPuntaje1("constructor", 1);
        }
        else
        {
            ActPuntaje1("constructor", 0);
        }
    }
    private void ContadorCalled()
    {
        print("Dijiste contador");
        PalaDich.text += ", contador";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "contador")
        {

            ActPuntaje1("contador", 1);
        }
        else
        {
            ActPuntaje1("contador", 0);
        }
    }
    private void DoctorCalled()
    {
        print("Dijiste doctor");
        PalaDich.text += ", doctor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "médico")
        {

            ActPuntaje1("médico", 1);
        }
        else
        {
            ActPuntaje1("médico", 0);
        }
    }
    private void DoctoraCalled()
    {
        print("Dijiste doctora");
        PalaDich.text += ", doctora";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "médica")
        {

            ActPuntaje1("médica", 1);
        }
        else
        {
            ActPuntaje1("médica", 0);
        }
    }
    private void ElectricistaCalled()
    {
        print("Dijiste electricista");
        PalaDich.text += ", electricista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "electricista")
        {

            ActPuntaje1("electricista", 1);
        }
        else
        {
            ActPuntaje1("electricista", 0);
        }
    }
    private void EnfermeraCalled()
    {
        print("Dijiste Enfermera");
        PalaDich.text += ", enfermera";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "enfermera")
        {

            ActPuntaje1("enfermera", 1);
        }
        else
        {
            ActPuntaje1("enfermera", 0);
        }
    }
    private void EscritorCalled()
    {
        print("Dijiste escritor");
        PalaDich.text += ", escritor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "escritor")
        {

            ActPuntaje1("escritor", 1);
        }
        else
        {
            ActPuntaje1("escritor", 0);
        }
    }
    private void EscultorCalled()
    {
        print("Dijiste escultor");
        PalaDich.text += ", escultor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "escultor")
        {

            ActPuntaje1("escultor", 1);
        }
        else
        {
            ActPuntaje1("escultor", 0);
        }
    }
    private void FarmaceutaCalled()
    {
        print("Dijiste farmaceuta");
        PalaDich.text += ", farmaceuta";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "farmaceuta")
        {

            ActPuntaje1("farmaceuta", 1);
        }
        else
        {
            ActPuntaje1("farmaceuta", 0);
        }
    }
    private void FisioterapeutaCalled()
    {
        print("Dijiste fisioterapeuta");
        PalaDich.text += ", fisioterapeuta";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "fisioterapeuta")
        {

            ActPuntaje1("fisioterapeuta", 1);
        }
        else
        {
            ActPuntaje1("fisioterapeuta", 0);
        }
    }
    private void FutbolistaCalled()
    {
        print("Dijiste futbolista");
        PalaDich.text += ", futbolista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "futbolista")
        {

            ActPuntaje1("futbolista", 1);
        }
        else
        {
            ActPuntaje1("futbolista", 0);
        }
    }
    private void GimnastaCalled()
    {
        print("Dijiste gimnasta");
        PalaDich.text += ", gimnasta";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "gimnasta")
        {

            ActPuntaje1("gimnasta", 1);
        }
        else
        {
            ActPuntaje1("gimnasta", 0);
        }
    }
    private void IngenieroCalled()
    {
        print("Dijiste ingeniero");
        PalaDich.text += ", ingeniero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ingeniero")
        {

            ActPuntaje1("ingeniero", 1);
        }
        else
        {
            ActPuntaje1("ingeniero", 0);
        }
    }
    private void JardineroCalled()
    {
        print("Dijiste jardinero");
        PalaDich.text += ", jardinero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "jardinero")
        {

            ActPuntaje1("jardinero", 1);
        }
        else
        {
            ActPuntaje1("jardinero", 0);
        }
    }
    private void LavanderaCalled()
    {
        print("Dijiste lavandero");
        PalaDich.text += ", lavandero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "lavandero")
        {

            ActPuntaje1("lavandero", 1);
        }
        else
        {
            ActPuntaje1("lavandero", 0);
        }
    }
    private void LocutorCalled()
    {
        print("Dijiste locutor");
        PalaDich.text += ", locutor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "locutor")
        {

            ActPuntaje1("locutor", 1);
        }
        else
        {
            ActPuntaje1("locutor", 0);
        }
    }
    private void ManicuristaCalled()
    {
        print("Dijiste manicurista");
        PalaDich.text += ", manicurista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "manicurista")
        {

            ActPuntaje1("manicurista", 1);
        }
        else
        {
            ActPuntaje1("manicurista", 0);
        }
    }
    private void MarineroCalled()
    {
        print("Dijiste marinero");
        PalaDich.text += ", marinero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "marinero")
        {

            ActPuntaje1("marinero", 1);
        }
        else
        {
            ActPuntaje1("marinero", 0);
        }
    }
    private void MasajistaCalled()
    {
        print("Dijiste masajista");
        PalaDich.text += ", masajista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "masajista")
        {

            ActPuntaje1("masajista", 1);
        }
        else
        {
            ActPuntaje1("masajista", 0);
        }
    }
    private void MecanicoCalled()
    {
        print("Dijiste mecanico");
        PalaDich.text += ", mecanico";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mecanico")
        {

            ActPuntaje1("mecanico", 1);
        }
        else
        {
            ActPuntaje1("mecanico", 0);
        }
    }
    private void MedicaCalled()
    {
        print("Dijiste medica");
        PalaDich.text += ", medica";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "medica")
        {

            ActPuntaje1("medica", 1);
        }
        else
        {
            ActPuntaje1("medica", 0);
        }
    }
    private void MedicoCalled()
    {
        print("Dijiste medico");
        PalaDich.text += ", medico";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "medico")
        {

            ActPuntaje1("medico", 1);
        }
        else
        {
            ActPuntaje1("medico", 0);
        }
    }
    private void MensajeroCalled()
    {
        print("Dijiste mensajero");
        PalaDich.text += ", mensajero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mensajero")
        {

            ActPuntaje1("mensajero", 1);
        }
        else
        {
            ActPuntaje1("mensajero", 0);
        }
    }
    private void MeseroCalled()
    {
        print("Dijiste mesero");
        PalaDich.text += ", mesero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "mesero")
        {

            ActPuntaje1("mesero", 1);
        }
        else
        {
            ActPuntaje1("mesero", 0);
        }
    }
    private void NineraCalled()
    {
        print("Dijiste ninera");
        PalaDich.text += ", ninera";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ninera")
        {

            ActPuntaje1("ninera", 1);
        }
        else
        {
            ActPuntaje1("ninera", 0);
        }
    }
    private void ObreroCalled()
    {
        print("Dijiste obrero");
        PalaDich.text += ", obrero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "obrero")
        {

            ActPuntaje1("obrero", 1);
        }
        else
        {
            ActPuntaje1("obrero", 0);
        }
    }
    private void OdontologoCalled()
    {
        print("Dijiste odontologo");
        PalaDich.text += ", odontologo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "odontologo")
        {

            ActPuntaje1("odontologo", 1);
        }
        else
        {
            ActPuntaje1("odontologo", 0);
        }
    }
    private void DentistaCalled()
    {
        print("Dijiste dentista");
        PalaDich.text += ", dentista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "odontologo")
        {

            ActPuntaje1("odontologo", 1);
        }
        else
        {
            ActPuntaje1("odontologo", 0);
        }
    }
    private void OficialCalled()
    {
        print("Dijiste oficial");
        PalaDich.text += ", oficial";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "constructor")
        {

            ActPuntaje1("constructor", 1);
        }
        else
        {
            ActPuntaje1("constructor", 0);
        }
    }
    private void AyudanteCalled()
    {
        print("Dijiste ayudante");
        PalaDich.text += ", ayudante";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "constructor")
        {

            ActPuntaje1("constructor", 1);
        }
        else
        {
            ActPuntaje1("constructor", 0);
        }
    }
    private void MaestroCalled()
    {
        print("Dijiste maestro");
        PalaDich.text += ", maestro";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "constructor")
        {

            ActPuntaje1("constructor", 1);
        }
        else
        {
            ActPuntaje1("constructor", 0);
        }
    }
    private void MaestrodeobraCalled()
    {
        print("Dijiste maestro de obra");
        PalaDich.text += ", maestrodeobra";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "constructor")
        {

            ActPuntaje1("constructor", 1);
        }
        else
        {
            ActPuntaje1("constructor", 0);
        }
    }
    private void OftalmologoCalled()
    {
        print("Dijiste oftalmologo");
        PalaDich.text += ", oftalmologo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "oftalmologo")
        {

            ActPuntaje1("oftalmologo", 1);
        }
        else
        {
            ActPuntaje1("oftalmologo", 0);
        }
    }
    private void OrdenadorCalled()
    {
        print("Dijiste ordeñador");
        PalaDich.text += ", ordeñador";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "ordeñador")
        {

            ActPuntaje1("ordeñador", 1);
        }
        else
        {
            ActPuntaje1("ordeñador", 0);
        }
    }
    private void PanaderoCalled()
    {
        print("Dijiste panadero");
        PalaDich.text += ", panadero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "panadero")
        {

            ActPuntaje1("panadero", 1);
        }
        else
        {
            ActPuntaje1("panadero", 0);
        }
    }
    private void PeluqueroCalled()
    {
        print("Dijiste peluquero");
        PalaDich.text += ", peluquero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "peluquero")
        {

            ActPuntaje1("peluquero", 1);
        }
        else
        {
            ActPuntaje1("peluquero", 0);
        }
    }
    private void PeriodistaCalled()
    {
        print("Dijiste periodista");
        PalaDich.text += ", periodista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "periodista")
        {

            ActPuntaje1("periodista", 1);
        }
        else
        {
            ActPuntaje1("periodista", 0);
        }
    }
    private void PilotoCalled()
    {
        print("Dijiste piloto");
        PalaDich.text += ", piloto";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "piloto")
        {

            ActPuntaje1("piloto", 1);
        }
        else
        {
            ActPuntaje1("piloto", 0);
        }
    }
    private void PintorCalled()
    {
        print("Dijiste pintor");
        PalaDich.text += ", pintor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "pintor")
        {

            ActPuntaje1("pintor", 1);
        }
        else
        {
            ActPuntaje1("pintor", 0);
        }
    }
    private void PoliciaCalled()
    {
        print("Dijiste policia");
        PalaDich.text += ", policia";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "policia")
        {

            ActPuntaje1("policia", 1);
        }
        else
        {
            ActPuntaje1("policia", 0);
        }
    }
    private void PoliticoCalled()
    {
        print("Dijiste politico");
        PalaDich.text += ", politico";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "politico")
        {

            ActPuntaje1("politico", 1);
        }
        else
        {
            ActPuntaje1("politico", 0);
        }
    }
    private void ProfesorCalled()
    {
        print("Dijiste profesor");
        PalaDich.text += ", profesor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "profesor")
        {

            ActPuntaje1("profesor", 1);
        }
        else
        {
            ActPuntaje1("profesor", 0);
        }
    }
    private void PsicologoCalled()
    {
        print("Dijiste psicologo");
        PalaDich.text += ", psicologo";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "psicologo")
        {

            ActPuntaje1("psicologo", 1);
        }
        else
        {
            ActPuntaje1("psicologo", 0);
        }
    }
    private void SacerdoteCalled()
    {
        print("Dijiste sacerdote");
        PalaDich.text += ", sacerdote";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "padre")
        {

            ActPuntaje1("padre", 1);
        }
        else
        {
            ActPuntaje1("padre", 0);
        }
    }
    private void SastreCalled()
    {
        print("Dijiste sastre");
        PalaDich.text += ", sastre";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "sastre")
        {

            ActPuntaje1("sastre", 1);
        }
        else
        {
            ActPuntaje1("sastre", 0);
        }
    }
    private void SecretariaCalled()
    {
        print("Dijiste secretaria");
        PalaDich.text += ", secretaria";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "secretaria")
        {

            ActPuntaje1("secretaria", 1);
        }
        else
        {
            ActPuntaje1("secretaria", 0);
        }
    }
    private void SoldadoCalled()
    {
        print("Dijiste soldado");
        PalaDich.text += ", soldado";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "soldado")
        {

            ActPuntaje1("soldado", 1);
        }
        else
        {
            ActPuntaje1("soldado", 0);
        }
    }
    private void TenistaCalled()
    {
        print("Dijiste tenista");
        PalaDich.text += ", tenista";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "tenista")
        {

            ActPuntaje1("tenista", 1);
        }
        else
        {
            ActPuntaje1("tenista", 0);
        }
    }
    private void VendedorCalled()
    {
        print("Dijiste vendedor");
        PalaDich.text += ", vendedor";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "vendedor")
        {

            ActPuntaje1("vendedor", 1);
        }
        else
        {
            ActPuntaje1("vendedor", 0);
        }
    }
    private void VeterinarioCalled()
    {
        print("Dijiste veterinario");
        PalaDich.text += ", veterinario";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "veterinario")
        {

            ActPuntaje1("veterinario", 1);
        }
        else
        {
            ActPuntaje1("veterinario", 0);
        }
    }
    private void ZapateroCalled()
    {
        print("Dijiste zapatero");
        PalaDich.text += ", zapatero";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "zapatero")
        {

            ActPuntaje1("zapatero", 1);
        }
        else
        {
            ActPuntaje1("zapatero", 0);
        }
    }
}
/*Este código es el mismo que para el otro entrenamiento lo que cambia son las variables que se pasan del código actualizar.*/
