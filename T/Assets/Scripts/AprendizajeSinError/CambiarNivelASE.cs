﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambiarNivelASE : MonoBehaviour {
    public Image SelecEntre;
    public GameObject ToPa;
    public string VarEntrenamiento;
    public InputField NBlanco;
    public Dropdown Categoria;
    List<string> NamesCategorias = new List<string>() { "Animales", "Frutas", "Profesiones" };
    public string VarCategoria;
    // Use this for initialization
    void Start () {
        VarCategoria = "Animales";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void cargarNivel(string pNombreNivel)
    {
        VarEntrenamiento = PlayerPrefs.GetString("Juego");//22012020 esto sobra a estas alturas solo se invoca este método de este script en particular
        //int aux = int.Parse(NBlanco.text);
        try
        {
            if (int.Parse(NBlanco.text) < 10)
                NBlanco.text = "10";
            if (int.Parse(NBlanco.text) > 50)
                NBlanco.text = "50";
        }
        catch
        {
            NBlanco.text = "10";
        }
        PlayerPrefs.SetString("ASENBlanco", NBlanco.text);
        PlayerPrefs.SetString("ASECategoria", VarCategoria);
        SelecEntre.gameObject.SetActive(false);
        ToPa.SetActive(false);
        SceneManager.LoadScene(pNombreNivel);
    }

    public void DropDown_IndexCategoria(int index)
    {
        VarCategoria = NamesCategorias[index];
        //Debug.Log("Ya entré Categoria"+ NamesCategorias[index]);
    }
}
