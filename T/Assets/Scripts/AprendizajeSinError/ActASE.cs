﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SpeechLib;//texto a voz

public class ActASE : MonoBehaviour {
    public GameObject Boton;
    public GameObject Global;
    public SI_ASE objeto_ASE;
    GameController Variables;
    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    Text textoDistra;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    float[] tiempo1;
    float[] tiempo2;
    List<string> NamesFotosAnimales;
    List<string> NamesFotosFrutas;
    List<string> NamesFotosProfesiones;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    //List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pez", "pollo", "ratón", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    //List<string> NamesFotosAnimales = new List<string>() { "","águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "ratón", "sapo", "tigre", "toro", "tortuga", "vaca" };
    //List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz","marrano","perro"};
    //List<string> NamesFotosFrutas = new List<string>() { "", "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "lulo", "mandarina", "mango", "manzana", "maracuyá", "melón", "naranja", "papaya", "pera", "piña", "sandía" };
    //List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "lulo", "mandarina", "mango", "manzana", "maracuyá", "melón", "mora", "naranja", "papaya", "pera", "piña", "sandía", "uchuva", "uva", "zapote" };
    //List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    //List<string> NamesFotosProfesiones = new List<string>() { "", "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    public static string NamesFotos;
    public string NamesFotos1;
    public int[] index;
    public string imgpos = null;
    
    public static bool bandera = false;
    public static string ImgAct;
    public Text tiempoText;
    static int i = 0;
    //tiempos
    public static float[] G_tiempo;
    public static float[] auxtiempo;
    public float t;
    public float P_tiempo;
    public static float tiempo = 0f;
    public static float[] GE_tiempo;
    public static int e; //variable auxiliar para almacenar los tiempos de los errores



    public string NBlanco;
    public int Nblanco;
    public static int numerodeestimulos;
    public string categoria;
    public EvolPal NamesWords;


    private SpVoice voice;
    // Use this for initialization
    void Start()
    {
        //inicializar variables
        i = 0;
        bandera = false;
        NamesFotos = "";
        try
        {
            NamesFotosAnimales.Clear();
            NamesFotosFrutas.Clear();
            NamesFotosProfesiones.Clear();
        }
        catch
        {

        }
        tiempo = 0f;
        NBlanco = PlayerPrefs.GetString("ASENBlanco");
        Nblanco = int.Parse(NBlanco);
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Texto1.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;
        textoCategoria = Texto2.GetComponent<Text>();
        textoCategoria.text = "" + Variables.VarCategoria;
        textoBlanco = Texto3.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        categoria = PlayerPrefs.GetString("ASECategoria");
        if (categoria == "Animales")
            NamesFotosAnimales = NamesWords.RecOrdPal();
        else if (categoria == "Frutas")
            NamesFotosFrutas = NamesWords.RecOrdPal();
        else if (categoria == "Profesiones")
            NamesFotosProfesiones = NamesWords.RecOrdPal();

        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;

        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Debes decir el nombre de las imágenes que salgan, en esta ocasión, verás " + categoria + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);

        e = 0;
        //OrdenarSegunNivel();

        //Construir algoritmo genético de palabras
        /*
        switch (Variables.VarCategoria)
        {
            case "Animales":
                index = RandomNums(Nblanco, NamesFotosAnimales.Count);
                numerodeestimulos = Nblanco;

                foreach (int i in index)
                {
                    if (i != 0)
                    {
                        NamesFotos += NamesFotosAnimales[i] + "|";
                    }
                }

                NamesFotos1 = NamesFotos;
                break;
            case "Frutas":
                index = RandomNums(Nblanco, NamesFotosFrutas.Count);
                numerodeestimulos = Nblanco;

                foreach (int i in index)
                {
                    if (i != 0)
                    {
                        NamesFotos += NamesFotosFrutas[i] + "|";
                    }
                }
                break;
            case "Profesiones":
                index = RandomNums(Nblanco, NamesFotosProfesiones.Count);
                numerodeestimulos = Nblanco;

                foreach (int i in index)
                {
                    if (i != 0)
                    {
                        NamesFotos += NamesFotosProfesiones[i] + "|";
                    }
                }
                break;
        }*/

        //Termina algoritmo genético
        switch (categoria)
        {
            case "Animales":
                index = RandomNums(Nblanco, NamesFotosAnimales.Count);
                numerodeestimulos = Nblanco;
                /*for (int j=0;j<NamesFotosAnimales.Count;j++)
                {
                    NamesFotos += NamesFotosAnimales[j] + "|";
                }*/
                foreach (int i in index)
                {
                    if (i!=0)
                    {
                        NamesFotos += NamesFotosAnimales[i] + "|";
                    }                    
                }
                /*index = RandomNums(5, 5);
                numerodeestimulos = 5;
                for (int j = 0; j < 5; j++)
                {
                    NamesFotos += NamesFotosAnimales[j];
                }*/
                NamesFotos1 = NamesFotos;
                break;
            case "Frutas":
                index = RandomNums(Nblanco, NamesFotosFrutas.Count);
                numerodeestimulos = Nblanco;
                /*for (int j = 0; j < NamesFotosFrutas.Count; j++)
                {
                    NamesFotos += NamesFotosFrutas[j];
                }*/
                foreach (int i in index)
                {
                    if (i != 0)
                    {
                        NamesFotos += NamesFotosFrutas[i] + "|";
                    }
                }
                break;
            case "Profesiones":
                index = RandomNums(Nblanco, NamesFotosProfesiones.Count);
                numerodeestimulos = Nblanco;
                /*for (int j = 0; j < NamesFotosProfesiones.Count; j++)
                {
                    NamesFotos += NamesFotosProfesiones[j];
                }*/
                foreach (int i in index)
                {
                    if (i != 0)
                    {
                        NamesFotos += NamesFotosProfesiones[i] + "|";
                    }
                }
                break;
        }
        //index = RandomNums(3, 3);

        /* switch (Variables.VarCategoria)
         {
             case "Animales":
                    index = new int[NamesFotosAnimales.Count];
                    for (int i = 0; i< NamesFotosAnimales.Count;i++)
                        index[i] = i;
                 break;
             case "Frutas":
                    index = new int[NamesFotosFrutas.Count];
                    for (int i = 0; i < NamesFotosFrutas.Count; i++)
                        index[i] = i;
                    break;
             case "Profesiones":
                    index = new int[NamesFotosProfesiones.Count];
                    for (int i = 2; i < NamesFotosProfesiones.Count; i++)
                        index[i-2] = i;
                    break;
         }*/
        G_tiempo = new float[index.Length + 1];
        auxtiempo = new float[index.Length + 1];
        GE_tiempo = new float[index.Length + 1];
    }

// Update is called once per frame
public void Update () {
 tiempo = tiempo + Time.deltaTime;
 switch (categoria)           
 {
     case "Animales":
         if (i == 0)
         {
             G_tiempo[i] = tiempo;
             auxtiempo[i] = tiempo;
         }
         else
         {
             auxtiempo[i] = tiempo;
             G_tiempo[i] = tiempo - auxtiempo[i - 1];
         }
         if (i < numerodeestimulos)
         {
             if (i == 0)
             {
                 //ImgAct = NamesFotosAnimales[index[i]];
                 //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 ImgAct = NamesFotosAnimales[index[i]];
                 Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 imgpos = NamesFotosAnimales[index[i]];//esto e un error porque no se esta usando index en a la hora de poner la imagen. Sin embargo funciona por que index no se esta generando es solo un vector de 200 posiciones
                 //objeto_GES.DBWrite();
                 i++;
             }
             if (bandera)
             {
                 //ImgAct = NamesFotosAnimales[index[i]];
                 //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 ImgAct = NamesFotosAnimales[index[i]];
                 Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 imgpos = NamesFotosAnimales[index[i]];
                 bandera = false;
                 //objeto_GES.DBWrite();
                 i++;
             }
         }
         else if (i >= numerodeestimulos)
         {
             if (bandera)
             {
                        objeto_ASE.DBWrite();//primera vez que se invoca este objeto
                        SceneManager.LoadScene("Lobby");
             }
         }
         break;
     case "Frutas":
         if (i == 0)
         {
             G_tiempo[i] = tiempo;
             auxtiempo[i] = tiempo;
         }
         else
         {
             auxtiempo[i] = tiempo;
             G_tiempo[i] = tiempo - auxtiempo[i - 1];
         }
         //index = RandomNums(NamesFotosFrutas.Count, NamesFotosFrutas.Count);
         if (i < numerodeestimulos)
         {
             if (i == 0)
             {
                 //ImgAct = NamesFotosAnimales[index[i]];
                 //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 ImgAct = NamesFotosFrutas[index[i]];
                 Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[index[i]]);
                 imgpos = NamesFotosFrutas[index[i]];
                 //objeto_GES.DBWrite();
                 i++;
             }
             if (bandera)
             {
                 //ImgAct = NamesFotosAnimales[index[i]];
                 //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 ImgAct = NamesFotosFrutas[index[i]];
                 Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[index[i]]);
                 imgpos = NamesFotosFrutas[index[i]];
                 bandera = false;
                 //objeto_GES.DBWrite();
                 i++;
             }
         }
         else if (i >= numerodeestimulos)
         {
             if (bandera)
             {
                 objeto_ASE.DBWrite();//segunda vez que se invoca este objeto
                 SceneManager.LoadScene("Lobby");
             }
         }
         break;
     case "Profesiones":
         if (i == 0)
         {
             G_tiempo[i] = tiempo;
             auxtiempo[i] = tiempo;
         }
         else
         {
             auxtiempo[i] = tiempo;
             G_tiempo[i] = tiempo - auxtiempo[i - 1];
         }
         //index = RandomNums(NamesFotosProfesiones.Count, NamesFotosProfesiones.Count);
         if (i < numerodeestimulos)
         {
             if (i == 0)
             {
                 //ImgAct = NamesFotosAnimales[index[i]];
                 //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 ImgAct = NamesFotosProfesiones[index[i]];
                 Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[index[i]]);
                 imgpos = NamesFotosProfesiones[index[i]];
                 //objeto_GES.DBWrite();
                 i++;
             }
             if (bandera)
             {
                 //ImgAct = NamesFotosAnimales[index[i]];
                 //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                 ImgAct = NamesFotosProfesiones[index[i]];
                 Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[index[i]]);
                 imgpos = NamesFotosProfesiones[index[i]];
                 bandera = false;
                 //objeto_GES.DBWrite();
                 i++;
             }
         }
         else if (i >= numerodeestimulos)
         {
             if (bandera)
             {
                        objeto_ASE.DBWrite();//terera vez que se invoca este objeto
                        SceneManager.LoadScene("Lobby");
             }
         }
         break;
 }
}
int[] RandomNums(int cantidad, int maxval)
{
        int aux;
 int[] index = new int[cantidad];
 for (int i = 0; i < cantidad; i++)
 {
     index[i] = Random.Range(1, maxval);
 }
 for (int i=0;i<cantidad-1;i++)
 {
      if (index[i]==index[i+1])
      {
            aux = index[i];
            index[i] = index[cantidad-1];
            index[cantidad-1] = index[i];
      }
 }
 return index;
}
    public static void TiempoErrores()
    {
        GE_tiempo[e] = tiempo - auxtiempo[i - 1];
        e++;
    }
public void CerebroBoton()
    {
        GameObject.Find("Imagen").GetComponent<PerASE>().ActPuntaje1(ImgAct, 1);
    }
}

/*en este código hay que programar que se pongan categorías aleatorias en pantalla, que se diga el nombre de la categoria con sintesis de voz
y finalmente el reconocimiento de voz y los puntajes. Este código si cambia radicalmente en comparación con el del otro entrenamiento
pues la lógica para este entrenamiento es diferente*/

/*tiempo -= Time.deltaTime;
tiempoText.text = "" + tiempo.ToString("f0");
if (tiempo < 1 && bandera)
{
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);                        
i++;
tiempo = 2.0f;
}
if (i == NamesFotosAnimales.Count)
{
bandera = false;
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/fresa");
}*/
