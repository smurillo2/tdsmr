﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comentarios{
    string t_Mensaje;
    string r_Mensaje;
    int v_Aleatorio;
    public Comentarios(string valor)
    {
        t_Mensaje = valor;
        r_Mensaje = "Mensaje por defecto";
    }
    public string respuesta(string valor)
    {
        t_Mensaje = valor;
        v_Aleatorio = Random.Range(1, 10);
        if (t_Mensaje == "p")
        {
            if (v_Aleatorio == 1)
                r_Mensaje = "!Muy bien!";
            if (v_Aleatorio == 2)
                r_Mensaje = "!Lo has hecho!";
            if (v_Aleatorio == 3)
                r_Mensaje = "!Lo has logrado!";
            if (v_Aleatorio == 4)
                r_Mensaje = "!Vas muy bien!";
            if (v_Aleatorio == 5)
                r_Mensaje = "!sigue adelante!, ";
            if (v_Aleatorio == 6)
                r_Mensaje = "!Así se hace!";
            if (v_Aleatorio == 7)
                r_Mensaje = "!Excelente!";
            if (v_Aleatorio == 8)
                r_Mensaje = "!bien!, !muy bien!";
            if (v_Aleatorio == 9)
                r_Mensaje = "!Es correcto!";
            if (v_Aleatorio == 10)
                r_Mensaje = "!Tu puedes!";
        }
        else if (t_Mensaje=="n")
        {
            if (v_Aleatorio == 1)
                r_Mensaje = "!Inténtalo de nuevo!";
            if (v_Aleatorio == 2)
                r_Mensaje = "!Concéntrate, tu puedes!";
            if (v_Aleatorio == 3)
                r_Mensaje = "!Concéntrate un poco más!";
            if (v_Aleatorio == 4)
                r_Mensaje = "!Debes prestar mas atención!";
            if (v_Aleatorio == 5)
                r_Mensaje = "!Puedes hacerlo mejor, concéntrate!, ";
            if (v_Aleatorio == 6)
                r_Mensaje = "!Debes poner más atención!";
            if (v_Aleatorio == 7)
                r_Mensaje = "!Es preciso que te concentres!";
            if (v_Aleatorio == 8)
                r_Mensaje = "!Por favor pon más atención!";
            if (v_Aleatorio == 9)
                r_Mensaje = "!Puedes mejorar, concéntrate!";
            if (v_Aleatorio == 10)
                r_Mensaje = "!La clave es la atención!";
        }
        return (r_Mensaje);
    }
}
