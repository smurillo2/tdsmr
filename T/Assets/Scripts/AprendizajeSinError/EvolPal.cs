﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;



public class EvolPal : MonoBehaviour
{
    List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "ratón", "sapo", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "mandarina", "mango", "manzana", "maracuyá", "melón", "naranja", "papaya", "pera", "sandía" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "padre", "sastre", "secretaria", "vendedor", "zapatero" };
    string categoria = "";
    string usuario = "";
    public string datos;
    public string[] sesiones;
    public string[] sesionesPart;
    public string pal;
    public string tie;
    public List<string> palabras;
    public List<string> tiempos;
    public List<float> Tiempos;
    public List<PalTim> palabrota;
    String palabritas;
    String Indipalabritas;
    String Countpalabritas;
    List<string> palab;
    List<string> Indipalab;
    List<string> NumPal;
    List<string> CountPal;
    public List<float>  TiempoF;
    string Fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    // Use this for initialization
    void Start()
    {


    }

    public List<string> RecOrdPal()
    {
        usuario = PlayerPrefs.GetInt("idUsuario").ToString();
        categoria = PlayerPrefs.GetString("ASECategoria");
        LeerData();
        sesiones = datos.Split('#');
        if (sesiones.Length > 1)
        {
            for (int i = 0; i < sesiones.Length - 1; i++)
            {
                sesionesPart = sesiones[i].Split('@');
                pal += sesionesPart[0];
                tie += sesionesPart[1];
            }
            palabras = pal.Split('|').ToList();
            tiempos = tie.Split('|').ToList();
            for (int i = 0; i < tiempos.Count - 1; i++)
            {
                Tiempos.Add(float.Parse(tiempos[i]));
            }

            palabras.Remove("");
        }
            //Tiempos.Remove("");//esta línea me genera desconfianza
        switch (categoria)
        {
            case "Animales":
                {
                    for (int i = 0; i < NamesFotosAnimales.Count; i++)
                    {
                        if (palabras.IndexOf(NamesFotosAnimales[i]) == -1)
                        {
                            palabras.Add(NamesFotosAnimales[i]);
                            if (sesiones.Length <= 1)
                            {
                                Tiempos.Add(1);
                            }
                            else
                            {
                                Tiempos.Add(Tiempos.Max());
                            }
                        }              
                    }
                    for (int i = 0; i < palabras.Count; i++)
                    {
                        if (NamesFotosFrutas.IndexOf(palabras[i]) != -1)
                        {
                            palabras.Remove(palabras[i]);
                            Tiempos.Remove(Tiempos[i]);
                            i--;
                        }
                        else if (NamesFotosProfesiones.IndexOf(palabras[i]) != -1)
                        {
                            palabras.Remove(palabras[i]);
                            Tiempos.Remove(Tiempos[i]);
                            i--;
                        }

                    }
                    break;
                }
            case "Frutas":
                {
                    for (int i = 0; i < NamesFotosFrutas.Count; i++)
                    {
                        if (palabras.IndexOf(NamesFotosFrutas[i]) == -1)
                        {
                            palabras.Add(NamesFotosFrutas[i]);
                            if (sesiones.Length <= 1)
                            {
                                Tiempos.Add(1);
                            }
                            else
                            {
                                Tiempos.Add(Tiempos.Max());
                            }
                        }
                    }
                    for (int i = 0; i < palabras.Count; i++)
                    {
                        if (NamesFotosAnimales.IndexOf(palabras[i]) != -1)
                        {
                            palabras.Remove(palabras[i]);
                            Tiempos.Remove(Tiempos[i]);
                            i--;
                        }
                        else if (NamesFotosProfesiones.IndexOf(palabras[i]) != -1)
                        {
                            palabras.Remove(palabras[i]);
                            Tiempos.Remove(Tiempos[i]);
                            i--;
                        }
                    }
                    break;
                }
            case "Profesiones":
                {
                    for (int i = 0; i < NamesFotosProfesiones.Count; i++)
                    {
                        if (palabras.IndexOf(NamesFotosProfesiones[i]) == -1)
                        {
                            palabras.Add(NamesFotosProfesiones[i]);
                            if (sesiones.Length <= 1)
                            {
                                Tiempos.Add(1);
                            }
                            else
                            {
                                Tiempos.Add(Tiempos.Max());
                            }
                        }
                    }
                    /*List<string> ensayo = new List<string>{ "Hola", "Hola", "Nunca" };
                    ensayo.Remove("Hola");//de esto sacamos que remove solo quita la primera aparicion de la palabra*/
                    for (int i = 0; i < palabras.Count; i++)
                    {
                        if (NamesFotosAnimales.IndexOf(palabras[i]) != -1)
                        {
                            palabras.Remove(palabras[i]);
                            Tiempos.Remove(Tiempos[i]);
                            i--;
                        }
                        else if (NamesFotosFrutas.IndexOf(palabras[i]) != -1)
                        {
                            palabras.Remove(palabras[i]);
                            Tiempos.Remove(Tiempos[i]);
                            i--;
                        }
                    }
                    /*
                    for (int i = 0; i < NamesFotosAnimales.Count; i++)
                    {
                        if (palabras.IndexOf(NamesFotosAnimales[i]) != -1)
                            palabras.Remove(NamesFotosAnimales[i]);
                    }
                    for (int i = 0; i < NamesFotosFrutas.Count; i++)
                    {
                        if (palabras.IndexOf(NamesFotosFrutas[i]) != -1)
                            palabras.Remove(NamesFotosFrutas[i]);
                    }
                    */
                    break;
                }
        }
        //Promediar tiempos por palabra. Esto agrupa repetidas, obtiene el indice para luego promediar
        foreach (var registro in 
                        palabras.Select((v, i) => new {  Valor = v, Indice = i}) // Obtener indice y valor
                        .GroupBy(x => x.Valor) // Agrupar por el valor
                        //.Where(x => x.ToList().Count() > 1) // En caso necesitas obtener cant. repetidas mayor a 1
                        .Select(x => new{ 
                                Valor = x.Key, // key de la agrupación (valor)
                                Cantidad = x.Count(), // Cantidad de duplicidad
                                //Indices = string.Join(", ", x.Select(i => i.Indice.ToString()).ToArray()) // Concatenar los indices
                                Indices = string.Join(", ", x.Select(i => i.Indice.ToString()).ToArray()) // Concatenar los indices
                        }))
        {
            palabritas = palabritas + "|" + registro.Valor;
            Indipalabritas = Indipalabritas + "|" + registro.Indices;
            Countpalabritas = Countpalabritas + "|" + registro.Cantidad;
                    //Console.WriteLine(string.Format("Valor: '{0}'\tCant. Repetidas: {1}\tIndices: {2}", registro.Valor, registro.Cantidad, registro.Indices));
        }

        palab = palabritas.Split('|').ToList();
        Indipalab = Indipalabritas.Split('|').ToList();
        CountPal = Countpalabritas.Split('|').ToList();
        palab.Remove("");
        Indipalab.Remove("");
        CountPal.Remove("");
        int aux;
        float faux;
        float acumulador;
        for (int i = 0; i < Indipalab.Count;i++)
        {
            acumulador = 0;
            NumPal = Indipalab[i].Split(',').ToList();
            foreach(string numero in NumPal)
            {
                aux = Int32.Parse(numero);
                acumulador = acumulador + Tiempos[aux];
            }
            faux = acumulador / NumPal.Count;
            TiempoF.Add(faux);
        }

        List<PalTim> par = new List<PalTim>();
        for (int i = 0; i < palab.Count - 1; i++)
        {
            par.Add(new PalTim(palab[i], TiempoF[i]));
        }
        List<string> esto = new List<string>() { };
        List<string> estoTiempo = new List<string>() { };
        List<PalTim> SortedPal = par.OrderByDescending(o => o.Tiempo).ToList();
        for (int i = 0; i < SortedPal.Count; i++)
        {
            if (i < SortedPal.Count / 2)
            {
                esto.Add(SortedPal[i].Palabras);
                estoTiempo.Add(SortedPal[i].Tiempo.ToString());
            }
            else if (UnityEngine.Random.Range(0, 100) >= 50)//agregar imagenes que han dejado de salir por que no se demoran en ser reconocidas por el usuario
            {
                esto.Add(SortedPal[i].Palabras);
                estoTiempo.Add(SortedPal[i].Tiempo.ToString());
            }
        }
        esto.Remove("");
        DBWritePal(esto,estoTiempo);
        return esto;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DBWritePal(List<string> esto,List<string> estoTiempo)
    {
        string pala = String.Join("|", esto);
        string palaTiempo = String.Join("|", estoTiempo);
        StartCoroutine(PalWrite(pala,palaTiempo));
    }


    public string LeerData()
    {
        StartCoroutine(LeerPal());
        return datos;
    }
    IEnumerator LeerPal()
    {
        WWW conexion = new WWW("http://localhost/CTA/LeerPal.php?uss=" + usuario + "&cat=" + categoria);
        while (!conexion.isDone)
        {
            Debug.Log("Esperando Descarga");
        }
        //yield return (conexion);//esto me estaba matando el sistema. Finalmente no entiendo muy bien que hace. Aparentemente verifica que todo haya descargado bien, pero no se mas.

        //Debug.Log(conexion.text);
        if (conexion.text == "401")
        {
            print("Usuario o contraseña incorrectos");
        }
        else
        {
            datos = conexion.text;
        }
        return (conexion);
    }
    IEnumerator PalWrite(string pala, string palaTiempo)
    {
        WWW conexion = new WWW("http://localhost/CTA/PalWrite.php?idu=" + usuario + "&cat=" + categoria + "&pal=" + pala + "&tim=" + palaTiempo + "&fec" + Fecha);
        return (conexion);

    }
}

public class PalTim
{
    public string Palabras;
    public float Tiempo;
    public PalTim(string Palabras, float Tiempo)
    {
        this.Palabras = Palabras;
        this.Tiempo = Tiempo;
    }
}