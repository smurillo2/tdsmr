﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamPan : MonoBehaviour {
    public Image SelecEntre;
    public Image SelecConfASE;
    public Image SelecConfNB;
    public Image SelecConfS;
    public Image SelecConfCDT;
    public Image SelecConf;
    public Image SelecConfC;
    public Dropdown Entrenamiento;
    public Dropdown Idioma;
    public string VarEntrenamiento;
    public string VarIdioma;
    List<string> NamesEntrenamiento = new List<string>() { "AprendizajeSinError", "NBack", "Go No go", "Stop Signal", "Cancelación", "Categorización"};
    List<string> NamesIdioma = new List<string>() { "EspañolM", "EspañolF", "InglésM", "PortuguésM", "InglésF", "PortuguésF" };
    // Use this for initialization
    public void Start () {
        VarEntrenamiento = PlayerPrefs.GetString("Juego");
        switch (VarEntrenamiento)
        {
            case "ASE":
            case "":
                SelecEntre.gameObject.SetActive(false);
                SelecConfASE.gameObject.SetActive(true);
                SelecConfNB.gameObject.SetActive(false);
                SelecConfS.gameObject.SetActive(false);
                SelecConfCDT.gameObject.SetActive(false);
                SelecConf.gameObject.SetActive(false);
                SelecConfC.gameObject.SetActive(false);
                break;
            case "NB":
                SelecEntre.gameObject.SetActive(false);
                SelecConfASE.gameObject.SetActive(false);
                SelecConfNB.gameObject.SetActive(true);
                SelecConfS.gameObject.SetActive(false);
                SelecConfCDT.gameObject.SetActive(false);
                SelecConf.gameObject.SetActive(false);
                SelecConfC.gameObject.SetActive(false);
                break;
            case "GNG":
                SelecEntre.gameObject.SetActive(false);
                SelecConfASE.gameObject.SetActive(false);
                SelecConfNB.gameObject.SetActive(false);
                SelecConfS.gameObject.SetActive(true);
                SelecConfCDT.gameObject.SetActive(false);
                SelecConf.gameObject.SetActive(false);
                SelecConfC.gameObject.SetActive(false);
                break;
            case "SS":
                SelecEntre.gameObject.SetActive(false);
                SelecConfASE.gameObject.SetActive(false);
                SelecConfNB.gameObject.SetActive(false);
                SelecConfS.gameObject.SetActive(false);
                SelecConfCDT.gameObject.SetActive(true);
                SelecConf.gameObject.SetActive(false);
                SelecConfC.gameObject.SetActive(false);
                break;
            case "C":
                SelecEntre.gameObject.SetActive(false);
                SelecConfASE.gameObject.SetActive(false);
                SelecConfNB.gameObject.SetActive(false);
                SelecConfS.gameObject.SetActive(false);
                SelecConfCDT.gameObject.SetActive(false);
                SelecConf.gameObject.SetActive(true);
                SelecConfC.gameObject.SetActive(false);
                break;
            case "CA":
                SelecEntre.gameObject.SetActive(false);
                SelecConfASE.gameObject.SetActive(false);
                SelecConfNB.gameObject.SetActive(false);
                SelecConfS.gameObject.SetActive(false);
                SelecConfCDT.gameObject.SetActive(false);
                SelecConf.gameObject.SetActive(false);
                SelecConfC.gameObject.SetActive(true);
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

      public void PanelsContr()
      {
      switch (VarEntrenamiento)
              {
                  case "AprendizajeSinError":
                  case "":
                      SelecEntre.gameObject.SetActive(false);
                      SelecConfASE.gameObject.SetActive(true);
                      SelecConfNB.gameObject.SetActive(false);
                      SelecConfS.gameObject.SetActive(false);
                      SelecConfCDT.gameObject.SetActive(false);
                      SelecConf.gameObject.SetActive(false);
                      SelecConfC.gameObject.SetActive(false);
                      break;
                  case "NBack":
                      SelecEntre.gameObject.SetActive(false);
                      SelecConfASE.gameObject.SetActive(false);
                      SelecConfNB.gameObject.SetActive(true);
                      SelecConfS.gameObject.SetActive(false);
                      SelecConfCDT.gameObject.SetActive(false);
                      SelecConf.gameObject.SetActive(false);
                      SelecConfC.gameObject.SetActive(false);
                      break;
                  case "Go No go":
                      SelecEntre.gameObject.SetActive(false);
                      SelecConfASE.gameObject.SetActive(false);
                      SelecConfNB.gameObject.SetActive(false);
                      SelecConfS.gameObject.SetActive(true);
                      SelecConfCDT.gameObject.SetActive(false);
                      SelecConf.gameObject.SetActive(false);
                      SelecConfC.gameObject.SetActive(false);
                      break;
                  case "Stop Signal":
                      SelecEntre.gameObject.SetActive(false);
                      SelecConfASE.gameObject.SetActive(false);
                      SelecConfNB.gameObject.SetActive(false);
                      SelecConfS.gameObject.SetActive(false);
                      SelecConfCDT.gameObject.SetActive(true);
                      SelecConf.gameObject.SetActive(false);
                      SelecConfC.gameObject.SetActive(false);
                      break;
                case "Cancelación":
                      SelecEntre.gameObject.SetActive(false);
                      SelecConfASE.gameObject.SetActive(false);
                      SelecConfNB.gameObject.SetActive(false);
                      SelecConfS.gameObject.SetActive(false);
                      SelecConfCDT.gameObject.SetActive(false);
                      SelecConf.gameObject.SetActive(true);
                      SelecConfC.gameObject.SetActive(false);
                      break;
                  case "Categorización":
                      SelecEntre.gameObject.SetActive(false);
                      SelecConfASE.gameObject.SetActive(false);
                      SelecConfNB.gameObject.SetActive(false);
                      SelecConfS.gameObject.SetActive(false);
                      SelecConfCDT.gameObject.SetActive(false);
                      SelecConf.gameObject.SetActive(false);
                      SelecConfC.gameObject.SetActive(true);
                      break;
              }
      }
    public void DropDown_IndexEntrenamiento(int index)
    {
        VarEntrenamiento = NamesEntrenamiento[index];
        //Debug.Log("Ya entré entrenamiento");
    }
    public void DropDown_Idioma(int index)
    {
        VarIdioma = NamesIdioma[index];
        //Debug.Log("Ya entré Blanco");
    }
}
