﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SpeechLib;//texto a voz

public class ActCa : MonoBehaviour {
    public GameObject Boton;
    public GameObject Global;
    GameController Variables;
    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    Text textoDistra;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "aguila", "avestruz", "ballena", "burro", "caballo", "caiman", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "raton", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    List<string> Todas = new List<string>();
    List<string> TodasM = new List<string>();
    int[] index = new int[200];
    public string imgpos = null;

    public static bool bandera = false;
    public Text tiempoText;
    static int i = 0;
    public static string bandera1;
    public static string Ne;

    //Base de datos tiempos datos para monitoreo y almacenamiento en base de datos
    public static float[] G_tiempo;
    public static float[] auxtiempo;
    public float t;
    public float P_tiempo;
    public static string NamesFotos;
    public static float tiempo = 0f;
    public SI_Ca objeto_Ca;
    public static string ImgAct;

    public static float[] GE_tiempo;
    public static int e;
    private SpVoice voice;

    // Use this for initialization
    void Start()
    {
        //Base de datos inicialización de variables
        NamesFotos = "";
        tiempo = 0f;
        i = 0;

        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Texto1.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;

        Ne = PlayerPrefs.GetString("CBlanco");
        textoCategoria = Texto2.GetComponent<Text>();
        textoCategoria.text = "" + Variables.VarCategoria;
        textoBlanco = Texto3.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        //OrdenarSegunNivel();
        //NamesFotosAnimales.Sort();
        TodasM.AddRange(NamesFotosAnimales);
        TodasM.AddRange(NamesFotosFrutas);
        TodasM.AddRange(NamesFotosProfesiones);
        TodasM.Sort();//mejorar las formas de barajada
        index = RandomNums(int.Parse(Ne), TodasM.Count);
        for (int j=0;j< int.Parse(Ne); j++)
        {
            Todas.Add(TodasM[index[j]]);
            NamesFotos += TodasM[index[j]] + "|";
        }
        //Base de datos  

        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Debes decir la categoría a la que pertenecen las imágenes que salgan, aparecerán imágenes de las tres categorías." + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);

        e = 0;

        G_tiempo = new float[index.Length + 1];
        auxtiempo = new float[index.Length + 1];
        GE_tiempo = new float[index.Length + 1];
    }

    // Update is called once per frame
    public void Update()
    {
        tiempo = tiempo + Time.deltaTime;
        if (i == 0)
        {
            G_tiempo[i] = tiempo;
            auxtiempo[i] = tiempo;
        }
        else
        {
            auxtiempo[i] = tiempo;
            G_tiempo[i] = tiempo - auxtiempo[i - 1];
        }
        if (i<int.Parse(Ne))
        {
            if (i == 0)
            {
                ImgAct = Todas[i];
                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + Todas[i]);
                //bool bandera1 = NamesFotosAnimales.Contains(Todas[i]);
                if (NamesFotosAnimales.Contains(Todas[i]))
                    bandera1 = "Animales";
                else if (NamesFotosFrutas.Contains(Todas[i]))
                    bandera1 = "Frutas";
                else if (NamesFotosProfesiones.Contains(Todas[i]))
                    bandera1 = "Profesiones";
                i++;
                bandera = false;
            }
            if (bandera)
            {
                ImgAct = Todas[i];
                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + Todas[i]);
                bandera = false;
                if (NamesFotosAnimales.Contains(Todas[i]))
                    bandera1 = "Animales";
                else if (NamesFotosFrutas.Contains(Todas[i]))
                    bandera1 = "Frutas";
                else if (NamesFotosProfesiones.Contains(Todas[i]))
                    bandera1 = "Profesiones";
                i++;
            }
        }
        if (i >= int.Parse(Ne))
        {
            if (bandera)
            {
                objeto_Ca.DBWrite();
                SceneManager.LoadScene("Lobby");
            }
        }
    }
    int[] RandomNums(int cantidad, int maxval)
    {
        int aux;
        int[] index = new int[cantidad];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = Random.Range(1, maxval);
        }
        for (int i = 0; i < cantidad - 1; i++)
        {
            if (index[i] == index[i + 1])
            {
                aux = index[i];
                index[i] = index[cantidad - 1];
                index[cantidad - 1] = index[i];
            }
        }
        return index;
    }
    public static void TiempoErrores()
    {
        GE_tiempo[e] = tiempo - auxtiempo[i - 1];
        e++;
    }

}
