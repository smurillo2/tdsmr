﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;
using SpeechLib;
using System.Xml;
using System.IO;

public class PerCa : MonoBehaviour {
    // Verificar métodos de entrada
    public bool TMouse;
    public bool TTactil;
    public bool TVoz;
    // Actualizar puntaje y actualizar color botones
    public Text Puntaje;

    public string imgpos1;
    public List<string> posimg = new List<string>() { };
    int index;
    public int puntos;
    public int puntosN; //variable para contar los desaciertos. No se muestra para evitar producir un efecto negativo en el entrenado.
    public GameObject LeerVars;
    public GameObject otro;
    ActCa Variable;
    public Text FinNivel;
    GameObject Global;
    GameController Variables;
    public Color colorV = new Color(0.1F, 0.5F, 0F, 0.5F);
    public Color colorR = new Color(0.5F, 0.1F, 0F, 0.5F);
    public int Nivel;
    //public Button Buttons;
    public Button Button0;
    //Botones Animal Fruta Profesion
    public Button Animal;
    public Button Fruta;
    public Button Profesion;
    //Reconocimiento de voz
    KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public Text PalaDich;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "aguila", "avestruz", "ballena", "burro", "caballo", "caiman", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "raton", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    private SpVoice voice;
    public Text Instrucciones;
    int frases = 1;
    //Base de datos
    public static int acierto;
    public static int error;
    public SI_GES objeto_GES;
    public Comentarios mensaje;
    string frase;

    public static string aciertosyerrores;
    public static string palabraserror;

    // Use this for initialization
    void Start()
    {
        //Inicializar variables
        acierto = 0;
        error = 0;


        // Leer Variables de control
        LeerVars = GameObject.Find("ActC");
        Variable = LeerVars.GetComponent<ActCa>();
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        imgpos1 = Variable.imgpos;
        // Verificar métodos de entrada habilitados
        TMouse = Variables.VarTMouse;
        TTactil = Variables.VarTTactil;
        TVoz = Variables.VarTVoz;

        if (TMouse)//esto se puede considerar quitarse o programarse diferente.
        {
            print("Está activo el método Mouse");//aqui toca hacer una confirmacion
            Animal.onClick.AddListener(() => ActPuntaje1("Animales", 1));
            Fruta.onClick.AddListener(() => ActPuntaje1("Frutas", 1));
            Profesion.onClick.AddListener(() => ActPuntaje1("Profesiones", 1));
        }
        if (TTactil)
        {
            print("Está activo el método táctil");
            Animal.onClick.AddListener(() => ActPuntaje1("Animales", 1));
            Fruta.onClick.AddListener(() => ActPuntaje1("Frutas", 1));
            Profesion.onClick.AddListener(() => ActPuntaje1("Profesiones", 1));
        }
        if (TVoz)
        {
            print("Esta activo el método voz");
            keywords.Add("animal", () =>
            {
                AnimalCalled();
            });
            keywords.Add("fruta", () =>
            {
                FrutaCalled();
            });
            keywords.Add("profesion", () =>
            {
                ProfesionCalled();
            });
            KeywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
            KeywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
            KeywordRecognizer.Start();
        }

        //voz
        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;  //   Rate (no xml)   

        aciertosyerrores = "";
        palabraserror = "";

    }

    // Update is called once per frame
    void Update()
    {

    }

    void ActPuntaje1(string Nombre, int iscorrect)/*la función más importante de este script debe ser conectarse con la base de datos para almacenar el dato*/
    {
        print("El animal es:" + ActCa.ImgAct);//este hay que modificarlo en todas las partes donde se reutilice este codigo
        ActCa.bandera = true;//este hay que modificarlo en todas las partes donde se reutilice este codigo//No se para que puse esta nota: es necesario redeclarar esta variable por una variable tipo string
        if (Nombre == ActCa.bandera1)//cambiar en los codigos donde se reutilice
        {
                print("Correcto");
                aciertosyerrores = aciertosyerrores + "|1";
                acierto++;
                Puntaje.text = acierto.ToString();
                mensaje = new Comentarios("p");
                frase = mensaje.respuesta("p");
                print("Correcto");
                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                         + frase
                                                     + "</speak>",
                                                     SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
               
                objeto_GES.DBWrite();            
        }
        else
        {
            mensaje = new Comentarios("n");
            frase = mensaje.respuesta("n");
            print("Incorrecto");
            ActCa.TiempoErrores();
            aciertosyerrores = aciertosyerrores + "|0";
            palabraserror = palabraserror + Nombre + "|";

            error++;
            voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                      + frase
                                                      + "</speak>",
                                                      SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
            objeto_GES.DBWrite();
        }
    }
    /*
    void ActPuntaje1(string Nombre, int iscorrect)/*la función más importante de este script debe ser conectarse con la base de datos para almacenar el dato*/
    /*{
        print("El animal es:" + ActC.ImgAct);
        ActC.bandera = true;//es necesario redeclarar esta variable por una variable tipo string
        if (Nombre == ActC.bandera1)
        {
            print("Correcto");
            Puntaje.text = "Correcto";
            /*voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-ES'>"
                                            + "!Muy bien!, !sigue adelante!"
                                            + "</speak>",
                                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);*/
    /* }
     else
     {
         print("Incorrecto");
         Puntaje.text = "Incorrecto";
     }

     }
 /*    IEnumerator Pere()
     {
         Debug.Log(Time.time);
         yield return new WaitForSeconds(10);
         Debug.Log(Time.time);
         Debug.Log("Just waited 10 second");
     }*/
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    //funciones para el conteo de los puntos
    void AnimalCalled()
    {
        print("Dijiste animal");
        PalaDich.text += ", animal";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "animal")
        {
            ActPuntaje1("Animales", 1);
        }
        else
        {
            ActPuntaje1("Animales", 0);
        }
    }
    void FrutaCalled()
    {
        print("Dijiste fruta");
        PalaDich.text += ", fruta";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "fruta")
        {
            ActPuntaje1("Frutas", 1);
        }
        else
        {
            ActPuntaje1("Frutas", 0);
        }
    }
    void ProfesionCalled()
    {
        print("Dijiste profesion");
        PalaDich.text += ", profesion";
        imgpos1 = Variable.imgpos;
        if (imgpos1 == "profesion")
        {
            ActPuntaje1("Profesiones", 1);
        }
        else
        {
            ActPuntaje1("Profesiones", 0);
        }
    }
}
