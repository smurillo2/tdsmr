﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class SI_Ca : MonoBehaviour
{
    public int id_usuario = 0;//esto hay que conectarlo con el menu inicial
    public int aciertos = 0;
    public int errores = 0;
    public int NumEstimulos = 0;
    public float TiempoGeneral = 0f;
    public float PromTiempoRes = 0f;
    public float[] Tiempoporimagen;
    public string TiempoXImg = "";
    public string Imagenes = "";
    string Fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

    public string Usuario;
    public string nombreUsuario;
    public string escena;

    public string aciertosyerrores;
    public float[] GE_tiempo;
    public string GE_tiempos;
    public string palabraserror;
    /*
     400 - No establece conexion
     401 - No encuentra datos
     402 - El usuario ya existe
     200 - Datos encontrados
     201 - Usuario registrado
     202 - Puntaje actualizado
     203 - Palabras y comentarios guardados
     */
    // Use this for initialization
    void Start()
    {
        Usuario = PlayerPrefs.GetString("Usuario");
        nombreUsuario = PlayerPrefs.GetString("nombreUsuario");
        id_usuario = PlayerPrefs.GetInt("idUsuario");
        escena = "CA";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SI()
    {

    }

    public void DBWrite()
    {
        float aux = 0f;
        float media = 0f;
        Tiempoporimagen = ActCa.G_tiempo;
        for (int i = 1; i < int.Parse(ActCa.Ne) + 1; i++)
        {
            aux = aux + Tiempoporimagen[i];
            TiempoXImg += Tiempoporimagen[i].ToString() + "|";
        }
        media = aux / int.Parse(ActCa.Ne);
        aciertos = PerCa.acierto;
        errores = PerCa.error;
        NumEstimulos = int.Parse(ActCa.Ne);
        TiempoGeneral = ActCa.tiempo;
        PromTiempoRes = media;
        Imagenes = ActCa.NamesFotos;
        aciertosyerrores = PerCa.aciertosyerrores;
        palabraserror = PerCa.palabraserror;
        GE_tiempo = ActCa.GE_tiempo;
        for (int i = 0;i< ActCa.e; i++)
        {
            GE_tiempos = GE_tiempos + GE_tiempo[i].ToString() + "|";
        }



        StartCoroutine(CWrite());
    }

    public void DBRead()
    {

    }
    IEnumerator CWrite()
    {
        //WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/CAwrite.php?idu=" + id_usuario + "&aci=" + aciertos + "&err=" + errores + "&cat=" + categoria + "&nue=" + NumEstimulos + "&tig=" + TiempoGeneral + "&ptr=" + PromTiempoRes + "&txi=" + TiempoXImg + "&fec=" + Fecha + "&img=" + Imagenes);
        WWW conexion = new WWW("http://localhost/CTA/CAwrite.php?idu=" + id_usuario + "&aci=" + aciertos + "&err=" + errores + "&nue=" + NumEstimulos + "&tig=" + TiempoGeneral + "&ptr=" + PromTiempoRes + "&txi=" + TiempoXImg + "&fec=" + Fecha + "&img=" + Imagenes + "&aye=" + aciertosyerrores + "&pae=" + palabraserror + "&ter=" + GE_tiempos);
        Debug.Log(conexion.text);
        yield return (conexion);

        if (conexion.text == "402")
        {
            Debug.LogError("El usuario ya existe");
        }
        else if (conexion.text == "203")
        {
            Debug.LogError("Registro realizado");
        }
        else
        {
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
}
