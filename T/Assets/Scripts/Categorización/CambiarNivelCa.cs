﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambiarNivelCa : MonoBehaviour {
    public Image SelecEntre;
    public GameObject ToPa;
    public InputField NBlanco;
    /*public InputField TBlanco;
    public InputField PBlanco;
    public Dropdown Categoria;
    public string VarCategoria;
    List<string> NamesCategorias = new List<string>() { "Animales", "Frutas", "Profesiones" };*/
    public string VarEntrenamiento;
    // Use this for initialization
    void Start()
    {
        //VarCategoria = "Animales";
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void cargarNivel(string pNombreNivel)
    {
        VarEntrenamiento = PlayerPrefs.GetString("Juego");//22012020 esto sobra a estas alturas solo se invoca este método de este script en particular
        PlayerPrefs.SetString("CBlanco", NBlanco.text);
        /*PlayerPrefs.SetString("GNGCategoria", VarCategoria);
        PlayerPrefs.SetString("GNGTBlanco", TBlanco.text);
        PlayerPrefs.SetString("GNGPBlanco", PBlanco.text);*/
        SelecEntre.gameObject.SetActive(false);
        ToPa.SetActive(false);
        SceneManager.LoadScene(pNombreNivel);
    }
}
