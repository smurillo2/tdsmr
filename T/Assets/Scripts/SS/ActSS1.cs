﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ActSS1 : MonoBehaviour {
    public GameObject Boton;
    public GameObject Global;
    GameController Variables;
    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    Text textoDistra;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    /*List<string> NamesFotosAnimales = new List<string>() { "aguila", "avestruz", "ballena", "burro", "caballo", "caiman", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "raton", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    */
    List<string> NamesFotosAnimales = new List<string>() { "avestruz", "ballena", "burro", "caballo", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "paloma", "pato", "perro", "pollo", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() {  "aguacate", "banano", "durazno", "fresa", "guayaba", "lulo", "mandarina", "mango", "manzana", "naranja", "papaya", "pera", "piña" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "pintor", "policía", "profesor", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };




    List<string> Todas = new List<string>();
    List<string> TodasM = new List<string>();
    public int[] index = new int[200];
    public int[] indexA = new int[200];
    int[] indexF = new int[200];
    int[] indexP = new int[200];
    public string imgpos = null;
   
    public static bool bandera = false;
    public static string ImgAct;
    public Text tiempoText;
    public int i = 1;
    public int cont = 0;
    public static string bandera1;
    public static string bandera2;
    public int maxAleatoriedad = 1;//IA esto lo controlara el agente esta aleatoriedad es para controlar el estimulo Nogo
    public int Aleatoriedad;//IA esto lo controlara el agente esta aleatoriedad es para controlar el estimulo Nogo
    public int maxAleatoriedadSS = 1;//IA esto lo controlara el agente esta aleatoriedad es para controlar la senal de SS
    public int AleatoriedadSS;//IA esto lo controlara el agente esta aleatoriedad es para controlar la senal de SS
    float tiempo = 8.0f;
    public float Dtiempo = 5f;
    public float DtiempoS = 5f;
    //sonido Stop Signal
    AudioSource Beep;
    public AudioClip beep;
    void Awake()
    {
        Beep = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start () {
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Texto1.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;
        textoCategoria = Texto2.GetComponent<Text>();
        textoCategoria.text = "" + Variables.VarCategoria;
        textoBlanco = Texto3.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        //OrdenarSegunNivel();
        //NamesFotosAnimales.Sort();
        TodasM.AddRange(NamesFotosAnimales);
        TodasM.AddRange(NamesFotosFrutas);
        TodasM.AddRange(NamesFotosProfesiones);
        TodasM.Sort();//mejorar las formas de barajada
        index = RandomNums(TodasM.Count, TodasM.Count);//IA esto lo puede modificar un agente
        ///indexF = RandomNums(NamesFotosFrutas.Count, NamesFotosFrutas.Count);//IA esto lo puede modificar un agente
        //indexP = RandomNums(NamesFotosProfesiones.Count, NamesFotosProfesiones.Count);//IA esto lo puede modificar un agente
        /*switch (Variables.VarCategoria)
        {
            case "Animales":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
            case "Frutas":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
            case "Profesiones":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
        }*/

        /*for (int j = 0; j < TodasM.Count; j++)
        {
            Todas.Add(TodasM[index[j]]);
        }*/

    }
	
	// Update is called once per frame
	public void Update () {
        //index = RandomNums(NamesFotosAnimales.Count,NamesFotosAnimales.Count);
        tiempo -= Time.deltaTime;
        tiempoText.text = "" + tiempo.ToString("f0");
        if (tiempo < 1)
        {
            if (i > 0)
            {
                GameObject.Find("Botones").GetComponent<PerSS>().ActPuntaje1("No", 1);
            }
            print("Tiempo cumplido" + cont.ToString());
            tiempo = Dtiempo;
            cont++;
            switch (Variables.VarCategoria)           
            {
                case "Animales":                    
                    Aleatoriedad = Random.Range(0, maxAleatoriedad);
                    if ((i % 2) == 0)
                    {
                        AleatoriedadSS = Random.Range(0, maxAleatoriedadSS);
                        tiempo = DtiempoS;//IA Determina el tiempo que no se muestra estimulo tambien lo puede controlar el agente
                        if (AleatoriedadSS == 1)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                        {
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Negro");//Cambiear el directorio el Names y el index
                            Beep.clip = beep;//este sonido puede manipularse en que momento aparece pero no se aun como hacerlo, de momento aparece a la para con la imagen sin estimulo
                            Beep.Play();
                            bandera2 = "beep";
                            print("beep");
}
                        else
                        {
                            Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Negro");//Cambiear el directorio el Names y el index
                        }
                        i++;
                    }
                    else
                    {
                        tiempo = Dtiempo;
                        if (Aleatoriedad == 1)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                        {
                            if (i/2 < NamesFotosFrutas.Count)
                            {
                                ImgAct = NamesFotosFrutas[indexF[i/2]];//cambiar el tipo de indice
                                print(ImgAct);
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + TodasM[index[i/2]]);//Cambiear el directorio el Names y el index
                                bandera1 = "Frutas";
                                bandera = true;
                                i++;
                                //imgpos = NamesFotosAnimales[index[i]];
                                /*if (i == 0)
                                {
                                    //ImgAct = NamesFotosAnimales[index[i]];
                                    //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                    ImgAct = NamesFotosAnimales[i];
                                    print(ImgAct);
                                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[i]);
                                    imgpos = NamesFotosAnimales[index[i]];
                                    i++;
                                }
                                if (bandera)
                                {
                                    //ImgAct = NamesFotosAnimales[index[i]];
                                    //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                    ImgAct = NamesFotosAnimales[i];
                                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[i]);
                                    imgpos = NamesFotosAnimales[index[i]];
                                    bandera = false;
                                    i++;
                                }*/
                            }
                            else if (i/2 >= NamesFotosAnimales.Count)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                            {
                                if (bandera)
                                {
                                    SceneManager.LoadScene("Menu1");
                                }
                            }
                        }
                        else if (Aleatoriedad == 2)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                        {
                            if (i/2 < NamesFotosProfesiones.Count)//cambiar names
                            {
                                ImgAct = NamesFotosProfesiones[indexP[i/2]];//cambiar index
                                print(ImgAct);
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + TodasM[index[i/2]]);//cambiar ruta names e index
                                bandera1 = "Profesiones";
                                bandera = true;
                                i++;
                                //imgpos = NamesFotosAnimales[index[i]];
                                /*if (i == 0)
                                {
                                    //ImgAct = NamesFotosAnimales[index[i]];
                                    //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                    ImgAct = NamesFotosAnimales[i];
                                    print(ImgAct);
                                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[i]);
                                    imgpos = NamesFotosAnimales[index[i]];
                                    i++;
                                }
                                if (bandera)
                                {
                                    //ImgAct = NamesFotosAnimales[index[i]];
                                    //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                    ImgAct = NamesFotosAnimales[i];
                                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[i]);
                                    imgpos = NamesFotosAnimales[index[i]];
                                    bandera = false;
                                    i++;
                                }*/
                            }
                            else if (i/2 >= NamesFotosAnimales.Count)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                            {
                                if (bandera)
                                {
                                    SceneManager.LoadScene("Menu1");
                                }
                            }
                        }
                        else
                        {
                            if (i/2 < NamesFotosAnimales.Count)
                            {
                                ImgAct = NamesFotosAnimales[indexA[i/2]];
                                print(ImgAct);
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + TodasM[index[i/2]]);
                                bandera1 = "Animales";
                                bandera = true;
                                i++;
                                //imgpos = NamesFotosAnimales[index[i]];
                                /*if (i == 0)
                                {
                                    //ImgAct = NamesFotosAnimales[index[i]];
                                    //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                    ImgAct = NamesFotosAnimales[i];
                                    print(ImgAct);
                                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[i]);
                                    imgpos = NamesFotosAnimales[index[i]];
                                    i++;
                                }
                                if (bandera)
                                {
                                    //ImgAct = NamesFotosAnimales[index[i]];
                                    //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                    ImgAct = NamesFotosAnimales[i];
                                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[i]);
                                    imgpos = NamesFotosAnimales[index[i]];
                                    bandera = false;
                                    i++;
                                }*/
                            }
                            else if (i/2 >= NamesFotosAnimales.Count)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                            {
                                /*if (bandera)
                                {*/
                                SceneManager.LoadScene("Menu1");
                                //}
                            }
                        }
                    }
                    break;                   
                case "Frutas":
                    bandera1 = Variables.VarCategoria;
                    if (Random.Range(0, 100) == 1)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < NamesFotosFrutas.Count)
                        {
                            if (i == 0)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosFrutas[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[i]);
                                imgpos = NamesFotosFrutas[indexF[i]];
                                i++;
                            }
                            if (bandera)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosFrutas[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[i]);
                                imgpos = NamesFotosFrutas[indexF[i]];
                                bandera = false;
                                i++;
                            }

                            else if (i >= NamesFotosAnimales.Count)
                            {
                                if (bandera)
                                {
                                    SceneManager.LoadScene("Menu1");
                                }
                            }
                        }
                    }
                    break;
                case "Profesiones":
                    bandera1 = Variables.VarCategoria;
                    if (Random.Range(0, 100) == 1)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < NamesFotosProfesiones.Count)
                        {
                            if (i == 0)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosProfesiones[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[i]);
                                imgpos = NamesFotosProfesiones[indexP[i]];
                                i++;
                            }
                            if (bandera)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosProfesiones[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[i]);
                                imgpos = NamesFotosProfesiones[indexP[i]];
                                bandera = false;
                                i++;
                            }
                        }
                        else if (i >= NamesFotosAnimales.Count)
                        {
                            if (bandera)
                            {
                                SceneManager.LoadScene("Menu1");
                            }
                        }
                    }
                        break;                    
            }
        }
    }
    int[] RandomNums(int cantidad, int maxval)
    {
        int[] index = new int[maxval];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = Random.Range(0, maxval);
        }
        return index;
    }
}

/*en este código hay que programar que se pongan categorías aleatorias en pantalla, que se diga el nombre de la categoria con sintesis de voz
y finalmente el reconocimiento de voz y los puntajes. Este código si cambia radicalmente en comparación con el del otro entrenamiento
pues la lógica para este entrenamiento es diferente*/

/*tiempo -= Time.deltaTime;
tiempoText.text = "" + tiempo.ToString("f0");
if (tiempo < 1 && bandera)
{
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);                        
i++;
tiempo = 2.0f;
}
if (i == NamesFotosAnimales.Count)
{
bandera = false;
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/fresa");
}*/
