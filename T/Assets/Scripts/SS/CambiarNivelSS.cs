﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambiarNivelSS : MonoBehaviour {
    public Image SelecEntre;
    public GameObject ToPa;
    public InputField NBlanco;
    public InputField TE;
    public InputField TEE;
    public InputField PS;
    public string VarEntrenamiento;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void cargarNivel(string pNombreNivel)
    {
        VarEntrenamiento = PlayerPrefs.GetString("Juego");
        PlayerPrefs.SetString("SSBlanco", NBlanco.text);
        PlayerPrefs.SetString("SSTE", TE.text);
        PlayerPrefs.SetString("SSTEE", TEE.text);
        PlayerPrefs.SetString("SSPS", PS.text);
        //SelecEntre.gameObject.transform.localScale = new Vector3(0, 0, 0);
        SelecEntre.gameObject.SetActive(false);
        ToPa.SetActive(false);
        SceneManager.LoadScene(pNombreNivel);
    }
}
