﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SpeechLib;//texto a voz

public class ActSS : MonoBehaviour {
    public GameObject Boton;
    public GameObject Global;
    GameController Variables;
    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    Text textoDistra;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    /*List<string> NamesFotosAnimales = new List<string>() { "aguila", "avestruz", "ballena", "burro", "caballo", "caiman", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "raton", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    */
    List<string> NamesFotosAnimales = new List<string>() { "avestruz", "ballena", "burro", "caballo", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "paloma", "pato", "perro", "pollo", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() {  "aguacate", "banano", "durazno", "fresa", "guayaba", "lulo", "mandarina", "mango", "manzana", "naranja", "papaya", "pera", "pina" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "pintor", "policia", "profesor", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };




    List<string> Todas = new List<string>();
    List<string> TodasM = new List<string>();
    public int[] index = new int[200];
    public int[] indexA = new int[200];
    int[] indexF = new int[200];
    int[] indexP = new int[200];
    public string imgpos = null;
   
    public static bool bandera = false;
    public Text tiempoText;
    public static int i = 1;
    public int cont = 0;
    public static string bandera1;
    public static string bandera2;
    public int maxAleatoriedad = 1;//IA esto lo controlara el agente esta aleatoriedad es para controlar el estimulo Nogo
    public int Aleatoriedad;//IA esto lo controlara el agente esta aleatoriedad es para controlar el estimulo Nogo
    public int maxAleatoriedadSS = 1;//IA esto lo controlara el agente esta aleatoriedad es para controlar la senal de SS
    public int AleatoriedadSS;//IA esto lo controlara el agente esta aleatoriedad es para controlar la senal de SS



    //Base de datos tiempos datos para monitoreo y almacenamiento en base de datos
    public static float[] G_tiempo;
    public static float[] auxtiempo;
    public float t;
    public float P_tiempo;
    public static string NamesFotos;
    public static float tiempo = 8f;
    public SI_SS objeto_SS;
    public static string ImgAct;
    public static string Ne;
    public static float Dtiempo = 5f;
    public static float DtiempoS = 5f;
    public static float tiempoG = 0f;
    float tiempoI = 8.0f;
    public static int Pi = 0;

    public static float[] GE_tiempo;
    public static int e;

    private SpVoice voice;

    //sonido Stop Signal
    AudioSource Beep;
    public AudioClip beep;
    void Awake()
    {
        Beep = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start () {
        //Base de datos inicialización de variables
        NamesFotos = "";
        bandera2 = "";
        tiempoG = 0;
        tiempo = 8f;
        tiempoI = 8f;
        i = 0;
        Pi = 0;
        bandera = true;
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Texto1.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;
        textoCategoria = Texto2.GetComponent<Text>();
        textoCategoria.text = "" + Variables.VarCategoria;
        textoBlanco = Texto3.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        //OrdenarSegunNivel();
        //NamesFotosAnimales.Sort();

        Ne = PlayerPrefs.GetString("SSBlanco");
        Dtiempo = float.Parse(PlayerPrefs.GetString("SSTE"));
        DtiempoS = float.Parse(PlayerPrefs.GetString("SSTEE"));
        maxAleatoriedadSS = int.Parse(PlayerPrefs.GetString("SSPS"));

        TodasM.AddRange(NamesFotosAnimales);
        TodasM.AddRange(NamesFotosFrutas);
        TodasM.AddRange(NamesFotosProfesiones);
        TodasM.Sort();//mejorar las formas de barajada
        index = RandomNums(int.Parse(Ne), TodasM.Count);//IA esto lo puede modificar un agente
        for (int j = 0; j < int.Parse(Ne); j++)
        {
            Todas.Add(TodasM[index[j]]);
            NamesFotos += TodasM[index[j]] + "|";
        }

        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "Debes decir la categoría de la imagen, exepto cuando escuches el sonido." + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);



        G_tiempo = new float[index.Length + 1];
        auxtiempo = new float[index.Length + 1];
        auxtiempo[0] = tiempoI;
        e = 0;
        GE_tiempo = new float[index.Length + 1];

    }

    // Update is called once per frame
    public void Update () {
        //index = RandomNums(NamesFotosAnimales.Count,NamesFotosAnimales.Count);

        tiempo -= Time.deltaTime;
        tiempoText.text = "" + tiempo.ToString("f0");
        tiempoG += Time.deltaTime;
        Pi = i;

        if (tiempo < 0)
        {
            if (i > 1)
            {
                GameObject.Find("Botones").GetComponent<PerSS>().ActPuntaje1("No", 1);
            }
            print("Tiempo cumplido" + cont.ToString());
            //tiempo = Dtiempo;
            cont++;                
            Aleatoriedad = Random.Range(0, maxAleatoriedad);
            if ((i % 2) == 0)
            {
                AleatoriedadSS = Random.Range(0, 100);
                tiempo = DtiempoS;//IA Determina el tiempo que no se muestra estimulo tambien lo puede controlar el agente
                if (AleatoriedadSS <= maxAleatoriedadSS)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                {
                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Negro");//Cambiear el directorio el Names y el index
                    Beep.clip = beep;//este sonido puede manipularse en que momento aparece pero no se aun como hacerlo, de momento aparece a la para con la imagen sin estimulo
                    Beep.Play();
                    bandera2 = "beep";
                    print("beep");
                }
                else
                {
                    Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Negro");//Cambiear el directorio el Names y el index
                }
                i++;
            }
            else
            {
                tiempo = Dtiempo;
                if (i/2 < Todas.Count)
                {
                    if (i == 0)
                    {
                        ImgAct = Todas[i/2];
                        Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + Todas[i/2]);
                        //bool bandera1 = NamesFotosAnimales.Contains(Todas[i]);
                        if (NamesFotosAnimales.Contains(Todas[i/2]))
                            bandera1 = "Animales";
                        else if (NamesFotosFrutas.Contains(Todas[i/2]))
                            bandera1 = "Frutas";
                        else if (NamesFotosProfesiones.Contains(Todas[i/2]))
                            bandera1 = "Profesiones";                       
                        bandera = true;
                    }
                    if (i>0)
                    {
                        ImgAct = Todas[i/2];
                        Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Todas/" + Todas[i/2]);
                        bandera = true;
                        if (NamesFotosAnimales.Contains(Todas[i/2]))
                            bandera1 = "Animales";
                        else if (NamesFotosFrutas.Contains(Todas[i/2]))
                            bandera1 = "Frutas";
                        else if (NamesFotosProfesiones.Contains(Todas[i/2]))
                            bandera1 = "Profesiones";
                    }
                }
                if (i/2 > Todas.Count)
                {
                    objeto_SS.DBWrite();
                    SceneManager.LoadScene("Lobby");
                }
                i++;
            }
        }
    }
    int[] RandomNums(int cantidad, int maxval)
    {
        int aux;
        int[] index = new int[cantidad];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = Random.Range(1, maxval);
        }
        for (int i = 0; i < cantidad - 1; i++)
        {
            if (index[i] == index[i + 1])
            {
                aux = index[i];
                index[i] = index[cantidad - 1];
                index[cantidad - 1] = index[i];
            }
        }
        return index;
    }
    public static void TiempoRespuesta()
    {
        auxtiempo[Pi / 2] = tiempoG;
        G_tiempo[Pi / 2] = tiempoG - auxtiempo[(Pi / 2) - 1] - DtiempoS;
    }
    public static void TiempoErrores()
    {
        GE_tiempo[e] = tiempoG - auxtiempo[(Pi / 2) - 1] - DtiempoS;
        e++;
    }

}

/*en este código hay que programar que se pongan categorías aleatorias en pantalla, que se diga el nombre de la categoria con sintesis de voz
y finalmente el reconocimiento de voz y los puntajes. Este código si cambia radicalmente en comparación con el del otro entrenamiento
pues la lógica para este entrenamiento es diferente*/

/*tiempo -= Time.deltaTime;
tiempoText.text = "" + tiempo.ToString("f0");
if (tiempo < 1 && bandera)
{
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);                        
i++;
tiempo = 2.0f;
}
if (i == NamesFotosAnimales.Count)
{
bandera = false;
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/fresa");
}*/
