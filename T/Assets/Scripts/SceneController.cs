﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SceneController : MonoBehaviour {
    public Text TextoEntrenamiento;
    public Text TextoCategorias;
    public Text TextoNivel;
    public Text MiNivel;
    List<string> NamesEntrenamiento = new List<string>() { "Cancelación", "Otro", "Basico" };
    List<string> NamesCategorias = new List<string>() {"Animales","Frutas","Profesiones"};
    List<string> NamesNivel = new List<string>() { "1", "2", "3", "4", "5", "6","7", "8", "8","10" };
    public Dropdown TipoEntrenamiento;
    public Dropdown Categoria;
    public Dropdown Nivel;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    public void DropDown_IndexEntrenamiento(int index)
    {
        //TextoEntrenamiento.text = ("Entrenamiento: " + NamesCategorias[index]);
    }
    public void DropDown_IndexCategoria(int index)
    {
        //TextoCategorias.text = ("Categoría: " + NamesCategorias[index]);
    }
    public void DropDown_IndexNivel(int index)
    {
        //TextoNivel.text = ("Nivel: " + NamesNivel[index]+".");
    }
}
