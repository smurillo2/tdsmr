﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlScenaNuevoNivel : MonoBehaviour {
    public Text NumNivel;
    GameObject Global;
    GameObject Global1;
    GameController Variables;
    public int Blanco;
    public int Distra;
    public string Categoria;
    public int Nivel;
    // Use this for initialization
    void Start () {
        Global1 = GameObject.Find("SelecEntre");
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        Nivel = Blanco;
        Blanco = int.Parse(Variables.VarBlanco);
        Distra = int.Parse(Variables.VarDistra);
        Categoria = Variables.VarCategoria;
        if (Blanco > Distra)
        {
            Nivel = Blanco;
        }
        else
        {
            Nivel = Distra;
        }
        if (Blanco + Distra < 19)
        {
            Nivel++;
            Blanco++;
            Distra++;
            NumNivel.text = Nivel.ToString();
            Variables.VarBlanco = Blanco.ToString();
            Variables.VarDistra = Distra.ToString();
            Variables.VarCategoria = Categoria;
            SceneManager.LoadScene("Juego");            
        }
        else
        {
            Nivel=1;
            Blanco=1;
            Distra=1;
            NumNivel.text = Nivel.ToString();            
            Variables.VarBlanco = Blanco.ToString();
            Variables.VarDistra = Distra.ToString();
            Variables.VarCategoria = Categoria;
            Destroy(Global); // Fue necesario destuirlo para que volviera a aparecer el primer menú.
            SceneManager.LoadScene("Menu1");
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
/*    IEnumerator Pere()
    {
        Debug.Log(Time.time);
        yield return new WaitForSeconds(3);
        Debug.Log(Time.time);
        Debug.Log("Just waited 10 second");
    }
    */
}
