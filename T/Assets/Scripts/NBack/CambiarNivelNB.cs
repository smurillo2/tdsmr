﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambiarNivelNB : MonoBehaviour {
    public Image SelecEntre;
    public GameObject ToPa;
    public string VarEntrenamiento;
    public InputField NBlanco;
    public InputField TBlanco;
    public InputField TEBlanco;
    public InputField Aleatoriedad;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void cargarNivel(string pNombreNivel)
    {
        VarEntrenamiento = PlayerPrefs.GetString("Juego");
        PlayerPrefs.SetString("NBNBlanco", NBlanco.text);
        PlayerPrefs.SetString("NBTBlanco", TBlanco.text);
        PlayerPrefs.SetString("NBTEBlanco", TEBlanco.text);
        PlayerPrefs.SetString("NBAle", Aleatoriedad.text);
        SelecEntre.gameObject.SetActive(false);
        ToPa.SetActive(false);
        SceneManager.LoadScene(pNombreNivel);
    }
}
