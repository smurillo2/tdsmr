﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using SpeechLib;//texto a voz

public class ActNB : MonoBehaviour {
    public GameObject Boton;
    public GameObject Global;
    GameController Variables;
    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    Text textoDistra;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "", "avestruz", "ballena", "burro", "caballo", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "paloma", "pato", "perro", "pollo", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "", "aguacate", "banano", "durazno", "fresa", "guayaba", "lulo", "mandarina", "mango", "manzana", "naranja", "papaya", "pera", "piña" };
    List<string> NamesFotosProfesiones = new List<string>() { "", "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "pintor", "policía", "profesor", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    List<string> Todas = new List<string>();
    List<string> TodasM = new List<string>();
    public int[] indexA = new int[200];
    int[] indexF = new int[200];
    int[] indexP = new int[200];
    public string imgpos = null;
   
    public static bool bandera = false;
    public static string ImgAct;
    public static string ImgAnt;
    public static List<string> bandera1 = new List<string>();
    public static List<string> bandera2 = new List<string>();
    public Text tiempoText;
    static int i = 0;
    public static int Pi = 0;
    public int cont = 0;
    //public static string bandera1;
    int maxAleatoriedad = 90;//IA esto lo controlara el agente esta aleatoriedad es para controlar el estimulo Nogo
    public int Aleatoriedad;//IA esto lo controlara el agente esta aleatoriedad es para controlar el estimulo Nogo
    public int AleatoriRep = 0;//IA esto lo controlara el agente esta aleatoriedad es para controlar la senal de SS
    public static int MAleatoriRep = 0;
    float tiempo = 8.0f;
    float tiempoI = 8.0f;

    public static float Dtiempo = 4f;
    public static float DtiempoS = 3f;
    public static float tiempoG = 0f;

    public static float[] G_tiempo;
    public static float[] auxtiempo;


    public static float[] GE_tiempo;
    public static int e;
    public static int NEstimulos;

    //sonido Stop Signal
    AudioSource Beep;
    public AudioClip beep;

    private SpVoice voice;

    public static string NamesFotos;
    public SI_GES objeto_GES;
    public SI_NB objeto_NB;
    void Awake()
    {
        Beep = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start () {
        i = 0;
        Pi = 0;
        tiempoG = 0;
        bandera1.Clear();
        bandera1.Add("Null");
        bandera2.Clear();
        NamesFotos="";
        ImgAnt = "";
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Texto1.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;
        textoCategoria = Texto2.GetComponent<Text>();
        textoCategoria.text = "" + Variables.VarCategoria;
        textoBlanco = Texto3.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        //OrdenarSegunNivel();
        //NamesFotosAnimales.Sort();
        TodasM.AddRange(NamesFotosAnimales);
        TodasM.AddRange(NamesFotosFrutas);
        TodasM.AddRange(NamesFotosProfesiones);
        TodasM.Sort();//mejorar las formas de barajada
        NEstimulos = Int32.Parse(PlayerPrefs.GetString("NBNBlanco"));
        Dtiempo = Int32.Parse(PlayerPrefs.GetString("NBTBlanco"));
        DtiempoS = Int32.Parse(PlayerPrefs.GetString("NBTEBlanco"));
        MAleatoriRep = Int32.Parse(PlayerPrefs.GetString("NBAle"));
        indexA = RandomNums(NEstimulos, NamesFotosAnimales.Count);//IA esto lo puede modificar un agente
        indexF = RandomNums(NEstimulos, NamesFotosFrutas.Count);//IA esto lo puede modificar un agente
        indexP = RandomNums(NEstimulos, NamesFotosProfesiones.Count);//IA esto lo puede modificar un agente

        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + "No digas nada en la primer imagen, luego, debes decir ¡igual! si la imagen anterior, tiene la misma categoría de la imagen actual." + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);


        /*switch (Variables.VarCategoria)
        {
            case "Animales":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
            case "Frutas":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
            case "Profesiones":
                index = RandomNums(NamesFotosAnimales.Count, NamesFotosAnimales.Count);
                break;
        }*/

        /*for (int j = 0; j < TodasM.Count; j++)
        {
            Todas.Add(TodasM[index[j]]);
        }*/
        G_tiempo = new float[NEstimulos + 1];
        auxtiempo = new float[NEstimulos + 1];
        auxtiempo[0] = tiempoI;
        e = 0;
        GE_tiempo = new float[NEstimulos + 1];

    }

    // Update is called once per frame
    public void Update () {
        //index = RandomNums(NamesFotosAnimales.Count,NamesFotosAnimales.Count);
        tiempo -= Time.deltaTime;
        tiempoText.text = "" + tiempo.ToString("f0");
        tiempoG += Time.deltaTime;
        Pi = i;
        if (tiempo < 0)
        {
            print("Tiempo cumplido" + cont.ToString());
            tiempo = Dtiempo;
            cont++;

            if (AleatoriRep<1)
            {
                AleatoriRep = UnityEngine.Random.Range(0, MAleatoriRep);
                Aleatoriedad = UnityEngine.Random.Range(0, maxAleatoriedad);
            }


            if ((i % 2) == 0)//aqui entra al cuadro negro
            {
                tiempo = DtiempoS;//IA Determina el tiempo que no se muestra estimulo tambien lo puede controlar el agente
                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Negro");//Cambiear el directorio el Names y el index                        
                i++;
                GameObject.Find("Botones").GetComponent<PerNB>().ActPuntaje1("No", 1);
            }
            else
            {
                tiempo = Dtiempo;
                if (Aleatoriedad <= 30)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                {
                    if (i / 2 < NEstimulos)
                    {
                        ImgAct = NamesFotosFrutas[indexF[i / 2]];//cambiar el tipo de indice
                        print(ImgAct);
                        Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[indexF[i / 2]]);//Cambiear el directorio el Names y el index
                        bandera1.Add("Frutas");
                        bandera2.Add(NamesFotosFrutas[indexF[i / 2]]);
                        bandera = true;
                        if (i > 2)
                        {
                            ImgAnt = bandera2[(Pi / 2) - 1];
                            objeto_GES.DBWrite();
                        }
                        i++;
                        AleatoriRep --;
                        NamesFotos += NamesFotosFrutas[indexF[(i / 2)-1]] + "|";
                    }
                    else if (i / 2 >= NEstimulos)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                    {
                        ImgAnt = bandera2[(Pi / 2) - 1];
                        objeto_GES.DBWrite();
                        objeto_NB.DBWrite();
                        SceneManager.LoadScene("Lobby");
                    }
                }
                else if (Aleatoriedad > 30 & Aleatoriedad > 60)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                {
                    if (i / 2 < NEstimulos)//cambiar names
                    {
                        ImgAct = NamesFotosProfesiones[indexP[i / 2]];//cambiar index
                        print(ImgAct);
                        Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[indexP[i / 2]]);//cambiar ruta names e index
                        bandera = true;
                        bandera1.Add("Profesiones");
                        bandera2.Add(NamesFotosProfesiones[indexP[i / 2]]);
                        if (i > 2)
                        {
                            ImgAnt = bandera2[(Pi / 2) - 1];
                            objeto_GES.DBWrite();
                        }
                        i++;
                        AleatoriRep--;
                        NamesFotos += NamesFotosProfesiones[indexP[(i / 2)-1]] + "|";
                    }
                    else if (i / 2 >= NEstimulos)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                    {
                        ImgAnt = bandera2[(Pi / 2) - 1];
                        objeto_GES.DBWrite();
                        objeto_NB.DBWrite();
                        SceneManager.LoadScene("Lobby");
                    }
                }
                else
                {
                    if (i / 2 < NEstimulos)
                    {
                        ImgAct = NamesFotosAnimales[indexA[i / 2]];
                        print(ImgAct);
                        Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[indexA[i / 2]]);
                        bandera = true;
                        bandera1.Add("Animales");
                        bandera2.Add(NamesFotosAnimales[indexA[i / 2]]);
                        if (i > 2)
                        {
                            ImgAnt = bandera2[(Pi / 2) - 1];
                            objeto_GES.DBWrite();
                        }
                        i++;
                        AleatoriRep--;
                        NamesFotos += NamesFotosAnimales[indexA[(i / 2)-1]] + "|";
                    }
                    else if (i / 2 >= NEstimulos)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                    {
                        ImgAnt = bandera2[(Pi / 2) - 1];
                        objeto_GES.DBWrite();
                        objeto_NB.DBWrite();
                        SceneManager.LoadScene("Lobby");
                    }
                }

            }
        }
    }
    /*
    int[] RandomNums(int cantidad, int maxval)
    {
        int[] index = new int[maxval];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = UnityEngine.Random.Range(0, maxval);
        }
        return index;
    }
    */
    int[] RandomNums(int cantidad, int maxval)
    {
        int aux;
        int[] index = new int[cantidad];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = UnityEngine.Random.Range(1, maxval);
        }
        for (int i = 0; i < cantidad - 1; i++)
        {
            if (index[i] == index[i + 1])
            {
                aux = index[i];
                index[i] = index[cantidad - 1];
                index[cantidad - 1] = index[i];
            }
        }
        return index;
    }
    public static void TiempoRespuesta()
    {
        auxtiempo[Pi / 2] = tiempoG;
        G_tiempo[Pi / 2] = tiempoG - auxtiempo[(Pi / 2) - 1] - DtiempoS;
    }
    public static void TiempoErrores()
    {
        GE_tiempo[e] = tiempoG - auxtiempo[(Pi / 2) - 1] - DtiempoS;
        e++;
    }

}

/*en este código hay que programar que se pongan categorías aleatorias en pantalla, que se diga el nombre de la categoria con sintesis de voz
y finalmente el reconocimiento de voz y los puntajes. Este código si cambia radicalmente en comparación con el del otro entrenamiento
pues la lógica para este entrenamiento es diferente*/

/*tiempo -= Time.deltaTime;
tiempoText.text = "" + tiempo.ToString("f0");
if (tiempo < 1 && bandera)
{
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);                        
i++;
tiempo = 2.0f;
}
if (i == NamesFotosAnimales.Count)
{
bandera = false;
Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/fresa");
}*/
/*
 
     public void Update () {
        //index = RandomNums(NamesFotosAnimales.Count,NamesFotosAnimales.Count);
        tiempo -= Time.deltaTime;
        tiempoText.text = "" + tiempo.ToString("f0");
        Pi = i;
        if (tiempo < 1)
        {
            print("Tiempo cumplido" + cont.ToString());
            tiempo = Dtiempo;
            cont++;
            switch (Variables.VarCategoria)           
            {
                case "Animales":                    
                    Aleatoriedad = UnityEngine.Random.Range(0, maxAleatoriedad);
                    if ((i % 2) == 0)//aqui entra al cuadro negro
                    {
                        AleatoriedadSS = UnityEngine.Random.Range(0, maxAleatoriedadSS);
                        tiempo = DtiempoS;//IA Determina el tiempo que no se muestra estimulo tambien lo puede controlar el agente
                        Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Negro");//Cambiear el directorio el Names y el index                        
                        i++;
                    }
                    else
                    {
                        tiempo = Dtiempo;
                        if (Aleatoriedad <= 30)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                        {
                            if (i/2 < NEstimulos)
                            {
                                ImgAct = NamesFotosFrutas[indexF[i/2]];//cambiar el tipo de indice
                                print(ImgAct);
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[indexF[i/2]]);//Cambiear el directorio el Names y el index
                                bandera1.Add("Frutas");
                                bandera = true;
                                i++;
                            }
                            else if (i/2 >= NEstimulos)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                            {
                                if (bandera)
                                {
                                    SceneManager.LoadScene("Lobby");
                                }
                            }
                        }
                        else if (Aleatoriedad > 30 & Aleatoriedad > 60)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                        {
                            if (i/2 < NEstimulos)//cambiar names
                            {
                                ImgAct = NamesFotosProfesiones[indexP[i/2]];//cambiar index
                                print(ImgAct);
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[indexP[i/2]]);//cambiar ruta names e index
                                bandera = true;
                                bandera1.Add("Profesiones");
                                i++;
                            }
                            else if (i/2 >= NEstimulos)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                            {
                                if (bandera)
                                {
                                    SceneManager.LoadScene("Lobby");
                                }
                            }
                        }
                        else
                        {
                            if (i/2 < NEstimulos)
                            {
                                ImgAct = NamesFotosAnimales[indexA[i/2]];
                                print(ImgAct);
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[indexA[i/2]]);
                                bandera = true;
                                bandera1.Add("Animales");
                                i++;
                            }
                            else if (i/2 >= NEstimulos)//esto toca controlarlo no con el count de NamesFotosAnimales sino con otro parametro que controlara el ia
                            {
                                SceneManager.LoadScene("Lobby");
                            }
                        }
                    }
                    break;                   
                case "Frutas":
                    //bandera1 = Variables.VarCategoria;
                    if (UnityEngine.Random.Range(0, 100) == 1)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < NamesFotosFrutas.Count)
                        {
                            if (i == 0)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosFrutas[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[i]);
                                imgpos = NamesFotosFrutas[indexF[i]];
                                i++;
                            }
                            if (bandera)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosFrutas[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[i]);
                                imgpos = NamesFotosFrutas[indexF[i]];
                                bandera = false;
                                i++;
                            }

                            else if (i >= NamesFotosAnimales.Count)
                            {
                                if (bandera)
                                {
                                    SceneManager.LoadScene("Menu1");
                                }
                            }
                        }
                    }
                    break;
                case "Profesiones":
                    //bandera1 = Variables.VarCategoria;
                    if (UnityEngine.Random.Range(0, 100) == 1)//IA esto lo puede modificar un agente. En principio equivale a una mutacion del 1%
                    {
                        if (i < NamesFotosProfesiones.Count)
                        {
                            if (i == 0)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosProfesiones[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[i]);
                                imgpos = NamesFotosProfesiones[indexP[i]];
                                i++;
                            }
                            if (bandera)
                            {
                                //ImgAct = NamesFotosAnimales[index[i]];
                                //Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[index[i]]);
                                ImgAct = NamesFotosProfesiones[i];
                                Boton.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[i]);
                                imgpos = NamesFotosProfesiones[indexP[i]];
                                bandera = false;
                                i++;
                            }
                        }
                        else if (i >= NamesFotosAnimales.Count)
                        {
                            if (bandera)
                            {
                                SceneManager.LoadScene("Menu1");
                            }
                        }
                    }
                        break;                    
            }
        }
     */
