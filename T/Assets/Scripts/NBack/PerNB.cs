﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;
using SpeechLib;
using System.Xml;
using System.IO;

public class PerNB : MonoBehaviour {
    // Verificar métodos de entrada
    public bool TMouse;
    public bool TTactil;
    public bool TVoz;
    // Actualizar puntaje y actualizar color botones
    public string imgpos1;
    public List<string> posimg = new List<string>() { };
    public static int index;
    public static int acierto;
    public static int error; //variable para contar los desaciertos. No se muestra para evitar producir un efecto negativo en el entrenado.
    public GameObject LeerVars;
    public GameObject otro;
    ActNB Variable; //este toca cambiarlo en todos los codigos donde se reutilice
    public Text FinNivel;
    GameObject Global;
    GameController Variables;
    public Color colorV = new Color(0.1F, 0.5F, 0F, 0.5F);
    public Color colorR = new Color(0.5F, 0.1F, 0F, 0.5F);
    public int Nivel;
    //public Button Buttons;
    public Button Button0;
    //Botones Animal Fruta Profesion
    public Button Si;
    public Button No;
    //public Button Profesion;
    //Reconocimiento de voz
    KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public Text PalaDich;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "aguila", "avestruz", "ballena", "burro", "caballo", "caiman", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "raton", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };
    private SpVoice voice;
    public Text Instrucciones;
    //comentarios
    public Comentarios mensaje;
    string frase;

    public static string aciertosyerrores;
    public static string palabraserror;
    // Use this for initialization
    void Start()
    {
        //Inicializar variables
        acierto = 0;
        error = 0;
        // Leer Variables de control
        LeerVars = GameObject.Find("ActNB");//este toca cambiarlo en todos los codigos donde se reutilice//1esto se puede hacer en una sola linea
        Variable = LeerVars.GetComponent<ActNB>();//este toca cambiarlo en todos los codigos donde se reutilice1
        Global = GameObject.Find("GameController");//2esto se puede hacer en una sola linea
        Variables = Global.GetComponent<GameController>();//2
        imgpos1 = Variable.imgpos;
        // Verificar métodos de entrada habilitados
        TMouse = Variables.VarTMouse;
        TTactil = Variables.VarTTactil;
        TVoz = Variables.VarTVoz;

        if (TMouse)//1esto se puede considerar quitarse o programarse diferente.
        {
            print("Está activo el método Mouse");//aqui toca hacer una confirmacion
            Si.onClick.AddListener(() => ActPuntaje1("si", 1));
            //No.onClick.AddListener(() => ActPuntaje1("no", 1));        
        }//1hasta aca
        if (TTactil)
        {
            print("Está activo el método táctil");
            Si.onClick.AddListener(() => ActPuntaje1("igual", 1));
            //No.onClick.AddListener(() => ActPuntaje1("no", 1));            
        }
        if (TVoz)
        {
            print("Esta activo el método voz");
            keywords.Add("igual", () =>
            {
                SiCalled();
            });
            /*keywords.Add("no", () =>
            {
                NoCalled();
            });*/
            KeywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
            KeywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
            KeywordRecognizer.Start();
        }

        aciertosyerrores = "";
        palabraserror = "";

        //voz
        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;  //   Rate (no xml)     
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ActPuntaje1(string Nombre, int iscorrect)/*la función más importante de este script debe ser conectarse con la base de datos para almacenar el dato*/
    {
        if (ActNB.bandera == true)
        {
            ActNB.TiempoRespuesta();
            print("La imagen es:" + ActNB.ImgAct);//este hay que modificarlo en todas las partes donde se reutilice este codigo
                                                  //ActC.bandera = true;//este hay que modificarlo en todas las partes donde se reutilice este codigo//No se para que puse esta nota: es necesario redeclarar esta variable por una variable tipo string
            if (Nombre == "si")
            {
                if (ActNB.Pi > 2)
                {
                    if (ActNB.bandera1[ActNB.Pi / 2] == ActNB.bandera1[(ActNB.Pi / 2) - 1])//cambiar en los codigos donde se reutilice
                    {
                        ActNB.bandera = false;
                        mensaje = new Comentarios("p");
                        frase = mensaje.respuesta("p");
                        print("Correcto");
                        aciertosyerrores = aciertosyerrores + "|1";
                        acierto++;
                        Debug.Log(acierto);
                        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + frase
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    }
                    else
                    {
                        ActNB.bandera = false;
                        mensaje = new Comentarios("n");
                        frase = mensaje.respuesta("n");
                        print("Incorrecto");
                        ActNB.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + "Dijo igual|";

                        error++;
                        Debug.Log(error);
                        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                    + frase
                                    + "</speak>",
                                    SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    }
                }
            }
            if (Nombre == "No")//esto es para los errores/aciertos cuando no hay respuesta de la persona
            {
                if (ActNB.Pi > 2)
                {

                    if (ActNB.bandera1[ActNB.Pi / 2] == ActNB.bandera1[(ActNB.Pi / 2) - 1])//cambiar en los codigos donde se reutilice
                    {
                        ActNB.bandera = false;
                        mensaje = new Comentarios("p");
                        frase = "La categoría de las imágenes era igual";
                        print("Incorrecto");
                        ActNB.TiempoErrores();
                        aciertosyerrores = aciertosyerrores + "|0";
                        palabraserror = palabraserror + "No responde|";

                        error++;
                        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + frase
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    }
                    else if (ActNB.bandera1[ActNB.Pi / 2] != ActNB.bandera1[(ActNB.Pi / 2) - 1])//cambiar en los codigos donde se reutilice
                    {
                        ActNB.bandera = false;
                        print("Correcto");
                        aciertosyerrores = aciertosyerrores + "|1";
                        acierto++;
                    }

                }
            }
        }
    }
    public void DecirNombre()
    {
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                        + ActNB.ImgAct
                                                        + "</speak>",
                                                        SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
    }
    /*    IEnumerator Pere()
        {
            Debug.Log(Time.time);
            yield return new WaitForSeconds(10);
            Debug.Log(Time.time);
            Debug.Log("Just waited 10 second");
        }*/
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    //funciones para el conteo de los puntos
    void SiCalled()
    {
        print("Dijiste si");
        //PalaDich.text += ", si";
        //imgpos1 = Variable.imgpos;
        ActPuntaje1("si", 1);
        /* if (imgpos1 == "animal")
         {
             ActPuntaje1("si", 1);
         }
         else
         {
             ActPuntaje1("si", 0);
         }*/
    }
   /* void NoCalled()
    {
        print("Dijiste No");
        //PalaDich.text += ", No";
        //imgpos1 = Variable.imgpos;
        ActPuntaje1("no", 1);
        /*if (imgpos1 == "fruta")
        {
            ActPuntaje1("no", 1);
        }
        else
        {
            ActPuntaje1("no", 0);
        }
    }*/
}
