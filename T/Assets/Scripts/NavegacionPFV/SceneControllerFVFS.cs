﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class SceneControllerFVFS : MonoBehaviour {
    public string palabras, nombreUsuario, contrasena, escena;
    public int idUsuario, prueba=0;
    public InputField pal; // esto conectarlo con la escena con el campo texto donde quedan las palabras.
    public bool bandera = true;
    private void Awake()
    {
        nombreUsuario = PlayerPrefs.GetString("nombreUsuario");
        idUsuario = PlayerPrefs.GetInt("idUsuario");
        contrasena = PlayerPrefs.GetString("contrasena");
        escena = PlayerPrefs.GetString("escena");
    }
    void Start()
    {

    }
    // Update is called once per frame

    void Update()
    {
        
    }
    public void GuardarPalabras()
    {
        PlayerPrefs.SetString("escena", "PFVFS");
        escena = PlayerPrefs.GetString("escena");
        palabras = pal.text;
        StartCoroutine(GuardarP());
        SceneManager.LoadScene("Lobby");
    }
    public void LOBBY()
    {
        SceneManager.LoadScene("Lobby");
    }
    IEnumerator GuardarP()
    {
        String Hoy = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //palabras = GameObject.Find("Palabras").GetComponent<UnityEngine.UI.Text>().text;
        WWW conexion = new WWW("http://localhost/CTA/gpfvsa.php?uss=" + idUsuario + "&pal=" + palabras + "&escn=" + escena + "&fec=" + Hoy);
        yield return (conexion);
        if (conexion.text == "203")
        {
            Debug.Log("Palabras y comentarios guardados");
        }
        else
        {
            print(conexion.text);
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
}
