﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using UnityEngine.UI;
using System;

public class Recognition : MonoBehaviour {
    KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public Text PalaDich;
    List<string> NamesFotosAnimales = new List<string>() { "bufalo", "caballo", "camello", "conejo", "delfin", "elefante", "gallina", "gato", "guacamaya", "jrafa", "leon", "lobo",  "loro",  "marrano", "oso", "oveja", "pajaro", "paloma", "papagayo", "pato",  "perro", "pescado", "pisco", "pollo", "raton", "rinoceronte", "tigre", "vaca1", "vaca2" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "cereza", "fresa", "guanabana", "guayaba", "lulo", "mandarina", "mango", "manzana", "mora", "naranja", "papaya", "pera", "pina", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "enfermera", "medica", "medico", "policia", "profesor" };
    // Use this for initialization
    void Start() {
        PalaDich.text += "Construyendo Diccionario";
        for (int i = 0; i < NamesFotosAnimales.Count; i++)
        {
            PalaDich.text += NamesFotosAnimales[i];
        }
        for (int i = 0; i < NamesFotosFrutas.Count; i++)
        {
            PalaDich.text += NamesFotosFrutas[i];
        }
        for (int i = 0; i < NamesFotosProfesiones.Count; i++)
        {
            PalaDich.text += NamesFotosProfesiones[i];
        }
        PalaDich.text += NamesFotosAnimales;
        keywords.Add("go", () =>
         {
             GoCalled();
         });
        keywords.Add("bufalo", () =>
        {
            BufaloCalled();
        });
        keywords.Add("caballo", () =>
        {
            CaballoCalled();
        });
        keywords.Add("camello", () =>
        {
            CamelloCalled();
        });
        keywords.Add("conejo", () =>
        {
            ConejoCalled();
        });
        keywords.Add("delfin", () =>
        {
            DelfinCalled();
        });
        keywords.Add("elefante", () =>
        {
            ElefanteCalled();
        });
        keywords.Add("gallina", () =>
        {
            GallinaCalled();
        });
        keywords.Add("gato", () =>
        {
            GatoCalled();
        });
        keywords.Add("guacamaya", () =>
        {
            GuacamayaCalled();
        });
        keywords.Add("jirafa", () =>
        {
            JirafaCalled();
        });
        keywords.Add("leon", () =>
        {
            LeonCalled();
        });
        keywords.Add("lobo", () =>
        {
            LoboCalled();
        });
        keywords.Add("loro", () =>
        {
            LoroCalled();
        });
        keywords.Add("marrano", () =>
        {
            MarranoCalled();
        });
        keywords.Add("oso", () =>
        {
            OsoCalled();
        });
        keywords.Add("oveja", () =>
        {
            OvejaCalled();
        });
        keywords.Add("pajaro", () =>
        {
            PajaroCalled();
        });
        keywords.Add("paloma", () =>
        {
            PalomaCalled();
        });
        keywords.Add("papagayo", () =>
        {
            PapagayoCalled();
        });
        keywords.Add("pato", () =>
        {
            PatoCalled();
        });
        keywords.Add("perro", () =>
        {
            PerroCalled();
        });
        keywords.Add("pescado", () =>
        {
            PescadoCalled();
        });
        keywords.Add("pisco", () =>
        {
            PiscoCalled();
        });
        keywords.Add("pollo", () =>
        {
            PolloCalled();
        });
        keywords.Add("raton", () =>
        {
            RatonCalled();
        });
        keywords.Add("rinoceronte", () =>
        {
            RinoceronteCalled();
        });
        keywords.Add("tigre", () =>
        {
            TigreCalled();
        });
        keywords.Add("vaca", () =>
        {
            VacaCalled();
        });
        //frutas
        keywords.Add("aguacate", () =>
        {
            AguacateCalled();
        });
        keywords.Add("banano", () =>
        {
            BananoCalled();
        });
        keywords.Add("cereza", () =>
        {
            CerezaCalled();
        });
        keywords.Add("fresa", () =>
        {
            FresaCalled();
        });
        keywords.Add("guanabana", () =>
        {
            GuanabanaCalled();
        });
        keywords.Add("guayaba", () =>
        {
            GuayabaCalled();
        });
        keywords.Add("lulo", () =>
        {
            LuloCalled();
        });
        keywords.Add("mandarina", () =>
        {
            MandarinaCalled();
        });
        keywords.Add("mango", () =>
        {
            MangoCalled();
        });
        keywords.Add("manzana", () =>
        {
            ManzanaCalled();
        });
        keywords.Add("mora", () =>
        {
            MoraCalled();
        });
        keywords.Add("Naranja", () =>
        {
            NaranjaCalled();
        });
        keywords.Add("papaya", () =>
        {
            PapayaCalled();
        });
        keywords.Add("pera", () =>
        {
            PeraCalled();
        });
        keywords.Add("piña", () =>
        {
            PiñaCalled();
        });
        keywords.Add("zapote", () =>
        {
            ZapoteCalled();
        });
        //Profesiones
        keywords.Add("abogado", () =>
        {
            AbogadoCalled();
        });
        keywords.Add("enfermera", () =>
        {
            EnfermeraCalled();
        });
        keywords.Add("medica", () =>
        {
            MedicaCalled();
        });
        keywords.Add("medico", () =>
        {
            MedicoCalled();
        });
        keywords.Add("policia", () =>
        {
            PoliciaCalled();
        });
        keywords.Add("profesor", () =>
        {
            ProfesorCalled();
        });
        keywords.Add("hola", () =>
        {
            HolaCalled();
        });

        PalaDich.text += "Diccionario Construido";
        KeywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        KeywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        PalaDich.text += "Iniciando reconocimiento";
        KeywordRecognizer.Start();
	}


    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
	// Update is called once per frame
	void Update () {
		
	}

    void GoCalled()
    {
        print("Dijiste Go");
        PalaDich.text += ", go";
    }
    void HolaCalled()
    {
        print("Dijiste Hola");
        PalaDich.text += ", Hola";
    }
    void CaballoCalled()
    {
        print("Dijiste caballo");
        PalaDich.text += ", caballo";
    }
    void PerroCalled()
    {
        print("Dijiste perro");
        PalaDich.text += ", perro";
    }
    void GatoCalled()
    {
        print("Dijiste gato");
        PalaDich.text += ", gato";
    }
    void BufaloCalled()
    {
        print("Dijiste bufalo");
        PalaDich.text += ", bufalo";
    }

    private void GallinaCalled()
    {
        print("Dijiste gallina");
        PalaDich.text += ", gallina";
    }

    private void ElefanteCalled()
    {
        print("Dijiste elefante");
        PalaDich.text += ", elefante";
    }

    private void DelfinCalled()
    {
        print("Dijiste delfin");
        PalaDich.text += ", delfin";
    }

    private void ConejoCalled()
    {
        print("Dijiste conejo");
        PalaDich.text += ", conejo";
    }

    private void CamelloCalled()
    {
        print("Dijiste camello");
        PalaDich.text += ", camello";
    }

    private void GuacamayaCalled()
    {
        print("Dijiste guacamaya");
        PalaDich.text += ", guacamaya";
    }

    private void JirafaCalled()
    {
        print("Dijiste jirafa");
        PalaDich.text += ", jirafa";
    }

    private void LeonCalled()
    {
        print("Dijiste leon");
        PalaDich.text += ", leon";
    }

    private void LoboCalled()
    {
        print("Dijiste lobo");
        PalaDich.text += ", lobo";
    }

    private void LoroCalled()
    {
        print("Dijiste loro");
        PalaDich.text += ", loro";
    }

    private void MarranoCalled()
    {
        print("Dijiste marrano");
        PalaDich.text += ", marrano";
    }

    private void OsoCalled()
    {
        print("Dijiste oso");
        PalaDich.text += ", oso";
    }

    private void OvejaCalled()
    {
        print("Dijiste oveja");
        PalaDich.text += ", oveja";
    }

    private void PajaroCalled()
    {
        print("Dijiste pajaro");
        PalaDich.text += ", pajaro";
    }

    private void PalomaCalled()
    {
        print("Dijiste paloma");
        PalaDich.text += ", paloma";
    }

    private void PapagayoCalled()
    {
        print("Dijiste papagayo");
        PalaDich.text += ", papagayo";
    }

    private void PatoCalled()
    {
        print("Dijiste pato");
        PalaDich.text += ", pato";
    }

    private void PescadoCalled()
    {
        print("Dijiste pescado");
        PalaDich.text += ", pescado";
    }

    private void PiscoCalled()
    {
        print("Dijiste pisco");
        PalaDich.text += ", pisco";
    }

    private void PolloCalled()
    {
        print("Dijiste pollo");
        PalaDich.text += ", pollo";
    }

    private void RatonCalled()
    {
        print("Dijiste raton");
        PalaDich.text += ", raton";
    }

    private void RinoceronteCalled()
    {
        print("Dijiste rinoceronte");
        PalaDich.text += ", rinoceronte";
    }

    private void TigreCalled()
    {
        print("Dijiste tigre");
        PalaDich.text += ", tigre";
    }

    private void VacaCalled()
    {
        print("Dijiste vaca");
        PalaDich.text += ", vaca";
    }

    private void BananoCalled()
    {
        print("Dijiste banano");
        PalaDich.text += ", banano";
    }

    private void CerezaCalled()
    {
        print("Dijiste cereza");
        PalaDich.text += ", cereza";
    }

    private void FresaCalled()
    {
        print("Dijiste fresa");
        PalaDich.text += ", fresa";
    }

    private void GuanabanaCalled()
    {
        print("Dijiste Guanabana");
        PalaDich.text += ", guanabana";
    }

    private void GuayabaCalled()
    {
        print("Dijiste Guayaba");
        PalaDich.text += ", guayaba";
    }
    private void LuloCalled()
    {
        print("Dijiste lulo");
        PalaDich.text += ", lulo";
    }

    private void MandarinaCalled()
    {
        print("Dijiste Mandarina");
        PalaDich.text += ", mandarina";
    }

    private void MangoCalled()
    {
        print("Dijiste ManGo");
        PalaDich.text += ", Mango";
    }

    private void ManzanaCalled()
    {
        print("Dijiste Manzana");
        PalaDich.text += ", Manzana";
    }

    private void MoraCalled()
    {
        print("Dijiste Mora");
        PalaDich.text += ", mora";
    }

    private void NaranjaCalled()
    {
        print("Dijiste Naranja");
        PalaDich.text += ", naranja";
    }

    private void PapayaCalled()
    {
        print("Dijiste papaya");
        PalaDich.text += ", papaya";
    }

    private void PeraCalled()
    {
        print("Dijiste pera");
        PalaDich.text += ", pera";
    }

    private void PiñaCalled()
    {
        print("Dijiste piña");
        PalaDich.text += ", piña";
    }

    private void ZapoteCalled()
    {
        print("Dijiste zapote");
        PalaDich.text += ", zapote";
    }

    private void AguacateCalled()
    {
        print("Dijiste aguacate");
        PalaDich.text += ", aguacate";
    }

    private void ProfesorCalled()
    {
        print("Dijiste profesor");
        PalaDich.text += ", profesor";
    }

    private void PoliciaCalled()
    {
        print("Dijiste policia");
        PalaDich.text += ", policia";
    }

    private void MedicoCalled()
    {
        print("Dijiste medico");
        PalaDich.text += ", medico";
    }

    private void MedicaCalled()
    {
        print("Dijiste medica");
        PalaDich.text += ", medica";
    }

    private void EnfermeraCalled()
    {
        print("Dijiste Enfermera");
        PalaDich.text += ", enfermera";
    }

    private void AbogadoCalled()
    {
        print("Dijiste abogado");
        PalaDich.text += ", abogado";
    }
}
