﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ControlMenus : MonoBehaviour {
    public GameObject nuevoUsuario;
    public GameObject panel;
    private Animator AnimacionNU;//animacion panel nuevo usuario
    private Animator AnimacionP;//animacion panel inicio
    EventSystem system;
    private void Awake()
    {
        AnimacionNU = nuevoUsuario.GetComponent<Animator>();
        AnimacionP = panel.GetComponent<Animator>();
        nuevoUsuario.SetActive(false);
    }
    void Start () {
        system = EventSystem.current;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

            if (next != null)
            {

                InputField inputfield = next.GetComponent<InputField>();
                if (inputfield != null)
                    inputfield.OnPointerClick(new PointerEventData(system));  //if it's an input field, also set the text caret

                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
            }
            //else Debug.Log("next nagivation element not found");

        }
    }


    void Iniciar()
    {
        SceneManager.LoadScene("Inicio");
    }
    //Esto es para activar las animaciones que sacan y entran los menus
    public void mostrarNuevoUsuario()
    {
        nuevoUsuario.SetActive(true);
        AnimacionP.SetBool("Close", true);
        AnimacionNU.SetBool("Open",true);
        panel.SetActive(false);
    }
    public void mostrarPanel()
    {
        panel.SetActive(true);
        AnimacionP.SetBool("Open", true);
        AnimacionNU.SetBool("Close", true);
        nuevoUsuario.SetActive(false);
    }

    public void Cerrar()
    {
        Application.Quit();
    }
}
