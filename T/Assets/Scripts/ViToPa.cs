﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViToPa : MonoBehaviour {
    public GameObject EnPa;
    public GameObject ToPa;
    // Use this for initialization
    void Start () {
        PlayerPrefs.GetString("Juego");
        EnPa.SetActive(true);
        ToPa.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {       
        if (Input.GetKeyDown("q"))
        {
            if (ToPa.activeSelf)
            {               
                ToPa.SetActive(false);                
            }
            else
            {
                ToPa.SetActive(true);
            }                          
        }
        if (Input.GetKeyDown("w"))
        {
            if (EnPa.activeSelf)
            {
                EnPa.SetActive(false);
            }
            else
            {
                EnPa.SetActive(true);
            }
        }
    }
}
