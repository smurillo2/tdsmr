﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GestionDB : MonoBehaviour
{
    public InputField txtUsuario;
    public InputField txtContrasena;
    public InputField txtNombre;
    public InputField txtPApellido;
    public InputField txtSApellido;
    public InputField txtCedula;
    public InputField txtEdad;
    public Dropdown   Lateralidad;
    public string txtLateralidad;
    List<string> Later = new List<string>() { "Lateralidad", "Diestro", "Zurdo", "Ambidiestro"};
    public InputField txtOcupacion;
    public Dropdown   Escolaridad;
    public string txtEscolaridad;
    List<string> Escol = new List<string>() { "Escolaridad", "Analfabeta", "Sabe Leer y Escribir", "Primaria Incompleta", "Primaria Completa", "Secundaria Incompleta", "Secundaria Completa", "Pregrado","Especialización", "Maestría", "Doctorado" };
    public Dropdown   Sexo;
    public string txtSexo;
    List<string> Sex = new List<string>() { "Sexo", "Masculino", "Femenino" };
    DateTime Hoy;
    public string Usuario;
    public string nombreUsuario;
    public int scoreUsuario;
    public int idUsuario;
    public string contrasena;
    public bool sesioniniciada;
    //public static GestionDB singleton;
    GameObject aux;
    public string palabras;
    public string comentarios;
    public string conexion1;
    public Text Tbase;
    /*Respuestas WEB
     400 - No establece conexion
     401 - No encuentra datos
     402 - El usuario ya existe
     200 - Datos encontrados
     201 - Usuario registrado
     202 - Puntaje actualizado
     203 - Palabras y comentarios guardados
     */
    public void Awake()
    {
        //Dado que utilizaré playerPrefs me olvidaré del uso del DontDestroy hasta que le encuentre una utilidad.
        //Creo que esto sirve si lo que se necesita es conservar objetos, a la hora de guardar datos es mejor el playerPrefs 
        //y en cuanto a las funciones toca usar un script que este en escena pues no reconoce los que estan en objetos dontDestroy ensayé 
        //incluso con objetos en el root y no me funcionó, quiza tenga algo mas por mirar pero de momento creo que la mejor opcion es con los player Prefs
        /*
        DontDestroyOnLoad(transform.gameObject);
        if (singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(gameObject);
        }*/
    }
    public void Start()
    {
        //DontDestroyOnLoad(this.gameObject);
        //DDLList();
        //DDEList();
        //DDSList();
    }
    public void DDLList()
    {
        Lateralidad.AddOptions(Later);
    }
    public void Dropdown_Lat_IndexChanged(int index)
    {
        txtLateralidad = Later[index];
    }
    public void DDEList()
    {
        Escolaridad.AddOptions(Escol);
    }
    public void Dropdown_Esc_IndexChanged(int index)
    {
        txtEscolaridad = Escol[index];
    }
    public void DDSList()
    {
        Sexo.AddOptions(Sex);
    }
    public void Dropdown_Sex_IndexChanged(int index)
    {
        txtSexo = Sex[index];
    }
    public void IniciarSesion()
    {
        StartCoroutine(Login());
    }

    public void RegistrarUsuario()
    {
        StartCoroutine(Registro());
    }

    public void ActualizarPuntaje(int nScore)
    {
        StartCoroutine(Actualizar(nScore));
    }
    public void GuardarPalabras()
    {
        StartCoroutine(GuardarP());
    }
    IEnumerator Datos()
    {
        WWW conexion = new WWW("http://localhost/CTA/datos.php?uss=" + txtUsuario.text);
        yield return (conexion);
        //Debug.Log(conexion.text);
        if (conexion.text == "401")
        {
            Tbase.text = "Usuario o contraseña incorrectos";
            print("Usuario o contraseña incorrectos");
        }
        else
        {
            string[] ndatos = conexion.text.Split('|');
            if (ndatos.Length != 5)
            {
                Tbase.text = "Error en el split";
                print("Error en el split");
            }
            else
            {
                Usuario = ndatos[0];
                nombreUsuario = ndatos[1];
                scoreUsuario = int.Parse(ndatos[2]);
                idUsuario = int.Parse(ndatos[3]);
                contrasena = ndatos[4];
                sesioniniciada = true;
                PlayerPrefs.SetString("Usuario", Usuario);
                PlayerPrefs.SetString("nombreUsuario", nombreUsuario);
                PlayerPrefs.SetInt("scoreUsuario", scoreUsuario);
                PlayerPrefs.SetInt("idUsuario", idUsuario);
                PlayerPrefs.SetString("contrasena", contrasena);
                PlayerPrefs.SetString("escena", "GestionDB");
                SceneManager.LoadScene("Lobby");
                //StartCoroutine(Actualizar(0));
            }
        }
    }
    IEnumerator Login()
    {
        WWW conexion = new WWW("http://localhost/CTA/login.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text);
        yield return (conexion);
        //Debug.Log(conexion.text);
        if (conexion.text == "200")
        {
            print("El usuario si existe");
            StartCoroutine(Datos());
        }
        else if (conexion.text == "401")
        {
            Tbase.text = "Usuario o contraseña incorrectos";
            print("Usuario o contraseña incorrectos");
        }
        else
        {
            Tbase.text = "Error en la conexión con la base de datos";
            print("Error en la conexión con la base de datos");
        }
    }
    IEnumerator Registro()
    {
        String Hoy = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //string[] words = Hoy.Split('/');
        //Hoy = "";
        // Hoy += words[0] + "-" + words[1] + "-" + words[2];
        //Debug.Log(Hoy);
        //string[] words1 = Hoy.Split(' ');
        //Hoy = "";
        // Hoy += words1[0] + "%20" + words1[1];
        //Hoy += "%2000:00:00";
        //Debug.Log(Hoy);
        //WWW conexion = new WWW("http://localhost/CTA/registro.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text + "&nom=" + txtNombre.text + "&pap=" + txtPApellido.text + "&sap=" + txtSApellido.text + "&ced=" + txtCedula.text + "&eda=" + txtEdad.text + "&lat=" + txtLateralidad + "&ocu=" + txtOcupacion.text + "&esc=" + txtEscolaridad + "&sex=" + txtSexo + "&fec=2019-02-06%2017:42:35" );
        conexion1 = "http://localhost/CTA/registro.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text + "&nom=" + txtNombre.text + "&pap=" + txtPApellido.text + "&sap=" + txtSApellido.text + "&ced=" + txtCedula.text + "&eda=" + txtEdad.text + "&lat=" + txtLateralidad + "&ocu=" + txtOcupacion.text + "&esc=" + txtEscolaridad + "&sex=" + txtSexo + "&fec=" + Hoy;
        WWW conexion = new WWW("http://localhost/CTA/registro.php?uss=" + txtUsuario.text + "&pss=" + txtContrasena.text + "&nom=" + txtNombre.text + "&pap=" + txtPApellido.text + "&sap=" + txtSApellido.text + "&ced=" + txtCedula.text + "&eda=" + txtEdad.text + "&lat=" + txtLateralidad + "&ocu=" + txtOcupacion.text + "&esc=" + txtEscolaridad + "&sex=" + txtSexo + "&fec=" + Hoy);
        
        //WWW conexion = new WWW(conexion1);
        yield return (conexion);
        Debug.Log(conexion.text);
        if (conexion.text == "402")
        {
            Tbase.text = "El usuario ya existe";
            Debug.LogError("El usuario ya existe");
        }
        else if (conexion.text == "201")
        {
            nombreUsuario = txtUsuario.text;
            scoreUsuario = 0;
            sesioniniciada = true;
        }
        else
        {
            print(conexion.text);
            Tbase.text = "Error en la conexión con la base de datos";
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
    IEnumerator Actualizar(int nScore)
    {
        Debug.Log("nuevo puntaje"+scoreUsuario.ToString());
        //WWW conexion = new WWW("http://localhost/CTA/update.php?uss=" + txtUsuario.text + "&nScore=" + (nScore+10).ToString());
        WWW conexion = new WWW("http://localhost/CTA/update.php?uss=" + txtUsuario.text + "&nScore=" + nScore + 10.ToString());//con esto se corrige el registro de la fecha
        yield return (conexion);
        Debug.Log(conexion.text);
        if (conexion.text == "202")
        {
            Debug.Log("Valor actualizado");
            scoreUsuario = nScore;
        }
        else
        {
            print(conexion.text);
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
    IEnumerator GuardarP()
    {
        String Hoy = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        palabras = comentarios = GameObject.Find("Palabras").GetComponent<UnityEngine.UI.Text>().text;
        conexion1 = "http://localhost/CTA/gpfvsa.php?uss=" + idUsuario + "&pal=" + palabras + "&fec=" + Hoy;
        WWW conexion = new WWW("http://localhost/CTA/gpfvsa.php?uss=" + idUsuario + "&pal=" + palabras + "&fec=" + Hoy);
        yield return (conexion);
        if (conexion.text == "203")
        {
            Debug.Log("Palabras y comentarios guardados");
        }
        else
        {
            print(conexion.text);
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
}
