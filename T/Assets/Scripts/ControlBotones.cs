﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ControlBotones : MonoBehaviour {
	void Start () {
	}
	// Update is called once per frame
	void Update () {
    }
    public void LOBBY()
    {
        SceneManager.LoadScene("Lobby");
    }
    public void PRV()
    {
        SceneManager.LoadScene("PRV");
    }
    public void LSFVSA()
    {
        SceneManager.LoadScene("PFVSA");
    }
    public void LSFVSF()
    {
        SceneManager.LoadScene("PFVSF");
    }
    public void LSFVSP()
    {
        SceneManager.LoadScene("PruebasFVSP");
    }
    public void LSFVFA()
    {
        SceneManager.LoadScene("PruebasFVFA");
    }
    public void LSFVFF()
    {
        SceneManager.LoadScene("PruebasFVFF");
    }
    public void LSFVFS()
    {
        SceneManager.LoadScene("PruebasFVFS");
    }
    public void LSFVFM()
    {
        SceneManager.LoadScene("PruebasFVFM");
    }
    public void salir()
    {
        SceneManager.LoadScene("login");
    }
}
