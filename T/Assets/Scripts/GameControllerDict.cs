﻿using UnityEngine;
using System.Collections;
//using System.Diagnostics;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using UnityEngine.UI;
using System.Threading;
using SpeechLib;
using System.Xml;
using System.IO;

public class GameControllerDict : MonoBehaviour {
    private DictationRecognizer dictationRecognizer;
    public Text dictado;

    //private SpVoice voice;
    public Text TiempoFV; // texto tiempo duración prueba de FV
    public float timeFV = -10; //tiempo duración prueba de FV
    AudioSource Beep;
    public AudioClip beep;
    //KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public InputField PalaDich;
    //public Text prueba;
    List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "constructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };
    public GameObject Nodo;
    //Vector3 posicion;
    //Quaternion rotacion;
    //Graficaci[on
    public string[] PalaDichSep;
    public Vector3[] palarep = new Vector3[100];
    private LineRenderer linea;
    public float g = 1, n = 0, m = 0, enx = 0, eny = 0, radiocuadr = 10000;
    float w = 0;
    bool v = true, z = false;
    float t = 0, esp = 5;
    Vector3 guarda;
    bool bandera = true;//estas banderas son necesarias para evitar que el codigo pase dos veces por el mismo if dado que en el update pueden ocurrir varias pasadas en las que el tiempo es el mismo
    bool bandera1 = true;

    public float r = 10;
    public float a = 0;
    public float b = 0;
    float AcuAng = 0;//Acumulacion angular
    public int Npun = 5;
    Vector3[] positions = new Vector3[100];
    public int tamano = 65;
    public int X_pos = 505;
    public int Y_pos = 318;
    public string escena;
    public string voz;
    // Use this for initialization
    // measures
    public int NTP = 0;//Numero total de palabras dichas.
    public int NTPC = 0;//Numero total de palabras correctas.
    public int TMC = 0;//Tamano medio de los clusters. Determinado como el promedio del tamano de los clusters.
    public int NTS = 0;//Numero total de switches
    //sinesis de voz
    private SpVoice voice;
    public Text TiempoI;//texto tiempo instrucción comando de voz
    public float timeI = 7; //tiempo instrucción comando de voz

    void Awake()
    {
        Beep = GetComponent<AudioSource>();
    }
    void Start()
    {
    }
    public void empezar()
    {
        //timeFV = 70;
        TextandVoice();
        //ReconocimientoVoz();
    }
    void TextandVoice()
    {
        // Cambiar las instruciones para el usuario según la categoría seleccionada
        escena = PlayerPrefs.GetString("escena");
        if (escena == "GestionDB")
        {
            string nombre = GameObject.Find("SceneController").GetComponent<SceneControllerPRV>().nombreUsuario;
            timeFV = 30;
            voz = "Hola " + nombre + ", después del sonido, repite por favor: ¡Hoy es un excelente día para entrenar mi fluidez verbal!";
        }
        else if (escena == "PRV")
        {
            timeFV = 67;
            voz = "Después del sonido, tiene un minuto para decir todos los nombres de animales que recuerde";
        }
        else if (escena == "PFVSA")
        {
            timeFV = 67;
            voz = "Después del sonido, tiene un minuto para decir todos los nombres de frutas que recuerde";
        }
        else if (escena == "PFVSF")
        {
            timeFV = 67;
            voz = "Después del sonido, tiene un minuto para decir todos los nombres de profesiones que recuerde";
        }
        else if (escena == "PFVSP")
        {
            timeFV = 69;
            voz = "Después del sonido, tiene un minuto para decir todas las palabras que recuerde que comienzen con la letra: AA, ";
        }
        else if (escena == "PFVFA")
        {
            timeFV = 69;
            voz = "Después del sonido, tiene un minuto para decir todas las palabras que recuerde que comienzen con la letra: F";
        }
        else if (escena == "PFVFF")
        {
            voz = "Después del sonido, tiene un minuto para decir todas las palabras que recuerde que comienzen con la letra: M";
            timeFV = 69;
        }
        else if (escena == "PFVFM")
        {
            timeFV = 69;
            voz = "Después del sonido, tiene un minuto para decir todas las palabras que recuerde que comienzen con la letra: S";
        }
        /*else if (escena == "PFVFS")
            escena = "todos las palabras que recuerde que comienzen con la letra S";*/
        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;  //   Rate (no xml)     
        voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                             + voz
                                            /*+ Instrucciones.text*/
                                            + "</speak>",
                                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
    }

    /* public void ReconocimientoVoz()
     {
         //PhraseRecognitionSystem.Shutdown();    // estos son importantes.. si quieren usar el keywords... tienen que poner estos aca... lo que hace es que apaga el phraserecognition que es el se encarga de los "keywords" y hay que apagarlos.. no se puede usar los dos al mismo tiempo.. por cuestiones de logica.. pero si se pueden usar los dos pero uno iniciando y cerrando el otro.. 
         dictationRecognizer = new DictationRecognizer();
         dictationRecognizer.DictationResult += DictationRecognizer_DictationResult;    //estos son los que tienen que declarar obviamente para  que funcione el dictation
         dictationRecognizer.DictationComplete += DictationRecognizer_DictationComplete;
         dictationRecognizer.Start(); //  Lo que hace aca es que activa la funcon de dictar.. 


         /*dictationRecognizer = new DictationRecognizer();
         //dictationRecognizer.InitialSilenceTimeoutSeconds = 5;  //este es el tiempo de comienzo  
         //dictationRecognizer.AutoSilenceTimeoutSeconds = 20;  //ya se habran dado cuenta de este.. el tiempo siempre es medido en decimal.. bueno.. asi lo hago yo... 
         dictationRecognizer.DictationResult += DictationRecognizer_DictationResult;    //estos son los que tienen que declarar obviamente para  que funcione el dictation
         dictationRecognizer.DictationHypothesis += DictationRecognizer_DictationHypothesis;
         dictationRecognizer.DictationComplete += DictationRecognizer_DictationComplete;
         dictationRecognizer.DictationError += DictationRecognizer_DictationError;
         PhraseRecognitionSystem.Shutdown ();    // estos son importantes.. si quieren usar el keywords... tienen que poner estos aca... lo que hace es que apaga el phraserecognition que es el se encarga de los "keywords" y hay que apagarlos.. no se puede usar los dos al mismo tiempo.. por cuestiones de logica.. pero si se pueden usar los dos pero uno iniciando y cerrando el otro.. 
         dictationRecognizer.Start(); //  Lo que hace aca es que activa la funcon de dictar.. *//*
     }

     private void DictationRecognizer_DictationResult(string text, ConfidenceLevel confidence)   //aqui va todo lo que se maneje con resultado de voz... osea lo convertido a "texto"
     {
         //dictationRecognizer.Start();
         string s = string.Concat(text, " ");
         PalaDich.text += s;
         //Application.OpenURL("https://www.google.com/search?q="+ text);   // aqui agregue algo ingenioso si es que se le puede decir.. cada vez que digo algo iniciara el buscador de google en el cual la busqueda sera añadida el "texto" reconocido.. algo simple pero genial.. 
     }

     private void DictationRecognizer_DictationHypothesis(string text)
     {
         // 
     }

     private void DictationRecognizer_DictationComplete(DictationCompletionCause cause)  //aqui va cuando ya todo haya acabado.. osea cuando dejes de hablar y pase el tiempo de reconocimiento
     {
         //dictationRecognizer.Stop();     //aqui detiene el servicio de dictar
         //PhraseRecognitionSystem.Restart();  //y esto sirve para reactivar el phrase.. osea para que nuestro script "voz" funcione a la normalidad... 
     }

     private void DictationRecognizer_DictationError(string error, int hresult)
     {
         //no sera necesario.. 
     }

     void OnDestroy()
     {
         // aqui esto es recomendable colocarlo.. ya que asi no guarda los procesos y los destruye.. 
         //dictationRecognizer.DictationResult -= DictationRecognizer_DictationResult;
         //dictationRecognizer.DictationComplete -= DictationRecognizer_DictationComplete;
         //dictationRecognizer.DictationHypothesis -= DictationRecognizer_DictationHypothesis;
         //dictationRecognizer.DictationError -= DictationRecognizer_DictationError;
         //dictationRecognizer.Dispose();  //creo que no es necesario explicar pero es necesario que vaya todo esto..
     }
     void Update()
     {
         //esto no fue necesario
         //timeI -= Time.deltaTime;
         //TiempoI.text = ("Tiempo: " + timeI.ToString("0"));
         //int tiempoI = (int)timeI;
         //if (tiempoI == 0)
         //{
         if (Input.GetKeyDown("escape"))
         {
             Application.Quit(); 
         }
         if (timeFV > -2)
         {
             timeFV -= Time.deltaTime;
             int tiempoFV = (int)timeFV;//cambio
             if (timeFV <= 60 & timeFV >= 0)//cambio
             {
                 TiempoFV.text = ("Tiempo: " + timeFV.ToString("0"));//el cero del ToString es para el formato en el que se presenta el número en este caso sin decimales
             }//cambio
             if (tiempoFV == 60 & bandera1)
             {
                 Beep.clip = beep;
                 bandera1 = false;
                 Beep.Play();
                 //ReconocimientoVoz();
             }
             if (tiempoFV == 20 & bandera1)
             {
                 Beep.clip = beep;
                 bandera1 = false;
                 Beep.Play();
                 Debug.Log("esta bien");
                 //ReconocimientoVoz();               
             }
             if (tiempoFV == -1 & bandera)
             {
                 //dictationRecognizer.Stop();//esto recien lo agregue
                 //dictationRecognizer.Dispose();//esto recien lo agregue
                 SepararTexto();
                 bandera = false;
             }
         }
         //}

     }*/

    public void ReconocimientoVoz()
    {
       /* //PhraseRecognitionSystem.Shutdown();    // estos son importantes.. si quieren usar el keywords... tienen que poner estos aca... lo que hace es que apaga el phraserecognition que es el se encarga de los "keywords" y hay que apagarlos.. no se puede usar los dos al mismo tiempo.. por cuestiones de logica.. pero si se pueden usar los dos pero uno iniciando y cerrando el otro.. 
        dictationRecognizer = new DictationRecognizer();
        dictationRecognizer.DictationResult += DictationRecognizer_DictationResult;    //estos son los que tienen que declarar obviamente para  que funcione el dictation
        dictationRecognizer.DictationComplete += DictationRecognizer_DictationComplete;
        dictationRecognizer.Start(); //  Lo que hace aca es que activa la funcon de dictar.. */


        dictationRecognizer = new DictationRecognizer();
        //dictationRecognizer.InitialSilenceTimeoutSeconds = 5;  //este es el tiempo de comienzo  
        //dictationRecognizer.AutoSilenceTimeoutSeconds = 20;  //ya se habran dado cuenta de este.. el tiempo siempre es medido en decimal.. bueno.. asi lo hago yo... 
        dictationRecognizer.DictationResult += DictationRecognizer_DictationResult;    //estos son los que tienen que declarar obviamente para  que funcione el dictation
        dictationRecognizer.DictationHypothesis += DictationRecognizer_DictationHypothesis;
        dictationRecognizer.DictationComplete += DictationRecognizer_DictationComplete;
        dictationRecognizer.DictationError += DictationRecognizer_DictationError;
        PhraseRecognitionSystem.Shutdown ();    // estos son importantes.. si quieren usar el keywords... tienen que poner estos aca... lo que hace es que apaga el phraserecognition que es el se encarga de los "keywords" y hay que apagarlos.. no se puede usar los dos al mismo tiempo.. por cuestiones de logica.. pero si se pueden usar los dos pero uno iniciando y cerrando el otro.. 
        dictationRecognizer.Start(); //  Lo que hace aca es que activa la funcon de dictar.. 
    }

    private void DictationRecognizer_DictationResult(string text, ConfidenceLevel confidence)   //aqui va todo lo que se maneje con resultado de voz... osea lo convertido a "texto"
    {
        dictationRecognizer.Start();
        string s = string.Concat(text, " ");        
        //prueba.text += s;
        PalaDich.text += s;
        //Application.OpenURL("https://www.google.com/search?q="+ text);   // aqui agregue algo ingenioso si es que se le puede decir.. cada vez que digo algo iniciara el buscador de google en el cual la busqueda sera añadida el "texto" reconocido.. algo simple pero genial.. 
    }

    private void DictationRecognizer_DictationHypothesis(string text)
    {
        // 
    }

    private void DictationRecognizer_DictationComplete(DictationCompletionCause cause)  //aqui va cuando ya todo haya acabado.. osea cuando dejes de hablar y pase el tiempo de reconocimiento
    {
        dictationRecognizer.Stop();     //aqui detiene el servicio de dictar
        PhraseRecognitionSystem.Restart();  //y esto sirve para reactivar el phrase.. osea para que nuestro script "voz" funcione a la normalidad... 
    }

    private void DictationRecognizer_DictationError(string error, int hresult)
    {
        //no sera necesario.. 
    }

    void OnDestroy()
    {
        // aqui esto es recomendable colocarlo.. ya que asi no guarda los procesos y los destruye.. 
        dictationRecognizer.DictationResult -= DictationRecognizer_DictationResult;
        dictationRecognizer.DictationComplete -= DictationRecognizer_DictationComplete;
        dictationRecognizer.DictationHypothesis -= DictationRecognizer_DictationHypothesis;
        dictationRecognizer.DictationError -= DictationRecognizer_DictationError;
        dictationRecognizer.Dispose();  //creo que no es necesario explicar pero es necesario que vaya todo esto..
    }
    void Update()
    {
        //esto no fue necesario
        //timeI -= Time.deltaTime;
        //TiempoI.text = ("Tiempo: " + timeI.ToString("0"));
        //int tiempoI = (int)timeI;
        //if (tiempoI == 0)
        //{
        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
        if (timeFV > -2)
        {
            timeFV -= Time.deltaTime;
            int tiempoFV = (int)timeFV;//cambio
            if (timeFV <= 60 & timeFV >= 0)//cambio
            {
                TiempoFV.text = ("Tiempo: " + timeFV.ToString("0"));//el cero del ToString es para el formato en el que se presenta el número en este caso sin decimales
            }//cambio
            if (tiempoFV == 60 & bandera1)
            {
                Beep.clip = beep;
                bandera1 = false;
                Beep.Play();
                ReconocimientoVoz();
            }
            if (tiempoFV == 20 & bandera1)
            {
                Beep.clip = beep;
                bandera1 = false;
                Beep.Play();
                Debug.Log("esta bien");
                ReconocimientoVoz();               
            }
            if (tiempoFV == 0 & bandera)
            {
                Beep.clip = beep;
                Beep.Play();
                dictationRecognizer.Stop();//esto recien lo agregue
                //dictationRecognizer.Dispose();//esto recien lo agregue
                SepararTexto();
                bandera = false;
            }
        }
        //}

    }
    public void SepararTexto()
    {
        PalaDichSep = PalaDich.text.Split(new string[] { " " }, StringSplitOptions.None);
        //PalaDichSep = new string[] { "Manzana", "Pera", " Mango", "Banano", "Manzana", "Banano", "Pera", " Mango" };
        //PalaDichSep = new string[] { "perro", "gato", " raton", "enano", "Piña", "Manzana", "Pera", " Mango", "Banano", "Piña", "Kiwi", "Fresa", "Cereza", "Franbuesa", "Naranja", " Mandarina", "Pera", "Lulo", "Curuba", "Mango", "Fresa" };
        Array.Resize(ref positions, PalaDichSep.Length);
        CalcularPuntos(PalaDichSep.Length);
        for (int i = 0; i < PalaDichSep.Length; i++)
        {     
            for (int j = 0; j < i; j++)
            {
                if (PalaDichSep[i] == PalaDichSep[j])
                {
                    positions[i] = positions[j];
                    PalaDichSep[i] = "";
                    break;
                }
            }
            linea = GetComponent<LineRenderer>();
            linea.positionCount = positions.Length;
            linea.SetPosition(i, positions[i]);
            GameObject objeto = Instantiate(Nodo, new Vector3(0,0,0), Quaternion.identity);
            objeto.transform.SetParent(this.transform);
            objeto.GetComponentInChildren<Text>().text = PalaDichSep[i];
            objeto.transform.position = new Vector3((positions[i].x * tamano)+X_pos,(positions[i].y*tamano)+Y_pos,0.0f);
        }
    }
    void CalcularPuntos(float Npun)//Npun numero de puntos (palabras)
    {
        float DesAng = 2 * Mathf.PI / Npun;//Desplazamiento angular;
        for (int i = 0; i < Npun; i++)
        {
            positions[i] = new Vector3(r * Mathf.Cos(AcuAng), r * Mathf.Sin(AcuAng), 0.0f);
            AcuAng += DesAng;
        }
    }
    void CalcularPuntosCC(float Npun, float Xd, float Yd)//Npun numero de puntos (palabras)
    {
        float DesAng = 2 * Mathf.PI / Npun;//Desplazamiento angular;
        for (int i = 0; i < Npun; i++)
        {
            positions[i] = new Vector3((r * Mathf.Cos(AcuAng))+Xd, (r * Mathf.Sin(AcuAng))+Yd, 0.0f);
            AcuAng += DesAng;
        }
    }
}
