﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;
using SpeechLib;
public class Perception : MonoBehaviour
{
    // Verificar métodos de entrada
    public bool TMouse;
    public bool TTactil;
    public bool TVoz;
    // Actualizar puntaje y actualizar color botones
    public Text Puntaje;
    public static List<string> PosCat = new List<string>() { };
    public static List<string> PosDis = new List<string>() { };
    public static List<string> PosBla = new List<string>() { };
    public static List<string> imgpos = new List<string>() { };
    public static List<string> posimg = new List<string>() { };
    public static List<string> imgposs = new List<string>() { };
    public static List<string> posimgs = new List<string>() { };

    //public Actualizar objeto_CA;

    int index;
    public static int aciertos;
    public static int errores; //variable para contar los desaciertos. No se muestra para evitar producir un efecto negativo en el entrenado.
    public GameObject LeerVars;
    Actualizar Variable;
    public Text FinNivel;
    GameObject Global;
    GameController Variables;
    public Color colorV = new Color(0.1F, 0.5F, 0F, 0.5F);
    public Color colorR = new Color(0.5F, 0.1F, 0F, 0.5F);
    public int Nivel;
    //public Button Buttons;
    public Button Button0;
    public Button Button1;
    public Button Button2;
    public Button Button3;
    public Button Button4;
    public Button Button5;
    public Button Button6;
    public Button Button7;
    public Button Button8;
    public Button Button9;
    public Button Button10;
    public Button Button11;
    public Button Button12;
    public Button Button13;
    public Button Button14;
    public Button Button15;
    public Button Button16;
    public Button Button17;
    public Button Button18;
    public Button Button19;

    //Reconocimiento de voz
    KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public Text PalaDich;
    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "ratón", "sapo", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "mandarina", "mango", "manzana", "maracuyá", "melón", "naranja", "papaya", "pera", "sandía" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "padre", "sastre", "secretaria", "vendedor", "zapatero" };
    //sintesis de voz
    private SpVoice voice;
    //reconocimiento de gestos
    public SI_GES objeto_GES;
    //Sistema inteligente Cancelacion
    public SI_C objeto_C;
    public static string Respuesta;
    public static string AciertoError;//esta variable puede tener 3 valores 0 si es error, 1 si es acierto y 2 si es una palabra que no estaba en pantalla
    public static List<string> Respuestas = new List<string>() { };
    public static List<int> R10 = new List<int>() { };
    public static string imgposss;
    public static string posimgss;
    int j;

    //comentarios
    public Comentarios mensaje;
    string frase;
    //tiempos
    public static float[] G_tiempo;
    public static float[] auxtiempo;
    public float t=5f;
    public float P_tiempo;
    public static float tiempo = 0f;
    public static float tiempoG = 0f;
    public static int i;
    public static List<string> RespuestaG = new List<string> { };
    public static string R10s;//R10 serializado
    // Use this for initialization
    void Start()
    {
        // Inicializacion parametros
        aciertos = 0;
        errores = 0;
        Respuesta = "";
        AciertoError = "";
        i = 0;
        j = 0;
        RespuestaG = imgpos;
        R10.Clear();
        // Leer Variables de control
        /*LeerVars = GameObject.Find("Actualizar");
        Variable = LeerVars.GetComponent<Actualizar>();*/ //deprecated methods
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();

        PosCat = Actualizar.PosCat1;
        PosDis = Actualizar.PosDis1;
        PosBla = Actualizar.PosBla1;
        imgpos = Actualizar.imgpos;
        posimg = Actualizar.posimg;

        imgposs = imgpos.Where(x => x != "").ToList();
        posimgs = posimg.Where(x => x != "").ToList();
        imgposss = "";
        posimgss = "";
        foreach (string palabra in imgposs)
        {
            R10.Add(1);
            imgposss = imgposss + "|" + imgposs[j];
            posimgss = posimgss + "|" + posimgs[j];
            j++;
        }
        // Verificar métodos de entrada habilitados
        TMouse = Variables.VarTMouse;
        TTactil = Variables.VarTTactil;
        TVoz = Variables.VarTVoz;

        if (TMouse)
        {
            print("Está activo el método Mouse");
            Button0.onClick.AddListener(() => ActPuntaje(Button0.name, imgpos[posimg.IndexOf("0")] , 0));
            Button1.onClick.AddListener(() => ActPuntaje(Button1.name, imgpos[posimg.IndexOf("1")], 1));
            Button2.onClick.AddListener(() => ActPuntaje(Button2.name, imgpos[posimg.IndexOf("2")], 2));
            Button3.onClick.AddListener(() => ActPuntaje(Button3.name, imgpos[posimg.IndexOf("3")], 3));
            Button4.onClick.AddListener(() => ActPuntaje(Button4.name, imgpos[posimg.IndexOf("4")], 4));
            Button5.onClick.AddListener(() => ActPuntaje(Button5.name, imgpos[posimg.IndexOf("5")], 5));
            Button6.onClick.AddListener(() => ActPuntaje(Button6.name, imgpos[posimg.IndexOf("6")], 6));
            Button7.onClick.AddListener(() => ActPuntaje(Button7.name, imgpos[posimg.IndexOf("7")], 7));
            Button8.onClick.AddListener(() => ActPuntaje(Button8.name, imgpos[posimg.IndexOf("8")], 8));
            Button9.onClick.AddListener(() => ActPuntaje(Button9.name, imgpos[posimg.IndexOf("9")], 9));
            Button10.onClick.AddListener(() => ActPuntaje(Button10.name, imgpos[posimg.IndexOf("10")], 10));
            Button11.onClick.AddListener(() => ActPuntaje(Button11.name, imgpos[posimg.IndexOf("11")], 11));
            Button12.onClick.AddListener(() => ActPuntaje(Button12.name, imgpos[posimg.IndexOf("12")], 12));
            Button13.onClick.AddListener(() => ActPuntaje(Button13.name, imgpos[posimg.IndexOf("13")], 13));
            Button14.onClick.AddListener(() => ActPuntaje(Button14.name, imgpos[posimg.IndexOf("14")], 14));
            Button15.onClick.AddListener(() => ActPuntaje(Button15.name, imgpos[posimg.IndexOf("15")], 15));
            Button16.onClick.AddListener(() => ActPuntaje(Button16.name, imgpos[posimg.IndexOf("16")], 16));
            Button17.onClick.AddListener(() => ActPuntaje(Button17.name, imgpos[posimg.IndexOf("17")], 17));
            Button18.onClick.AddListener(() => ActPuntaje(Button18.name, imgpos[posimg.IndexOf("18")], 18));
            Button19.onClick.AddListener(() => ActPuntaje(Button19.name, imgpos[posimg.IndexOf("19")], 19));
        }
        if (TTactil)
        {
            print("Está activo el método táctil");
            Button0.onClick.AddListener(() => ActPuntaje(Button0.name, imgpos[posimg.IndexOf("0")], 0));
            Button1.onClick.AddListener(() => ActPuntaje(Button1.name, imgpos[posimg.IndexOf("1")], 1));
            Button2.onClick.AddListener(() => ActPuntaje(Button2.name, imgpos[posimg.IndexOf("2")], 2));
            Button3.onClick.AddListener(() => ActPuntaje(Button3.name, imgpos[posimg.IndexOf("3")], 3));
            Button4.onClick.AddListener(() => ActPuntaje(Button4.name, imgpos[posimg.IndexOf("4")], 4));
            Button5.onClick.AddListener(() => ActPuntaje(Button5.name, imgpos[posimg.IndexOf("5")], 5));
            Button6.onClick.AddListener(() => ActPuntaje(Button6.name, imgpos[posimg.IndexOf("6")], 6));
            Button7.onClick.AddListener(() => ActPuntaje(Button7.name, imgpos[posimg.IndexOf("7")], 7));
            Button8.onClick.AddListener(() => ActPuntaje(Button8.name, imgpos[posimg.IndexOf("8")], 8));
            Button9.onClick.AddListener(() => ActPuntaje(Button9.name, imgpos[posimg.IndexOf("9")], 9));
            Button10.onClick.AddListener(() => ActPuntaje(Button10.name, imgpos[posimg.IndexOf("10")], 10));
            Button11.onClick.AddListener(() => ActPuntaje(Button11.name, imgpos[posimg.IndexOf("11")], 11));
            Button12.onClick.AddListener(() => ActPuntaje(Button12.name, imgpos[posimg.IndexOf("12")], 12));
            Button13.onClick.AddListener(() => ActPuntaje(Button13.name, imgpos[posimg.IndexOf("13")], 13));
            Button14.onClick.AddListener(() => ActPuntaje(Button14.name, imgpos[posimg.IndexOf("14")], 14));
            Button15.onClick.AddListener(() => ActPuntaje(Button15.name, imgpos[posimg.IndexOf("15")], 15));
            Button16.onClick.AddListener(() => ActPuntaje(Button16.name, imgpos[posimg.IndexOf("16")], 16));
            Button17.onClick.AddListener(() => ActPuntaje(Button17.name, imgpos[posimg.IndexOf("17")], 17));
            Button18.onClick.AddListener(() => ActPuntaje(Button18.name, imgpos[posimg.IndexOf("18")], 18));
            Button19.onClick.AddListener(() => ActPuntaje(Button19.name, imgpos[posimg.IndexOf("19")], 19));
        }
        if (TVoz)
        {
            print("Esta activo el método voz");
            keywords.Add("go", () =>
            {
                GoCalled();
            });
            keywords.Add("abeja", () =>
            {
                AbejaCalled();
            });
            keywords.Add("águila", () =>
            {
                AguilaCalled();
            });
            keywords.Add("alacrán", () =>
            {
                AlacranCalled();
            });
            keywords.Add("araña", () =>
            {
                AranaCalled();
            });
            keywords.Add("ardilla", () =>
            {
                ArdillaCalled();
            });
            keywords.Add("armadillo", () =>
            {
                ArmadilloCalled();
            });
            keywords.Add("asno", () =>
            {
                AsnoCalled();
            });
            keywords.Add("avestruz", () =>
            {
                AvestruzCalled();
            });
            keywords.Add("ballena", () =>
            {
                BallenaCalled();
            });
            keywords.Add("barranquero", () =>
            {
                BarranqueroCalled();
            });
            keywords.Add("buey", () =>
            {
                BueyCalled();
            });
            keywords.Add("bufalo", () =>
            {
                BufaloCalled();
            });
            keywords.Add("buho", () =>
            {
                BuhoCalled();
            });
            keywords.Add("buitre", () =>
            {
                BuitreCalled();
            });
            keywords.Add("burro", () =>
            {
                BurroCalled();
            });
            keywords.Add("caballo", () =>
            {
                CaballoCalled();
            });
            keywords.Add("caballo de mar", () =>
            {
                CaballodemarCalled();
            });
            keywords.Add("cabra", () =>
            {
                CabraCalled();
            });
            keywords.Add("caimán", () =>
            {
                CaimanCalled();
            });
            keywords.Add("camaleón", () =>
            {
                CamaleonCalled();
            });
            keywords.Add("camello", () =>
            {
                CamelloCalled();
            });
            keywords.Add("canario", () =>
            {
                CanarioCalled();
            });
            keywords.Add("cangrejo", () =>
            {
                CangrejoCalled();
            });
            keywords.Add("chivo", () =>
            {
                ChivoCalled();
            });
            keywords.Add("cienpiés", () =>
            {
                CienpiesCalled();
            });
            keywords.Add("Chimpancé", () =>
            {
                MicoCalled();
            });
            keywords.Add("Simio", () =>
            {
                SimioCalled();
            });
            keywords.Add("cigüeña", () =>
            {
                CiguenaCalled();
            });
            keywords.Add("cocodrilo", () =>
            {
                CocodriloCalled();
            });
            keywords.Add("colibrí", () =>
            {
                ColibriCalled();
            });
            keywords.Add("cóndor", () =>
            {
                CondorCalled();
            });
            keywords.Add("conejo", () =>
            {
                ConejoCalled();
            });
            keywords.Add("cucaracha", () =>
            {
                CucarachaCalled();
            });
            keywords.Add("cucarrón", () =>
            {
                CucarronCalled();
            });
            keywords.Add("culebra", () =>
            {
                CulebraCalled();
            });
            keywords.Add("delfín", () =>
            {
                DelfinCalled();
            });
            keywords.Add("elefante", () =>
            {
                ElefanteCalled();
            });
            keywords.Add("faisán", () =>
            {
                FaisanCalled();
            });
            keywords.Add("foca", () =>
            {
                FocaCalled();
            });
            keywords.Add("gallina", () =>
            {
                GallinaCalled();
            });
            keywords.Add("gallinazo", () =>
            {
                GallinazoCalled();
            });
            keywords.Add("gallo", () =>
            {
                GalloCalled();
            });
            keywords.Add("ganzo", () =>
            {
                GanzoCalled();
            });
            keywords.Add("garrapata", () =>
            {
                GarrapataCalled();
            });
            keywords.Add("garza", () =>
            {
                GarzaCalled();
            });
            keywords.Add("gato", () =>
            {
                GatoCalled();
            });
            keywords.Add("gavilán", () =>
            {
                GavilanCalled();
            });
            keywords.Add("grillo", () =>
            {
                GrilloCalled();
            });
            keywords.Add("guacamaya", () =>
            {
                GuacamayaCalled();
            });
            keywords.Add("guatín", () =>
            {
                GuatinCalled();
            });
            keywords.Add("hipopótamo", () =>
            {
                HipopotamoCalled();
            });
            keywords.Add("hormiga", () =>
            {
                HormigaCalled();
            });
            keywords.Add("iguana", () =>
            {
                IguanaCalled();
            });
            keywords.Add("jirafa", () =>
            {
                JirafaCalled();
            });
            keywords.Add("lagartija", () =>
            {
                LagartijaCalled();
            });
            keywords.Add("lagartijo", () =>
            {
                LagartijaCalled();
            });
            keywords.Add("lechuza", () =>
            {
                LechuzaCalled();
            });
            keywords.Add("león", () =>
            {
                LeonCalled();
            });
            keywords.Add("leopardo", () =>
            {
                LeopardoCalled();
            });
            keywords.Add("puma", () =>
            {
                PumaCalled();
            });
            keywords.Add("lobo", () =>
            {
                LoboCalled();
            });
            keywords.Add("lombriz", () =>
            {
                LombrizCalled();
            });
            keywords.Add("loro", () =>
            {
                LoroCalled();
            });
            keywords.Add("mariposa", () =>
            {
                MariposaCalled();
            });
            keywords.Add("marrano", () =>
            {
                MarranoCalled();
            });
            keywords.Add("cerdo", () =>
            {
                MarranoCalled();
            });
            keywords.Add("chancho", () =>
            {
                MarranoCalled();
            });
            keywords.Add("mico", () =>
            {
                MicoCalled();
            });
            keywords.Add("mirla", () =>
            {
                MirlaCalled();
            });
            keywords.Add("mosca", () =>
            {
                MoscaCalled();
            });
            keywords.Add("mula", () =>
            {
                MulaCalled();
            });
            keywords.Add("murciélago", () =>
            {
                MurcielagoCalled();
            });
            keywords.Add("novillo", () =>
            {
                ToroCalled();
            });
            keywords.Add("oso", () =>
            {
                OsoCalled();
            });
            keywords.Add("oveja", () =>
            {
                OvejaCalled();
            });
            keywords.Add("ovejo", () =>
            {
                OvejaCalled();
            });
            keywords.Add("pájaro", () =>
            {
                PajaroCalled();
            });
            keywords.Add("pájarito", () =>
            {
                PajaroCalled();
            });
            keywords.Add("paloma", () =>
            {
                PalomaCalled();
            });
            keywords.Add("palomo", () =>
            {
                PalomaCalled();
            });
            keywords.Add("pantera", () =>
            {
                PanteraCalled();
            });
            keywords.Add("papagayo", () =>
            {
                PapagayoCalled();
            });
            keywords.Add("pato", () =>
            {
                PatoCalled();
            });
            keywords.Add("pavo", () =>
            {
                PavoCalled();
            });
            keywords.Add("perro", () =>
            {
                PerroCalled();
            });
            keywords.Add("pescado", () =>
            {
                PezCalled();
            });
            keywords.Add("pez", () =>
            {
                PezCalled();
            });
            keywords.Add("piojo", () =>
            {
                PiojoCalled();
            });
            keywords.Add("pisco", () =>
            {
                PizcoCalled();
            });
            keywords.Add("pollo", () =>
            {
                PolloCalled();
            });
            keywords.Add("pollito", () =>
            {
                PollitoCalled();
            });
            keywords.Add("pulga", () =>
            {
                PulgaCalled();
            });
            keywords.Add("pulpo", () =>
            {
                PulpoCalled();
            });
            keywords.Add("rata", () =>
            {
                RataCalled();
            });
            keywords.Add("rana", () =>
            {
                RanaCalled();
            });
            keywords.Add("ratón", () =>
            {
                RatonCalled();
            });
            keywords.Add("rinoceronte", () =>
            {
                RinoceronteCalled();
            });
            keywords.Add("sapo", () =>
            {
                SapoCalled();
            });
            keywords.Add("serpiente", () =>
            {
                SerpienteCalled();
            });
            keywords.Add("ternero", () =>
            {
                TerneroCalled();
            });
            keywords.Add("tiburón", () =>
            {
                TiburonCalled();
            });
            keywords.Add("tigre", () =>
            {
                TigreCalled();
            });
            keywords.Add("toro", () =>
            {
                ToroCalled();
            });
            keywords.Add("tortuga", () =>
            {
                TortugaCalled();
            });
            keywords.Add("trucha", () =>
            {
                TruchaCalled();
            });
            keywords.Add("vaca", () =>
            {
                VacaCalled();
            });
            keywords.Add("venado", () =>
            {
                VenadoCalled();
            });
            keywords.Add("yegua", () =>
            {
                YeguaCalled();
            });
            keywords.Add("zancudo", () =>
            {
                ZancudoCalled();
            });
            keywords.Add("zebra", () =>
            {
                ZebraCalled();
            });
            keywords.Add("zorro", () =>
            {
                ZorroCalled();
            });
            //frutas
            keywords.Add("aguacate", () =>
            {
                AguacateCalled();
            });
            keywords.Add("albaricoque", () =>
            {
                AlbaricoqueCalled();
            });
            keywords.Add("arazá", () =>
            {
                ArazaCalled();
            });
            keywords.Add("auyama", () =>
            {
                AuyamaCalled();
            });
            keywords.Add("badea", () =>
            {
                BadeaCalled();
            });
            keywords.Add("banano", () =>
            {
                BananoCalled();
            });
            keywords.Add("breva", () =>
            {
                BrevaCalled();
            });
            keywords.Add("carambolo", () =>
            {
                CaramboloCalled();
            });
            keywords.Add("cereza", () =>
            {
                CerezaCalled();
            });
            keywords.Add("chirimoya", () =>
            {
                ChirimoyaCalled();
            });
            keywords.Add("chontaduro", () =>
            {
                ChontaduroCalled();
            });
            keywords.Add("ciruela", () =>
            {
                CiruelaCalled();
            });
            keywords.Add("coco", () =>
            {
                CocoCalled();
            });
            keywords.Add("durazno", () =>
            {
                DuraznoCalled();
            });
            keywords.Add("feijoa", () =>
            {
                FeijoaCalled();
            });
            keywords.Add("frambuesa", () =>
            {
                FrambuesaCalled();
            });
            keywords.Add("fresa", () =>
            {
                FresaCalled();
            });
            keywords.Add("granada", () =>
            {
                GranadaCalled();
            });
            keywords.Add("granadilla", () =>
            {
                GranadillaCalled();
            });
            keywords.Add("guaba", () =>
            {
                GuabaCalled();
            });
            keywords.Add("guanabana", () =>
            {
                GuanabanaCalled();
            });
            keywords.Add("guayaba", () =>
            {
                GuayabaCalled();
            });
            keywords.Add("kiwi", () =>
            {
                KiwiCalled();
            });
            keywords.Add("limón", () =>
            {
                LimonCalled();
            });
            keywords.Add("lulo", () =>
            {
                LuloCalled();
            });
            keywords.Add("madroño", () =>
            {
                MadronoCalled();
            });
            keywords.Add("mamoncillo", () =>
            {
                MamoncilloCalled();
            });
            keywords.Add("mandarina", () =>
            {
                MandarinaCalled();
            });
            keywords.Add("mango", () =>
            {
                MangoCalled();
            });
            keywords.Add("manzana", () =>
            {
                ManzanaCalled();
            });
            keywords.Add("maracuyá", () =>
            {
                MaracuyaCalled();
            });
            keywords.Add("melocotón", () =>
            {
                MelocotonCalled();
            });
            keywords.Add("melón", () =>
            {
                MelonCalled();
            });
            keywords.Add("mora", () =>
            {
                MoraCalled();
            });
            keywords.Add("Naranja", () =>
            {
                NaranjaCalled();
            });
            keywords.Add("níspero", () =>
            {
                NisperoCalled();
            });
            keywords.Add("pomarrosa", () =>
            {
                PomarrosaCalled();
            });
            keywords.Add("papaya", () =>
            {
                PapayaCalled();
            });
            keywords.Add("pepino", () =>
            {
                PepinoCalled();
            });
            keywords.Add("pera", () =>
            {
                PeraCalled();
            });
            keywords.Add("piña", () =>
            {
                PiñaCalled();
            });
            keywords.Add("pitaya", () =>
            {
                PitayaCalled();
            });
            keywords.Add("pomelo", () =>
            {
                PomeloCalled();
            });
            keywords.Add("sandia", () =>
            {
                SandiaCalled();
            });
            keywords.Add("patilla", () =>
            {
                PatillaCalled();
            });
            keywords.Add("tomate", () =>
            {
                TomateCalled();
            });
            keywords.Add("tomate de árbol", () =>
            {
                TomatedearbolCalled();
            });
            keywords.Add("uchuva", () =>
            {
                UchuvaCalled();
            });
            keywords.Add("victoria", () =>
            {
                VictoriaCalled();
            });
            keywords.Add("zapote", () =>
            {
                ZapoteCalled();
            });
            //Profesiones
            keywords.Add("abogado", () =>
            {
                AbogadoCalled();
            });
            keywords.Add("agricultor", () =>
            {
                AgricultorCalled();
            });
            keywords.Add("campesino", () =>
            {
                CampesinoCalled();
            });
            keywords.Add("sembrador", () =>
            {
                SembradorCalled();
            });
            keywords.Add("albañil", () =>
            {
                AlbanilCalled();
            });
            keywords.Add("ama de casa", () =>
            {
                AmadecasaCalled();
            });
            keywords.Add("arquitecto", () =>
            {
                ArquitectoCalled();
            });
            keywords.Add("artesano", () =>
            {
                ArtesanoCalled();
            });
            keywords.Add("aseador", () =>
            {
                AseadorCalled();
            });
            keywords.Add("azafata", () =>
            {
                AzafataCalled();
            });
            keywords.Add("barrendero", () =>
            {
                BarrenderoCalled();
            });
            keywords.Add("bombero", () =>
            {
                BomberoCalled();
            });
            keywords.Add("camionero", () =>
            {
                CamioneroCalled();
            });
            keywords.Add("cantante", () =>
            {
                CantanteCalled();
            });
            keywords.Add("carnicero", () =>
            {
                CarniceroCalled();
            });
            keywords.Add("carpintero", () =>
            {
                CarpinteroCalled();
            });
            keywords.Add("cerrajero", () =>
            {
                CerrajeroCalled();
            });
            keywords.Add("chef", () =>
            {
                ChefCalled();
            });
            keywords.Add("cirujano", () =>
            {
                CirujanoCalled();
            });
            keywords.Add("conductor", () =>
            {
                ConductorCalled();
            });
            keywords.Add("chofer", () =>
            {
                ChoferCalled();
            });
            keywords.Add("taxista", () =>
            {
                TaxistaCalled();
            });
            keywords.Add("constructor", () =>
            {
                ConstructorCalled();
            });
            keywords.Add("contador", () =>
            {
                ContadorCalled();
            });
            keywords.Add("doctor", () =>
            {
                DoctorCalled();
            });
            keywords.Add("doctora", () =>
            {
                DoctoraCalled();
            });
            keywords.Add("electricista", () =>
            {
                ElectricistaCalled();
            });
            keywords.Add("enfermera", () =>
            {
                EnfermeraCalled();
            });
            keywords.Add("escritor", () =>
            {
                EscritorCalled();
            });
            keywords.Add("escultor", () =>
            {
                EscultorCalled();
            });
            keywords.Add("farmaceuta", () =>
            {
                FarmaceutaCalled();
            });
            keywords.Add("fisioterapeuta", () =>
            {
                FisioterapeutaCalled();
            });
            keywords.Add("futbolista", () =>
            {
                FutbolistaCalled();
            });
            keywords.Add("gimnasta", () =>
            {
                GimnastaCalled();
            });
            keywords.Add("ingeniero", () =>
            {
                IngenieroCalled();
            });
            keywords.Add("jardinero", () =>
            {
                JardineroCalled();
            });
            keywords.Add("lavandera", () =>
            {
                LavanderaCalled();
            });
            keywords.Add("locutor", () =>
            {
                LocutorCalled();
            });
            keywords.Add("manicurista", () =>
            {
                ManicuristaCalled();
            });
            keywords.Add("marinero", () =>
            {
                MarineroCalled();
            });
            keywords.Add("masajista", () =>
            {
                MasajistaCalled();
            });
            keywords.Add("mecánico", () =>
            {
                MecanicoCalled();
            });
            keywords.Add("médica", () =>
            {
                MedicaCalled();
            });
            keywords.Add("médico", () =>
            {
                MedicoCalled();
            });
            keywords.Add("mensajero", () =>
            {
                MensajeroCalled();
            });
            keywords.Add("mesero", () =>
            {
                MeseroCalled();
            });
            keywords.Add("niñera", () =>
            {
                NineraCalled();
            });
            keywords.Add("obrero", () =>
            {
                ObreroCalled();
            });
            keywords.Add("odontólogo", () =>
            {
                OdontologoCalled();
            });
            keywords.Add("dentista", () =>
            {
                DentistaCalled();
            });
            keywords.Add("oficial", () =>
            {
                OficialCalled();
            });
            keywords.Add("ayudante", () =>
            {
                AyudanteCalled();
            });
            keywords.Add("maestro", () =>
            {
                MaestroCalled();
            });
            keywords.Add("maestro de obra", () =>
            {
                MaestrodeobraCalled();
            });
            keywords.Add("oftalmólogo", () =>
            {
                OftalmologoCalled();
            });
            keywords.Add("ordenador", () =>
            {
                OrdenadorCalled();
            });
            keywords.Add("panadero", () =>
            {
                PanaderoCalled();
            });
            keywords.Add("peluquero", () =>
            {
                PeluqueroCalled();
            });
            keywords.Add("periodista", () =>
            {
                PeriodistaCalled();
            });
            keywords.Add("piloto", () =>
            {
                PilotoCalled();
            });
            keywords.Add("pintor", () =>
            {
                PintorCalled();
            });
            keywords.Add("policía", () =>
            {
                PoliciaCalled();
            });
            keywords.Add("político", () =>
            {
                PoliticoCalled();
            });
            keywords.Add("profesor", () =>
            {
                ProfesorCalled();
            });
            keywords.Add("docente", () =>
            {
                DocenteCalled();
            });
            keywords.Add("psicólogo", () =>
            {
                PsicologoCalled();
            });
            keywords.Add("sacerdote", () =>
            {
                SacerdoteCalled();
            });
            keywords.Add("padre", () =>
            {
                SacerdoteCalled();
            });
            keywords.Add("cura", () =>
            {
                SacerdoteCalled();
            });
            keywords.Add("sastre", () =>
            {
                SastreCalled();
            });
            keywords.Add("costurera", () =>
            {
                SastreCalled();
            });
            keywords.Add("modista", () =>
            {
                SastreCalled();
            });
            keywords.Add("secretaria", () =>
            {
                SecretariaCalled();
            });
            keywords.Add("soldado", () =>
            {
                SoldadoCalled();
            });
            keywords.Add("tenista", () =>
            {
                TenistaCalled();
            });
            keywords.Add("vendedor", () =>
            {
                VendedorCalled();
            });
            keywords.Add("tendero", () =>
            {
                TenderoCalled();
            });
            keywords.Add("veterinario", () =>
            {
                VeterinarioCalled();
            });
            keywords.Add("zapatero", () =>
            {
                ZapateroCalled();
            });
            keywords.Add("hola", () =>
            {
                HolaCalled();
            });
            KeywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
            KeywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
            KeywordRecognizer.Start();
        }

        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;
        tiempoG = 0;
        G_tiempo = new float[Actualizar.NBlancos + Actualizar.NDistractores + 1];
        auxtiempo = new float[Actualizar.NBlancos + Actualizar.NDistractores + 1];
    }

    // Update is called once per frame
    void Update()
    {
        tiempoG = tiempoG + Time.deltaTime;
        t = t - Time.deltaTime;
        RespuestaG=imgpos;
        if (t<0)
        {
            objeto_GES.DBWrite();
            t = 5f;
        }
    }

    public static void TiempoRespuesta()
    {
        auxtiempo[i] = tiempoG;
        G_tiempo[i] = tiempoG - auxtiempo[i - 1];
    }

    void ActPuntaje(string boton, string Palabra, int indice)
    {
        i++;       
        if (Respuestas.IndexOf(Palabra) == -1)
        {
            Respuesta = Respuesta + "|" + Palabra;
            Respuestas.Add(Palabra);
            TiempoRespuesta();
            Debug.Log(Palabra + indice.ToString());
            /*Global = GameObject.Find("GameController");
            Variables = Global.GetComponent<GameController>();*/
            //Nivel = int.Parse(Variables.VarBlanco);
            if (Actualizar.NBlancos > Actualizar.NDistractores)
            {
                Nivel = Actualizar.NBlancos;
            }
            else
            {
                Nivel = Actualizar.NDistractores;
            }
            if (boton == ("Button21"))
            {
                //boton = ("Button" + PosDis[0]);//esto hace que si no se dijo la palabra que era se asocie con un distractor y cuente error
                errores++;
                AciertoError = AciertoError + "|2";
                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                            + "Revisa nuevamente la palabra que dijiste no esta en pantalla."
                                                            + "</speak>",
                                                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);

            }
            for (int i = 0; i < Nivel; i++)
            {
                if (boton == ("Button" + PosCat[i]))
                {
                    aciertos++;
                    AciertoError = AciertoError + "|1";
                    mensaje = new Comentarios("p");
                    frase = mensaje.respuesta("p");
                    voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                + frase
                                                + "</speak>",
                                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    Button b = GameObject.Find("Button" + PosCat[i]).GetComponent<Button>();
                    GameObject.Find("Button" + PosCat[i]).GetComponent<Button>().interactable = false;
                    ColorBlock cb = b.colors;
                    cb.normalColor = Color.green;
                    cb.disabledColor = colorV;
                    b.colors = cb;
                    b.interactable = false;
                }
            }
            for (int i = 0; i < Nivel; i++)
            {
                if (boton == ("Button" + PosDis[i]))
                {
                    errores++;
                    R10[imgposs.IndexOf(Palabra)] = 0;
                    for (int j=0;j<R10.Count();j++)
                    {
                        R10s = R10s + "|" + R10[j];
                    }
                    R10s = R10s + "@";
                    AciertoError = AciertoError + "|0";
                    mensaje = new Comentarios("n");
                    frase = mensaje.respuesta("n");
                    voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                                + frase
                                                + "</speak>",
                                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                    Button b = GameObject.Find("Button" + PosDis[i]).GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = Color.red;
                    cb.disabledColor = colorR;
                    b.colors = cb;
                    b.interactable = false;// con esto el objeto esta en la pantalla pero no se puede interactuar con el
                                           //b.gameObject.SetActive(false); // Con esto el objeto no se muestra en la pantalla
                }
            }
        }
        if (aciertos == Actualizar.NBlancos)
        {
            objeto_C.DBWrite();
            SceneManager.LoadScene("Lobby");
        }
    }


    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    
    //funciones para el conteo de los puntos
    void GoCalled()
    {
        print("Dijiste Go");
        PalaDich.text += ", go";
    }
    void HolaCalled()
    {
        print("Dijiste Hola");
        PalaDich.text += ", Hola";
    }
    void AbejaCalled()
    {
        print("Dijiste abeja");
        PalaDich.text += ", abeja";
        index = imgpos.IndexOf("abeja");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index], int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index], int.Parse(posimg[index]));
        }
    }
    void AguilaCalled()
    {
        print("Dijiste águila");
        PalaDich.text += ", águila";
        index = imgpos.IndexOf("águila");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index], int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index], int.Parse(posimg[index]));
        }
    }
    void AlacranCalled()
    {
        print("Dijiste alacran");
        PalaDich.text += ", alacran";
        index = imgpos.IndexOf("alacran");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void AranaCalled()
    {
        print("Dijiste araña");
        PalaDich.text += ", araña";
        index = imgpos.IndexOf("arana");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ArdillaCalled()
    {
        print("Dijiste ardilla");
        PalaDich.text += ", ardilla";
        index = imgpos.IndexOf("ardilla");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ArmadilloCalled()
    {
        print("Dijiste armadillo");
        PalaDich.text += ", armadillo";
        index = imgpos.IndexOf("armadillo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void AsnoCalled()
    {
        print("Dijiste asno");
        PalaDich.text += ", asno";
        index = imgpos.IndexOf("asno");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void AvestruzCalled()
    {
        print("Dijiste avestruz");
        PalaDich.text += ", avestruz";
        index = imgpos.IndexOf("avestruz");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BallenaCalled()
    {
        print("Dijiste ballena");
        PalaDich.text += ", ballena";
        index = imgpos.IndexOf("ballena");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BarranqueroCalled()
    {
        print("Dijiste barranquero");
        PalaDich.text += ", barranquero";
        index = imgpos.IndexOf("barranquero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BueyCalled()
    {
        print("Dijiste buey");
        PalaDich.text += ", buey";
        index = imgpos.IndexOf("buey");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BufaloCalled()
    {
        print("Dijiste bufalo");
        PalaDich.text += ", bufalo";
        index = imgpos.IndexOf("bufalo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BuhoCalled()
    {
        print("Dijiste buho");
        PalaDich.text += ", buho";
        index = imgpos.IndexOf("buho");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BuitreCalled()
    {
        print("Dijiste buitre");
        PalaDich.text += ", buitre";
        index = imgpos.IndexOf("buitre");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void BurroCalled()
    {
        print("Dijiste burro");
        PalaDich.text += ", burro";
        index = imgpos.IndexOf("burro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CaballoCalled()
    {
        print("Dijiste caballo");
        PalaDich.text += ", caballo";
        index = imgpos.IndexOf("caballo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CaballodemarCalled()
    {
        print("Dijiste Caballo de mar");
        PalaDich.text += ", Caballo de mar";
        index = imgpos.IndexOf("caballodemar");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CabraCalled()
    {
        print("Dijiste cabra");
        PalaDich.text += ", cabra";
        index = imgpos.IndexOf("cabra");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CaimanCalled()
    {
        print("Dijiste caimán");
        PalaDich.text += ", caimán";
        index = imgpos.IndexOf("caimán");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CamaleonCalled()
    {
        print("Dijiste camaleon");
        PalaDich.text += ", camaleon";
        index = imgpos.IndexOf("camaleon");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CamelloCalled()
    {
        print("Dijiste camello");
        PalaDich.text += ", camello";
        index = imgpos.IndexOf("camello");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CanarioCalled()
    {
        print("Dijiste canario");
        PalaDich.text += ", canario";
        index = imgpos.IndexOf("canario");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CangrejoCalled()
    {
        print("Dijiste cangrejo");
        PalaDich.text += ", cangrejo";
        index = imgpos.IndexOf("cangrejo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ChivoCalled()
    {
        print("Dijiste chivo");
        PalaDich.text += ", chivo";
        index = imgpos.IndexOf("chivo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CienpiesCalled()
    {
        print("Dijiste cienpies");
        PalaDich.text += ", cienpies";
        index = imgpos.IndexOf("cienpies");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CiguenaCalled()
    {
        print("Dijiste ciguena");
        PalaDich.text += ", ciguena";
        index = imgpos.IndexOf("ciguena");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CocodriloCalled()
    {
        print("Dijiste cocodrilo");
        PalaDich.text += ", cocodrilo";
        index = imgpos.IndexOf("cocodrilo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ColibriCalled()
    {
        print("Dijiste colibri");
        PalaDich.text += ", colibri";
        index = imgpos.IndexOf("colibri");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CondorCalled()
    {
        print("Dijiste condor");
        PalaDich.text += ", condor";
        index = imgpos.IndexOf("condor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ConejoCalled()
    {
        print("Dijiste conejo");
        PalaDich.text += ", conejo";
        index = imgpos.IndexOf("conejo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CucarachaCalled()
    {
        print("Dijiste cucaracha");
        PalaDich.text += ", cucaracha";
        index = imgpos.IndexOf("cucaracha");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CucarronCalled()
    {
        print("Dijiste cucarron");
        PalaDich.text += ", cucarron";
        index = imgpos.IndexOf("cucarron");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void CulebraCalled()
    {
        print("Dijiste culebra");
        PalaDich.text += ", culebra";
        index = imgpos.IndexOf("culebra");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void DelfinCalled()
    {
        print("Dijiste delfin");
        PalaDich.text += ", delfin";
        index = imgpos.IndexOf("delfin");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ElefanteCalled()
    {
        print("Dijiste elefante");
        PalaDich.text += ", elefante";
        index = imgpos.IndexOf("elefante");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void FaisanCalled()
    {
        print("Dijiste faisan");
        PalaDich.text += ", faisan";
        index = imgpos.IndexOf("faisan");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void FocaCalled()
    {
        print("Dijiste foca");
        PalaDich.text += ", foca";
        index = imgpos.IndexOf("foca");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GallinaCalled()
    {
        print("Dijiste gallina");
        PalaDich.text += ", gallina";
        index = imgpos.IndexOf("gallina");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GallinazoCalled()
    {
        print("Dijiste gallinazo");
        PalaDich.text += ", gallinazo";
        index = imgpos.IndexOf("gallinazo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GalloCalled()
    {
        print("Dijiste gallo");
        PalaDich.text += ", gallo";
        index = imgpos.IndexOf("gallo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GanzoCalled()
    {
        print("Dijiste ganzo");
        PalaDich.text += ", ganzo";
        index = imgpos.IndexOf("ganzo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GarrapataCalled()
    {
        print("Dijiste garrapata");
        PalaDich.text += ", garrapata";
        index = imgpos.IndexOf("garrapata");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GarzaCalled()
    {
        print("Dijiste garza");
        PalaDich.text += ", garza";
        index = imgpos.IndexOf("garza");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GatoCalled()
    {
        print("Dijiste gato");
        PalaDich.text += ", gato";
        index = imgpos.IndexOf("gato");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GavilanCalled()
    {
        print("Dijiste gavilan");
        PalaDich.text += ", gavilan";
        index = imgpos.IndexOf("gavilan");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GrilloCalled()
    {
        print("Dijiste grillo");
        PalaDich.text += ", grillo";
        index = imgpos.IndexOf("grillo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GuacamayaCalled()
    {
        print("Dijiste guacamaya");
        PalaDich.text += ", guacamaya";
        index = imgpos.IndexOf("guacamaya");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void GuatinCalled()
    {
        print("Dijiste guatin");
        PalaDich.text += ", guatin";
        index = imgpos.IndexOf("guatin");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void HipopotamoCalled()
    {
        print("Dijiste hipopotamo");
        PalaDich.text += ", hipopotamo";
        index = imgpos.IndexOf("hipopotamo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void HormigaCalled()
    {
        print("Dijiste hormiga");
        PalaDich.text += ", hormiga";
        index = imgpos.IndexOf("hormiga");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void IguanaCalled()
    {
        print("Dijiste iguana");
        PalaDich.text += ", iguana";
        index = imgpos.IndexOf("iguana");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void JirafaCalled()
    {
        print("Dijiste jirafa");
        PalaDich.text += ", jirafa";
        index = imgpos.IndexOf("jirafa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void LagartijaCalled()
    {
        print("Dijiste lagartija");
        PalaDich.text += ", lagartija";
        index = imgpos.IndexOf("lagartija");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void LeonCalled()
    {
        print("Dijiste león");
        PalaDich.text += ", león";
        index = imgpos.IndexOf("león");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void LechuzaCalled()
    {
        print("Dijiste lechuza");
        PalaDich.text += ", lechuza";
        index = imgpos.IndexOf("lechuza");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void LeopardoCalled()
    {
        print("Dijiste leopardo");
        PalaDich.text += ", leopardo";
        index = imgpos.IndexOf("leopardo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PumaCalled()
    {
        print("Dijiste puma");
        PalaDich.text += ", puma";
        index = imgpos.IndexOf("puma");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }

    void LoboCalled()
    {
        print("Dijiste lobo");
        PalaDich.text += ", lobo";
        index = imgpos.IndexOf("lobo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void LombrizCalled()
    {
        print("Dijiste lombriz");
        PalaDich.text += ", lombriz";
        index = imgpos.IndexOf("lombriz");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void LoroCalled()
    {
        print("Dijiste loro");
        PalaDich.text += ", loro";
        index = imgpos.IndexOf("loro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MariposaCalled()
    {
        print("Dijiste mariposa");
        PalaDich.text += ", mariposa";
        index = imgpos.IndexOf("mariposa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MarranoCalled()
    {
        print("Dijiste marrano");
        PalaDich.text += ", marrano";
        index = imgpos.IndexOf("marrano");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MicoCalled()
    {
        print("Dijiste mico");
        PalaDich.text += ", mico";
        index = imgpos.IndexOf("mico");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void SimioCalled()
    {
        print("Dijiste simio");
        PalaDich.text += ", simio";
        index = imgpos.IndexOf("mico");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MirlaCalled()
    {
        print("Dijiste mirla");
        PalaDich.text += ", mirla";
        index = imgpos.IndexOf("mirla");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MoscaCalled()
    {
        print("Dijiste mosca");
        PalaDich.text += ", mosca";
        index = imgpos.IndexOf("mosca");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MulaCalled()
    {
        print("Dijiste mula");
        PalaDich.text += ", mula";
        index = imgpos.IndexOf("mula");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void MurcielagoCalled()
    {
        print("Dijiste murciélago");
        PalaDich.text += ", murciélago";
        index = imgpos.IndexOf("murcielago");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void OsoCalled()
    {
        print("Dijiste oso");
        PalaDich.text += ", oso";
        index = imgpos.IndexOf("oso");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void OvejaCalled()
    {
        print("Dijiste oveja");
        PalaDich.text += ", oveja";
        index = imgpos.IndexOf("oveja");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PajaroCalled()
    {
        print("Dijiste pájaro");
        PalaDich.text += ", pájaro";
        index = imgpos.IndexOf("pájaro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PalomaCalled()
    {
        print("Dijiste paloma");
        PalaDich.text += ", paloma";
        index = imgpos.IndexOf("paloma");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PanteraCalled()
    {
        print("Dijiste pantera");
        PalaDich.text += ", pantera";
        index = imgpos.IndexOf("pantera");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PapagayoCalled()
    {
        print("Dijiste papagayo");
        PalaDich.text += ", papagayo";
        index = imgpos.IndexOf("papagayo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PatoCalled()
    {
        print("Dijiste pato");
        PalaDich.text += ", pato";
        index = imgpos.IndexOf("pato");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PavoCalled()
    {
        print("Dijiste pavo");
        PalaDich.text += ", pavo";
        index = imgpos.IndexOf("pavo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PerroCalled()
    {
        print("Dijiste perro");
        PalaDich.text += ", perro";
        index = imgpos.IndexOf("perro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PescadoCalled()
    {
        print("Dijiste pescado");
        PalaDich.text += ", pescado";
        index = imgpos.IndexOf("pescado");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PezCalled()
    {
        print("Dijiste pez");
        PalaDich.text += ", pez";
        index = imgpos.IndexOf("pez");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PiojoCalled()
    {
        print("Dijiste piojo");
        PalaDich.text += ", piojo";
        index = imgpos.IndexOf("piojo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PizcoCalled()
    {
        print("Dijiste pizco");
        PalaDich.text += ", pizco";
        index = imgpos.IndexOf("pizco");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PolloCalled()
    {
        print("Dijiste pollo");
        PalaDich.text += ", pollo";
        index = imgpos.IndexOf("pollo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PollitoCalled()
    {
        print("Dijiste pollo");
        PalaDich.text += ", pollo";
        index = imgpos.IndexOf("pollo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PulgaCalled()
    {
        print("Dijiste pulga");
        PalaDich.text += ", pulga";
        index = imgpos.IndexOf("pulga");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void PulpoCalled()
    {
        print("Dijiste pulpo");
        PalaDich.text += ", pulpo";
        index = imgpos.IndexOf("pulpo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void RataCalled()
    {
        print("Dijiste rata");
        PalaDich.text += ", rata";
        index = imgpos.IndexOf("rata");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void RanaCalled()
    {
        print("Dijiste rana");
        PalaDich.text += ", rana";
        index = imgpos.IndexOf("rana");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void RatonCalled()
    {
        print("Dijiste ratón");
        PalaDich.text += ", ratón";
        index = imgpos.IndexOf("ratón");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void RinoceronteCalled()
    {
        print("Dijiste rinoceronte");
        PalaDich.text += ", rinoceronte";
        index = imgpos.IndexOf("rinoceronte");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void SapoCalled()
    {
        print("Dijiste sapo");
        PalaDich.text += ", sapo";
        index = imgpos.IndexOf("sapo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void SerpienteCalled()
    {
        print("Dijiste serpiente");
        PalaDich.text += ", serpiente";
        index = imgpos.IndexOf("serpiente");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void TerneroCalled()
    {
        print("Dijiste ternero");
        PalaDich.text += ", ternero";
        index = imgpos.IndexOf("ternero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void TiburonCalled()
    {
        print("Dijiste tiburon");
        PalaDich.text += ", tiburon";
        index = imgpos.IndexOf("tiburon");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void TigreCalled()
    {
        print("Dijiste tigre");
        PalaDich.text += ", tigre";
        index = imgpos.IndexOf("tigre");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ToroCalled()
    {
        print("Dijiste toro");
        PalaDich.text += ", toro";
        index = imgpos.IndexOf("toro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void TortugaCalled()
    {
        print("Dijiste tortuga");
        PalaDich.text += ", tortuga";
        index = imgpos.IndexOf("tortuga");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void TruchaCalled()
    {
        print("Dijiste trucha");
        PalaDich.text += ", trucha";
        index = imgpos.IndexOf("trucha");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void VacaCalled()
    {
        print("Dijiste vaca");
        PalaDich.text += ", vaca";
        index = imgpos.IndexOf("vaca");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void VenadoCalled()
    {
        print("Dijiste venado");
        PalaDich.text += ", venado";
        index = imgpos.IndexOf("venado");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void YeguaCalled()
    {
        print("Dijiste yegua");
        PalaDich.text += ", sapo";
        index = imgpos.IndexOf("sapo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ZancudoCalled()
    {
        print("Dijiste zancudo");
        PalaDich.text += ", zancudo";
        index = imgpos.IndexOf("zacudo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ZebraCalled()
    {
        print("Dijiste zebra");
        PalaDich.text += ", zebra";
        index = imgpos.IndexOf("zebra");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void ZorroCalled()
    {
        print("Dijiste zorro");
        PalaDich.text += ", zorro";
        index = imgpos.IndexOf("zorro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    //frutas
    private void AguacateCalled()
    {
        print("Dijiste aguacate");
        PalaDich.text += ", aguacate";
        index = imgpos.IndexOf("aguacate");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AlbaricoqueCalled()
    {
        print("Dijiste albaricoque");
        PalaDich.text += ", albaricoque";
        index = imgpos.IndexOf("albaricoque");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ArazaCalled()
    {
        print("Dijiste araza");
        PalaDich.text += ", araza";
        index = imgpos.IndexOf("araza");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AuyamaCalled()
    {
        print("Dijiste auyama");
        PalaDich.text += ", auyama";
        index = imgpos.IndexOf("auyama");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void BadeaCalled()
    {
        print("Dijiste badea");
        PalaDich.text += ", badea";
        index = imgpos.IndexOf("badea");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void BananoCalled()
    {
        print("Dijiste banano");
        PalaDich.text += ", banano";
        index = imgpos.IndexOf("banano");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void BrevaCalled()
    {
        print("Dijiste breva");
        PalaDich.text += ", breva";
        index = imgpos.IndexOf("breva");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CaramboloCalled()
    {
        print("Dijiste carambolo");
        PalaDich.text += ", carambolo";
        index = imgpos.IndexOf("carambolo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CerezaCalled()
    {
        print("Dijiste cereza");
        PalaDich.text += ", cereza";
        index = imgpos.IndexOf("cereza");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ChirimoyaCalled()
    {
        print("Dijiste chirimoya");
        PalaDich.text += ", chirimoya";
        index = imgpos.IndexOf("chirimoya");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ChontaduroCalled()
    {
        print("Dijiste chontaduro");
        PalaDich.text += ", chontaduro";
        index = imgpos.IndexOf("chontaduro");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CiruelaCalled()
    {
        print("Dijiste ciruela");
        PalaDich.text += ", ciruela";
        index = imgpos.IndexOf("ciruela");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CocoCalled()
    {
        print("Dijiste coco");
        PalaDich.text += ", coco";
        index = imgpos.IndexOf("coco");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void DuraznoCalled()
    {
        print("Dijiste durazno");
        PalaDich.text += ", durazno";
        index = imgpos.IndexOf("durazno");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void FeijoaCalled()
    {
        print("Dijiste feijoa");
        PalaDich.text += ", feijoa";
        index = imgpos.IndexOf("feijoa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void FrambuesaCalled()
    {
        print("Dijiste frambuesa");
        PalaDich.text += ", frambuesa";
        index = imgpos.IndexOf("frembuesa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void FresaCalled()
    {
        print("Dijiste fresa");
        PalaDich.text += ", fresa";
        index = imgpos.IndexOf("fresa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void GranadaCalled()
    {
        print("Dijiste granada");
        PalaDich.text += ", granada";
        index = imgpos.IndexOf("granada");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void GranadillaCalled()
    {
        print("Dijiste granadilla");
        PalaDich.text += ", granadilla";
        index = imgpos.IndexOf("granadilla");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void GuabaCalled()
    {
        print("Dijiste guaba");
        PalaDich.text += ", guaba";
        index = imgpos.IndexOf("guaba");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void GuanabanaCalled()
    {
        print("Dijiste guanábana");
        PalaDich.text += ", guanábana";
        index = imgpos.IndexOf("guanábana");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }

    private void GuayabaCalled()
    {
        print("Dijiste Guayaba");
        PalaDich.text += ", guayaba";
        index = imgpos.IndexOf("guayaba");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void KiwiCalled()
    {
        print("Dijiste kiwi");
        PalaDich.text += ", kiwi";
        index = imgpos.IndexOf("kiwi");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void LimonCalled()
    {
        print("Dijiste limón");
        PalaDich.text += ", limón";
        index = imgpos.IndexOf("limón");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void LuloCalled()
    {
        print("Dijiste lulo");
        PalaDich.text += ", lulo";
        index = imgpos.IndexOf("lulo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MadronoCalled()
    {
        print("Dijiste madrono");
        PalaDich.text += ", madrono";
        index = imgpos.IndexOf("madrono");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MamoncilloCalled()
    {
        print("Dijiste mamoncillo");
        PalaDich.text += ", mamoncillo";
        index = imgpos.IndexOf("mamoncillo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MandarinaCalled()
    {
        print("Dijiste Mandarina");
        PalaDich.text += ", mandarina";
        index = imgpos.IndexOf("mandarina");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }

    private void MangoCalled()
    {
        print("Dijiste ManGo");
        PalaDich.text += ", Mango";
        index = imgpos.IndexOf("mango");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }

    private void ManzanaCalled()
    {
        print("Dijiste Manzana");
        PalaDich.text += ", Manzana";
        index = imgpos.IndexOf("manzana");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MaracuyaCalled()
    {
        print("Dijiste maracuyá");
        PalaDich.text += ", maracuyá";
        index = imgpos.IndexOf("maracuyá");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MelocotonCalled()
    {
        print("Dijiste melocoton");
        PalaDich.text += ", melocoton";
        index = imgpos.IndexOf("melocoton");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MelonCalled()
    {
        print("Dijiste melón");
        PalaDich.text += ", melón";
        index = imgpos.IndexOf("melón");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MoraCalled()
    {
        print("Dijiste Mora");
        PalaDich.text += ", mora";
        index = imgpos.IndexOf("mora");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }

    private void NaranjaCalled()
    {
        print("Dijiste Naranja");
        PalaDich.text += ", naranja";
        index = imgpos.IndexOf("naranja");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void NisperoCalled()
    {
        print("Dijiste nispero");
        PalaDich.text += ", nispero";
        index = imgpos.IndexOf("nispero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PapayaCalled()
    {
        print("Dijiste papaya");
        PalaDich.text += ", papaya";
        index = imgpos.IndexOf("papaya");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PepinoCalled()
    {
        print("Dijiste pepino");
        PalaDich.text += ", pepino";
        index = imgpos.IndexOf("pepino");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PeraCalled()
    {
        print("Dijiste pera");
        PalaDich.text += ", pera";
        index = imgpos.IndexOf("pera");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PiñaCalled()
    {
        print("Dijiste piña");
        PalaDich.text += ", piña";
        index = imgpos.IndexOf("pina");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PitayaCalled()
    {
        print("Dijiste pitaya");
        PalaDich.text += ", pitaya";
        index = imgpos.IndexOf("pitaya");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PomarrosaCalled()
    {
        print("Dijiste pomarrosa");
        PalaDich.text += ", pomarrosa";
        index = imgpos.IndexOf("pomarrosa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PomeloCalled()
    {
        print("Dijiste pomelo");
        PalaDich.text += ", pomelo";
        index = imgpos.IndexOf("pomelo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void SandiaCalled()
    {
        print("Dijiste sandía");
        PalaDich.text += ", sandía";
        index = imgpos.IndexOf("sandía");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PatillaCalled()
    {
        print("Dijiste patilla");
        PalaDich.text += ", patilla";
        index = imgpos.IndexOf("sandía");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    void TomateCalled()
    {
        print("Dijiste tomate");
        PalaDich.text += ", tomate";
        index = imgpos.IndexOf("tomate");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void TomatedearbolCalled()
    {
        print("Dijiste tomatedearbol");
        PalaDich.text += ", tomatedearbol";
        index = imgpos.IndexOf("tomatedearbol");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void UchuvaCalled()
    {
        print("Dijiste uchuva");
        PalaDich.text += ", uchuva";
        index = imgpos.IndexOf("uchuva");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void VictoriaCalled()
    {
        print("Dijiste victoria");
        PalaDich.text += ", victoria";
        index = imgpos.IndexOf("victoria");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ZapoteCalled()
    {
        print("Dijiste zapote");
        PalaDich.text += ", zapote";
        index = imgpos.IndexOf("zapote");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    //profesiones
    private void AbogadoCalled()
    {
        print("Dijiste abogado");
        PalaDich.text += ", abogado";
        index = imgpos.IndexOf("abogado");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AgricultorCalled()
    {
        print("Dijiste agricultor");
        PalaDich.text += ", agricultor";
        index = imgpos.IndexOf("agricultor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CampesinoCalled()
    {
        print("Dijiste campesino");
        PalaDich.text += ", campesino";
        index = imgpos.IndexOf("agricultor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void SembradorCalled()
    {
        print("Dijiste campesino");
        PalaDich.text += ", campesino";
        index = imgpos.IndexOf("agricultor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AlbanilCalled()
    {
        print("Dijiste albañil");
        PalaDich.text += ", albañil";
        index = imgpos.IndexOf("albañil");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void OficialCalled()
    {
        print("Dijiste oficial");
        PalaDich.text += ", oficial";
        index = imgpos.IndexOf("albañil");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AyudanteCalled()
    {
        print("Dijiste atudante");
        PalaDich.text += ", atudante";
        index = imgpos.IndexOf("albañil");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MaestroCalled()
    {
        print("Dijiste maestro");
        PalaDich.text += ", maestro";
        index = imgpos.IndexOf("profesor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MaestrodeobraCalled()
    {
        print("Dijiste maestro");
        PalaDich.text += ", maestro";
        index = imgpos.IndexOf("albañil");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AmadecasaCalled()
    {
        print("Dijiste ama de casa");
        PalaDich.text += ", ama de casa";
        index = imgpos.IndexOf("ama de casa");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ArquitectoCalled()
    {
        print("Dijiste arquitecto");
        PalaDich.text += ", arquitecto";
        index = imgpos.IndexOf("arquitecto");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ArtesanoCalled()
    {
        print("Dijiste artesano");
        PalaDich.text += ", artesano";
        index = imgpos.IndexOf("artesano");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AseadorCalled()
    {
        print("Dijiste aseador");
        PalaDich.text += ", aseador";
        index = imgpos.IndexOf("aseador");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void AzafataCalled()
    {
        print("Dijiste azafata");
        PalaDich.text += ", azafata";
        index = imgpos.IndexOf("azafata");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void BarrenderoCalled()
    {
        print("Dijiste barrendero");
        PalaDich.text += ", barrendero";
        index = imgpos.IndexOf("barrendero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void BomberoCalled()
    {
        print("Dijiste bombero");
        PalaDich.text += ", bombero";
        index = imgpos.IndexOf("bombero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CamioneroCalled()
    {
        print("Dijiste camionero");
        PalaDich.text += ", camionero";
        index = imgpos.IndexOf("camionero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CantanteCalled()
    {
        print("Dijiste cantante");
        PalaDich.text += ", cantante";
        index = imgpos.IndexOf("cantante");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CarniceroCalled()
    {
        print("Dijiste carnicero");
        PalaDich.text += ", carnicero";
        index = imgpos.IndexOf("carnicero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CarpinteroCalled()
    {
        print("Dijiste carpintero");
        PalaDich.text += ", carpintero";
        index = imgpos.IndexOf("carpintero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CerrajeroCalled()
    {
        print("Dijiste cerrajero");
        PalaDich.text += ", cerrajero";
        index = imgpos.IndexOf("cerrajero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ChefCalled()
    {
        print("Dijiste chef");
        PalaDich.text += ", chef";
        index = imgpos.IndexOf("chef");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void CirujanoCalled()
    {
        print("Dijiste cirujano");
        PalaDich.text += ", cirujano";
        index = imgpos.IndexOf("cirujano");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ConductorCalled()
    {
        print("Dijiste conductor");
        PalaDich.text += ", conductor";
        index = imgpos.IndexOf("conductor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void TaxistaCalled()
    {
        print("Dijiste taxista");
        PalaDich.text += ", taxista";
        index = imgpos.IndexOf("conductor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ChoferCalled()
    {
        print("Dijiste chofer");
        PalaDich.text += ", chofer";
        index = imgpos.IndexOf("conductor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ConstructorCalled()
    {
        print("Dijiste constructor");
        PalaDich.text += ", constructor";
        index = imgpos.IndexOf("constructor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ContadorCalled()
    {
        print("Dijiste contador");
        PalaDich.text += ", contador";
        index = imgpos.IndexOf("contador");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void DoctorCalled()
    {
        print("Dijiste doctor");
        PalaDich.text += ", doctor";
        index = imgpos.IndexOf("médico");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void DoctoraCalled()
    {
        print("Dijiste doctora");
        PalaDich.text += ", doctora";
        index = imgpos.IndexOf("médica");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
   
    private void ElectricistaCalled()
    {
        print("Dijiste electricista");
        PalaDich.text += ", electricista";
        index = imgpos.IndexOf("electricista");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void EnfermeraCalled()
    {
        print("Dijiste Enfermera");
        PalaDich.text += ", enfermera";
        index = imgpos.IndexOf("enfermera");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void EscritorCalled()
    {
        print("Dijiste escritor");
        PalaDich.text += ", escritor";
        index = imgpos.IndexOf("escritor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void EscultorCalled()
    {
        print("Dijiste escultor");
        PalaDich.text += ", escultor";
        index = imgpos.IndexOf("escultor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void FarmaceutaCalled()
    {
        print("Dijiste farmaceuta");
        PalaDich.text += ", farmaceuta";
        index = imgpos.IndexOf("farmaceuta");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void FisioterapeutaCalled()
    {
        print("Dijiste fisioterapeuta");
        PalaDich.text += ", fisioterapeuta";
        index = imgpos.IndexOf("fisioterapeuta");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void FutbolistaCalled()
    {
        print("Dijiste futbolista");
        PalaDich.text += ", futbolista";
        index = imgpos.IndexOf("futbolista");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void GimnastaCalled()
    {
        print("Dijiste gimnasta");
        PalaDich.text += ", gimnasta";
        index = imgpos.IndexOf("gimnasta");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void IngenieroCalled()
    {
        print("Dijiste ingeniero");
        PalaDich.text += ", ingeniero";
        index = imgpos.IndexOf("ingeniero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void JardineroCalled()
    {
        print("Dijiste jardinero");
        PalaDich.text += ", jardinero";
        index = imgpos.IndexOf("jardinero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void LavanderaCalled()
    {
        print("Dijiste lavandero");
        PalaDich.text += ", lavandero";
        index = imgpos.IndexOf("lavandero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void LocutorCalled()
    {
        print("Dijiste locutor");
        PalaDich.text += ", locutor";
        index = imgpos.IndexOf("locutor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ManicuristaCalled()
    {
        print("Dijiste manicurista");
        PalaDich.text += ", manicurista";
        index = imgpos.IndexOf("manicurista");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MarineroCalled()
    {
        print("Dijiste marinero");
        PalaDich.text += ", marinero";
        index = imgpos.IndexOf("marinero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MasajistaCalled()
    {
        print("Dijiste masajista");
        PalaDich.text += ", masajista";
        index = imgpos.IndexOf("masajista");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MecanicoCalled()
    {
        print("Dijiste mecánico");
        PalaDich.text += ", mecánico";
        index = imgpos.IndexOf("mecánico");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MedicaCalled()
    {
        print("Dijiste médica");
        PalaDich.text += ", médica";
        index = imgpos.IndexOf("médica");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MedicoCalled()
    {
        print("Dijiste médico");
        PalaDich.text += ", médico";
        index = imgpos.IndexOf("médico");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MensajeroCalled()
    {
        print("Dijiste mensajero");
        PalaDich.text += ", mensajero";
        index = imgpos.IndexOf("mensajero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void MeseroCalled()
    {
        print("Dijiste mesero");
        PalaDich.text += ", mesero";
        index = imgpos.IndexOf("mesero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void NineraCalled()
    {
        print("Dijiste ninera");
        PalaDich.text += ", ninera";
        index = imgpos.IndexOf("ninera");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ObreroCalled()
    {
        print("Dijiste obrero");
        PalaDich.text += ", obrero";
        index = imgpos.IndexOf("obrero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void OdontologoCalled()
    {
        print("Dijiste odontólogo");
        PalaDich.text += ", odontólogo";
        index = imgpos.IndexOf("odontólogo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void DentistaCalled()
    {
        print("Dijiste dentista");
        PalaDich.text += ", dentista";
        index = imgpos.IndexOf("odontólogo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void OftalmologoCalled()
    {
        print("Dijiste oftalmologo");
        PalaDich.text += ", oftalmologo";
        index = imgpos.IndexOf("oftalmologo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void OrdenadorCalled()
    {
        print("Dijiste ordeñador");
        PalaDich.text += ", ordeñador";
        index = imgpos.IndexOf("ordenador");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PanaderoCalled()
    {
        print("Dijiste panadero");
        PalaDich.text += ", panadero";
        index = imgpos.IndexOf("panadero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PeluqueroCalled()
    {
        print("Dijiste peluquero");
        PalaDich.text += ", peluquero";
        index = imgpos.IndexOf("peluquero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PeriodistaCalled()
    {
        print("Dijiste periodista");
        PalaDich.text += ", periodista";
        index = imgpos.IndexOf("periodista");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PilotoCalled()
    {
        print("Dijiste piloto");
        PalaDich.text += ", piloto";
        index = imgpos.IndexOf("piloto");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PintorCalled()
    {
        print("Dijiste pintor");
        PalaDich.text += ", pintor";
        index = imgpos.IndexOf("pintor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PoliciaCalled()
    {
        print("Dijiste policía");
        PalaDich.text += ", policía";
        index = imgpos.IndexOf("policía");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PoliticoCalled()
    {
        print("Dijiste politico");
        PalaDich.text += ", politico";
        index = imgpos.IndexOf("politico");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ProfesorCalled()
    {
        print("Dijiste profesor");
        PalaDich.text += ", profesor";
        index = imgpos.IndexOf("profesor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void DocenteCalled()
    {
        print("Dijiste docente");
        PalaDich.text += ", docente";
        index = imgpos.IndexOf("profesor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PsicologoCalled()
    {
        print("Dijiste psicólogo");
        PalaDich.text += ", psicólogo";
        index = imgpos.IndexOf("psicólogo");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void SacerdoteCalled()
    {
        print("Dijiste sacerdote");
        PalaDich.text += ", sacerdote";
        index = imgpos.IndexOf("padre");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void PadreCalled()
    {
        print("Dijiste padre");
        PalaDich.text += ", padre";
        index = imgpos.IndexOf("padre");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void SastreCalled()
    {
        print("Dijiste sastre");
        PalaDich.text += ", sastre";
        index = imgpos.IndexOf("sastre");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void SecretariaCalled()
    {
        print("Dijiste secretaria");
        PalaDich.text += ", secretaria";
        index = imgpos.IndexOf("secretaria");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void SoldadoCalled()
    {
        print("Dijiste soldado");
        PalaDich.text += ", soldado";
        index = imgpos.IndexOf("soldado");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void TenistaCalled()
    {
        print("Dijiste tenista");
        PalaDich.text += ", tenista";
        index = imgpos.IndexOf("tenista");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void VendedorCalled()
    {
        print("Dijiste vendedor");
        PalaDich.text += ", vendedor";
        index = imgpos.IndexOf("vendedor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void TenderoCalled()
    {
        print("Dijiste tendero");
        PalaDich.text += ", tendero";
        index = imgpos.IndexOf("vendedor");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void VeterinarioCalled()
    {
        print("Dijiste veterinario");
        PalaDich.text += ", veterinario";
        index = imgpos.IndexOf("veterinario");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
    private void ZapateroCalled()
    {
        print("Dijiste zapatero");
        PalaDich.text += ", zapatero";
        index = imgpos.IndexOf("zapatero");
        if (index == -1)
        {
            ActPuntaje("Button21", imgpos[index] , int.Parse(posimg[index]));
        }
        else
        {
            ActPuntaje("Button" + posimg[index].ToString(), imgpos[index] , int.Parse(posimg[index]));
        }
    }
}