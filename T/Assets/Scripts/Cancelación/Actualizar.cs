﻿using System.Collections.Generic;
//using System;// esto era para hacer el clear de el array de números
using UnityEngine;
using UnityEngine.UI;

public class Actualizar : MonoBehaviour {
    // Para retomar valores de la escena anterior
    GameController Variables;
    public GameObject Global;
    public GameObject Canvas;
    public GameObject Canvas1;
    public GameObject Canvas2;
    public GameObject Canvas3;

    // Para mostrar de forma ordenada las figuras
    //public GameObject Boton;
    public GameObject Imagen;

    Text textoEntrenamiento;
    Text textoCategoria;
    Text textoBlanco;
    Text textoDistra;

    //public int ultimoi;

    public static List<string> PosCat1 = new List<string>() {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
    public static List<string> PosDis1 = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
    public static List<string> PosBla1 = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };

    //que imagen en que posición

    public static List<string> imgpos = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
    public static List<string> posimg = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
    //int[] frutas = new int[10];
    //int[] animales = new int[10];
    //int[] profesiones = new int[10];
    //float[] imagenes = new float[10];
    //public int[] PosCat1 = new int[20];
    //public int[] PosDis1 = new int[20];    
    //public int[] PosBla1 = new int[18];    
    /*public Image imagen1;
    public Image imagen2;
    public Image imagen3;
    public Image imagen4;
    public Image imagen5;
    public Image imagen6;
    public Image imagen7;
    public Image imagen8;
    public Image imagen9;
    public Image imagen10;
    public Image imagen11;
    public Image imagen12;
    public Image imagen13;
    public Image imagen14;
    public Image imagen15;
    public Image imagen16;
    public Image imagen17;
    public Image imagen18;
    public Image imagen19;
    public Image imagen20;
    */
    public Text Instrucciones;

    /*List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
    /*Se recortó el número de imágenes ajustándolo al número máximo de de palabras dichas en las pruebas*/
    /*List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "leon", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pajaro", "paloma", "pato", "perro", "pez", "pollo", "ratón", "rinoceronte", "sapo", "tigre", "toro", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanabana", "guayaba", "limon", "lulo", "mandarina", "mango", "manzana", "maracuya", "melon", "mora", "naranja", "papaya", "pera", "pina", "sandia", "uchuva", "uva", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "doctor", "doctora", "enfermera", "ingeniero", "mecanico", "medica", "medico", "odontologo", "pintor", "policia", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "vendedor", "zapatero" };*/
    List<string> NamesFotosAnimales = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "ratón", "sapo", "tortuga", "vaca" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "mandarina", "mango", "manzana", "maracuyá", "melón", "naranja", "papaya", "pera", "sandía" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "padre", "sastre", "secretaria", "vendedor", "zapatero" };

    public static string Categoria;
    public static int NBlancos;
    public static int NDistractores;
    public static string imagenes;

    // Use this for initialization
    void Start() {
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();
        textoEntrenamiento = Canvas.GetComponent<Text>();
        textoEntrenamiento.text = "" + Variables.VarEntrenamiento;
        textoCategoria = Canvas1.GetComponent<Text>();
        textoCategoria.text = "" + Variables.VarCategoria;
        textoBlanco = Canvas2.GetComponent<Text>();
        textoBlanco.text = "" + Variables.VarBlanco;
        textoDistra = Canvas3.GetComponent<Text>();
        textoDistra.text = "" + Variables.VarDistra;


        Categoria = PlayerPrefs.GetString("CACategoria");
        NBlancos = PlayerPrefs.GetInt("CANBlanco");
        NDistractores = PlayerPrefs.GetInt("CANDistractores");

        PosCat1 = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
        PosDis1 = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
        PosBla1 = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
        imgpos = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
        posimg = new List<string>() { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };


        OrdenarSegunNivel();


    //que imagen en que posición


}

// Update is called once per frame
void Update() {

    }
    public void OrdenarSegunNivel()
    {
        //Primer Blanco. 1 de categoría y 1 de distracción (Otra categoría u objeto)
        //Segundo Blanco. 2 de categoría y 2 de distracción (Otra categoría u objeto)...
        //int[] Imagenes = new int[Blanco*2];
        //magenes = RandomNumsOther(Blanco * 2, 0, 20);
        //Debug.Log(Imagen.transform.GetChild(0).name);
        //Debug.Log(Imagen.transform.GetChild(1).name);
        //string categoria = textoCategoria.text;
        // Cargar las imágenes por Blanco y categoria principal
        //Escoger posisiones para graficar las imágenes
        // los siguiente se hizo para seleccionar aleatoriamente las figuras donde se iban a cargar las imágenes. Creo que puedo hacer una mejor implementación
        // Mejor implementación
        // Ubicar de forma ordenada
        // Posisiones por Blanco
        int Nivel = 1;
//        int Blanco = Nivel;
        /*if (int.Parse(Variables.VarBlanco) > int.Parse(Variables.VarDistra))
        {
            Nivel = int.Parse(Variables.VarBlanco);
        }
        else
        {
            Nivel = int.Parse(Variables.VarDistra);
        }*/
        List<string> numeros1 = new List<string>() { };
        List<string> numeros = new List<string>() { };
        Nivel = ((NBlancos + NDistractores)/2)+1;
        //List<string> numeros1 = new List<string>() { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19" };
        switch (Nivel.ToString())        
        {
            case "1":
            case "2":
            case "3":
                {
                    List<string> numeros1add = new List<string>() { "6", "7", "8", "11", "12", "13" };
                    for (int i = 0; i < NBlancos + NDistractores; i++)
                    {
                        numeros1.Add(numeros1add[0]);
                        numeros1add.Remove(numeros1add[0]);
                    }                   
                    List<string> numerosadd = new List<string>() { "0", "1", "2", "3", "4", "5", "9", "10", "14", "15", "16", "17", "18", "19" };
                    numeros.AddRange(numerosadd);
                    numeros.AddRange(numeros1add);
                    break;
                }
            case "4":
            case "5":
                {
                    List<string> numeros1add = new List<string>() {"5", "6", "7", "8", "9", "10", "11", "12", "13", "14" };
                    for (int i = 0; i < NBlancos + NDistractores; i++)
                    {
                        numeros1.Add(numeros1add[0]);
                        numeros1add.Remove(numeros1add[0]);
                    }
                    List<string> numerosadd = new List<string>() { "0", "1", "2", "3", "4", "15", "16", "17", "18", "19" };
                    numeros.AddRange(numerosadd);
                    numeros.AddRange(numeros1add);
                    break;
                }
            case "6":
                {
                    List<string> numeros1add = new List<string>() { "1", "2", "3", "6", "7", "8", "11", "12", "13", "16", "17", "18" };
                    for (int i = 0; i < NBlancos + NDistractores; i++)
                    {
                        numeros1.Add(numeros1add[0]);
                        numeros1add.Remove(numeros1add[0]);
                    }
                    List<string> numerosadd = new List<string>() { "0", "4", "5", "9", "10", "14", "15", "19" };
                    numeros.AddRange(numerosadd);
                    numeros.AddRange(numeros1add);
                    break;
                }
            case "7":
            case "8":
                {
                    List<string> numeros1add = new List<string>() { "1", "2", "3", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "16", "17", "18" };
                    for (int i = 0; i < NBlancos + NDistractores; i++)
                    {
                        numeros1.Add(numeros1add[0]);
                        numeros1add.Remove(numeros1add[0]);
                    }
                    List<string> numerosadd = new List<string>() { "0", "4", "15", "19" };
                    numeros.AddRange(numerosadd);
                    numeros.AddRange(numeros1add);
                    break;
                }
            case "9":
            case "10":
            case "11":
                {
                    List<string> numeros1add = new List<string>() {"0", "1", "2", "3","4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14","15", "16", "17", "18","19" };
                    for (int i = 0; i < NBlancos + NDistractores; i++)
                    {
                        numeros1.Add(numeros1add[0]);
                        numeros1add.Remove(numeros1add[0]);
                    }
                    numeros.AddRange(numeros1add);
                    break;
                }
        }        
        
        int alea = 0;
        string[] FotAni = new string[20];
        string[] FotFru = new string[20];
        string[] FotPro = new string[20];

        for (int i = 0; i < NBlancos; i++)
        {
            // Para las imágenes 
            alea = Random.Range(0, numeros1.Count);
            PosCat1[i] = numeros1[alea];
            numeros1.Remove(numeros1[alea]);
        }
        for (int i = 0; i < NDistractores; i++)
        {
            // Para las imágenes 
            alea = Random.Range(0, numeros1.Count);
            PosDis1[i] = numeros1[alea];
            numeros1.Remove(numeros1[alea]);
        }
        if (NBlancos > NDistractores)
        {
            for (int i = 0; i < NBlancos; i++)
            {
                // Para las fotos de animales
                if (NamesFotosAnimales.Count != 0)
                {
                    alea = Random.Range(0, NamesFotosAnimales.Count);
                    FotAni[i] = NamesFotosAnimales[alea];
                    NamesFotosAnimales.Remove(NamesFotosAnimales[alea]);
                }
                else
                {
                    llenarCat("Animales");
                    alea = Random.Range(0, NamesFotosAnimales.Count);
                    FotAni[i] = NamesFotosAnimales[alea];
                    NamesFotosAnimales.Remove(NamesFotosAnimales[alea]);
                }
                // Para las fotos de frutas
                if (NamesFotosFrutas.Count != 0)
                {
                    alea = Random.Range(0, NamesFotosFrutas.Count);
                    FotFru[i] = NamesFotosFrutas[alea];
                    NamesFotosFrutas.Remove(NamesFotosFrutas[alea]);
                }
                else
                {
                    llenarCat("Frutas");
                    alea = Random.Range(0, NamesFotosFrutas.Count);
                    FotFru[i] = NamesFotosFrutas[alea];
                    NamesFotosFrutas.Remove(NamesFotosFrutas[alea]);
                }
                // Para las fotos de profesiones
                if (NamesFotosProfesiones.Count != 0)
                {
                    alea = Random.Range(0, NamesFotosProfesiones.Count);
                    FotPro[i] = NamesFotosProfesiones[alea];
                    NamesFotosProfesiones.Remove(NamesFotosProfesiones[alea]);
                }
                else
                {
                    llenarCat("Profesiones");
                    alea = Random.Range(0, NamesFotosProfesiones.Count);
                    FotPro[i] = NamesFotosProfesiones[alea];
                    NamesFotosProfesiones.Remove(NamesFotosProfesiones[alea]);
                }
            }
        }
        else
        {
            for (int i = 0; i < NDistractores; i++)
            {
                // Para las fotos de animales
                if (NamesFotosAnimales.Count != 0)
                {
                    alea = Random.Range(0, NamesFotosAnimales.Count);
                    FotAni[i] = NamesFotosAnimales[alea];
                    NamesFotosAnimales.Remove(NamesFotosAnimales[alea]);
                }
                else
                {
                    llenarCat("Animales");
                    alea = Random.Range(0, NamesFotosAnimales.Count);
                    FotAni[i] = NamesFotosAnimales[alea];
                    NamesFotosAnimales.Remove(NamesFotosAnimales[alea]);
                }
                // Para las fotos de frutas
                if (NamesFotosFrutas.Count != 0)
                {
                    alea = Random.Range(0, NamesFotosFrutas.Count);
                    FotFru[i] = NamesFotosFrutas[alea];
                    NamesFotosFrutas.Remove(NamesFotosFrutas[alea]);
                }
                else
                {
                    llenarCat("Frutas");
                    alea = Random.Range(0, NamesFotosFrutas.Count);
                    FotFru[i] = NamesFotosFrutas[alea];
                    NamesFotosFrutas.Remove(NamesFotosFrutas[alea]);
                }
                // Para las fotos de profesiones
                if (NamesFotosProfesiones.Count != 0)
                {
                    alea = Random.Range(0, NamesFotosProfesiones.Count);
                    FotPro[i] = NamesFotosProfesiones[alea];
                    NamesFotosProfesiones.Remove(NamesFotosProfesiones[alea]);
                }
                else
                {
                    llenarCat("Profesiones");
                    alea = Random.Range(0, NamesFotosProfesiones.Count);
                    FotPro[i] = NamesFotosProfesiones[alea];
                    NamesFotosProfesiones.Remove(NamesFotosProfesiones[alea]);
                }
            }
        }       
        for (int i = 0; i < numeros.Count;i++)
        {
            numeros1.Add(numeros[i]);// Se adiciona lo que queda, aunque creo que con esta nueva versión no es necesario
        }
        for (int i = 0; i < numeros1.Count; i++)
        {
            PosBla1[i] = numeros1[i];
        }
        
        // Ahora toca seleccionar las imágenes según la categoría
        int ContCat = new int();
        int ContDis = new int();
        int ContBla = new int();
        ContCat = 0;
        ContDis = 0;
        ContBla = 0;
        //animales = RandomNums(int.Parse(Variables.VarBlanco), NamesFotosAnimales.Count);
        //frutas = RandomNums(int.Parse(Variables.VarBlanco), NamesFotosFrutas.Count);
        //profesiones = RandomNums(int.Parse(Variables.VarBlanco), NamesFotosProfesiones.Count);
        //var aleatorio = Random.Range(0, NamesFotosAnimales.Count);
        //string seleccion = NamesFotosAnimales[aleatorio];
        //NamesFotosAnimales.Remove(seleccion);
        for (int i = 0; i < 20; i++)
        {
            /*if (PosCat1[ContCat] == 20)
            {
                PosCat1[ContCat] = 0;
            }
            else if(PosDis1[ContDis] == 20)
            {
                PosDis1[ContDis] = 0;
            }
            else if (PosBla1[ContBla] == 20)
            {
                PosBla1[ContBla] = 0;
            }*/
                        
            if (i < NBlancos + NDistractores)
            {
                if (Categoria == "Animales" & i < NBlancos)// Para la categoría animales
                {
                    Imagen.transform.GetChild(int.Parse(PosCat1[ContCat])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + FotAni[ContCat]);
                    //ultimoi = i;
                    imgpos.Add(FotAni[ContCat]);
                    posimg.Add(PosCat1[ContCat]);
                    ContCat++;                    
                }
                else if (Categoria == "Animales" & i >= NBlancos)// Distractores Frutas
                {
                    int valor = Random.Range(0, 2);
                    if (valor == 0)
                    {
                        Imagen.transform.GetChild(int.Parse(PosDis1[ContDis])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + FotFru[ContDis]);
                        imgpos.Add(FotFru[ContDis]);
                        posimg.Add(PosDis1[ContDis]);
                    }
                    else // Distractores Profesiones
                    {
                        Imagen.transform.GetChild(int.Parse(PosDis1[ContDis])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + FotPro[ContDis]);
                        imgpos.Add(FotPro[ContDis]);
                        posimg.Add(PosDis1[ContDis]);
                    }
                    ContDis++;
                }
                else if (Categoria == "Frutas" & i < NBlancos)// Para categoría frutas
                {
                    Imagen.transform.GetChild(int.Parse(PosCat1[ContCat])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + FotFru[ContCat]);
                    //ultimoi = i;
                    imgpos.Add(FotFru[ContCat]);
                    posimg.Add(PosCat1[ContCat]);
                    ContCat++;
                }
                else if (Categoria == "Frutas" & i >= NBlancos)// Distractores animales
                {
                    int valor = Random.Range(0, 2);
                    if (valor == 0)
                    {
                        Imagen.transform.GetChild(int.Parse(PosDis1[ContDis])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + FotAni[ContDis]);
                        imgpos.Add(FotAni[ContDis]);
                        posimg.Add(PosDis1[ContDis]);
                    }
                    else// Distractores Profesiones
                    {
                        Imagen.transform.GetChild(int.Parse(PosDis1[ContDis])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + FotPro[ContDis]);
                        imgpos.Add(FotPro[ContDis]);
                        posimg.Add(PosDis1[ContDis]);
                    }
                    ContDis++;
                }
                else if (Categoria == "Profesiones" & i < NBlancos)// Para categoría profesiones
                {
                    Imagen.transform.GetChild(int.Parse(PosCat1[ContCat])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + FotPro[ContCat]);
                    //ultimoi = i;
                    imgpos.Add(FotPro[ContCat]);
                    posimg.Add(PosCat1[ContCat]);
                    ContCat++;
                }
                else if (Categoria == "Profesiones" & i >= NBlancos)// Distractores Animales
                {
                    int valor = Random.Range(0, 2);
                    if (valor == 0)
                    {
                        Imagen.transform.GetChild(int.Parse(PosDis1[ContDis])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + FotAni[ContDis]);
                        imgpos.Add(FotAni[ContDis]);
                        posimg.Add(PosDis1[ContDis]);
                    }
                    else// Distractores Frutas
                    {
                        Imagen.transform.GetChild(int.Parse(PosDis1[ContDis])).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + FotFru[ContDis]);
                        imgpos.Add(FotFru[ContDis]);
                        posimg.Add(PosDis1[ContDis]);
                    }
                    ContDis++;
                }
            }
            else//para poner el resto en blanco
            {
                Imagen.transform.GetChild(int.Parse(PosBla1[ContBla])).GetComponent<Image>().color = new Color(0, 0, 0, 0);
                ContBla++;
            }
        }
        for (int j = 0; j < imgpos.Count; j++)
        {
            if (imgpos[j]!="")
                imagenes = imagenes + "|" + imgpos[j];
        }
        Debug.Log(imagenes);
    }
    int[] RandomNums(int cantidad, int maxval)
    {
        int[] index = new int[10];
        for (int i = 0; i < cantidad; i++)
        {
            index[i] = Random.Range(0, maxval);

        }
        return index;
    }
    public void llenarCat(string Categoria)
    {
        /* List<string> NamesFotosAnimalesOr = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
         List<string> NamesFotosFrutasOr = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
         List<string> NamesFotosProfesionesOr = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "contructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };*/
        List<string> NamesFotosAnimalesOr = new List<string>() { "águila", "avestruz", "ballena", "burro", "caballo", "caimán", "cocodrilo", "conejo", "elefante", "gallina", "gato", "hormiga", "jirafa", "león", "lombriz", "loro", "marrano", "mico", "oso", "oveja", "pájaro", "paloma", "pato", "perro", "pollo", "ratón", "sapo", "tortuga", "vaca" };
        List<string> NamesFotosFrutasOr = new List<string>() { "aguacate", "banano", "durazno", "fresa", "guanábana", "guayaba", "limón", "mandarina", "mango", "manzana", "maracuyá", "melón", "naranja", "papaya", "pera", "sandía" };
        List<string> NamesFotosProfesionesOr = new List<string>() { "abogado", "agricultor", "albañil", "ama de casa", "arquitecto", "barrendero", "carpintero", "conductor", "constructor", "enfermera", "ingeniero", "mecánico", "médica", "médico", "odontólogo", "pintor", "policía", "profesor", "psicólogo", "padre", "sastre", "secretaria", "vendedor", "zapatero" };

        NamesFotosAnimales.AddRange(NamesFotosAnimalesOr);
        NamesFotosFrutas.AddRange(NamesFotosFrutasOr);
        NamesFotosProfesiones.AddRange(NamesFotosProfesionesOr);

        /*if (categoria=="Animales")
        {
            for (int i = 0; i < NamesFotosAnimalesOr.Count; i++)
            {
                NamesFotosAnimales.Add(NamesFotosAnimalesOr[i]);
            }
        }
        else if (categoria=="Frutas")
        {
            for (int i = 0; i < NamesFotosFrutasOr.Count; i++)
            {
                NamesFotosFrutas.Add(NamesFotosFrutasOr[i]);
            }
        }
        else if (categoria=="Profesiones")
        {
            for (int i = 0; i < NamesFotosProfesionesOr.Count; i++)
            {
                NamesFotosProfesiones.Add(NamesFotosProfesionesOr[i]);
            }
        }      */  
    }
}

/*
 * /*int[] RandomNums()
    {
        for (int i = 0; i < 20; i++)
        {
            index[i] = Random.Range(0, 55);
        }
        return index;
    }
}

if (categoria == "Animales" & i<Blanco)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i]]);
                    /*switch (i.ToString())
                    {
                        case "0":
                            {
                                Imagen.transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[0]]);
                                break;
                            }
                        case "1":
                            {
                                imagen1.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[1]]);
                                break;
                            }
                        case "2":
                            {
                                imagen2.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[2]]);
                                break;
                            }
                        case "3":
                            {
                                imagen3.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[3]]);
                                break;
                            }
                        case "4":
                            {
                                imagen4.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[4]]);
                                break;
                            }
                        case "5":
                            {
                                imagen5.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[5]]);
                                break;
                            }
                        case "6":
                            {
                                imagen6.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[6]]);
                                break;
                            }
                        case "7":
                            {
                                imagen7.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[7]]);
                                break;
                            }
                        case "8":
                            {
                                imagen8.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[8]]);
                                break;
                            }
                        case "9":
                            {
                                imagen9.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[9]]);
                                break;
                            }
                    }
ultimoi = i;
}
else if (categoria == "Animales" & i >= Blanco)
{
int valor = Random.Range(0, 2);
if (valor == 0)
{
    switch ((i-ultimoi).ToString())
    {                           
        case "1":
            {
                imagen10.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[0]]);
                break;
            }
        case "2":
            {
                imagen11.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[1]]);
                break;
            }
        case "3":
            {
                imagen12.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[2]]);
                break;
            }
        case "4":
            {
                imagen13.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[3]]);
                break;
            }
        case "5":
            {
                imagen14.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[4]]);
                break;
            }
        case "6":
            {
                imagen15.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[5]]);
                break;
            }
        case "7":
            {
                imagen16.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[6]]);
                break;
            }
        case "8":
            {
                imagen17.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[7]]);
                break;
            }
        case "9":
            {
                imagen18.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[8]]);
                break;
            }
        case "10":
            {
                imagen19.sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[9]]);
                break;
            }
    }

}
else if (valor ==1)
{
    switch ((i - ultimoi).ToString())
    {
        case "1":
            {
                imagen10.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[0]]);
                break;
            }
        case "2":
            {
                imagen11.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[1]]);
                break;
            }
        case "3":
            {
                imagen12.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[2]]);
                break;
            }
        case "4":
            {
                imagen13.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[3]]);
                break;
            }
        case "5":
            {
                imagen14.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[4]]);
                break;
            }
        case "6":
            {
                imagen15.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[5]]);
                break;
            }
        case "7":
            {
                imagen16.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[6]]);
                break;
            }
        case "8":
            {
                imagen17.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[7]]);
                break;
            }
        case "9":
            {
                imagen18.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[8]]);
                break;
            }
        case "10":
            {
                imagen19.sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[9]]);
                break;
            }
    }
}
}
else//para poner el resto en blanco
{
switch (i.ToString())
{
    case "2":
        {
            imagen2.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "3":
        {
            imagen3.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "4":
        {
            imagen4.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "5":
        {
            imagen5.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "6":
        {
            imagen6.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "7":
        {
            imagen7.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "8":
        {
            imagen8.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "9":
        {
            imagen9.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "10":
        {
            imagen10.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "11":
        {
            imagen11.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "12":
        {
            imagen12.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "13":
        {
            imagen13.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "14":
        {
            imagen14.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "15":
        {
            imagen15.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "16":
        {
            imagen16.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "17":
        {
            imagen17.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "18":
        {
            imagen18.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
    case "19":
        {
            imagen19.sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
            break;
        }
}
}
}
// Segunda versión muy funcional
for (int i = 0; i < 20; i++)
    {
        if (i < Blanco * 2)
        {
            if (categoria == "Animales" & i < Blanco)
            {
                Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i]]);
                ultimoi = i;
            }
            else if (categoria == "Animales" & i >= Blanco)
            {
                int valor = Random.Range(0, 2);
                if (valor == 0)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[i - ultimoi - 1]]);

                }
                else if (valor == 1)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[i - ultimoi - 1]]);
                }
            }
            else if (categoria == "Frutas" & i < Blanco)
            {
                Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[i]]);
                ultimoi = i;
            }
            else if (categoria == "Frutas" & i >= Blanco)
            {
                int valor = Random.Range(0, 2);
                if (valor == 0)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i - ultimoi - 1]]);

                }
                else if (valor == 1)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[i - ultimoi - 1]]);
                }
            }
            else if (categoria == "Profesiones" & i < Blanco)
            {
                Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[i]]);
                ultimoi = i;
            }
            else if (categoria == "Profesiones" & i >= Blanco)
            {
                int valor = Random.Range(0, 2);
                if (valor == 0)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i - ultimoi - 1]]);

                }
                else if (valor == 1)
                {
                    Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[i - ultimoi - 1]]);
                }
            }
        }
        else//para poner el resto en blanco
        {
            Imagen.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/blanco");
        }
    }
// esto se hizo para seleccionar las imagenes donde se iban a cargar las fotografías

int[] numeros = new int[20] { 16, 5, 11, 12, 7, 17, 18, 4, 8, 9, 10, 1, 2, 13, 19, 0, 6, 3, 14, 15 };      
int ContCat = new int();
ContCat = 0;
int ContDis = new int();
ContDis = 0;
int ContBla = new int();
ContBla = 0;
while (ContCat < Blanco | ContDis < Blanco)
{
ContCat = 0;
ContDis = 0;
ContBla = 0;
for (int j = 0; j < 20; j++)
{
    int valor = Random.Range(0, 10);
    if (valor == 0 & ContCat < Blanco)
    {
        PosCat1[ContCat] = numeros[j];
        ContCat++;
    }
    else if (ContDis < Blanco)
    {
        PosDis1[ContDis] = numeros[j];
        ContDis++;
    }
    else
    {
        PosBla1[ContBla] = numeros[j];
        ContBla++;
    }
}
}

    // poner las imagenes. Estaba como acontinuación. Luego se hizo una mejora para que evite repetir imagenes que esto solo se haga si se acaban las fotos de la categoría
for (int i = 0; i < 20; i++)
    {
        if (i < Blanco * 2)
        {
            if (categoria == "Animales" & i < Blanco)// Para la categoría animales
            {
                Imagen.transform.GetChild(PosCat1[ContCat]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i]]);
                ultimoi = i;
                ContCat++;
            }
            else if (categoria == "Animales" & i >= Blanco)// Distractores Frutas
            {
                int valor = Random.Range(0, 2);
                if (valor == 0)
                {
                    Imagen.transform.GetChild(PosDis1[ContDis]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[i - ultimoi - 1]]);

                }
                else // Distractores Profesiones
                {
                    Imagen.transform.GetChild(PosDis1[ContDis]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[i - ultimoi - 1]]);
                }
                ContDis++;
            }
            else if (categoria == "Frutas" & i < Blanco)// Para categoría frutas
            {
                Imagen.transform.GetChild(PosCat1[ContCat]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[i]]);
                ultimoi = i;
                ContCat++;
            }
            else if (categoria == "Frutas" & i >= Blanco)// Distractores animales
            {
                int valor = Random.Range(0, 2);
                if (valor == 0)
                {
                    Imagen.transform.GetChild(PosDis1[ContDis]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i - ultimoi - 1]]);

                }
                else// Distractores Profesiones
                {
                    Imagen.transform.GetChild(PosDis1[ContDis]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[i - ultimoi - 1]]);
                }
                ContDis++;
            }
            else if (categoria == "Profesiones" & i < Blanco)// Para categoría profesiones
            {
                Imagen.transform.GetChild(PosCat1[ContCat]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Proffesions/" + NamesFotosProfesiones[(int)profesiones[i]]);
                ultimoi = i;
                ContCat++;
            }
            else if (categoria == "Profesiones" & i >= Blanco)// Distractores Animales
            {
                int valor = Random.Range(0, 2);
                if (valor == 0)
                {
                    Imagen.transform.GetChild(PosDis1[ContDis]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Animals/" + NamesFotosAnimales[(int)animales[i - ultimoi - 1]]);

                }
                else// Distractores Frutas
                {
                    Imagen.transform.GetChild(PosDis1[ContDis]).GetComponent<Image>().sprite = Resources.Load<Sprite>("Categories/Cat_Fruits/" + NamesFotosFrutas[(int)frutas[i - ultimoi - 1]]);
                }
                ContDis++;
            }
        }
        else//para poner el resto en blanco
        {
            Imagen.transform.GetChild(PosBla1[ContBla]).GetComponent<Image>().color = new Color(0,0,0,0);
            ContBla++;
        }                       
    }
} 
 * /

*/
