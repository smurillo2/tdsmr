﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class SI_C : MonoBehaviour
{
    public int id_usuario = 0;//esto hay que conectarlo con el menu inicial
    public int aciertos = 0;
    public int errores = 0;
    public string categoria = "";
    public int NBlancos = 0;
    public int NDistractores = 0;
    public static string imgposss;
    public static string posimgss;
    public static string R10s;//R10 serializado
    public float TiempoGeneral = 0f;
    public float PromTiempoRes = 0f;
    public float[] Tiempoporimagen;
    public string TiempoXImg = "";
    public string AciertoError;
    public string Respuesta;
    //public string Imagenes = "";//este no se considera necesario porque esta imgposss
    string Fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    public string Usuario;
    public string nombreUsuario;
    public string escena;
    public int numrespuestas;
    /*
     400 - No establece conexion
     401 - No encuentra datos
     402 - El usuario ya existe
     200 - Datos encontrados
     201 - Usuario registrado
     202 - Puntaje actualizado
     203 - Palabras y comentarios guardados
     */
    // Use this for initialization
    void Start()
    {
        Usuario = PlayerPrefs.GetString("Usuario");
        nombreUsuario = PlayerPrefs.GetString("nombreUsuario");
        id_usuario = PlayerPrefs.GetInt("idUsuario");
        escena = "C";
        R10s = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SI()
    {

    }

    public void DBWrite()
    {
        float aux = 0f;
        float media = 0f;
        Tiempoporimagen = Perception.G_tiempo;
        numrespuestas = Perception.i;
        for (int i = 1; i < numrespuestas + 1; i++)
        {
            aux = aux + Tiempoporimagen[i];
            TiempoXImg += Tiempoporimagen[i].ToString() + "|";
        }
        media = aux / numrespuestas;
        aciertos = Perception.aciertos;
        errores = Perception.errores;
        categoria = Actualizar.Categoria;
        NBlancos = Actualizar.NBlancos;
        NDistractores = Actualizar.NDistractores;
        imgposss =Perception.imgposss;
        posimgss = Perception.posimgss;    
        R10s = Perception.R10s;


        TiempoGeneral = Perception.tiempoG;
        PromTiempoRes = media;
        AciertoError = Perception.AciertoError;
        Respuesta = Perception.Respuesta;
        StartCoroutine(CWrite());
    }

    public void DBRead()
    {

    }
    IEnumerator CWrite()
    {
        //WWW conexion = new WWW("https://fluidezverbal.autonoma.edu.co/ASEwrite.php?idu=" + id_usuario + "&aci=" + aciertos + "&err=" + errores + "&cat=" + categoria + "&nue=" + NumEstimulos + "&tig=" + TiempoGeneral + "&ptr=" + PromTiempoRes + "&txi=" + TiempoXImg + "&fec=" + Fecha + "&img=" + Imagenes);
        WWW conexion = new WWW("http://localhost/CTA/Cwrite.php?idu=" + id_usuario + "&aci=" + aciertos + "&err=" + errores + "&cat=" + categoria + "&nbl=" + NBlancos + "&ndi=" + NDistractores + "&img=" + imgposss + "&pos=" + posimgss + "&r10=" + R10s + "&tig=" + TiempoGeneral + "&ptr=" + PromTiempoRes + "&ace=" + AciertoError + "&res=" + Respuesta + "&txi=" + TiempoXImg + "&fec=" + Fecha);
        Debug.Log(conexion.text);
        yield return (conexion);

        if (conexion.text == "402")
        {
            Debug.LogError("El usuario ya existe");
        }
        else if (conexion.text == "203")
        {
            Debug.LogError("Registro realizado");
        }
        else
        {
            Debug.LogError("Error en la conexión con la base de datos");
        }
    }
}

