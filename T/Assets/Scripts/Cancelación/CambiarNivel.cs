﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambiarNivel : MonoBehaviour {
    public Image SelecEntre;
    public GameObject ToPa;
    public string VarEntrenamiento;
    public InputField NBlanco;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void cargarNivel(string pNombreNivel)
    {
        //SelecEntre.gameObject.transform.localScale = new Vector3(0, 0, 0);
        VarEntrenamiento = PlayerPrefs.GetString("Juego");
        switch (VarEntrenamiento)
        {
            case "ASE":
            case "":
                PlayerPrefs.GetString("ASENBlanco",NBlanco.text);
                break;
            case "NB":

                break;
            case "GNG":

                break;
            case "SS":

                break;
            case "C":

                break;
            case "CA":

                break;
        }
        SelecEntre.gameObject.SetActive(false);
        ToPa.SetActive(false);
        SceneManager.LoadScene(pNombreNivel);
    }
}
