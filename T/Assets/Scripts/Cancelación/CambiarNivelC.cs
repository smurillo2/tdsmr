﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambiarNivelC : MonoBehaviour {
    public Image SelecEntre;
    public GameObject ToPa;
    public Dropdown Categoria;
    public InputField NBlanco;
    public InputField NDistractores;
    public string VarCategoria;
    List<string> NamesCategorias = new List<string>() { "Animales", "Frutas", "Profesiones" };
    // Use this for initialization
    void Start () {
        VarCategoria = "Animales";
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void cargarNivel(string pNombreNivel)
    {
        PlayerPrefs.SetString("CACategoria", VarCategoria);
        PlayerPrefs.SetInt("CANBlanco", int.Parse(NBlanco.text));
        PlayerPrefs.SetInt("CANDistractores", int.Parse(NDistractores.text));
        SelecEntre.gameObject.SetActive(false);
        ToPa.SetActive(false);
        SceneManager.LoadScene(pNombreNivel);
    }
    public void DropDown_IndexCategoria(int index)
    {
        VarCategoria = NamesCategorias[index];
        //Debug.Log("Ya entré Categoria"+ NamesCategorias[index]);
    }
}
