﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SpeechLib;
using System.Xml;
using System.IO;
public class Action : MonoBehaviour
{
    //leer variables menús de configuración
    GameController Variables;
    public GameObject Global;
    string Entrenamiento;
    string Categoria;
    string Blanco;
    string Distra;
    string Idioma;
    // Para las acciones. Realimentación por texto, realimentación por voz...Otros.
    private SpVoice voice;
    public Text Instrucciones;
    void Start()
    {
        //Leer variables de los menús de configuración
        Global = GameObject.Find("GameController");
        Variables = Global.GetComponent<GameController>();      
        Entrenamiento = "" + Variables.VarEntrenamiento;
        Categoria = "" + Variables.VarCategoria;
        Blanco = "" + Variables.VarBlanco;
        Distra = "" + Variables.VarDistra;
        Idioma = "" + Variables.VarIdioma;
        TextandVoice();      
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    void OtrasCosas()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            voice.Pause();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            voice.Pause();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            voice.Resume();
        }
        //TEST PER ANDROID
        /*	if (Input.GetTouch)
		{
			voice.Resume();
		}*/
    }
    void TextandVoice()
    {
        // Cambiar las instruciones para el usuario según la categoría seleccionada
        voice = new SpVoice();
        voice.Volume = 100; // Volume (no xml)
        voice.Rate = 0;  //   Rate (no xml)     
        switch (Categoria)
        {
            case "Animales":
                {                    
                    switch (Idioma)
                    {
                        case "EspañolF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todos los animales que vea en las fotografías.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                            + "Identifique todos los animales que vea en las fotografías"
                                            + "</speak>",
                                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "EspañolM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todos los animales que vea en las fotografías.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-ES'>"
                                            + "Identifique todos los animales que vea en las fotografías"
                                            + "</speak>",
                                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "InglésF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identify all the animals you see in the pictures.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>"
                                + "Identify all the animals you see in the pictures." + "</speak>", SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "InglésM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identify all the animals you see in the pictures.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>"
                                + "Identify all the animals you see in the pictures." + "</speak>", SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "PortuguésF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todos os animais que você vê nas fotos.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='pt-BR'>"
                                + "Identifique todos os animais que você vê nas fotos"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "PortuguésM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todos os animais que você vê nas fotos.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='pt-BR'>"
                                + "Identifique todos os animais que você vê nas fotos"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                    }
                    break;
                }
            case "Frutas":
                {                    
                    switch (Idioma)
                    {
                        case "EspañolF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas las frutas que vea en las fotografías.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                + "Identifique todas las frutas que vea en las fotografías"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "EspañolM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas las frutas que vea en las fotografías.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-ES'>"
                                + "Identifique todas las frutas que vea en las fotografías"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "InglésF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identify all the fruits you see in the pictures.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>"
                                + "Identify all the fruis you see in the pictures." + "</speak>", SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "InglésM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identify all the fruits you see in the pictures.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>"
                                + "Identify all the fruits you see in the pictures." + "</speak>", SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "PortuguésF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas as frutas que você vê nas fotos.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='pt-BR'>"
                                + "Identifique todas as frutas que você vê nas fotos"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "PortuguésM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas as frutas que você vê nas fotos.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='pt-BR'>"
                                + "Identifique todas as frutas que você vê nas fotos"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                    }
                    break;                    
                }
            case "Profesiones":
                {                    
                    switch (Idioma)
                    {
                        case "EspañolF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas las profesiones que vea en las fotografías.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-MX'>"
                                + "Identifique todas las profesiones que vea en las fotografías"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "EspañolM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas las profesiones que vea en las fotografías.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='es-ES'>"
                                + "Identifique todas las profesiones que vea en las fotografías"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "InglésF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identify all the professions you see in the pictures.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>"
                                + "Identify all the professions you see in the pictures." + "</speak>", SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "InglésM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identify all the professions you see in the pictures.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>"
                                + "Identify all the professions you see in the pictures." + "</speak>", SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "PortuguésF":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas as profissões que você vê nas fotografias.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='pt-BR'>"
                                + "Identifique todas as profissões que você vê nas fotografias"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                        case "PortuguésM":
                            {
                                Instrucciones.GetComponent<Text>().text = "Identifique todas as profissões que você vê nas fotografias.";
                                voice.Speak("<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='pt-BR'>"
                                + "Identifique todas as profissões que você vê nas fotografias"
                                + "</speak>",
                                SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFIsXML);
                                break;
                            }
                    }
                    break;
                }
        }   
    }
    // Con esto se cargan los XML
    string loadXMLStandalone(string fileName)
    {
        string path = Path.Combine("Resources", fileName);
        path = Path.Combine(Application.dataPath, path);
        //Debug.Log("Path:  " + path);
        StreamReader streamReader = new StreamReader(path);
        string streamString = streamReader.ReadToEnd();
        //Debug.Log("STREAM XML STRING: " + streamString);
        return streamString;
    }
}