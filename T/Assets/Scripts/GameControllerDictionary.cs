﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SpeechLib;
using System.Xml;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;

public class GameControllerDictionary : MonoBehaviour
{
    private SpVoice voice;
    public Text Tiempo, Palabrascorrec, numCluster, tamcluster, switches;
    public float time = 60;
    AudioSource Beep;
    public AudioClip beep;
    KeywordRecognizer KeywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    public Text PalaDich;
    List<string> NamesFotosAnimales = new List<string>() { "abeja", "aguila", "alacran", "arana", "ardilla", "armadillo", "asno", "avestruz", "ballena", "barranquero", "buey", "bufalo", "buho", "buitre", "burro", "caballo", "caballodemar", "cabra", "caiman", "camaleon", "camello", "canario", "cangrejo", "chivo", "cienpies", "ciguena", "cocodrilo", "colibri", "condor", "conejo", "cucaracha", "cucarron", "culebra", "delfin", "elefante", "faisan", "foca", "gallina", "gallinazo", "gallo", "ganzo", "garrapata", "garza", "bufalo", "gato", "gavilan", "grillo", "guacamaya", "guatin", "hipopotamo", "hormiga", "iguana", "jirafa", "lagartija", "leon", "leopardo", "lobo", "lombriz", "loro", "mariposa", "marrano", "mico", "mirla", "mosca", "mula", "murcielago", "oso", "oveja", "pajaro", "paloma", "pantera", "papagayo", "pato", "pavo", "perro", "pescado", "pez", "piojo", "pizco", "pollo", "pulga", "pulpo", "rata", "raton", "rinoceronte", "sapo", "serpiente", "ternero", "tiburon", "tigre", "toro", "tortuga", "trucha", "vaca", "venado", "yegua", "zancudo", "zebra", "zorro" };
    List<string> NamesFotosFrutas = new List<string>() { "aguacate", "albaricoque", "araza", "auyama", "badea", "banano", "breva", "carambolo", "cereza", "chirimoya", "chontaduro", "ciruela", "coco", "durazno", "feijoa", "frambuesa", "fresa", "granadilla", "guaba", "guanabana", "guayaba", "kiwi", "limon", "lulo", "madrono", "mamoncillo", "mandarina", "mango", "manzana", "maracuya", "melocoton", "melon", "mora", "naranja", "nispero", "pomarrosa", "papaya", "pepino", "pera", "pina", "pitaya", "pomelo", "sandia", "tomatedearbol", "uchuva", "victoria", "zapote" };
    List<string> NamesFotosProfesiones = new List<string>() { "abogado", "agricultor", "albanil", "amadecasa", "arquitecto", "artesano", "aseador", "azafata", "barrendero", "bombero", "camionero", "cantante", "carnicero", "carpintero", "cerrajero", "chef", "cirujano", "conductor", "constructor", "contador", "doctora", "electricista", "enfermera", "escritor", "escultor", "farmaceuta", "fisioterapeuta", "futbolista", "gimnasta", "ingeniero", "jardinero", "lavandera", "locutor", "manicurista", "marinero", "masajista", "mecanico", "medica", "medico", "mensajero", "mesero", "ninera", "obrero", "odontologo", "mensajero", "oftalmologo", "ordenador", "panadero", "peluquero", "periodista", "piloto", "pintor", "policia", "politico", "profesor", "psicologo", "sacerdote", "sastre", "secretaria", "soldado", "tenista", "vendedor", "veterinario", "zapatero" };
    public GameObject Nodo;
    Vector3 posicion;
    Quaternion rotacion;
    //Graficaci[on
    string[] PalaDichSep;
    public Vector3[] palarep = new Vector3[100];
    private LineRenderer linea;
    float g = 1, n = 0, m = 0, enx = 0, eny = 0, radiocuadr = 20000;
    float w = 0;
    bool v = true, z = false;
    float t = 0, esp = 50;
    Vector3 guarda;
    bool bandera = true;
    Vector3[] positions = new Vector3[100];
    float AcuAng = 0;//Acumulacion angular
    public float r = 10;
    public float a = 0;
    public float b = 0;

    void Awake()
    {
        Beep = GetComponent<AudioSource>();
    }
    void Start()
    {
        Beep.clip = beep;
        Beep.Play();
        //animales
        keywords.Add("Abeja", () =>
        {
            AbejaCalled();
        });
        keywords.Add("Aguila", () =>
        {
            AguilaCalled();
        });
        keywords.Add("Alacrán", () =>
        {
            AlacranCalled();
        });
        keywords.Add("Araña", () =>
        {
            AranaCalled();
        });
        keywords.Add("Ardilla", () =>
        {
            ArdillaCalled();
        });
        keywords.Add("Armadillo", () =>
        {
            ArmadilloCalled();
        });
        keywords.Add("Asno", () =>
        {
            AsnoCalled();
        });
        keywords.Add("Avestruz", () =>
        {
            AvestruzCalled();
        });
        keywords.Add("Ballena", () =>
        {
            BallenaCalled();
        });
        keywords.Add("Barranquero", () =>
        {
            BarranqueroCalled();
        });
        keywords.Add("Buey", () =>
        {
            BueyCalled();
        });
        keywords.Add("Bufalo", () =>
        {
            BufaloCalled();
        });
        keywords.Add("Buho", () =>
        {
            BuhoCalled();
        });
        keywords.Add("Buitre", () =>
        {
            BuitreCalled();
        });
        keywords.Add("Burro", () =>
        {
            BurroCalled();
        });
        keywords.Add("Caballo", () =>
        {
            CaballoCalled();
        });
        keywords.Add("Caballo de mar", () =>
        {
            CaballodemarCalled();
        });
        keywords.Add("Cabra", () =>
        {
            CabraCalled();
        });
        keywords.Add("Caimán", () =>
        {
            CaimanCalled();
        });
        keywords.Add("Camaleón", () =>
        {
            CamaleonCalled();
        });
        keywords.Add("Camello", () =>
        {
            CamelloCalled();
        });
        keywords.Add("Canario", () =>
        {
            CanarioCalled();
        });
        keywords.Add("Cangrejo", () =>
        {
            CangrejoCalled();
        });
        keywords.Add("Chivo", () =>
        {
            ChivoCalled();
        });
        keywords.Add("Cienpiés", () =>
        {
            CienpiesCalled();
        });
        keywords.Add("Cigüeña", () =>
        {
            CiguenaCalled();
        });
        keywords.Add("Cocodrilo", () =>
        {
            CocodriloCalled();
        });
        keywords.Add("Colibrí", () =>
        {
            ColibriCalled();
        });
        keywords.Add("Cóndor", () =>
        {
            CondorCalled();
        });
        keywords.Add("Conejo", () =>
        {
            ConejoCalled();
        });
        keywords.Add("Cucaracha", () =>
        {
            CucarachaCalled();
        });
        keywords.Add("Cucarrón", () =>
        {
            CucarronCalled();
        });
        keywords.Add("Culebra", () =>
        {
            CulebraCalled();
        });
        keywords.Add("Delfín", () =>
        {
            DelfinCalled();
        });
        keywords.Add("Elefante", () =>
        {
            ElefanteCalled();
        });
        keywords.Add("Faisán", () =>
        {
            FaisanCalled();
        });
        keywords.Add("Foca", () =>
        {
            FocaCalled();
        });
        keywords.Add("Gallina", () =>
        {
            GallinaCalled();
        });
        keywords.Add("Gallinazo", () =>
        {
            GallinazoCalled();
        });
        keywords.Add("Gallo", () =>
        {
            GalloCalled();
        });
        keywords.Add("Ganzo", () =>
        {
            GanzoCalled();
        });
        keywords.Add("Garrapata", () =>
        {
            GarrapataCalled();
        });
        keywords.Add("Garza", () =>
        {
            GarzaCalled();
        });
        keywords.Add("Gato", () =>
        {
            GatoCalled();
        });
        keywords.Add("Gavilán", () =>
        {
            GavilanCalled();
        });
        keywords.Add("Grillo", () =>
        {
            GrilloCalled();
        });
        keywords.Add("Guacamaya", () =>
        {
            GuacamayaCalled();
        });
        keywords.Add("Guatín", () =>
        {
            GuatinCalled();
        });
        keywords.Add("Hipopótamo", () =>
        {
            HipopotamoCalled();
        });
        keywords.Add("Hormiga", () =>
        {
            HormigaCalled();
        });
        keywords.Add("Iguana", () =>
        {
            IguanaCalled();
        });
        keywords.Add("Jirafa", () =>
        {
            JirafaCalled();
        });
        keywords.Add("Lagartija", () =>
        {
            LagartijaCalled();
        });
        keywords.Add("León", () =>
        {
            LeonCalled();
        });
        keywords.Add("Leopardo", () =>
        {
            LeopardoCalled();
        });
        keywords.Add("Lobo", () =>
        {
            LoboCalled();
        });
        keywords.Add("Lombriz", () =>
        {
            LombrizCalled();
        });
        keywords.Add("Loro", () =>
        {
            LoroCalled();
        });
        keywords.Add("Mariposa", () =>
        {
            MariposaCalled();
        });
        keywords.Add("Marrano", () =>
        {
            MarranoCalled();
        });
        keywords.Add("Mico", () =>
        {
            MicoCalled();
        });
        keywords.Add("Mirla", () =>
        {
            MirlaCalled();
        });
        keywords.Add("Mosca", () =>
        {
            MoscaCalled();
        });
        keywords.Add("Mula", () =>
        {
            MulaCalled();
        });
        keywords.Add("Murciélago", () =>
        {
            MurcielagoCalled();
        });
        keywords.Add("Oso", () =>
        {
            OsoCalled();
        });
        keywords.Add("Oveja", () =>
        {
            OvejaCalled();
        });
        keywords.Add("Pájaro", () =>
        {
            PajaroCalled();
        });
        keywords.Add("Paloma", () =>
        {
            PalomaCalled();
        });
        keywords.Add("Pantera", () =>
        {
            PanteraCalled();
        });
        keywords.Add("Papagayo", () =>
        {
            PapagayoCalled();
        });
        keywords.Add("Pato", () =>
        {
            PatoCalled();
        });
        keywords.Add("Pavo", () =>
        {
            PavoCalled();
        });
        keywords.Add("Perro", () =>
        {
            PerroCalled();
        });
        keywords.Add("Pescado", () =>
        {
            PescadoCalled();
        });
        keywords.Add("Pez", () =>
        {
            PezCalled();
        });
        keywords.Add("Piojo", () =>
        {
            PiojoCalled();
        });
        keywords.Add("Pisco", () =>
        {
            PizcoCalled();
        });
        keywords.Add("Pollo", () =>
        {
            PolloCalled();
        });
        keywords.Add("Pulga", () =>
        {
            PulgaCalled();
        });
        keywords.Add("Pulpo", () =>
        {
            PulpoCalled();
        });
        keywords.Add("Rata", () =>
        {
            RataCalled();
        });
        keywords.Add("Ratón", () =>
        {
            RatonCalled();
        });
        keywords.Add("Rinoceronte", () =>
        {
            RinoceronteCalled();
        });
        keywords.Add("Sapo", () =>
        {
            SapoCalled();
        });
        keywords.Add("Serpiente", () =>
        {
            SerpienteCalled();
        });
        keywords.Add("Ternero", () =>
        {
            TerneroCalled();
        });
        keywords.Add("Tiburón", () =>
        {
            TiburonCalled();
        });
        keywords.Add("Tigre", () =>
        {
            TigreCalled();
        });
        keywords.Add("Toro", () =>
        {
            ToroCalled();
        });
        keywords.Add("Tortuga", () =>
        {
            TortugaCalled();
        });
        keywords.Add("Trucha", () =>
        {
            TruchaCalled();
        });
        keywords.Add("Vaca", () =>
        {
            VacaCalled();
        });
        keywords.Add("Venado", () =>
        {
            VenadoCalled();
        });
        keywords.Add("Yegua", () =>
        {
            YeguaCalled();
        });
        keywords.Add("Zancudo", () =>
        {
            ZancudoCalled();
        });
        keywords.Add("Zebra", () =>
        {
            ZebraCalled();
        });
        keywords.Add("Zorro", () =>
        {
            ZorroCalled();
        });
        //frutas
        keywords.Add("aguacate", () =>
        {
            AguacateCalled();
        });
        keywords.Add("albaricoque", () =>
        {
            AlbaricoqueCalled();
        });
        keywords.Add("arazá", () =>
        {
            ArazaCalled();
        });
        keywords.Add("auyama", () =>
        {
            AuyamaCalled();
        });
        keywords.Add("badea", () =>
        {
            BadeaCalled();
        });
        keywords.Add("banano", () =>
        {
            BananoCalled();
        });
        keywords.Add("breva", () =>
        {
            BrevaCalled();
        });
        keywords.Add("carambolo", () =>
        {
            CaramboloCalled();
        });
        keywords.Add("cereza", () =>
        {
            CerezaCalled();
        });
        keywords.Add("chirimoya", () =>
        {
            ChirimoyaCalled();
        });
        keywords.Add("chontaduro", () =>
        {
            ChontaduroCalled();
        });
        keywords.Add("ciruela", () =>
        {
            CiruelaCalled();
        });
        keywords.Add("coco", () =>
        {
            CocoCalled();
        });
        keywords.Add("durazno", () =>
        {
            DuraznoCalled();
        });
        keywords.Add("feijoa", () =>
        {
            FeijoaCalled();
        });
        keywords.Add("frambuesa", () =>
        {
            FrambuesaCalled();
        });
        keywords.Add("fresa", () =>
        {
            FresaCalled();
        });
        keywords.Add("granada", () =>
        {
            GranadaCalled();
        });
        keywords.Add("granadilla", () =>
        {
            GranadillaCalled();
        });
        keywords.Add("guaba", () =>
        {
            GuabaCalled();
        });
        keywords.Add("guanabana", () =>
        {
            GuanabanaCalled();
        });
        keywords.Add("guayaba", () =>
        {
            GuayabaCalled();
        });
        keywords.Add("kiwi", () =>
        {
            KiwiCalled();
        });
        keywords.Add("limón", () =>
        {
            LimonCalled();
        });
        keywords.Add("lulo", () =>
        {
            LuloCalled();
        });
        keywords.Add("madroño", () =>
        {
            MadronoCalled();
        });
        keywords.Add("mamoncillo", () =>
        {
            MamoncilloCalled();
        });
        keywords.Add("mandarina", () =>
        {
            MandarinaCalled();
        });
        keywords.Add("mango", () =>
        {
            MangoCalled();
        });
        keywords.Add("manzana", () =>
        {
            ManzanaCalled();
        });
        keywords.Add("maracuyá", () =>
        {
            MaracuyaCalled();
        });
        keywords.Add("melocotón", () =>
        {
            MelocotonCalled();
        });
        keywords.Add("melón", () =>
        {
            MelonCalled();
        });
        keywords.Add("mora", () =>
        {
            MoraCalled();
        });
        keywords.Add("Naranja", () =>
        {
            NaranjaCalled();
        });
        keywords.Add("níspero", () =>
        {
            NisperoCalled();
        });
        keywords.Add("pomarrosa", () =>
        {
            PomarrosaCalled();
        });
        keywords.Add("papaya", () =>
        {
            PapayaCalled();
        });
        keywords.Add("pepino", () =>
        {
            PepinoCalled();
        });
        keywords.Add("pera", () =>
        {
            PeraCalled();
        });
        keywords.Add("piña", () =>
        {
            PiñaCalled();
        });
        keywords.Add("pitaya", () =>
        {
            PitayaCalled();
        });
        keywords.Add("pomelo", () =>
        {
            PomeloCalled();
        });
        keywords.Add("sandia", () =>
        {
            SandiaCalled();
        });
        keywords.Add("tomate de árbol", () =>
        {
            TomatedearbolCalled();
        });
        keywords.Add("uchuva", () =>
        {
            UchuvaCalled();
        });
        keywords.Add("victoria", () =>
        {
            VictoriaCalled();
        });
        keywords.Add("zapote", () =>
        {
            ZapoteCalled();
        });
        //Profesiones
        keywords.Add("abogado", () =>
        {
            AbogadoCalled();
        });
        keywords.Add("agricultor", () =>
        {
            AgricultorCalled();
        });
        keywords.Add("albañil", () =>
        {
            AlbanilCalled();
        });
        keywords.Add("ama de casa", () =>
        {
            AmadecasaCalled();
        });
        keywords.Add("arquitecto", () =>
        {
            ArquitectoCalled();
        });
        keywords.Add("artesano", () =>
        {
            ArtesanoCalled();
        });
        keywords.Add("aseador", () =>
        {
            AseadorCalled();
        });
        keywords.Add("azafata", () =>
        {
            AzafataCalled();
        });
        keywords.Add("barrendero", () =>
        {
            BarrenderoCalled();
        });
        keywords.Add("bombero", () =>
        {
            BomberoCalled();
        });
        keywords.Add("camionero", () =>
        {
            CamioneroCalled();
        });
        keywords.Add("cantante", () =>
        {
            CantanteCalled();
        });
        keywords.Add("carnicero", () =>
        {
            CarniceroCalled();
        });
        keywords.Add("carpintero", () =>
        {
            CarpinteroCalled();
        });
        keywords.Add("cerrajero", () =>
        {
            CerrajeroCalled();
        });
        keywords.Add("chef", () =>
        {
            ChefCalled();
        });
        keywords.Add("cirujano", () =>
        {
            CirujanoCalled();
        });
        keywords.Add("conductor", () =>
        {
            ConductorCalled();
        });
        keywords.Add("constructor", () =>
        {
            ConstructorCalled();
        });
        keywords.Add("contador", () =>
        {
            ContadorCalled();
        });
        keywords.Add("doctora", () =>
        {
            DoctoraCalled();
        });
        keywords.Add("electricista", () =>
        {
            ElectricistaCalled();
        });
        keywords.Add("enfermera", () =>
        {
            EnfermeraCalled();
        });
        keywords.Add("escritor", () =>
        {
            EscritorCalled();
        });
        keywords.Add("escultor", () =>
        {
            EscultorCalled();
        });
        keywords.Add("farmaceuta", () =>
        {
            FarmaceutaCalled();
        });
        keywords.Add("fisioterapeuta", () =>
        {
            FisioterapeutaCalled();
        });
        keywords.Add("futbolista", () =>
        {
            FutbolistaCalled();
        });
        keywords.Add("gimnasta", () =>
        {
            GimnastaCalled();
        });
        keywords.Add("ingeniero", () =>
        {
            IngenieroCalled();
        });
        keywords.Add("jardinero", () =>
        {
            JardineroCalled();
        });
        keywords.Add("lavandera", () =>
        {
            LavanderaCalled();
        });
        keywords.Add("locutor", () =>
        {
            LocutorCalled();
        });
        keywords.Add("manicurista", () =>
        {
            ManicuristaCalled();
        });
        keywords.Add("marinero", () =>
        {
            MarineroCalled();
        });
        keywords.Add("masajista", () =>
        {
            MasajistaCalled();
        });
        keywords.Add("mecánico", () =>
        {
            MecanicoCalled();
        });
        keywords.Add("médica", () =>
        {
            MedicaCalled();
        });
        keywords.Add("médico", () =>
        {
            MedicoCalled();
        });
        keywords.Add("mensajero", () =>
        {
            MensajeroCalled();
        });
        keywords.Add("mesero", () =>
        {
            MeseroCalled();
        });
        keywords.Add("niñera", () =>
        {
            NineraCalled();
        });
        keywords.Add("obrero", () =>
        {
            ObreroCalled();
        });
        keywords.Add("odontólogo", () =>
        {
            OdontologoCalled();
        });
        keywords.Add("oftalmólogo", () =>
        {
            OftalmologoCalled();
        });
        keywords.Add("ordenador", () =>
        {
            OrdenadorCalled();
        });
        keywords.Add("panadero", () =>
        {
            PanaderoCalled();
        });
        keywords.Add("peluquero", () =>
        {
            PeluqueroCalled();
        });
        keywords.Add("periodista", () =>
        {
            PeriodistaCalled();
        });
        keywords.Add("piloto", () =>
        {
            PilotoCalled();
        });
        keywords.Add("pintor", () =>
        {
            PintorCalled();
        });
        keywords.Add("policía", () =>
        {
            PoliciaCalled();
        });
        keywords.Add("político", () =>
        {
            PoliticoCalled();
        });
        keywords.Add("profesor", () =>
        {
            ProfesorCalled();
        });
        keywords.Add("psicólogo", () =>
        {
            PsicologoCalled();
        });
        keywords.Add("sacerdote", () =>
        {
            SacerdoteCalled();
        });
        keywords.Add("sastre", () =>
        {
            SastreCalled();
        });
        keywords.Add("secretaria", () =>
        {
            SecretariaCalled();
        });
        keywords.Add("soldado", () =>
        {
            SoldadoCalled();
        });
        keywords.Add("tenista", () =>
        {
            TenistaCalled();
        });
        keywords.Add("vendedor", () =>
        {
            VendedorCalled();
        });
        keywords.Add("veterinario", () =>
        {
            VeterinarioCalled();
        });
        keywords.Add("zapatero", () =>
        {
            ZapateroCalled();
        });
        KeywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        KeywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        KeywordRecognizer.Start();
    }
    void ActPuntaje(string nombre, int num)
    {
        //Vector3 pos = myCam.WorldToViewportPoint(transform.position);
        /*
        posicion = new Vector3(0,0,0);
        rotacion = new Quaternion(0,0,0,0);
        GameObject objeto = Instantiate(Nodo, posicion, rotacion);        
        objeto.transform.SetParent(this.transform);
        objeto.GetComponentInChildren<Text>().text = nombre;
        */
    }
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    void AbejaCalled()
    {
        print("Dijiste abeja");
        PalaDich.text += ", abeja";
        ActPuntaje(PalaDich.text, 1);
    }
    void AguilaCalled()
    {
        print("Dijiste aguila");
        PalaDich.text += ", aguila";
        ActPuntaje(PalaDich.text, 1);
    }
    void AlacranCalled()
    {
        print("Dijiste alacran");
        PalaDich.text += ", alacran";
        ActPuntaje(PalaDich.text, 1);
    }
    void AranaCalled()
    {
        print("Dijiste araña");
        PalaDich.text += ", araña";
        ActPuntaje(PalaDich.text, 1);
    }
    void ArdillaCalled()
    {
        print("Dijiste ardilla");
        PalaDich.text += ", ardilla";
        ActPuntaje(PalaDich.text, 1);
    }
    void ArmadilloCalled()
    {
        print("Dijiste armadillo");
        PalaDich.text += ", armadillo";
        ActPuntaje(PalaDich.text, 1);
    }
    void AsnoCalled()
    {
        print("Dijiste asno");
        PalaDich.text += ", asno";
        ActPuntaje(PalaDich.text, 1);
    }
    void AvestruzCalled()
    {
        print("Dijiste avestruz");
        PalaDich.text += ", avestruz";
        ActPuntaje(PalaDich.text, 1);
    }
    void BallenaCalled()
    {
        print("Dijiste ballena");
        PalaDich.text += ", ballena";
        ActPuntaje(PalaDich.text, 1);
    }
    void BarranqueroCalled()
    {
        print("Dijiste barranquero");
        PalaDich.text += ", barranquero";
        ActPuntaje(PalaDich.text, 1);
    }
    void BueyCalled()
    {
        print("Dijiste buey");
        PalaDich.text += ", buey";
        ActPuntaje(PalaDich.text, 1);
    }
    void BufaloCalled()
    {
        print("Dijiste bufalo");
        PalaDich.text += ", bufalo";
        ActPuntaje(PalaDich.text, 1);
    }
    void BuhoCalled()
    {
        print("Dijiste buho");
        PalaDich.text += ", buho";
        ActPuntaje(PalaDich.text, 1);
    }
    void BuitreCalled()
    {
        print("Dijiste buitre");
        PalaDich.text += ", buitre";
        ActPuntaje(PalaDich.text, 1);
    }
    void BurroCalled()
    {
        print("Dijiste burro");
        PalaDich.text += ", burro";
        ActPuntaje(PalaDich.text, 1);
    }
    void CaballoCalled()
    {
        print("Dijiste caballo");
        PalaDich.text += ", caballo";
        ActPuntaje(PalaDich.text, 1);
    }
    void CaballodemarCalled()
    {
        print("Dijiste Caballo de mar");
        PalaDich.text += ", Caballo de mar";
        ActPuntaje(PalaDich.text, 1);
    }
    void CabraCalled()
    {
        print("Dijiste cabra");
        PalaDich.text += ", cabra";
        ActPuntaje(PalaDich.text, 1);
    }
    void CaimanCalled()
    {
        print("Dijiste caiman");
        PalaDich.text += ", caiman";
        ActPuntaje(PalaDich.text, 1);
    }
    void CamaleonCalled()
    {
        print("Dijiste camaleon");
        PalaDich.text += ", camaleon";
        ActPuntaje(PalaDich.text, 1);
    }
    void CamelloCalled()
    {
        print("Dijiste camello");
        PalaDich.text += ", camello";
        ActPuntaje(PalaDich.text, 1);
    }
    void CanarioCalled()
    {
        print("Dijiste canario");
        PalaDich.text += ", canario";
        ActPuntaje(PalaDich.text, 1);
    }
    void CangrejoCalled()
    {
        print("Dijiste cangrejo");
        PalaDich.text += ", cangrejo";
        ActPuntaje(PalaDich.text, 1);
    }
    void ChivoCalled()
    {
        print("Dijiste chivo");
        PalaDich.text += ", chivo";
        ActPuntaje(PalaDich.text, 1);
    }
    void CienpiesCalled()
    {
        print("Dijiste cienpies");
        PalaDich.text += ", cienpies";
        ActPuntaje(PalaDich.text, 1);
    }
    void CiguenaCalled()
    {
        print("Dijiste ciguena");
        PalaDich.text += ", ciguena";
        ActPuntaje(PalaDich.text, 1);
    }
    void CocodriloCalled()
    {
        print("Dijiste cocodrilo");
        PalaDich.text += ", cocodrilo";
        ActPuntaje(PalaDich.text, 1);
    }
    void ColibriCalled()
    {
        print("Dijiste colibri");
        PalaDich.text += ", colibri";
        ActPuntaje(PalaDich.text, 1);
    }
    void CondorCalled()
    {
        print("Dijiste condor");
        PalaDich.text += ", condor";
        ActPuntaje(PalaDich.text, 1);
    }
    void ConejoCalled()
    {
        print("Dijiste conejo");
        PalaDich.text += ", conejo";
        ActPuntaje(PalaDich.text, 1);
    }
    void CucarachaCalled()
    {
        print("Dijiste cucaracha");
        PalaDich.text += ", cucaracha";
        ActPuntaje(PalaDich.text, 1);
    }
    void CucarronCalled()
    {
        print("Dijiste cucarron");
        PalaDich.text += ", cucarron";
        ActPuntaje(PalaDich.text, 1);
    }
    void CulebraCalled()
    {
        print("Dijiste culebra");
        PalaDich.text += ", culebra";
        ActPuntaje(PalaDich.text, 1);
    }
    void DelfinCalled()
    {
        print("Dijiste delfin");
        PalaDich.text += ", delfin";
        ActPuntaje(PalaDich.text, 1);
    }
    void ElefanteCalled()
    {
        print("Dijiste elefante");
        PalaDich.text += ", elefante";
        ActPuntaje(PalaDich.text, 1);
    }
    void FaisanCalled()
    {
        print("Dijiste faisan");
        PalaDich.text += ", faisan";
        ActPuntaje(PalaDich.text, 1);
    }
    void FocaCalled()
    {
        print("Dijiste foca");
        PalaDich.text += ", foca";
        ActPuntaje(PalaDich.text, 1);
    }
    void GallinaCalled()
    {
        print("Dijiste gallina");
        PalaDich.text += ", gallina";
        ActPuntaje(PalaDich.text, 1);
    }
    void GallinazoCalled()
    {
        print("Dijiste gallinazo");
        PalaDich.text += ", gallinazo";
        ActPuntaje(PalaDich.text, 1); ;
    }
    void GalloCalled()
    {
        print("Dijiste gallo");
        PalaDich.text += ", gallo";
        ActPuntaje(PalaDich.text, 1);
    }
    void GanzoCalled()
    {
        print("Dijiste ganzo");
        PalaDich.text += ", ganzo";
        ActPuntaje(PalaDich.text, 1);
    }
    void GarrapataCalled()
    {
        print("Dijiste garrapata");
        PalaDich.text += ", garrapata";
        ActPuntaje(PalaDich.text, 1);
    }
    void GarzaCalled()
    {
        print("Dijiste garza");
        PalaDich.text += ", garza";
        ActPuntaje(PalaDich.text, 1);
    }
    void GatoCalled()
    {
        print("Dijiste gato");
        PalaDich.text += ", gato";
        ActPuntaje(PalaDich.text, 1);
    }
    void GavilanCalled()
    {
        print("Dijiste gavilan");
        PalaDich.text += ", gavilan";
        ActPuntaje(PalaDich.text, 1);
    }
    void GrilloCalled()
    {
        print("Dijiste grillo");
        PalaDich.text += ", grillo";
        ActPuntaje(PalaDich.text, 1);
    }
    void GuacamayaCalled()
    {
        print("Dijiste guacamaya");
        PalaDich.text += ", guacamaya";
        ActPuntaje(PalaDich.text, 1);
    }
    void GuatinCalled()
    {
        print("Dijiste guatin");
        PalaDich.text += ", guatin";
        ActPuntaje(PalaDich.text, 1);
    }
    void HipopotamoCalled()
    {
        print("Dijiste hipopotamo");
        PalaDich.text += ", hipopotamo";
        ActPuntaje(PalaDich.text, 1);
    }
    void HormigaCalled()
    {
        print("Dijiste hormiga");
        PalaDich.text += ", hormiga";
        ActPuntaje(PalaDich.text, 1);
    }
    void IguanaCalled()
    {
        print("Dijiste iguana");
        PalaDich.text += ", iguana";
        ActPuntaje(PalaDich.text, 1);
    }
    void JirafaCalled()
    {
        print("Dijiste jirafa");
        PalaDich.text += ", jirafa";
        ActPuntaje(PalaDich.text, 1);
    }
    void LagartijaCalled()
    {
        print("Dijiste lagartija");
        PalaDich.text += ", lagartija";
        ActPuntaje(PalaDich.text, 1);
    }
    void LeonCalled()
    {
        print("Dijiste leon");
        PalaDich.text += ", leon";
        ActPuntaje(PalaDich.text, 1);
    }
    void LeopardoCalled()
    {
        print("Dijiste leopardo");
        PalaDich.text += ", leopardo";
        ActPuntaje(PalaDich.text, 1);
    }
    void LoboCalled()
    {
        print("Dijiste lobo");
        PalaDich.text += ", lobo";
        ActPuntaje(PalaDich.text, 1);
    }
    void LombrizCalled()
    {
        print("Dijiste lombriz");
        PalaDich.text += ", lombriz";
        ActPuntaje(PalaDich.text, 1);
    }
    void LoroCalled()
    {
        print("Dijiste loro");
        PalaDich.text += ", loro";
        ActPuntaje(PalaDich.text, 1);
    }
    void MariposaCalled()
    {
        print("Dijiste mariposa");
        PalaDich.text += ", mariposa";
        ActPuntaje(PalaDich.text, 1);
    }
    void MarranoCalled()
    {
        print("Dijiste marrano");
        PalaDich.text += ", marrano";
        ActPuntaje(PalaDich.text, 1);
    }
    void MicoCalled()
    {
        print("Dijiste mico");
        PalaDich.text += ", mico";
        ActPuntaje(PalaDich.text, 1);
    }
    void MirlaCalled()
    {
        print("Dijiste mirla");
        PalaDich.text += ", mirla";
        ActPuntaje(PalaDich.text, 1);
    }
    void MoscaCalled()
    {
        print("Dijiste mosca");
        PalaDich.text += ", mosca";
        ActPuntaje(PalaDich.text, 1);
    }
    void MulaCalled()
    {
        print("Dijiste mula");
        PalaDich.text += ", mula";
        ActPuntaje(PalaDich.text, 1);
    }
    void MurcielagoCalled()
    {
        print("Dijiste murciélago");
        PalaDich.text += ", murciélago";
        ActPuntaje(PalaDich.text, 1);
    }
    void OsoCalled()
    {
        print("Dijiste oso");
        PalaDich.text += ", oso";
        ActPuntaje(PalaDich.text, 1);
    }
    void OvejaCalled()
    {
        print("Dijiste oveja");
        PalaDich.text += ", oveja";
        ActPuntaje(PalaDich.text, 1);
    }
    void PajaroCalled()
    {
        print("Dijiste pajaro");
        PalaDich.text += ", pajaro";
        ActPuntaje(PalaDich.text, 1);
    }
    void PalomaCalled()
    {
        print("Dijiste paloma");
        PalaDich.text += ", paloma";
        ActPuntaje(PalaDich.text, 1);
    }
    void PanteraCalled()
    {
        print("Dijiste pantera");
        PalaDich.text += ", pantera";
        ActPuntaje(PalaDich.text, 1);
    }
    void PapagayoCalled()
    {
        print("Dijiste papagayo");
        PalaDich.text += ", papagayo";
        ActPuntaje(PalaDich.text, 1);
    }
    void PatoCalled()
    {
        print("Dijiste pato");
        PalaDich.text += ", pato";
        ActPuntaje(PalaDich.text, 1);
    }
    void PavoCalled()
    {
        print("Dijiste pavo");
        PalaDich.text += ", pavo";
        ActPuntaje(PalaDich.text, 1);
    }
    void PerroCalled()
    {
        print("Dijiste perro");
        PalaDich.text += ", perro";
        ActPuntaje(PalaDich.text, 1);
    }
    void PescadoCalled()
    {
        print("Dijiste pescado");
        PalaDich.text += ", pescado";
        ActPuntaje(PalaDich.text, 1);
    }
    void PezCalled()
    {
        print("Dijiste pez");
        PalaDich.text += ", pez";
        ActPuntaje(PalaDich.text, 1);
    }
    void PiojoCalled()
    {
        print("Dijiste piojo");
        PalaDich.text += ", piojo";
        ActPuntaje(PalaDich.text, 1);
    }
    void PizcoCalled()
    {
        print("Dijiste pizco");
        PalaDich.text += ", pizco";
        ActPuntaje(PalaDich.text, 1);
    }
    void PolloCalled()
    {
        print("Dijiste pollo");
        PalaDich.text += ", pollo";
        ActPuntaje(PalaDich.text, 1);
    }
    void PulgaCalled()
    {
        print("Dijiste pulga");
        PalaDich.text += ", pulga";
        ActPuntaje(PalaDich.text, 1);
    }
    void PulpoCalled()
    {
        print("Dijiste pulpo");
        PalaDich.text += ", pulpo";
        ActPuntaje(PalaDich.text, 1);
    }
    void RataCalled()
    {
        print("Dijiste rata");
        PalaDich.text += ", rata";
        ActPuntaje(PalaDich.text, 1);
    }
    void RatonCalled()
    {
        print("Dijiste raton");
        PalaDich.text += ", raton";
        ActPuntaje(PalaDich.text, 1);
    }
    void RinoceronteCalled()
    {
        print("Dijiste rinoceronte");
        PalaDich.text += ", rinoceronte";
        ActPuntaje(PalaDich.text, 1);
    }
    void SapoCalled()
    {
        print("Dijiste sapo");
        PalaDich.text += ", sapo";
        ActPuntaje(PalaDich.text, 1);
    }
    void SerpienteCalled()
    {
        print("Dijiste serpiente");
        PalaDich.text += ", serpiente";
        ActPuntaje(PalaDich.text, 1);
    }
    void TerneroCalled()
    {
        print("Dijiste ternero");
        PalaDich.text += ", ternero";
        ActPuntaje(PalaDich.text, 1);
    }
    void TiburonCalled()
    {
        print("Dijiste tiburon");
        PalaDich.text += ", tiburon";
        ActPuntaje(PalaDich.text, 1);
    }
    void TigreCalled()
    {
        print("Dijiste tigre");
        PalaDich.text += ", tigre";
        ActPuntaje(PalaDich.text, 1);
    }
    void ToroCalled()
    {
        print("Dijiste toro");
        PalaDich.text += ", toro";
        ActPuntaje(PalaDich.text, 1);
    }
    void TortugaCalled()
    {
        print("Dijiste tortuga");
        PalaDich.text += ", tortuga";
        ActPuntaje(PalaDich.text, 1);
    }
    void TruchaCalled()
    {
        print("Dijiste trucha");
        PalaDich.text += ", trucha";
        ActPuntaje(PalaDich.text, 1);
    }
    void VacaCalled()
    {
        print("Dijiste vaca");
        PalaDich.text += ", vaca";
        ActPuntaje(PalaDich.text, 1);
    }
    void VenadoCalled()
    {
        print("Dijiste venado");
        PalaDich.text += ", venado";
        ActPuntaje(PalaDich.text, 1);
    }
    void YeguaCalled()
    {
        print("Dijiste yegua");
        PalaDich.text += ", yegua";
        ActPuntaje(PalaDich.text, 1);
    }
    void ZancudoCalled()
    {
        print("Dijiste zancudo");
        PalaDich.text += ", zancudo";
        ActPuntaje(PalaDich.text, 1);
    }
    void ZebraCalled()
    {
        print("Dijiste zebra");
        PalaDich.text += ", zebra";
        ActPuntaje(PalaDich.text, 1);
    }
    void ZorroCalled()
    {
        print("Dijiste zorro");
        PalaDich.text += ", zorro";
        ActPuntaje(PalaDich.text, 1);
    }
    //frutas
    private void AguacateCalled()
    {
        print("Dijiste aguacate");
        PalaDich.text += ", aguacate";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AlbaricoqueCalled()
    {
        print("Dijiste albaricoque");
        PalaDich.text += ", albaricoque";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ArazaCalled()
    {
        print("Dijiste araza");
        PalaDich.text += ", araza";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AuyamaCalled()
    {
        print("Dijiste auyama");
        PalaDich.text += ", auyama";
        ActPuntaje(PalaDich.text, 1);
    }
    private void BadeaCalled()
    {
        print("Dijiste badea");
        PalaDich.text += ", badea";
        ActPuntaje(PalaDich.text, 1);
    }
    private void BananoCalled()
    {
        print("Dijiste banano");
        PalaDich.text += ", banano";
        ActPuntaje(PalaDich.text, 1);
    }
    private void BrevaCalled()
    {
        print("Dijiste breva");
        PalaDich.text += ", breva";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CaramboloCalled()
    {
        print("Dijiste carambolo");
        PalaDich.text += ", carambolo";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CerezaCalled()
    {
        print("Dijiste cereza");
        PalaDich.text += ", cereza";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ChirimoyaCalled()
    {
        print("Dijiste chirimoya");
        PalaDich.text += ", chirimoya";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ChontaduroCalled()
    {
        print("Dijiste chontaduro");
        PalaDich.text += ", chontaduro";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CiruelaCalled()
    {
        print("Dijiste ciruela");
        PalaDich.text += ", ciruela";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CocoCalled()
    {
        print("Dijiste coco");
        PalaDich.text += ", coco";
        ActPuntaje(PalaDich.text, 1);
    }
    private void DuraznoCalled()
    {
        print("Dijiste durazno");
        PalaDich.text += ", durazno";
        ActPuntaje(PalaDich.text, 1);
    }
    private void FeijoaCalled()
    {
        print("Dijiste feijoa");
        PalaDich.text += ", feijoa";
        ActPuntaje(PalaDich.text, 1);
    }
    private void FrambuesaCalled()
    {
        print("Dijiste frambuesa");
        PalaDich.text += ", frambuesa";
        ActPuntaje(PalaDich.text, 1);
    }
    private void FresaCalled()
    {
        print("Dijiste fresa");
        PalaDich.text += ", fresa";
        ActPuntaje(PalaDich.text, 1);
    }
    private void GranadaCalled()
    {
        print("Dijiste granada");
        PalaDich.text += ", granada";
        ActPuntaje(PalaDich.text, 1);
    }
    private void GranadillaCalled()
    {
        print("Dijiste granadilla");
        PalaDich.text += ", granadilla";
        ActPuntaje(PalaDich.text, 1);
    }
    private void GuabaCalled()
    {
        print("Dijiste guaba");
        PalaDich.text += ", guaba";
        ActPuntaje(PalaDich.text, 1);
    }
    private void GuanabanaCalled()
    {
        print("Dijiste Guanabana");
        PalaDich.text += ", guanabana";
        ActPuntaje(PalaDich.text, 1);
    }

    private void GuayabaCalled()
    {
        print("Dijiste Guayaba");
        PalaDich.text += ", guayaba";
        ActPuntaje(PalaDich.text, 1);
    }
    private void KiwiCalled()
    {
        print("Dijiste kiwi");
        PalaDich.text += ", kiwi";
        ActPuntaje(PalaDich.text, 1);
    }
    private void LimonCalled()
    {
        print("Dijiste limon");
        PalaDich.text += ", limon";
        ActPuntaje(PalaDich.text, 1);
    }
    private void LuloCalled()
    {
        print("Dijiste lulo");
        PalaDich.text += ", lulo";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MadronoCalled()
    {
        print("Dijiste madrono");
        PalaDich.text += ", madrono";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MamoncilloCalled()
    {
        print("Dijiste mamoncillo");
        PalaDich.text += ", mamoncillo";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MandarinaCalled()
    {
        print("Dijiste mandarina");
        PalaDich.text += ", mandarina";
        ActPuntaje(PalaDich.text, 1);
    }

    private void MangoCalled()
    {
        print("Dijiste manGo");
        PalaDich.text += ", mango";
        ActPuntaje(PalaDich.text, 1);
    }

    private void ManzanaCalled()
    {
        print("Dijiste manzana");
        PalaDich.text += ", manzana";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MaracuyaCalled()
    {
        print("Dijiste maracuya");
        PalaDich.text += ", maracuya";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MelocotonCalled()
    {
        print("Dijiste melocoton");
        PalaDich.text += ", melocoton";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MelonCalled()
    {
        print("Dijiste melon");
        PalaDich.text += ", melon";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MoraCalled()
    {
        print("Dijiste Mora");
        PalaDich.text += ", mora";
        ActPuntaje(PalaDich.text, 1);
    }

    private void NaranjaCalled()
    {
        print("Dijiste Naranja");
        PalaDich.text += ", naranja";
        ActPuntaje(PalaDich.text, 1);
    }
    private void NisperoCalled()
    {
        print("Dijiste nispero");
        PalaDich.text += ", nispero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PapayaCalled()
    {
        print("Dijiste papaya");
        PalaDich.text += ", papaya";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PepinoCalled()
    {
        print("Dijiste pepino");
        PalaDich.text += ", pepino";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PeraCalled()
    {
        print("Dijiste pera");
        PalaDich.text += ", pera";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PiñaCalled()
    {
        print("Dijiste piña");
        PalaDich.text += ", piña";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PitayaCalled()
    {
        print("Dijiste pitaya");
        PalaDich.text += ", pitaya";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PomarrosaCalled()
    {
        print("Dijiste pomarrosa");
        PalaDich.text += ", pomarrosa";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PomeloCalled()
    {
        print("Dijiste pomelo");
        PalaDich.text += ", pomelo";
        ActPuntaje(PalaDich.text, 1);
    }
    private void SandiaCalled()
    {
        print("Dijiste sandia");
        PalaDich.text += ", sandia";
        ActPuntaje(PalaDich.text, 1);
    }
    private void TomatedearbolCalled()
    {
        print("Dijiste tomatedearbol");
        PalaDich.text += ", tomatedearbol";
        ActPuntaje(PalaDich.text, 1);
    }
    private void UchuvaCalled()
    {
        print("Dijiste uchuva");
        PalaDich.text += ", uchuva";
        ActPuntaje(PalaDich.text, 1);
    }
    private void VictoriaCalled()
    {
        print("Dijiste victoria");
        PalaDich.text += ", victoria";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ZapoteCalled()
    {
        print("Dijiste zapote");
        PalaDich.text += ", zapote";
        ActPuntaje(PalaDich.text, 1);
    }
    //profesiones
    private void AbogadoCalled()
    {
        print("Dijiste abogado");
        PalaDich.text += ", abogado";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AgricultorCalled()
    {
        print("Dijiste agricultor");
        PalaDich.text += ", agricultor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AlbanilCalled()
    {
        print("Dijiste albanil");
        PalaDich.text += ", albanil";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AmadecasaCalled()
    {
        print("Dijiste amadecasa");
        PalaDich.text += ", amadecasa";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ArquitectoCalled()
    {
        print("Dijiste arquitecto");
        PalaDich.text += ", arquitecto";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ArtesanoCalled()
    {
        print("Dijiste artesano");
        PalaDich.text += ", artesano";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AseadorCalled()
    {
        print("Dijiste aseador");
        PalaDich.text += ", aseador";
        ActPuntaje(PalaDich.text, 1);
    }
    private void AzafataCalled()
    {
        print("Dijiste azafata");
        PalaDich.text += ", azafata";
        ActPuntaje(PalaDich.text, 1);
    }
    private void BarrenderoCalled()
    {
        print("Dijiste barrendero");
        PalaDich.text += ", barrendero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void BomberoCalled()
    {
        print("Dijiste bombero");
        PalaDich.text += ", bombero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CamioneroCalled()
    {
        print("Dijiste camionero");
        PalaDich.text += ", camionero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CantanteCalled()
    {
        print("Dijiste cantante");
        PalaDich.text += ", cantante";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CarniceroCalled()
    {
        print("Dijiste carnicero");
        PalaDich.text += ", carnicero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CarpinteroCalled()
    {
        print("Dijiste carpintero");
        PalaDich.text += ", carpintero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CerrajeroCalled()
    {
        print("Dijiste cerrajero");
        PalaDich.text += ", cerrajero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ChefCalled()
    {
        print("Dijiste chef");
        PalaDich.text += ", chef";
        ActPuntaje(PalaDich.text, 1);
    }
    private void CirujanoCalled()
    {
        print("Dijiste cirujano");
        PalaDich.text += ", cirujano";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ConductorCalled()
    {
        print("Dijiste coductor");
        PalaDich.text += ", coductor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ConstructorCalled()
    {
        print("Dijiste constructor");
        PalaDich.text += ", constructor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ContadorCalled()
    {
        print("Dijiste contador");
        PalaDich.text += ", contador";
        ActPuntaje(PalaDich.text, 1);
    }
    private void DoctoraCalled()
    {
        print("Dijiste doctora");
        PalaDich.text += ", doctora";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ElectricistaCalled()
    {
        print("Dijiste electricista");
        PalaDich.text += ", electricista";
        ActPuntaje(PalaDich.text, 1);
    }
    private void EnfermeraCalled()
    {
        print("Dijiste Enfermera");
        PalaDich.text += ", enfermera";
        ActPuntaje(PalaDich.text, 1);
    }
    private void EscritorCalled()
    {
        print("Dijiste escritor");
        PalaDich.text += ", escritor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void EscultorCalled()
    {
        print("Dijiste escultor");
        PalaDich.text += ", escultor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void FarmaceutaCalled()
    {
        print("Dijiste farmaceuta");
        PalaDich.text += ", farmaceuta";
        ActPuntaje(PalaDich.text, 1);
    }
    private void FisioterapeutaCalled()
    {
        print("Dijiste fisioterapeuta");
        PalaDich.text += ", fisioterapeuta";
        ActPuntaje(PalaDich.text, 1);
    }
    private void FutbolistaCalled()
    {
        print("Dijiste futbolista");
        PalaDich.text += ", futbolista";
        ActPuntaje(PalaDich.text, 1);
    }
    private void GimnastaCalled()
    {
        print("Dijiste gimnasta");
        PalaDich.text += ", gimnasta";
        ActPuntaje(PalaDich.text, 1);
    }
    private void IngenieroCalled()
    {
        print("Dijiste ingeniero");
        PalaDich.text += ", ingeniero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void JardineroCalled()
    {
        print("Dijiste jardinero");
        PalaDich.text += ", jardinero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void LavanderaCalled()
    {
        print("Dijiste lavandero");
        PalaDich.text += ", lavandero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void LocutorCalled()
    {
        print("Dijiste locutor");
        PalaDich.text += ", locutor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ManicuristaCalled()
    {
        print("Dijiste manicurista");
        PalaDich.text += ", manicurista";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MarineroCalled()
    {
        print("Dijiste marinero");
        PalaDich.text += ", marinero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MasajistaCalled()
    {
        print("Dijiste masajista");
        PalaDich.text += ", masajista";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MecanicoCalled()
    {
        print("Dijiste mecanico");
        PalaDich.text += ", mecanico";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MedicaCalled()
    {
        print("Dijiste medica");
        PalaDich.text += ", medica";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MedicoCalled()
    {
        print("Dijiste medico");
        PalaDich.text += ", medico";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MensajeroCalled()
    {
        print("Dijiste mensajero");
        PalaDich.text += ", mensajero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void MeseroCalled()
    {
        print("Dijiste mesero");
        PalaDich.text += ", mesero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void NineraCalled()
    {
        print("Dijiste ninera");
        PalaDich.text += ", ninera";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ObreroCalled()
    {
        print("Dijiste obrero");
        PalaDich.text += ", obrero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void OdontologoCalled()
    {
        print("Dijiste odontologo");
        PalaDich.text += ", odontologo";
        ActPuntaje(PalaDich.text, 1);

    }
    private void OftalmologoCalled()
    {
        print("Dijiste oftalmologo");
        PalaDich.text += ", oftalmologo";
        ActPuntaje(PalaDich.text, 1);
    }
    private void OrdenadorCalled()
    {
        print("Dijiste ordeñador");
        PalaDich.text += ", ordeñador";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PanaderoCalled()
    {
        print("Dijiste panadero");
        PalaDich.text += ", panadero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PeluqueroCalled()
    {
        print("Dijiste peluquero");
        PalaDich.text += ", peluquero";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PeriodistaCalled()
    {
        print("Dijiste periodista");
        PalaDich.text += ", periodista";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PilotoCalled()
    {
        print("Dijiste piloto");
        PalaDich.text += ", piloto";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PintorCalled()
    {
        print("Dijiste pintor");
        PalaDich.text += ", pintor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PoliciaCalled()
    {
        print("Dijiste policia");
        PalaDich.text += ", policia";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PoliticoCalled()
    {
        print("Dijiste politico");
        PalaDich.text += ", politico";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ProfesorCalled()
    {
        print("Dijiste profesor");
        PalaDich.text += ", profesor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void PsicologoCalled()
    {
        print("Dijiste psicologo");
        PalaDich.text += ", psicologo";
        ActPuntaje(PalaDich.text, 1);
    }
    private void SacerdoteCalled()
    {
        print("Dijiste sacerdote");
        PalaDich.text += ", sacerdote";
        ActPuntaje(PalaDich.text, 1);
    }
    private void SastreCalled()
    {
        print("Dijiste sastre");
        PalaDich.text += ", sastre";
        ActPuntaje(PalaDich.text, 1);
    }
    private void SecretariaCalled()
    {
        print("Dijiste secretaria");
        PalaDich.text += ", secretaria";
        ActPuntaje(PalaDich.text, 1);
    }
    private void SoldadoCalled()
    {
        print("Dijiste soldado");
        PalaDich.text += ", soldado";
        ActPuntaje(PalaDich.text, 1);
    }
    private void TenistaCalled()
    {
        print("Dijiste tenista");
        PalaDich.text += ", tenista";
        ActPuntaje(PalaDich.text, 1);
    }
    private void VendedorCalled()
    {
        print("Dijiste vendedor");
        PalaDich.text += ", vendedor";
        ActPuntaje(PalaDich.text, 1);
    }
    private void VeterinarioCalled()
    {
        print("Dijiste veterinario");
        PalaDich.text += ", veterinario";
        ActPuntaje(PalaDich.text, 1);
    }
    private void ZapateroCalled()
    {
        print("Dijiste zapatero");
        PalaDich.text += ", zapatero";
        ActPuntaje(PalaDich.text, 1);
    }
    void Update()
    {

        if (time > 0)
        {
            time -= Time.deltaTime;
            Tiempo.text = ("Tiempo: " + time.ToString("0"));
            int tiempo = (int)time;
            if (tiempo == 0 & bandera)
            {
                SepararTexto();
                bandera = false;
            }
        }
    }
    public void SepararTexto()
    {
        PalaDichSep = PalaDich.text.Split(new string[] { " " }, StringSplitOptions.None);
        //PalaDichSep = new string[] { "Manzana", "Pera", " Mango", "Banano", "Piña", "Kiwi", "Fresa", "Cereza", "Franbuesa", "Naranja", " Mandarina", "Pera", "Lulo", "Curuba", "Mango", "Fresa" };
        Array.Resize(ref positions, PalaDichSep.Length);
        CalcularPuntos(PalaDichSep.Length);
        for (int i = 0; i < PalaDichSep.Length; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (PalaDichSep[i] == PalaDichSep[j])
                {
                    positions[i] = positions[j];
                    PalaDichSep[i] = "";
                    break;
                }
            }
            linea = GetComponent<LineRenderer>();
            linea.positionCount = positions.Length;
            linea.SetPosition(i, positions[i]);
            GameObject objeto = Instantiate(Nodo, new Vector3(0, 0, 0), Quaternion.identity);
            objeto.transform.SetParent(this.transform);
            objeto.GetComponentInChildren<Text>().text = PalaDichSep[i];
            objeto.transform.position = new Vector3((positions[i].x * 65) + 515, (positions[i].y * 65) + 318, 0.0f);
        }
    }
    void CalcularPuntos(float Npun)//Npun numero de puntos (palabras)
    {
        float DesAng = 2 * Mathf.PI / Npun;//Desplazamiento angular;
        for (int i = 0; i < Npun; i++)
        {
            positions[i] = new Vector3(r * Mathf.Cos(AcuAng), r * Mathf.Sin(AcuAng), 0.0f);
            AcuAng += DesAng;
        }
    }
    void CalcularPuntosCC(float Npun, float Xd, float Yd)//Npun numero de puntos (palabras)
    {
        float DesAng = 2 * Mathf.PI / Npun;//Desplazamiento angular;
        for (int i = 0; i < Npun; i++)
        {
            positions[i] = new Vector3((r * Mathf.Cos(AcuAng)) + Xd, (r * Mathf.Sin(AcuAng)) + Yd, 0.0f);
            AcuAng += DesAng;
        }
    }
}

