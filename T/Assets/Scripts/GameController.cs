﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour {
    public static GameController estadoJuego;
    public GameObject Global;    
    public Dropdown Entrenamiento;
    public Dropdown Categoria;
    public Dropdown Blanco;
    public Dropdown Distra;
    public Dropdown Idioma;
    public Toggle TMouse;
    public Toggle TTactil;
    public Toggle TVoz;
    public string VarEntrenamiento;
    public string VarCategoria;
    //public int VarNivel;
    public string VarBlanco;
    public string VarDistra;
    public string VarIdioma;
    public bool VarTMouse;
    public bool VarTTactil;
    public bool VarTVoz;
    List<string> NamesEntrenamiento = new List<string>() { "AprendizajeSinError", "NBack", "Stroop", "CambioDeTarea", "Cancelación", "Categorización" };
    List<string> NamesCategorias = new List<string>() { "Animales", "Frutas", "Profesiones" };
    List<string> NamesBlanco = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
    List<string> NamesDistra = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
    List<string> NamesIdioma = new List<string>() { "EspañolM", "EspañolF", "InglésM", "PortuguésM", "InglésF", "PortuguésF"};

    public Image SelecEntre;
    public Image SelecConfASE;
    public Image SelecConfNB;
    public Image SelecConfS;
    public Image SelecConfCDT;
    public Image SelecConf;
    public Image SelecConfC;
    public string CargarPanel;
    //public Text Instrucciones;
    //public int valor = 5;
    private void Awake()
    {
        if (estadoJuego == null)
        {
            estadoJuego = this;
            DontDestroyOnLoad(gameObject);
            //Instrucciones_Texto(VarCategoria);
        }
        else if(estadoJuego != this)
        {
            Destroy(gameObject);
            //Debug.Log("Destruido");
            //Debug.Log(VarCategoria);
            // Instrucciones_Texto(VarCategoria);
        }
    }

    // Use this for initialization
    void Start () {
        //Debug.Log("Estoy haciendo eso");
        VarEntrenamiento = NamesEntrenamiento[0];
        VarCategoria = NamesCategorias[0];        
        VarBlanco = NamesBlanco[0];
        VarDistra = NamesDistra[0];
        VarIdioma = NamesIdioma[0];
        VarTMouse = TMouse.isOn;
        VarTTactil = TTactil.isOn;
        VarTVoz = TVoz.isOn;
    }
	
	// Update is called once per frame
	void Update ()
    {
        CargarPanel = PlayerPrefs.GetString("HabilitarPanel");
        if (CargarPanel=="true")
        {
            PlayerPrefs.SetString("HabilitarPanel", "false");
            VarEntrenamiento = PlayerPrefs.GetString("Juego");
            switch (VarEntrenamiento)
            {
                case "ASE":
                case "":
                    SelecEntre.gameObject.SetActive(false);
                    SelecConfASE.gameObject.SetActive(true);
                    SelecConfNB.gameObject.SetActive(false);
                    SelecConfS.gameObject.SetActive(false);
                    SelecConfCDT.gameObject.SetActive(false);
                    SelecConf.gameObject.SetActive(false);
                    SelecConfC.gameObject.SetActive(false);
                    break;
                case "NB":
                    SelecEntre.gameObject.SetActive(false);
                    SelecConfASE.gameObject.SetActive(false);
                    SelecConfNB.gameObject.SetActive(true);
                    SelecConfS.gameObject.SetActive(false);
                    SelecConfCDT.gameObject.SetActive(false);
                    SelecConf.gameObject.SetActive(false);
                    SelecConfC.gameObject.SetActive(false);
                    break;
                case "GNG":
                    SelecEntre.gameObject.SetActive(false);
                    SelecConfASE.gameObject.SetActive(false);
                    SelecConfNB.gameObject.SetActive(false);
                    SelecConfS.gameObject.SetActive(true);
                    SelecConfCDT.gameObject.SetActive(false);
                    SelecConf.gameObject.SetActive(false);
                    SelecConfC.gameObject.SetActive(false);
                    break;
                case "SS":
                    SelecEntre.gameObject.SetActive(false);
                    SelecConfASE.gameObject.SetActive(false);
                    SelecConfNB.gameObject.SetActive(false);
                    SelecConfS.gameObject.SetActive(false);
                    SelecConfCDT.gameObject.SetActive(true);
                    SelecConf.gameObject.SetActive(false);
                    SelecConfC.gameObject.SetActive(false);
                    break;
                case "C":
                    SelecEntre.gameObject.SetActive(false);
                    SelecConfASE.gameObject.SetActive(false);
                    SelecConfNB.gameObject.SetActive(false);
                    SelecConfS.gameObject.SetActive(false);
                    SelecConfCDT.gameObject.SetActive(false);
                    SelecConf.gameObject.SetActive(true);
                    SelecConfC.gameObject.SetActive(false);
                    break;
                case "CA":
                    SelecEntre.gameObject.SetActive(false);
                    SelecConfASE.gameObject.SetActive(false);
                    SelecConfNB.gameObject.SetActive(false);
                    SelecConfS.gameObject.SetActive(false);
                    SelecConfCDT.gameObject.SetActive(false);
                    SelecConf.gameObject.SetActive(false);
                    SelecConfC.gameObject.SetActive(true);
                    break;
            }
        }
    } 
    public void DropDown_IndexEntrenamiento(int index)
    {
        VarEntrenamiento = NamesEntrenamiento[index];
        //Debug.Log("Ya entré entrenamiento");
    }
    public void DropDown_IndexCategoria(int index)
    {
        VarCategoria = NamesCategorias[index];
        //Debug.Log("Ya entré Categoria"+ NamesCategorias[index]);
    }
    public void DropDown_IndexBlanco(int index)
    {
        VarBlanco = NamesBlanco[index];
        //Debug.Log("Ya entré Blanco");
    }
    public void DropDown_Distra(int index)
    {
        VarDistra = NamesDistra[index];
        //Debug.Log("Ya entré Blanco");
    }
    public void DropDown_Idioma(int index)
    {
        VarIdioma = NamesIdioma[index];
        //Debug.Log("Ya entré Blanco");
    }
    public void ToMouse()
    {
        VarTMouse = TMouse.isOn;
    }
    public void ToTactil()
    {
        VarTTactil = TTactil.isOn;
    }
    public void ToVoz()
    {
        VarTVoz = TVoz.isOn;
    }
    /*public void Instrucciones_Texto(string VarCategoria)
    {
        Debug.Log(VarBlanco);
        switch (VarCategoria)
        {
            case "Animales":
                {
                    Instrucciones.text = ("Diga los nombres de todos los animales que vea en las fotografías.");
                    Debug.Log("Animales");
                    break;
                }
            case "Frutas":
                {
                    Instrucciones.text = ("Diga los nombres de todas las frutas que vea en las fotografías.");
                    Debug.Log("Frutas");
                    break;
                }
            case "Profesiones":
                {
                    Instrucciones.text = ("Diga los nombres de todas las profesiones que vea en las fotografías.");
                    Debug.Log("Profesiones");
                    break;
                }
        }
    }*/
}
